<?php
/**
 * Product Categories
 */

class Category_Route_API extends WP_REST_Controller {
	public function register_routes() {
		$version   = VER_API;
		$namespace = NAMESPACE_API . '/v' . $version;
		$base      = 'products/categories';

		// ./products/categories
		register_rest_route( $namespace, '/' . $base, array(
			array(
				'methods'  => WP_REST_Server::READABLE,
				'callback' => array( $this, 'get_categories' )
			),
			array(
				'methods'             => WP_REST_Server::EDITABLE,
				'callback'            => array( $this, 'update_insert_category' ),
				'permission_callback' => array( $this, 'check_permissions_editor' ),
				'args'                => array(
					'slug' => array(
						'default'           => '',
						'sanitize_callback' => 'sanitize_slug'
					)
				)
			),
			array(
				'methods'             => WP_REST_Server::DELETABLE,
				'callback'            => array( $this, 'delete_lang_categories' ),
				'permission_callback' => array( $this, 'check_permissions_editor' ),
			)
		) );

		// ./products/categories/list
		register_rest_route( $namespace, '/' . $base . '/list', array(
			array(
				'methods'  => WP_REST_Server::READABLE,
				'callback' => array( $this, 'get_list_categories' )
			)
		) );

		// ./products/categories/sort
		register_rest_route( $namespace, '/' . $base . '/sort', array(
			array(
				'methods'             => WP_REST_Server::EDITABLE,
				'callback'            => array( $this, 'sort_categories' ),
				'permission_callback' => array( $this, 'check_permissions_editor' ),
			)
		) );

		// ./products/categories/translation
		register_rest_route( $namespace, '/' . $base . '/translation', array(
			array(
				'methods'  => WP_REST_Server::READABLE,
				'callback' => array( $this, 'get_categories_translations' ),
			)
		) );

		// ./products/categories/:id
		register_rest_route( $namespace, '/' . $base . '/(?P<id>[\d]+)', array(
			array(
				'methods'  => WP_REST_Server::READABLE,
				'callback' => array( $this, 'get_category' ),
				'args'     => array(
					'id' => array(
						'sanitize_callback' => 'absint'
					)
				)
			),
			array(
				'methods'             => WP_REST_Server::EDITABLE,
				'callback'            => array( $this, 'update_insert_category' ),
				'permission_callback' => array( $this, 'check_permissions_editor' ),
				'args'                => array(
					'id'   => array(
						'sanitize_callback' => 'absint'
					),
					'type' => array(
						'default'           => '',
						'sanitize_callback' => 'sanitize_slug'
					),
					'slug' => array(
						'default'           => '',
						'sanitize_callback' => 'sanitize_slug'
					)
				)
			),
			array(
				'methods'             => WP_REST_Server::DELETABLE,
				'callback'            => array( $this, 'delete_category' ),
				'permission_callback' => array( $this, 'check_permissions_editor' ),
				'args'                => array(
					'id' => array(
						'sanitize_callback' => 'absint'
					)
				)
			)
		) );

		// ./products/categories/slug/:slug
		register_rest_route( $namespace, '/' . $base . '/slug/(?P<slug>[\S]+)', array(
			array(
				'methods'  => WP_REST_Server::READABLE,
				'callback' => array( $this, 'get_category' ),
				'args'     => array(
					'slug' => array(
						'default'           => '',
						'sanitize_callback' => 'sanitize_slug'
					)
				)
			),
			array(
				'methods'             => WP_REST_Server::EDITABLE,
				'callback'            => array( $this, 'update_category_cover' ),
				'permission_callback' => array( $this, 'check_permissions_editor' ),
				'args'                => array(
					'slug' => array(
						'default'           => '',
						'sanitize_callback' => 'sanitize_slug'
					)
				)
			),
		) );
	}

	/**
	 * Get categories
	 */
	public function get_categories( WP_REST_Request $request ) {
		$lang     = $request->get_param( 'lang' ) ? $request->get_param( 'lang' ) : null;
		$settings = $request->get_param( 'settings' ) ? json_decode( $request->get_param( 'settings' ) ) : null;
		$site_id  = $settings->site ? ( $settings->site->value ? $settings->site->value : null ) : null;

		$items = $this->_get_categroies( $lang, 0, $site_id );

		if ( is_array( $items ) ) {
			return new WP_REST_Response( $items, 200 );
		} else {
			return new WP_Error( 'error_categories', __( 'Problem with loading categories', TEXTDOMAIN ), array( 'status' => 500 ) );
		}
	}

	/**
	 * Sort categories
	 */
	public function sort_categories( WP_REST_Request $request ) {
		$lang     = $request->get_param( 'lang' ) ? $request->get_param( 'lang' ) : null;
		$settings = $request->get_param( 'settings' ) ? json_decode( $request->get_param( 'settings' ) ) : null;
		$site_id  = $settings->site ? ( $settings->site->value ? $settings->site->value : null ) : null;
		$items    = $request->get_param( 'items' );

		if ( $items ) {
			$this->_order_menu_item( $items );
		}
		$categroies = $this->_get_categroies( $lang, 0, $site_id );

		return new WP_REST_Response( $categroies, 200 );
	}

	/**
	 * Get translations for categories
	 */
	public function get_categories_translations( WP_REST_Request $request ) {
		$settings = $request->get_param( 'settings' ) ? json_decode( $request->get_param( 'settings' ) ) : null;
		$site_id  = $settings->site ? ( $settings->site->value ? $settings->site->value : null ) : null;

		global $wpdb;
		$query = "SELECT DISTINCT lang FROM `product_category_translation`";
		if ( $site_id ) {
			$query = $query . "WHERE `parent` IN (SELECT ID FROM product_category WHERE `site_id` = " . $site_id . ")";
		}
		$categories_translation = $wpdb->get_results( $query );
		if ( $categories_translation ) {
			return new WP_REST_Response( $categories_translation, 200 );
		} else {
			return new WP_REST_Response( null, 204 );
		}
	}

	/**
	 * Edit and create menu item
	 */
	public function update_insert_category( WP_REST_Request $request ) {
		$id          = $request->get_param( 'id' ) ? $request->get_param( 'id' ) : null;
		$name        = htmlentities( $request->get_param( 'name' ), ENT_QUOTES );
		$description = $request->get_param( 'description' ) ? htmlentities( $request->get_param( 'description' ), ENT_QUOTES ) : null;
		$discount    = $request->get_param( 'discount' ) ? $request->get_param( 'discount' ) : null;
		$cover       = $request->get_param( 'cover' ) ? $request->get_param( 'cover' ) : null;
		$parent      = $request->get_param( 'parent' ) ? $request->get_param( 'parent' ) : 0;
		$site_id     = $request->get_param( 'site_id' ) ? $request->get_param( 'site_id' ) : null;
		$lang        = $request->get_param( 'lang' ) ? $request->get_param( 'lang' ) : null;
		$slug        = $request->get_param( 'slug' ) ? $request->get_param( 'slug' ) : sanitize_slug( $name );
		global $wpdb;
		if ( $name ) {
			if ( $id && $lang ) {
				$category_translation = get_translate_obj( 'product_category', $id, $lang );
				if ( $category_translation ) {
					$category_db = $wpdb->update( 'product_category_translation',
						[ 'name' => $name, 'description' => $description, 'cover' => $cover ],
						[ 'parent' => $id, 'lang' => $lang ]
					);
				} else {
					$category_db = $wpdb->insert( 'product_category_translation', [ 'parent' => $id, 'name' => $name, 'description' => $description, 'cover' => $cover, 'lang' => $lang ] );
				}
			} elseif ( $id ) {
				$checked_slug = $this->check_slug( $id, $slug, $site_id );
				$category_db  = $wpdb->update( 'product_category',
					[
						'name'        => $name,
						'description' => $description,
						'discount'    => $discount,
						'slug'        => $checked_slug,
						'cover'       => $cover,
						'parent'      => $parent,
						'site_id'     => $site_id,
					],
					[ "ID" => $id ]
				);
			} else {
				$category_db = $wpdb->insert( 'product_category', [
					'name'        => $name,
					'description' => $description,
					'discount'    => $discount,
					'cover'       => $cover,
					'parent'      => $parent,
					'site_id'     => $site_id,
				] );

				if ( $category_db ) {
					$new_category_id = $wpdb->insert_id;
					$checked_slug    = $this->check_slug( $new_category_id, $slug, $site_id );

					$category_db = $wpdb->update( 'product_category',
						[
							'slug' => $checked_slug
						],
						[ "ID" => $new_category_id ]
					);
				}
			}
			if ( $category_db > 0 ) {
				return new WP_REST_Response( $new_category_id ? $new_category_id : $category_db, 200 );
			} elseif ( $category_db == 0 ) {
				return new WP_Error( 'error_update', __( 'Maybe field is not different', TEXTDOMAIN ), array( 'status' => 400 ) );
			} else {
				return new WP_Error( 'error_insert', __( 'Problem with insert category', TEXTDOMAIN ), array( 'status' => 400 ) );
			}

		} else {
			return new WP_Error( 'wrong_params', __( 'Wrong params of item', TEXTDOMAIN ), array( 'status' => 400 ) );
		}
	}

	/**
	 * Delete Category
	 */
	public function delete_category( WP_REST_Request $request ) {
		$id = $request->get_param( 'id' );

		global $wpdb;

		if ( $id ) {
			$category_delete = $wpdb->delete( 'product_category', [ 'ID' => $id ], [ '%d' ] );
			if ( $category_delete ) {
				$category_delete = $wpdb->delete( 'product_category_translation', [ 'parent' => $id ], [ '%d' ] );
			} else {
				return new WP_Error( 'category_delete', __( 'Problem with deleting category', TEXTDOMAIN ), array( 'status' => 400 ) );
			}

			return new WP_REST_Response( $category_delete, 200 );
		} else {
			return new WP_Error( 'category_delete', __( 'Wrong category id', TEXTDOMAIN ), array( 'status' => 400 ) );
		}
	}

	/**
	 * Delete languages of categories
	 */
	public function delete_lang_categories( WP_REST_Request $request ) {
		$lang = $request->get_param( 'lang' ) ? $request->get_param( 'lang' ) : null;
		global $wpdb;

		if ( $lang ) {
			$category_lang_delete = $wpdb->delete( 'product_category_translation', [ 'lang' => $lang ], [ '%s' ] );

			return new WP_REST_Response( $category_lang_delete, 200 );
		} else {
			return new WP_Error( 'category_delete', __( 'Wrong languages of category', TEXTDOMAIN ), array( 'status' => 400 ) );
		}
	}

	/**
	 * Get category by slug
	 */
	public function get_category( WP_REST_Request $request ) {
		$id   = $request->get_param( 'id' ) ? $request->get_param( 'id' ) : null;
		$slug = $request->get_param( 'slug' ) ? $request->get_param( 'slug' ) : null;
		$lang = $request->get_param( 'lang' ) ? $request->get_param( 'lang' ) : null;

		global $wpdb;

		if ( $id ) {
			$category_db = $wpdb->get_row( "SELECT * FROM `product_category` WHERE `ID` = '$id'", OBJECT, 0 );
		} elseif ( $slug ) {
			$category_db = $wpdb->get_row( "SELECT * FROM `product_category` WHERE `slug` = '$slug'", OBJECT, 0 );
		} else {
			return new WP_Error( 'wrong_id_slug', __( 'Category slug or id not specified', TEXTDOMAIN ), array( 'status' => 500 ) );
		}

		if ( $wpdb->num_rows ) {
			$category = $this->_get_category( $category_db, $lang );

			return new WP_REST_Response( $category, 200 );
		} else {
			return new WP_Error( 'not_found', __( 'Category not found', TEXTDOMAIN ), array( 'status' => 400 ) );
		}
	}

	/**
	 * Get categories like list
	 */
	public function get_list_categories( WP_REST_Request $request ) {
		$offset = $request->get_param( 'offset' ) ? $request->get_param( 'offset' ) : 0;
		$limit  = $request->get_param( 'limit' ) ? $request->get_param( 'limit' ) : 1;
		$lang   = $request->get_param( 'lang' ) ? $request->get_param( 'lang' ) : null;

		global $wpdb;

		$query         = "SELECT * FROM `product_category` " . wheres_site_id( $request ) . " ORDER BY `order` ASC LIMIT $offset,$limit";
		$categroies_db = $wpdb->get_results( $query );

		if ( $wpdb->num_rows ) {
			$categories = [];
			foreach ( $categroies_db as $category ) {
				array_push( $categories, $this->_get_category( $category, $lang ) );
			}

			return new WP_REST_Response( $categories, 200 );
		} else {
			return new WP_Error( 'error_categories_list', __( 'Problem with loading categories list', TEXTDOMAIN ), array( 'status' => 500 ) );
		}
	}

	/**
	 * Update cover of Category by slug
	 */
	public function update_category_cover( WP_REST_Request $request ) {
		$slug  = $request->get_param( 'slug' ) ? $request->get_param( 'slug' ) : null;
		$cover = $request->get_param( 'cover' ) ? $request->get_param( 'cover' ) : null;

		global $wpdb;
		if ( $slug ) {
			$update = $wpdb->update( 'product_category', [ 'cover' => $cover ], [ "slug" => $slug ] );
			if ( $update ) {
				return new WP_REST_Response( $update, 200 );
			} else {
				return new WP_Error( 'error_update', __( 'Maybe cover url is not different', TEXTDOMAIN ), array( 'status' => 400 ) );
			}
		} else {
			return new WP_Error( 'wrong_slug', __( 'Wrong slug of category', TEXTDOMAIN ), array( 'status' => 400 ) );
		}
	}

	/**
	 * Preparing categories
	 */
	public function _get_categroies( $lang = null, $parent = 0, $site_id = null ) {
		global $wpdb;
		$query = "SELECT * FROM `product_category` WHERE `parent` = " . $parent;
		if ( $site_id ) {
			$query = $query . " AND `site_id` = " . $site_id;
		}
		$query         = $query . " ORDER BY `order` ASC";
		$categroies_db = $wpdb->get_results( $query );
		if ( $wpdb->num_rows ) {
			$categories = [];
			foreach ( $categroies_db as $category ) {

				$category_translate = $lang ? get_translate_obj( 'product_category', intval( $category->ID ), $lang ) : null;

				array_push( $categories, array(
					'ID'          => intval( $category->ID ),
					'name'        => $category_translate ? $category_translate->name : $category->name,
					'description' => $category_translate ? $category_translate->description : $category->description,
					'discount'    => $category->discount,
					'slug'        => $category->slug,
					'cover'       => $category_translate ? $category_translate->cover : $category->cover,
					'parent'      => intval( $category->parent ),
					'order'       => intval( $category->order ),
					'lang'        => $lang ? $lang : null,
					'site_id'     => intval( $category->site_id ),
					'children'    => $this->_get_categroies( $lang, intval( $category->ID ) )
				) );
			}

			return $categories;
		} else {
			return [];
		}
	}

	/**
	 * Update order
	 */
	public function _order_menu_item( $items, $p = 0 ) {
		global $wpdb;
		foreach ( $items as $item ) {
			$wpdb->update( 'product_category', [ 'order' => $item['order'], 'parent' => $item['parent'] ], [ 'ID' => $item['ID'] ] );
			if ( $item['children'] ) {
				$this->_order_menu_item( $item['children'], $item['ID'] );
			}
		}
	}

	/**
	 * Get clear category field
	 */
	public function _get_category( $category, $lang = null ) {
		$category_translate = $lang ? get_translate_obj( 'product_category', intval( $category->ID ), $lang ) : null;

		return [
			'ID'          => intval( $category->ID ),
			'name'        => $category_translate ? $category_translate->name : $category->name,
			'description' => $category_translate ? $category_translate->description : $category->description,
			'discount'    => $category->discount,
			'slug'        => $category->slug,
			'cover'       => $category_translate ? $category_translate->cover : $category->cover,
			'parent'      => intval( $category->parent ),
			'order'       => intval( $category->order ),
			'lang'        => $lang ? $lang : null,
			'site_id'     => intval( $category->site_id ),
			'children'    => $this->_get_categroies( $lang, intval( $category->ID ) )
		];
	}

	/**
	 * Check slug and generate uniq for each site
	 */
	public function check_slug( $id, $slug, $site_id, $i = 0 ) {
		$slug     = $slug ? $slug : $id;
		$new_slug = $i ? $slug . "-" . $i : $slug;
		$i ++;
		global $wpdb;
		$query = "SELECT * FROM `product_category` WHERE `ID` != " . $id . " AND `slug` = '$new_slug'";
		if ( $site_id ) {
			$query = $query . " AND `site_id` = " . $site_id;
		}
		$category = $wpdb->get_row( $query );
		if ( $category ) {
			return $this->check_slug( $id, $slug, $site_id, $i );
		} else {
			return $new_slug;
		}
	}

	/**
	 * Check if a given request has access like administrator
	 */
	public function check_permissions_administrator( WP_REST_Request $request ) {
		global $UserRole;

		return user_permissions( $request, $UserRole['ADMINISTRATOR'] );
	}

	/**
	 * Check if a given request has access like editor
	 */
	public function check_permissions_editor( WP_REST_Request $request ) {
		global $UserRole;

		return user_permissions( $request, $UserRole['EDITOR'] );
	}
}

add_action( 'rest_api_init', function () {
	$controller = new Category_Route_API;
	$controller->register_routes();
} );
