const {src, dest, task, series, parallel, watch} = require("gulp");
const path = require("path");
const util = require("gulp-util");
const gulpconfig = require("./gulpConfig");
const sass = require("gulp-sass");
const postcss = require("gulp-postcss");
const normalize = require("postcss-normalize");
const autoprefixer = require("gulp-autoprefixer");
const cssnext = require("postcss-cssnext");
const cssnano = require("cssnano");
const pug = require("gulp-pug");
const svgstore = require("gulp-svgstore");
const svgmin = require("gulp-svgmin");
const through2 = require("through2");
const cheerio = require("cheerio");
const Vinyl = require("vinyl");
const inject = require("gulp-inject");
const rename = require("gulp-rename");
const browserSync = require("browser-sync");
const reload = browserSync.reload;

let paths = {};
let options = {};
let production = false;

function settings(cb) {
	production = !!util.env.production;
	paths = gulpconfig.getPaths(production);
	options = gulpconfig.getOptions(production);
	cb();
}

function fileContents(filePath, file) {
	return file.contents.toString();
}

// images
function images() {
	return src(paths.images.src)
		.pipe(dest(paths.images.dist));
}

// fonts
function fonts() {
	return src(paths.fonts.src)
		.pipe(dest(paths.fonts.dist));
}

// svgstore
task("svg", function () {
	return src(paths.svgstore.src)
		.pipe(rename(function (file) {
			file.basename = options.svgStore.prefix + "-" + file.basename.toLowerCase().replace(/\s/g, "-");
		}))
		.pipe(svgmin(function (file) {
			var prefix = path.basename(file.relative, path.extname(file.relative));
			return {
				plugins: [{
					cleanupIDs: {
						prefix: prefix + "-",
						minify: true
					}
				}]
			};
		}))
		.pipe(svgstore(options.svgStore))
		.pipe(rename(function (file) {
			file.basename = paths.svgstore.name;
		}))
		.pipe(dest(paths.svgstore.dist));
});

// scss
function scss() {
	return src(paths.styles.src)
		.pipe(sass(options.sass).on("error", sass.logError))
		.pipe(autoprefixer(options.autoprefixer))
		.pipe(dest(paths.styles.dist));
}

// pug
task("pugs", function () {
	return src(paths.templates.src)
		.pipe(pug({
			pretty: true,
			data: {
				options: options
			}
		}))
		.pipe(dest(paths.templates.dist));
});

// injection
task("injection", function () {
	const svgs = src(paths.injection.svg);
	return src(paths.injection.files)
		.pipe(inject(svgs, {transform: fileContents}))
		.pipe(dest(paths.templates.dist));
});

// browser
function serve() {
	browserSync.init({
		watch: true,
		server: {
			baseDir: paths.dist
		},
		notify: false
	});
}

// watch
task("watcher", function (cb) {
	serve();
	watch([paths.styles.watch], scss).on("change", reload);
	watch([paths.images.watch], images).on("change", reload);
	watch([paths.fonts.watch], fonts).on("change", reload);
	watch([paths.templates.watch], series("pugs", "injection")).on("change", reload);
	watch([paths.svgstore.watch], series("svg", "injection")).on("change", reload);
	cb();
});


const build = parallel(images, fonts, scss, series("svg", "pugs", "injection"));
const buildApp = parallel(images, fonts, scss, series("svg"));

const run = !!util.env.production ? series(buildApp) : series(build, "watcher");

task("default", series(settings, run));
task("build", series(settings, build));
