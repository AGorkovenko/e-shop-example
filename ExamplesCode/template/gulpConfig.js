module.exports.getOptions = function (production = false) {
	return {
		sass: {
			outputStyle: "compact", // nested, expanded, compact, compress
		},

		autoprefixer: {
			browsers: ["last 2 versions"]
		},

		svgStore: {
			inlineSvg: true,
			prefix: "icon",
			minify: true,
			symbol: {
				viewBox: "0 0 100 100"
			}
		}
	};
};

module.exports.getPaths = function (production = false) {
	const path = require("path");

	let srcDir = "./src";
	let distDir = production ? "./../appSite/src/assets" : "./dist";

	return {
		src: srcDir,
		dist: distDir,

		styles: {
			src: path.join(srcDir, "scss/*.scss"),
			dist: path.join(distDir, "css"),
			watch: path.join(srcDir, "scss/**/*")
		},
		templates: {
			src: path.join(srcDir, "templates/*.pug"),
			dist: distDir,
			watch: path.join(srcDir, "templates/**/*")
		},
		images: {
			src: path.join(srcDir, "images/*"),
			dist: path.join(distDir, "images"),
			watch: path.join(srcDir, "images/**/*")
		},
		fonts: {
			src: path.join(srcDir, "fonts/**/*"),
			dist: path.join(distDir, "fonts"),
			watch: path.join(srcDir, "fonts/**/*")
		},
		svgstore: {
			src: path.join(srcDir, "svg/**/*.svg"),
			dist: path.join(distDir, "images"),
			name: "store",
			watch: path.join(srcDir, "svg/**/*")
		},
		injection: {
			svg: path.join(distDir, "images/store.svg"),
			files: path.join(distDir, "*.html"),
		}
	};
};
