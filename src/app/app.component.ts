import {Component} from "@angular/core";
import {SiteNavigationService} from "./_core/services/site-navigation";

@Component({
    selector: "app-root",
    template: `
        <router-outlet></router-outlet>
    `
})
export class AppComponent {
    
    constructor(private siteNavigationService: SiteNavigationService) {
        this.siteNavigationService.enableLoader();
    }
}
