import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {LogoutComponent} from "./logout.component";
import {LogoutResolver} from "./logout.resolver";

const routes: Routes = [
    {
        path: "",
        component: LogoutComponent,
        resolve: {
            logout: LogoutResolver
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [LogoutResolver]
})
export class LogoutRoutingModule {
}
