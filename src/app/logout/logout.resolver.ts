import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, Router} from "@angular/router";
import {EMPTY, Observable} from "rxjs";
import {catchError, tap} from "rxjs/operators";
import {ProfileService} from "../_core/services/profile/profile.service";
import {LoggerService} from "../_core/services/logger.service";
import {ExceptionService} from "../_core/services/exception.service";

@Injectable()
export class LogoutResolver implements Resolve<boolean> {
    
    constructor(private logger: LoggerService,
                private exceptionService: ExceptionService,
                private profileService: ProfileService,
                private router: Router) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    resolve(route: ActivatedRouteSnapshot): Observable<boolean> {
        const redirect = route.queryParams["redirect"] ? route.queryParams["redirect"] : "/";
        return this.profileService.logout()
        .pipe(
            tap((exit: boolean) => {
                this.router.navigate([redirect]);
            })
        );
    }
}
