import {NgModule} from "@angular/core";
import {SharedModule} from "../_shared/shared.module";
import {LogoutRoutingModule} from "./logout-routing.module";
import {LogoutComponent} from "./logout.component";

@NgModule({
    imports: [
        LogoutRoutingModule,
        SharedModule
    ],
    declarations: [
        LogoutComponent
    ]
})
export class LogoutModule {
}
