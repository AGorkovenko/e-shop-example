import {Component} from "@angular/core";
import {LoggerService} from "../_core/services/logger.service";

@Component({
    selector: "app-logout",
    template: `User logout`
})
export class LogoutComponent {
    constructor(private logger: LoggerService) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
}
