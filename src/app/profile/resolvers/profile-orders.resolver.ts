import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Params, Resolve} from "@angular/router";
import {EMPTY, forkJoin, Observable} from "rxjs";
import {catchError, map, tap} from "rxjs/operators";
import {LoggerService} from "../../_core/services/logger.service";
import {ExceptionService} from "../../_core/services/exception.service";
import {OrderListData} from "../../_core/services/order/orders.service";
import {OrdersBackendClient} from "../../_core/services/order/orders-backend.service";
import {OrderViewRequest} from "../../_core/models/dto/order/order-view-request.model";
import {Order} from "../../_core/models/dto/order/order.model";

@Injectable()
export class ProfileOrdersResolver implements Resolve<OrderListData> {
	
	constructor(private logger: LoggerService,
	            private exceptionService: ExceptionService,
	            private ordersBackendClient: OrdersBackendClient) {
		this.logger.debug(this.constructor.name + " is created.");
	}
	
	private static paramsToViewRequest(queryParams: Params): OrderViewRequest {
		const viewRequest: OrderViewRequest = new OrderViewRequest();
		viewRequest.order = queryParams["order"] ? queryParams["order"] : null;
		viewRequest.sort = queryParams["sort"] ? queryParams["sort"] : null;
		if (queryParams["page"]) {
			viewRequest.page = parseInt(queryParams["page"]);
		}
		return viewRequest;
	}
	
	resolve(route: ActivatedRouteSnapshot): Observable<OrderListData> {
		const viewRequest: OrderViewRequest = ProfileOrdersResolver.paramsToViewRequest(route.queryParams);
		
		this.logger.debug("Start loading orders", viewRequest);
		return forkJoin(
			this.ordersBackendClient.loadOrders(viewRequest),
			this.ordersBackendClient.loadCount(viewRequest))
			.pipe(
				catchError(errorInfo => {
					this.exceptionService.handleRequestErrorInfo(errorInfo.error);
					return EMPTY;
				}),
				tap((data: [Order[], number]) => {
					this.logger.info("Loaded orders", data[0]);
					this.logger.info("All orders by request ", data[1]);
				}),
				map((data: [Order[], number]) => {
					const orderListData: OrderListData = new OrderListData();
					orderListData.list = data[0];
					orderListData.count = data[1];
					orderListData.viewRequest = viewRequest;
					return orderListData;
				})
			);
	}
}
