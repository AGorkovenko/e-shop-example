import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, Router} from "@angular/router";
import {EMPTY, Observable} from "rxjs";
import {catchError, tap} from "rxjs/operators";
import {LoggerService} from "../../_core/services/logger.service";
import {ExceptionService} from "../../_core/services/exception.service";
import {ProfileDelivery} from "../../_core/models/dto/profile/profile-delivery.model";
import {ProfileService} from "../../_core/services/profile/profile.service";

@Injectable()
export class ProfileDeliveryResolver implements Resolve<ProfileDelivery> {
	
	constructor(private logger: LoggerService,
	            private exceptionService: ExceptionService,
	            private profileService: ProfileService,
	            private router: Router) {
		this.logger.debug(this.constructor.name + " is created.");
	}
	
	resolve(route: ActivatedRouteSnapshot): Observable<ProfileDelivery> {
		return this.profileService.loadProfileDelivery()
			.pipe(
				catchError(errorInfo => {
					this.exceptionService.handleRequestErrorInfo(errorInfo.error);
					this.router.navigate(["/profile"]);
					return EMPTY;
				})
			);
	}
}
