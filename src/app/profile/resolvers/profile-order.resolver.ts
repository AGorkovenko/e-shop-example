import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, Router} from "@angular/router";
import {EMPTY, Observable} from "rxjs";
import {catchError, tap} from "rxjs/operators";
import {LoggerService} from "../../_core/services/logger.service";
import {ExceptionService} from "../../_core/services/exception.service";
import {Order} from "../../_core/models/dto/order/order.model";
import {OrdersBackendClient} from "../../_core/services/order/orders-backend.service";

@Injectable()
export class ProfileOrderResolver implements Resolve<Order> {
	
	constructor(private logger: LoggerService,
	            private exceptionService: ExceptionService,
	            private ordersBackendClient: OrdersBackendClient,
	            private router: Router) {
		this.logger.debug(this.constructor.name + " is created.");
	}
	
	resolve(route: ActivatedRouteSnapshot): Observable<Order> {
		const id = parseInt(route.params["id"]);
		if (id) {
			return this.ordersBackendClient.loadOrder(id)
				.pipe(
					tap((order: Order) => {
						this.logger.info("Loaded order", order);
					}),
					catchError(errorInfo => {
						this.exceptionService.handleRequestErrorInfo(errorInfo.error);
						this.router.navigate(["/profile/orders"]);
						return EMPTY;
					})
				);
		} else {
			this.router.navigate(["/profile/orders"]);
		}
	}
}
