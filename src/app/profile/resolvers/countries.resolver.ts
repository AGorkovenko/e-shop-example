import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve} from "@angular/router";
import {Observable} from "rxjs";
import {LoggerService} from "../../_core/services/logger.service";
import {ExceptionService} from "../../_core/services/exception.service";
import {CountryService} from "../../_core/services/country.service";
import {SelectOption} from "../../_shared/components/select/select-option.enum";

@Injectable()
export class CountriesResolver implements Resolve<SelectOption[]> {
	
	constructor(private logger: LoggerService,
	            private exceptionService: ExceptionService,
	            private countryService: CountryService) {
		this.logger.debug(this.constructor.name + " is created.");
	}
	
	resolve(route: ActivatedRouteSnapshot): Observable<SelectOption[]> {
		return this.countryService.getCountryOptions();
	}
}
