import {NgModule} from "@angular/core";
import {ProfileRoutingModule} from "./profile-routing.module";
import {SharedModule} from "../_shared/shared.module";
import {ProfileComponent} from "./profile.component";
import {ProfileDetailsComponent} from "./components/profile-details.component";
import {ProfileAddressesComponent} from "./components/profile-addresses.component";
import {ProfileOrdersComponent} from "./components/profile-orders.component";
import {ProfileWishlistComponent} from "./components/profile-wishlist.component";
import {ProfileOrderComponent} from "./components/profile-order.component";

@NgModule({
    declarations: [
        ProfileComponent,
        ProfileDetailsComponent,
        ProfileAddressesComponent,
        ProfileOrdersComponent,
        ProfileWishlistComponent,
        ProfileOrderComponent
    ],
    imports: [
        SharedModule,
        ProfileRoutingModule
    ]
})
export class ProfileModule {
}
