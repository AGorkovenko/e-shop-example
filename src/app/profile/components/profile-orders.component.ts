import {Component, OnInit} from "@angular/core";
import {AsyncSubject, Observable, Subject} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {takeUntil, tap} from "rxjs/operators";
import {OrderListData, OrdersService} from "../../_core/services/order/orders.service";
import {Currency} from "../../_core/models/enum/currency.enum";
import {OrderStatus} from "../../_core/models/dto/order/order-status.enum";
import {LoggerService} from "../../_core/services/logger.service";
import {TimeService} from "../../_core/services/time.service";
import {CurrencyService} from "../../_core/services/currency.service";

@Component({
  selector: "app-profile-orders",
  template: `
    <div *ngIf="orderListData$ | async as orderListData">
      
      <div *ngIf="orderListData.list; else notFound" class="mb">
        <table class="orders-table">
          <thead>
          <tr>
            <th i18n="Table header on profile orders page@@tableOrdersHeaderNumber">Number</th>
            <th i18n="Table header on profile orders page@@tableOrdersHeaderDate">Date</th>
            <th i18n="Table header on profile orders page@@tableOrdersHeaderProducts">Products</th>
            <th i18n="Table header on profile orders page@@tableOrdersHeaderStatus">Status</th>
            <th i18n="Table header on profile orders page@@tableOrdersHeaderTotal">Total</th>
          </thead>
          <tbody>
          <tr *ngFor="let order of orderListData.list">
            <td class="text-center"><a [routerLink]="['/profile/orders/' + order.ID]">#{{order.ID}}</a></td>
            <td class="text-center">{{timeService.getTimeStamp(order.date, "YYYY-MM-DD HH:mm:ss")}}</td>
            <td class="text-center">{{order.quantity}}</td>
            <td class="text-center" [ngClass]="orderStatus.getCssClass(order.status)">{{orderStatus.getDisplayName(order.status)}}</td>
            <td class="text-center">{{currencyService.currencyExchange(order.total) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}</td>
          </tr>
          </tbody>
        </table>
      </div>
      <ng-template #notFound>
        <h4 class="text-center" i18n="No orders on profile orders page@@alertProfileNoOrders">You don't have orders</h4>
      </ng-template>
      
      <app-paginator *ngIf="orderListData.count / orderListData.viewRequest.pageSize > 1"
                     [currentPage]="orderListData.viewRequest.page" [totalCount]="orderListData.count"
                     [pageSize]="orderListData.viewRequest.pageSize" [isMoreButtonEnabled]="true"
                     (onShowMore)="loadMoreOrders($event)"></app-paginator>
    </div>
  `
})
export class ProfileOrdersComponent implements OnInit {
  
  orderListData$: Observable<OrderListData>;
  orderStatus = OrderStatus;
  currentCurrency: Currency;
  currency = Currency;
  
  isDestroyed$: Subject<void> = new Subject();
  
  constructor(private logger: LoggerService,
              private route: ActivatedRoute,
              private router: Router,
              private ordersService: OrdersService,
              public currencyService: CurrencyService,
              public timeService: TimeService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  ngOnInit() {
    this.orderListData$ = this.ordersService.orderListData$;
    
    this.route.data
    .pipe(takeUntil(this.isDestroyed$))
    .subscribe((data: { orderListData: OrderListData }) => this.ordersService.initOrderList(data.orderListData));
    
    this.currencyService.currentCurrency$
    .pipe(
      tap((currency: Currency) => {
        this.currentCurrency = currency;
      }),
      takeUntil(this.isDestroyed$)
    )
    .subscribe();
  }
  
  loadMoreOrders(showMoreEvent: { page: number, callbackNotifier: AsyncSubject<void> }) {
    this.ordersService.loadAndAppendPage(showMoreEvent.page)
    .subscribe(
      () => showMoreEvent.callbackNotifier.next(null),
      null,
      () => showMoreEvent.callbackNotifier.complete()
    );
  }
  
  ngOnDestroy(): void {
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }
  
}
