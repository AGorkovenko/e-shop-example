import {Component, OnDestroy, OnInit} from "@angular/core";
import {LoggerService} from "../../_core/services/logger.service";
import {ProfileService} from "../../_core/services/profile/profile.service";
import {Profile} from "../../_core/models/dto/profile/profile.model";
import {Subject} from "rxjs";
import {switchMap, takeUntil, tap} from "rxjs/operators";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ProfileUpdateService} from "../../_core/services/profile/profile-update.service";
import {PasswordValidation} from "../../_core/password.validatator";

@Component({
  selector: "app-profile-details",
  template: `
    <form class="form" [formGroup]="profileForm">
      <h3 i18n="Details title on details page@@titleYourDetails">Your details</h3>
      <div class="form-columns">
        <div class="form-line">
          <label i18n="Label on details page@@labelDetailsFirstName">First Name</label>
          <input class="form-element" type="text" formControlName="firstname" [ngClass]="{'form-element--error': profileForm.get('firstname').invalid && profileForm.get('firstname').touched}">
        </div>
        <div class="form-line">
          <label i18n="Label on details page@@labelDetailsLastName">Last Name</label>
          <input class="form-element" type="text" formControlName="lastname" [ngClass]="{'form-element--error': profileForm.get('lastname').invalid && profileForm.get('lastname').touched}">
        </div>
      </div>
      <div class="form-line text-right">
        <button class="button button--inline button--danger" (click)="updateProfile()">Update</button>
      </div>
      <div class="form-columns">
        <div class="form-line">
          <label i18n="Label on details page@@labelDetailsDisplayName">Login<sup>*</sup></label>
          <input class="form-element" type="text" formControlName="login">
        </div>
        <div class="form-line">
          <label i18n="Label on details page@@labelDetailsEmail">E-mail<sup>*</sup></label>
          <input class="form-element" type="text" formControlName="email" [ngClass]="{'form-element--error': profileForm.get('email').invalid && profileForm.get('email').touched}">
        </div>
      </div>
      <div class="form-help text-center" *ngIf="newEmail">We sent you a letter to confirm the change of your email to <b class="text-dark">{{newEmail}}</b></div>
      <div class="form-line text-right">
        <button class="button button--inline button--danger" (click)="updateEmail()">Change</button>
      </div>
    </form>
    
    <form class="form" [formGroup]="passwordForm">
      <h3 i18n="Password title on details page@@titleChangePassword">Change password</h3>
      <div class="form-line">
        <label i18n="Label on details page@@labelDetailsCurrentPassword">Current password</label>
        <input class="form-element" type="password" formControlName="oldpassword" [ngClass]="{'form-element--error': passwordForm.get('oldpassword').invalid && passwordForm.get('oldpassword').touched}">
      </div>
      <div class="form-line">
        <label i18n="Label on details page@@labelDetailsNewPassword">New password</label>
        <input class="form-element" type="password" formControlName="password" [ngClass]="{'form-element--error': passwordForm.get('password').invalid && passwordForm.get('password').touched}">
      </div>
      <div class="form-line">
        <label i18n="Label on details page@@labelDetailsConfirmNewPassword">Confirm new password</label>
        <input class="form-element" type="password" formControlName="repassword" [ngClass]="{'form-element--error': passwordForm.get('repassword').invalid && passwordForm.get('repassword').touched}">
      </div>
      <div class="form-line form-line--last">
        <button class="button button--inline button--danger" i18n="Label on details page@@buttonDetailsPasswordChange" (click)="changePassword()">Change</button>
        <span class="text-success ml" *ngIf="isPasswordChanged">Password was changed!</span>
      </div>
    </form>
  `
})
export class ProfileDetailsComponent implements OnInit, OnDestroy {
  
  profile: Profile;
  profileForm: FormGroup;
  newEmail: string;
  
  passwordForm: FormGroup;
  isPasswordChanged: boolean = false;
  
  isDestroyed$: Subject<void> = new Subject();
  
  constructor(private logger: LoggerService,
              private profileService: ProfileService,
              private profileUpdateService: ProfileUpdateService,
              private fb: FormBuilder) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  ngOnInit() {
    this.profileService.profile$
      .pipe(
        tap((profile: Profile) => {
          this.profile = profile;
          this.initForm();
          this.initPasswordForm();
        }),
        switchMap(() => this.profileService.loadRequestedEmail()),
        tap((email: string) => {
          this.newEmail = email;
        }),
        takeUntil(this.isDestroyed$)
      )
      .subscribe();
  }
  
  initForm(profile?: Profile) {
    profile = profile ? profile : this.profile;
    this.profileForm = this.fb.group({
      login: new FormControl({value: (profile && profile.login) ? profile.login : null, disabled: true}, [Validators.required, Validators.minLength(2)]),
      firstname: new FormControl((profile && profile.firstname) ? profile.firstname : null, [Validators.minLength(2)]),
      lastname: new FormControl((profile && profile.lastname) ? profile.lastname : null, [Validators.minLength(2)]),
      email: new FormControl((profile && profile.email) ? profile.email : null, [Validators.required, Validators.email]),
      ava: new FormControl((profile && profile.ava) ? profile.ava : null),
    });
  }
  
  initPasswordForm() {
    this.passwordForm = this.fb.group({
      oldpassword: new FormControl(null, [Validators.required, Validators.minLength(4)]),
      password: new FormControl(null, [Validators.required, Validators.minLength(4)]),
      repassword: new FormControl(null, [Validators.required, Validators.minLength(4)])
    }, {
      validator: PasswordValidation.MatchPassword
    });
  }
  
  updateProfile() {
    if (this.profileForm.get("firstname").valid && this.profileForm.get("lastname").valid) {
      const formData = this.profileForm.getRawValue();
      this.profileUpdateService.updateProfile(formData)
        .pipe(
          tap((profile: Profile) => {
            if (profile) {
              this.profileService.profileInit(profile);
            }
          }),
          takeUntil(this.isDestroyed$)
        )
        .subscribe();
    }
  }
  
  updateEmail() {
    if (this.profileForm.get("email").valid && this.profile.email !== this.profileForm.get("email").value) {
      this.profileUpdateService.sendRequestNewEmail(this.profileForm.get("email").value)
        .pipe(
          tap((status: boolean) => {
            this.newEmail = this.profileForm.get("email").value;
            this.profileForm.get("email").patchValue(this.profile.email);
          }),
          takeUntil(this.isDestroyed$)
        )
        .subscribe();
    }
  }
  
  changePassword() {
    if (this.passwordForm.valid) {
      this.profileUpdateService.passwordChange(this.passwordForm.get("password").value, this.passwordForm.get("repassword").value, this.passwordForm.get("oldpassword").value)
        .pipe(
          tap(pass => {
            if (pass) {
              this.passwordForm.reset();
              this.isPasswordChanged = true;
              setTimeout(() => {this.isPasswordChanged = false;}, 2000);
            }
          }),
          takeUntil(this.isDestroyed$)
        )
        .subscribe();
    }
  }
  
  ngOnDestroy(): void {
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }
  
}
