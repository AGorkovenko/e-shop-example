import {Component, OnDestroy, OnInit} from "@angular/core";
import {SelectOption} from "../../_shared/components/select/select-option.enum";
import {Subject} from "rxjs";
import {LoggerService} from "../../_core/services/logger.service";
import {ProfileService} from "../../_core/services/profile/profile.service";
import {ProfileUpdateService} from "../../_core/services/profile/profile-update.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ProfileDelivery} from "../../_core/models/dto/profile/profile-delivery.model";
import {ActivatedRoute} from "@angular/router";
import {takeUntil, tap} from "rxjs/operators";

@Component({
    selector: "app-profile-addresses",
    template: `
        <form class="form" [formGroup]="profileDeliveryForm">
            <h3 i18n="Address title on delivery address page@@titleAddressDelivery">Address for delivery</h3>
            <div class="form-columns">
                <div class="form-line">
                    <label i18n="Label on delivery address page@@labelAddressDeliveryFirstName">First Name</label>
                    <input class="form-element" type="text" formControlName="firstname">
                </div>
                <div class="form-line">
                    <label i18n="Label on delivery address page@@labelAddressDeliveryLastName">Last Name</label>
                    <input class="form-element" type="text" formControlName="lastname">
                </div>
            </div>
            <div class="form-columns">
                <div class="form-line">
                    <label i18n="Label on delivery address page@@labelAddressDeliveryPhone">Phone</label>
                    <input class="form-element" type="tel" formControlName="phone">
                </div>
                <div class="form-line">
                    <label i18n="Label on delivery address page@@labelAddressDeliveryCountry">Country</label>
                    <app-select [options]="countries" formControlName="country" placeholder="Select country" [lifeSearch]="true" (queryChanged)="countryFilter($event)"></app-select>
                </div>
            </div>
            <div class="form-columns">
                <div class="form-line">
                    <label i18n="Label on delivery address page@@labelAddressDeliveryZip">Postcode / ZIP</label>
                    <input class="form-element" type="text" formControlName="zip">
                </div>
                <div class="form-line">
                    <label i18n="Label on delivery address page@@labelAddressDeliveryOrt">Ort / City</label>
                    <input class="form-element" type="text" formControlName="ort">
                </div>
            </div>
            <div class="form-line">
                <label i18n="Label on delivery address page@@labelAddressDeliveryStreetAddress">Street Address</label>
                <input class="form-element" type="text" formControlName="address">
            </div>
            <div class="form-line">
                <button class="button button--inline button--danger" (click)="save()">Save</button>
                <span class="text-success ml" *ngIf="isProfileDeliverySaved">Delivery data was saved!</span>
            </div>
        </form>
    `
})
export class ProfileAddressesComponent implements OnInit, OnDestroy {
    
    profileDelivery: ProfileDelivery;
    countries: SelectOption[]              = [];
    countriesWithoutFilter: SelectOption[] = [];
    profileDeliveryForm: FormGroup;
    isProfileDeliverySaved: boolean        = false;
    
    isDestroyed$: Subject<void> = new Subject();
    
    constructor(private logger: LoggerService,
                private profileService: ProfileService,
                private profileUpdateService: ProfileUpdateService,
                private fb: FormBuilder,
                private route: ActivatedRoute) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    ngOnInit() {
        this.route.data
        .pipe(
            tap((data: { delivery: ProfileDelivery, countries: SelectOption[] }) => {
                this.profileDelivery = data.delivery;
                this.countries       = data.countries;
                Object.assign(this.countriesWithoutFilter, this.countries);
                this.initForm();
            }),
            takeUntil(this.isDestroyed$)
        )
        .subscribe();
    }
    
    initForm(profileDelivery?: ProfileDelivery) {
        profileDelivery          = profileDelivery ? profileDelivery : this.profileDelivery;
        this.profileDeliveryForm = this.fb.group({
            id: new FormControl((profileDelivery && profileDelivery.ID) ? profileDelivery.ID : null, [Validators.required]),
            uid: new FormControl((profileDelivery && profileDelivery.uid) ? profileDelivery.uid : null, [Validators.required]),
            firstname: new FormControl((profileDelivery && profileDelivery.firstname) ? profileDelivery.firstname : null),
            lastname: new FormControl((profileDelivery && profileDelivery.lastname) ? profileDelivery.lastname : null),
            country: new FormControl((profileDelivery && profileDelivery.country) ? profileDelivery.country : null),
            zip: new FormControl((profileDelivery && profileDelivery.zip) ? profileDelivery.zip : null),
            ort: new FormControl((profileDelivery && profileDelivery.ort) ? profileDelivery.ort : null),
            address: new FormControl((profileDelivery && profileDelivery.address) ? profileDelivery.address : null),
            phone: new FormControl((profileDelivery && profileDelivery.phone) ? profileDelivery.phone : null)
        });
    }
    
    save() {
        const formData = this.profileDeliveryForm.getRawValue();
        this.profileUpdateService.profileDeliveryUpdate(formData)
        .pipe(
            tap((profileDelivery: ProfileDelivery) => {
                if (profileDelivery) {
                    this.profileDelivery = profileDelivery;
                    this.initForm(profileDelivery);
                    this.isProfileDeliverySaved = true;
                    setTimeout(() => {
                        this.isProfileDeliverySaved = false;
                    }, 2000);
                }
            }),
            takeUntil(this.isDestroyed$)
        )
        .subscribe();
    }
    
    countryFilter(event) {
        this.countries = this.countriesWithoutFilter;
        if (event && event !== "") {
            this.countries = this.countries.filter((item: SelectOption) => {
                    return event.toLocaleLowerCase() == item.label.substring(0, event.length).toLocaleLowerCase();
                }
            );
        }
    }
    
    ngOnDestroy(): void {
        this.isDestroyed$.next();
        this.isDestroyed$.complete();
    }
}
