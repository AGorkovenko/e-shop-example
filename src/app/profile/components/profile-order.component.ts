import {Component, OnInit} from "@angular/core";
import {Subject} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {takeUntil, tap} from "rxjs/operators";
import {OrdersService} from "../../_core/services/order/orders.service";
import {Currency} from "../../_core/models/enum/currency.enum";
import {OrderStatus} from "../../_core/models/dto/order/order-status.enum";
import {LoggerService} from "../../_core/services/logger.service";
import {TimeService} from "../../_core/services/time.service";
import {Order} from "../../_core/models/dto/order/order.model";
import {CurrencyService} from "../../_core/services/currency.service";

@Component({
  selector: "app-profile-order",
  template: `
    <div *ngIf="order">
      <h3 class="text-center" i18n="Title on profile order page@@titleOrderPage">Order #{{order.ID}}</h3>
      <div class="form-columns text-center">
        <div class="form-line">
          <span i18n="Date on profile order page@@headOrderPageDate">Date update:</span>
          {{timeService.getTimeStamp(order.date, "YYYY-MM-DD HH:mm:ss")}}
        </div>
        <div class="form-line">
          <span i18n="Status on profile order page@@headOrderPageStatus">Status:</span>
          <span [ngClass]="orderStatus.getCssClass(order.status)"> {{orderStatus.getDisplayName(order.status)}}</span>
        </div>
      </div>
      <table class="cart-table cart-table--smart mb">
        <thead>
        <tr>
          <th i18n="Table header on profile order page@@tableOrderProductHeaderProduct">Product</th>
          <th i18n="Table header on profile order page@@tableOrderProductHeaderPrice">Price (Includes VAT)</th>
        </tr>
        </thead>
        <tbody>
        <tr *ngFor="let item of order.items">
          <td class="cart-table__product">
            <div class="cart-product">
              <div class="cart-product__info">
                <a class="cart-product-name" (click)="goToProduct(item.ID)">{{item.title}}</a>
                <div class="cart-product-count-price">{{currencyService.currencyExchange(item.price) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}} ✕ {{item.quantity}}</div>
              </div>
            </div>
          </td>
          <td class="cart-table__price">
            <div class="cart-show-on-mobile" i18n="Table product on profile order page@@tableOrderProductHeaderPriceMobile">(Includes VAT)</div>
            <span>{{currencyService.currencyExchange(item.total) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}</span>
          </td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
          <td class="cart-table__subtotal" i18n="Table footer on profile order page@@tableOrderFooterSubtotal">Subtotal</td>
          <td class="cart-table__price cart-table__price--subtotal">{{currencyService.currencyExchange(order.total) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}</td>
        </tr>
        <tr>
          <td class="cart-table__subtotal" i18n="Table footer on profile order page@@tableOrderFooterDelivery">Delivery</td>
          <td class="cart-table__price cart-table__price--subtotal">{{currencyService.currencyExchange(order.delivery_cost) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}</td>
        </tr>
        <tr>
          <td class="cart-table__subtotal" i18n="Table footer on profile order page@@tableOrderFooterExtraPayment">Extra / Payment</td>
          <td class="cart-table__price cart-table__price--subtotal">{{currencyService.currencyExchange(order.extra_pay) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}</td>
        </tr>
        <tr>
          <td class="cart-table__total" i18n="Table footer on profile order page@@tableOrderFooterTotal">Total</td>
          <td class="cart-table__price cart-table__price--total">{{currencyService.currencyExchange(order.total) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}</td>
        </tr>
        </tfoot>
      </table>
      
      <table class="order-table">
        <thead>
        <tr>
          <th colspan="2" i18n="Other table on profile order page@@tableOrderOtherHeaderShipingBilling">Shipping/Billing address</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <th class="text-right" i18n="Other table on profile order page@@tableOrderOtherHeaderNumber">Name</th>
          <td>{{order.firstname}} {{order.lastname}}</td>
        </tr>
        <tr>
          <th class="text-right" i18n="Other table on profile order page@@tableOrderOtherHeaderAddress">Address</th>
          <td>{{order.address}}</td>
        </tr>
        <tr>
          <th class="text-right" i18n="Other table on profile order page@@tableOrderOtherHeaderZip">ZIP, Ort</th>
          <td>{{order.zip}} {{order.ort}}</td>
        </tr>
        <tr>
          <th class="text-right" i18n="Other table on profile order page@@tableOrderOtherHeaderCountry">Country</th>
          <td>{{order.country.label}}</td>
        </tr>
        <tr *ngIf="order.warehouse">
          <th class="text-right" i18n="Other table on profile order page@@tableOrderOtherHeaderWarehouse">Warehouse</th>
          <td>{{order.warehouse}}</td>
        </tr>
        <tr>
          <th class="text-right" i18n="Other table on profile order page@@tableOrderOtherHeaderPhone">Phone</th>
          <td>{{order.phone}}</td>
        </tr>
        <tr>
          <th class="text-right" i18n="Other table on profile order page@@tableOrderOtherHeaderEmail">E-mail</th>
          <td>{{order.email}}</td>
        </tr>
        </tbody>
        <thead>
        <tr>
          <th colspan="2" i18n="Other table on profile order page@@tableOrderOtherHeaderInfo">Other Info</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <th class="text-right" i18n="Other table on profile order page@@tableOrderOtherHeaderDelivery">Delivery</th>
          <td>
            <div>{{order.delivery.label}}</div>
            <div>{{order.delivery.labelSecond}}</div>
          </td>
        </tr>
        <tr>
          <th class="text-right" i18n="Other table on profile order page@@tableOrderOtherHeaderPayment">Payment</th>
          <td>
            <div>{{order.payment.label}}</div>
            <div>{{order.payment.labelSecond}}</div>
          </td>
        </tr>
        <tr *ngIf="order.comment">
          <th class="text-right" i18n="Other table on profile order page@@tableOrderOtherHeaderComment">Comment</th>
          <td>{{order.comment}}</td>
        </tr>
        </tbody>
      </table>
    </div>
  `
})
export class ProfileOrderComponent implements OnInit {
  
  order: Order;
  orderStatus = OrderStatus;
  currentCurrency: Currency;
  currency = Currency;
  
  isDestroyed$: Subject<void> = new Subject();
  
  constructor(private logger: LoggerService,
              private route: ActivatedRoute,
              public currencyService: CurrencyService,
              public timeService: TimeService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  ngOnInit() {
    this.route.data
    .pipe(takeUntil(this.isDestroyed$))
    .subscribe((data: { order: Order }) => this.order = data.order);
  
    this.currencyService.currentCurrency$
    .pipe(
      tap((currency: Currency) => {
        this.currentCurrency = currency;
      }),
      takeUntil(this.isDestroyed$)
    )
    .subscribe();
  }
  
  goToProduct(id: number) {
    return true;
  }
  
  ngOnDestroy(): void {
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }
  
}
