import {Component, OnInit} from "@angular/core";
import {LoggerService} from "../_core/services/logger.service";

@Component({
    selector: "app-profile",
    template: `
      <div class="container">
        <h1 i18n="Title on account page@@titleAccount">Your account</h1>
        <div class="account">
          <div class="account__sidebar">
            <div class="account-menu">
              <a class="account-menu__item" routerLink="/profile" routerLinkActive="account-menu__item--active" [routerLinkActiveOptions]="{exact: true}">
                <span i18n="Link in sidebar on account page@@linkSidebarAccountDetails">Account details</span>
                <app-icon name="user"></app-icon>
              </a>
              <a class="account-menu__item" routerLink="/profile/addresses" routerLinkActive="account-menu__item--active" [routerLinkActiveOptions]="{exact: true}">
                <span i18n="Link in sidebar on account page@@linkSidebarAddresses">Addresses</span>
                <app-icon name="location"></app-icon>
              </a>
              <a class="account-menu__item" routerLink="/profile/orders" routerLinkActive="account-menu__item--active" [routerLinkActiveOptions]="{exact: true}">
                <span i18n="Link in sidebar on account page@@linkSidebarOrders">Orders</span>
                <app-icon name="check"></app-icon>
              </a>
<!--              <a class="account-menu__item" routerLink="/profile/wishlist" routerLinkActive="account-menu__item&#45;&#45;active" [routerLinkActiveOptions]="{exact: true}">-->
<!--                <span i18n="Link in sidebar on account page@@linkSidebarWishlist">Wishlist</span>-->
<!--                <app-icon name="heart"></app-icon>-->
<!--              </a>-->
              <a class="account-menu__item" routerLink="/logout">
                <span i18n="Link in sidebar on account page@@linkSidebarLogout">Logout</span>
                <app-icon name="logout"></app-icon>
              </a>
            </div>
          </div>
          <div class="account__content">
            <div class="box">
              <router-outlet></router-outlet>
            </div>
          </div>
        </div>
      </div>
    `
})
export class ProfileComponent implements OnInit {
    
    constructor(private logger: LoggerService,) {
      this.logger.debug(this.constructor.name + " is created.");
    }
    
    ngOnInit() {
    
    }
}
