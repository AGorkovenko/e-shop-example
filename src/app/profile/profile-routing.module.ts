import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {ProfileComponent} from "./profile.component";
import {ProfileDetailsComponent} from "./components/profile-details.component";
import {ProfileAddressesComponent} from "./components/profile-addresses.component";
import {ProfileOrdersComponent} from "./components/profile-orders.component";
import {ProfileWishlistComponent} from "./components/profile-wishlist.component";
import {ProfileDeliveryResolver} from "./resolvers/profile-delivery.resolver";
import {CountriesResolver} from "./resolvers/countries.resolver";
import {AuthGuard} from "../_core/auth.guard";
import {ProfileOrdersResolver} from "./resolvers/profile-orders.resolver";
import {ProfileOrderComponent} from "./components/profile-order.component";
import {ProfileOrderResolver} from "./resolvers/profile-order.resolver";

const routes: Routes = [
	{
		path: "",
		component: ProfileComponent,
		canActivateChild: [AuthGuard],
		children: [
			{
				path: "",
				component: ProfileDetailsComponent,
				data: {
					title: "Your profile"
				}
			},
			{
				path: "addresses",
				component: ProfileAddressesComponent,
				data: {
					title: "Your addresses"
				},
				resolve: {
					delivery: ProfileDeliveryResolver,
					countries: CountriesResolver
				}
			},
			{
				path: "orders/:id",
				component: ProfileOrderComponent,
				data: {
					title: "Your order"
				},
				resolve: {
					order: ProfileOrderResolver
				}
			},
			{
				path: "orders",
				component: ProfileOrdersComponent,
				data: {
					title: "Your orders"
				},
				resolve: {
					orderListData: ProfileOrdersResolver
				},
			},
			{
				path: "wishlist",
				component: ProfileWishlistComponent,
				data: {
					title: "Your wishlist"
				}
			}
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
	providers: [
		ProfileDeliveryResolver,
		CountriesResolver,
		ProfileOrdersResolver,
		ProfileOrderResolver
	]
})
export class ProfileRoutingModule {
}
