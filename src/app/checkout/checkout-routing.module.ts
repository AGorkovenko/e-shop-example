import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {CheckoutComponent} from "./components/checkout.component";
import {CheckoutCartResolver} from "./resolvers/checkout-cart.resolver";
import {CheckoutDeliveryResolver} from "./resolvers/checkout-delivery.resolver";
import {CheckoutPaymentResolver} from "./resolvers/checkout-payment.resolver";
import {CheckoutCountryResolver} from "./resolvers/checkout-country.resolver";
import {CheckoutConfirmComponent} from "./components/checkout-confirm.component";
import {CheckoutConfirmOrderResolver} from "./resolvers/checkout-confirm-order.resolver";
import {CheckoutConfirmPaymentResolver} from "./resolvers/checkout-confirm-payment.resolver";

const routes: Routes = [
  {
    path: "",
    component: CheckoutComponent,
    resolve: {
      cart: CheckoutCartResolver,
      delivery: CheckoutDeliveryResolver,
      payment: CheckoutPaymentResolver,
      countries: CheckoutCountryResolver
    }
  },
  {
    path: "confirm/:id",
    component: CheckoutConfirmComponent,
    resolve: {
      order: CheckoutConfirmOrderResolver,
      paymentInfo: CheckoutConfirmPaymentResolver,
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    CheckoutCartResolver,
    CheckoutDeliveryResolver,
    CheckoutPaymentResolver,
    CheckoutCountryResolver,
    CheckoutConfirmOrderResolver,
    CheckoutConfirmPaymentResolver
  ]
})
export class CheckoutRoutingModule {
}
