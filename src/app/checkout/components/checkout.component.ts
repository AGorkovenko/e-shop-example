import {Component, OnDestroy, OnInit} from "@angular/core";
import {LoggerService} from "../../_core/services/logger.service";
import {CartService} from "../../_core/services/cart.service";
import {Subject} from "rxjs";
import {CurrencyService} from "../../_core/services/currency.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Cart} from "../../_core/models/dto/cart/cart.model";
import {switchMap, takeUntil, tap} from "rxjs/operators";
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Profile} from "../../_core/models/dto/profile/profile.model";
import {ProfileService} from "../../_core/services/profile/profile.service";
import {Currency} from "../../_core/models/enum/currency.enum";
import {OrderStatus} from "../../_core/models/dto/order/order-status.enum";
import {CartItem} from "../../_core/models/dto/cart/cart-item.model";
import {ProfileDelivery} from "../../_core/models/dto/profile/profile-delivery.model";
import {SelectOption} from "../../_shared/components/select/select-option.enum";
import {SignInUp} from "../../_core/models/enum/sign-in-up.enum";
import {SignInUpService} from "../../_core/services/sign-in-up.service";
import {OrdersService} from "../../_core/services/order/orders.service";
import {DeliveryType} from "../../_core/models/enum/delivery-type.enum";
import {CountryService} from "../../_core/services/country.service";

@Component({
	selector: "app-checkout",
	template: `
		<div class="container">
			<div class="box">
				<div class="checkout">
					<h1 i18n="@@checkoutPageTitleCheckoutYourOrder">Checkout your order</h1>
					<form [formGroup]="checkoutForm" *ngIf="cart" class="form" [ngClass]="{'form--invalid': isUnvalid}">
						<h3 i18n="@@checkoutPageBlockNameYourOrder">Your order</h3>
						<table class="cart-table cart-table--smart">
							<thead>
							<tr>
								<th i18n="@@checkoutPageCartTitleProduct">Product</th>
								<th i18n="@@checkoutPageCartTitlePriceIncludesVat">Price (Includes VAT)</th>
							</tr>
							</thead>
							<tbody formArrayName="items" *ngFor="let product of getItems().controls; let i = index;">
							<tr [formGroupName]="i.toString()">
								<td class="cart-table__product">
									<div class="cart-product">
										<div class="cart-product__info">
											<div class="cart-product-name">{{product.get("title").value}}</div>
											<div class="cart-product-count-price">
												{{currencyService.currencyExchange(product.get("new_price").value) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}
												✕ {{product.get("quantity").value}}
											</div>
										</div>
									</div>
								</td>
								<td class="cart-table__price">
									<div class="cart-show-on-mobile" i18n="@@checkoutPageCartProductIncludesVat">(Includes VAT)</div>
									{{currencyService.currencyExchange(product.get("total").value) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}
								</td>
							</tr>
							</tbody>
							<tfoot>
							<tr>
								<td class="cart-table__subtotal" i18n="@@checkoutPageCartFooterSubtotal">Subtotal</td>
								<td class="cart-table__price cart-table__price--subtotal">
									{{currencyService.currencyExchange(cart.subtotal) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}
								</td>
							</tr>
							<tr>
								<td class="cart-table__subtotal" i18n="@@checkoutPageCartFooterDelivery">Delivery</td>
								<td class="cart-table__price cart-table__price--subtotal">
									{{currencyService.currencyExchange(this.checkoutForm.get("delivery_cost").value) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}
								</td>
							</tr>
							<tr>
								<td class="cart-table__subtotal" i18n="@@checkoutPageCartFooterExtraPayment">Extra / Payment</td>
								<td class="cart-table__price cart-table__price--subtotal">
									{{currencyService.currencyExchange(this.checkoutForm.get("extra_pay").value) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}
								</td>
							</tr>
							<tr>
								<td class="cart-table__total" i18n="@@checkoutPageCartFooterTotal">Total</td>
								<td class="cart-table__price cart-table__price--total">
									{{currencyService.currencyExchange(this.checkoutForm.get("total").value) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}
								</td>
							</tr>
							</tfoot>
						</table>
						
						<h3 i18n="@@checkoutPageBlockNameDoYouHaveAccount" *ngIf="!profile">Do you have account?</h3>
						<div *ngIf="!profile">
							<p>
								<span i18n="@@checkoutPageBlockInfoLoginToAccount">If you have account, you can <a (click)="openSignInModal()" class="cursor-link text-danger">login</a> and use personal data for ordering.</span>
								<br>
								<span class="text-dark" i18n="@@checkoutPageBlockInfoMergingCarts">(After logging in, the temporary user basket will be merged with your basket!)</span>
							</p>
						</div>
						
						<h3 i18n="@@checkoutPageBlockNameBillingAndDeliveryDetails">Billing and Delivery details</h3>
						<div class="form">
							<div class="form-columns">
								<div class="form-line">
									<label i18n="@@checkoutPageDeliveryLabelFirstname">First Name<sup>*</sup></label>
									<input class="form-element" type="text" formControlName="firstname">
								</div>
								<div class="form-line">
									<label i18n="@@checkoutPageDeliveryLabelLastname">Last Name<sup>*</sup></label>
									<input class="form-element" type="text" formControlName="lastname">
								</div>
							</div>
							<div class="form-columns">
								<div class="form-line">
									<label i18n="@@checkoutPageDeliveryLabelCompany">Company (option)</label>
									<input class="form-element" type="text" formControlName="company">
								</div>
								<div class="form-line">
									<label i18n="@@checkoutPageDeliveryLabelCountry">Country</label>
									<app-select [options]="countries" formControlName="country" placeholder="Select country" [lifeSearch]="true" (queryChanged)="countryFilter($event)"></app-select>
								</div>
							</div>
							<div class="form-line">
								<label i18n="@@checkoutPageDeliveryLabelAddress">Address (Street, house number)<sup>*</sup></label>
								<input class="form-element" type="text" formControlName="address">
							</div>
							<div class="form-columns">
								<div class="form-line">
									<label i18n="@@checkoutPageDeliveryLabelPostcode">Postcode / ZIP<sup>*</sup></label>
									<input class="form-element" type="text" formControlName="zip">
								</div>
								<div class="form-line">
									<label i18n="@@checkoutPageDeliveryLabelCity">City / Ort<sup>*</sup></label>
									<input class="form-element" type="text" formControlName="ort">
								</div>
							</div>
							<div class="form-columns">
								<div class="form-line">
									<label i18n="@@checkoutPageDeliveryLabelPhone">Phone<sup>*</sup></label>
									<input class="form-element" type="text" formControlName="phone">
								</div>
								<div class="form-line">
									<label i18n="@@checkoutPageDeliveryLabelEmail">E-mail address<sup>*</sup></label>
									<input class="form-element" type="email" formControlName="email">
								</div>
							</div>
							<div class="form-line">
								<label i18n="@@checkoutPageDeliveryLabelComment">Comment</label>
								<textarea class="form-element form-element--textarea" formControlName="comment"></textarea>
							</div>
						</div>
						
						<h3 i18n="@@checkoutPageBlockNameDeliveryMethods">Delivery Methods</h3>
						<div class="form-line deliveries">
							<ng-container *ngFor="let delivery of deliveryOptions">
								<div class="delivery" *ngIf="checkAvailableDelivery(delivery)">
									<div class="delivery-name">
										{{delivery.label}}
										<span *ngIf="delivery.valueSecond">(+{{currencyService.currencyExchange(delivery.valueSecond) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}})</span>
									</div>
									<div class="radio" [ngClass]="{'radio--checked': (checkoutForm.get('delivery').value && checkoutForm.get('delivery').value.value === delivery.value), 'radio--error': checkoutForm.get('delivery').invalid && isUnvalid}">
										<label>
											<img [alt]="delivery.label" [src]="'assets/images/' + delivery.value + '.svg'">
											<input type="radio" formControlName="delivery" [value]="delivery">
										</label>
									</div>
								</div>
							</ng-container>
						</div>
						<div class="form-line">
							<div class="mt" *ngIf="checkoutForm.get('delivery').value && managerCalculate(checkoutForm.get('delivery').value)">
								<div class="text-small text-info">* <span i18n="@@checkoutPageAlertShipingCostAfterOrder">Shipping cost will be calculated by the manager after placing order.</span></div>
								<div class="text-small text-info">** <span i18n="@@checkoutPageAlertShipingCostSendAdditionalMail">You will be notified of the cost of delivery by an additional letter and indicated in the info sheet.</span>
								</div>
							</div>
						</div>
						
						<ng-container *ngIf="checkoutForm.get('delivery').value">
							<h3 i18n="@@checkoutPageBlockNamePaymentMethods">Payment Methods</h3>
							<div class="form-line payments">
								<ng-container *ngFor="let payment of paymentOptions">
									<div class="payment" *ngIf="checkAvailablePayment(payment)">
										<div class="payment-name">
											{{payment.label}}
											<span *ngIf="payment.valueSecond">(+{{currencyService.currencyExchange(payment.valueSecond) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}})</span>
										</div>
										<div class="radio" [ngClass]="{'radio--checked': (checkoutForm.get('payment').value && checkoutForm.get('payment').value.value === payment.value), 'radio--error': checkoutForm.get('payment').invalid && isUnvalid}">
											<label>
												<img [alt]="payment.label" [src]="'assets/images/' + payment.value + '.svg'">
												<input type="radio" formControlName="payment" [value]="payment">
											</label>
										</div>
									</div>
								</ng-container>
							</div>
						</ng-container>
						
						<div class="form-line text-right">
							<button type="button" class="button button--inline button--danger button--uppercase button--md" i18n="@@checkoutPageButtonPlaceOrder" (click)="makeOrder()">Place order</button>
						</div>
					
					</form>
				</div>
			</div>
		</div>
	`
})
export class CheckoutComponent implements OnInit, OnDestroy {
	
	profile: Profile;
	profileDelivery: ProfileDelivery;
	cart: Cart;
	checkoutForm: FormGroup;
	currentCurrency: Currency;
	currency = Currency;
	countries: SelectOption[] = [];
	countriesWithoutFilter: SelectOption[] = [];
	deliveryOptions: SelectOption[] = [];
	paymentOptions: SelectOption[] = [];
	isUnvalid: boolean = false;
	isDestroyed$: Subject<void> = new Subject();
	
	constructor(private logger: LoggerService,
	            private route: ActivatedRoute,
	            public router: Router,
	            private signInUpService: SignInUpService,
	            private profileService: ProfileService,
	            private cartService: CartService,
	            private fb: FormBuilder,
	            public currencyService: CurrencyService,
	            private orderService: OrdersService,
	            private countryService: CountryService) {
		this.logger.debug(this.constructor.name + " is created.");
	}
	
	ngOnInit(): void {
		this.route.data
			.pipe(
				tap((data: {
					delivery: SelectOption[],
					payment: SelectOption[],
					countries: SelectOption[],
				}) => {
					this.deliveryOptions = data.delivery;
					this.paymentOptions = data.payment;
					this.countries = data.countries;
					Object.assign(this.countriesWithoutFilter, this.countries);
				}),
				takeUntil(this.isDestroyed$)
			)
			.subscribe();
		
		this.currencyService.currentCurrency$
			.pipe(
				tap((currency: Currency) => {
					this.currentCurrency = currency;
					// this.checkoutForm.patchValue({"currency": this.currentCurrency});
				}),
				takeUntil(this.isDestroyed$)
			)
			.subscribe();
		
		this.cartService.cart$
			.pipe(
				tap((cart: Cart) => {
					this.cart = cart;
					this.initForm();
				}),
				takeUntil(this.isDestroyed$)
			)
			.subscribe();
		
		this.profileService.profile$
			.pipe(
				tap((profile: Profile) => {
					this.profile = profile;
					if (this.profile) {
						this.initProfileData();
					}
				}),
				switchMap(() => this.profileService.loadProfileDelivery()),
				tap((profileDelivery: ProfileDelivery) => {
					this.profileDelivery = profileDelivery;
					if (this.profileDelivery) {
						this.initDeliveryData();
					}
				}),
				takeUntil(this.isDestroyed$)
			)
			.subscribe();
	}
	
	initForm() {
		this.checkoutForm = this.fb.group({
			status: new FormControl(OrderStatus.PENDING, [Validators.required]),
			currency: new FormControl(this.currentCurrency),
			
			uid: new FormControl({value: (this.profile && this.profile.ID) ? this.profile.ID : this.cartService.getTempUser(), disabled: true}, [Validators.required]),
			email: new FormControl((this.profile && this.profile.email) ? this.profile.email : null, [Validators.required, Validators.email]),
			
			phone: new FormControl((this.profileDelivery && this.profileDelivery.phone) ? this.profileDelivery.phone : null, [Validators.required]),
			firstname: new FormControl((this.profileDelivery && this.profileDelivery.firstname) ? this.profileDelivery.firstname : null, [Validators.required, Validators.minLength(1)]),
			lastname: new FormControl((this.profileDelivery && this.profileDelivery.lastname) ? this.profileDelivery.lastname : null, [Validators.required, Validators.minLength(1)]),
			country: new FormControl((this.profileDelivery && this.profileDelivery.country) ? this.profileDelivery.country : this.countries[0], [Validators.required]),
			ort: new FormControl((this.profileDelivery && this.profileDelivery.ort) ? this.profileDelivery.ort : null, [Validators.required, Validators.minLength(2)]),
			zip: new FormControl((this.profileDelivery && this.profileDelivery.zip) ? this.profileDelivery.zip : null, [Validators.required, Validators.minLength(2)]),
			address: new FormControl((this.profileDelivery && this.profileDelivery.address) ? this.profileDelivery.address : null, [Validators.required, Validators.minLength(2)]),
			
			company: new FormControl(null),
			warehouse: new FormControl(null),
			comment: new FormControl(""),
			
			delivery: new FormControl(null, [Validators.required]),
			payment: new FormControl(null, [Validators.required]),
			
			quantity: new FormControl({value: this.cart ? this.cart.quantity : 0, disabled: true}, [Validators.required]),
			subtotal: new FormControl({value: this.cart ? this.cart.subtotal : 0, disabled: true}, [Validators.required]),
			delivery_cost: new FormControl({value: (this.cart && this.cart.delivery_cost) ? this.cart.delivery_cost : 0, disabled: true}, [Validators.required]),
			extra_pay: new FormControl((this.cart && this.cart.extra_pay) ? this.cart.extra_pay : 0, [Validators.required]),
			couponId: new FormControl(null),
			total: new FormControl({value: this.cart ? this.cart.total : 0, disabled: true}, [Validators.required]),
			
			items: this.fb.array([])
		});
		
		if (this.cart && this.cart.items.length > 0) {
			this.cart.items.forEach((item: CartItem) => {
				this.setItem(item);
			});
		}
		
		this.checkoutForm.get("delivery").valueChanges
			.pipe(takeUntil(this.isDestroyed$))
			.subscribe((delivery: SelectOption) => {
				this.checkoutForm.patchValue({"delivery_cost": (delivery && delivery.valueSecond) ? delivery.valueSecond : 0});
			});
		
		this.checkoutForm.get("payment").valueChanges
			.pipe(takeUntil(this.isDestroyed$))
			.subscribe((payment: SelectOption) => {
				this.checkoutForm.patchValue({"extra_pay": (payment && payment.valueSecond) ? payment.valueSecond : 0});
			});
		
		this.checkoutForm.get("delivery_cost").valueChanges
			.pipe(takeUntil(this.isDestroyed$))
			.subscribe(() => this.calculateTotal());
		
		this.checkoutForm.get("extra_pay").valueChanges
			.pipe(takeUntil(this.isDestroyed$))
			.subscribe(() => this.calculateTotal());
	}
	
	setItem(productItem: CartItem) {
		const items: FormArray = this.checkoutForm.get("items") as FormArray;
		items.push(
			this.fb.group({
				ID: new FormControl(productItem.ID),
				comment: new FormControl(productItem.comment ? productItem.comment : ""),
				price: new FormControl(productItem.price ? productItem.price : 0, [Validators.required]),
				old_price: new FormControl(productItem.old_price ? productItem.old_price : 0, [Validators.required]),
				new_price: new FormControl(productItem.new_price, [Validators.required]),
				sku: new FormControl(productItem.sku, [Validators.required]),
				original_sku: new FormControl(productItem.original_sku ? productItem.original_sku : null),
				photo: new FormControl(productItem.photo ? productItem.photo : null),
				pid: new FormControl(productItem.pid ? productItem.pid : productItem.ID, [Validators.required]),
				pvid: new FormControl(null),
				quantity: new FormControl(productItem.quantity, [Validators.required]),
				title: new FormControl(productItem.title),
				total: new FormControl(productItem.total, [Validators.required])
			})
		);
	}
	
	getItems() {
		return <FormArray>this.checkoutForm.get("items");
	}
	
	calculateTotal() {
		const subtotal: number = this.checkoutForm.get("subtotal").value;
		const delivery_cost: number = this.checkoutForm.get("delivery_cost").value ? this.checkoutForm.get("delivery_cost").value : 0;
		const extra_pay: number = this.checkoutForm.get("extra_pay").value ? this.checkoutForm.get("extra_pay").value : 0;
		const total = (subtotal + delivery_cost + extra_pay) < 0 ? 0 : (subtotal + delivery_cost + extra_pay);
		this.checkoutForm.patchValue({"total": total});
	}
	
	initProfileData() {
		this.checkoutForm.patchValue({
			"uid": this.profile.ID,
			"email": this.profile.email
		});
	}
	
	initDeliveryData() {
		this.checkoutForm.patchValue({
			"firstname": this.profileDelivery.firstname,
			"lastname": this.profileDelivery.lastname,
			"country": this.profileDelivery.country,
			"ort": this.profileDelivery.ort,
			"zip": this.profileDelivery.zip,
			"address": this.profileDelivery.address
		});
	}
	
	countryFilter(event) {
		this.countries = this.countriesWithoutFilter;
		if (event && event !== "") {
			this.countries = this.countries.filter((item: SelectOption) => {
					return event.toLocaleLowerCase() === item.label.substring(0, event.length).toLocaleLowerCase();
				}
			);
		}
	}
	
	checkAvailableDelivery(delivery: SelectOption) {
		const currentDelivery = this.checkoutForm.get("delivery").value ? this.checkoutForm.get("delivery").value.value : false;
		if (currentDelivery && !this.countryService.availableDeliveryPayment(currentDelivery, this.checkoutForm.get("country").value.value)) {
			this.checkoutForm.get("delivery").reset();
		}
		return this.countryService.availableDeliveryPayment(delivery.value, this.checkoutForm.get("country").value.value);
	}
	
	checkAvailablePayment(payment: SelectOption) {
		const currentPayment = this.checkoutForm.get("payment").value ? this.checkoutForm.get("payment").value.value : false;
		if (currentPayment && !this.countryService.availableCountryPayment(currentPayment, this.checkoutForm.get("country").value.value)) {
			this.checkoutForm.get("payment").reset();
		}
		return this.countryService.availableCountryPayment(payment.value, this.checkoutForm.get("country").value.value);
	}
	
	makeOrder() {
		this.isUnvalid = false;
		if (this.checkoutForm.valid) {
			this.checkoutForm.patchValue({"currency": this.currentCurrency});
			this.orderService.addOrder(this.checkoutForm.getRawValue())
				.pipe(
					switchMap((id: number) => this.cartService.loadCart(), (id: number, cart: boolean) => id),
					tap((id: number) => {
						if (id) {
							this.router.navigate(["/checkout/confirm", id]);
						}
					}),
					takeUntil(this.isDestroyed$)
				)
				.subscribe();
		} else {
			this.isUnvalid = true;
		}
	}
	
	openSignInModal() {
		this.signInUpService.openModal(SignInUp.LOGIN);
	}
	
	managerCalculate(delivery: SelectOption) {
		return DeliveryType.getCalculateMethod(delivery.value);
	}
	
	ngOnDestroy(): void {
		this.isDestroyed$.next();
		this.isDestroyed$.complete();
	}
}
