import {Component, OnDestroy, OnInit} from "@angular/core";
import {LoggerService} from "../../_core/services/logger.service";
import {Subject} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {takeUntil, tap} from "rxjs/operators";
import {Order} from "../../_core/models/dto/order/order.model";
import {OrderStatus} from "../../_core/models/dto/order/order-status.enum";
import {Currency} from "../../_core/models/enum/currency.enum";
import {CurrencyService} from "../../_core/services/currency.service";
import {Payment} from "../../_core/models/dto/payment/payment.enum";
import {PaymentService} from "../../_core/services/payment.service";
import {PaymentRequest} from "../../_core/models/dto/payment/payment-request.model";
import {PaymentStatus} from "../../_core/models/dto/payment/payment-status.enum";
import {PaymentInfo} from "../../_core/models/dto/payment/payment-info.model";

@Component({
	selector: "app-checkout-confirm",
	template: `
		<div class="container">
			<div class="box">
				<div class="checkout">
					<h1 class="text-center" i18n="@@checkoutConfirmPageTitleThankYouOrderPlaced">Order data</h1>
					<!--          <h3 i18n="@@checkoutConfirmPageSubtitleOrderNumber">Order number: <span class="text-danger">{{order.ID}}</span></h3>-->
					<h3 i18n="@@checkoutConfirmPageSubtitleStatus">Status order: <span [ngClass]="orderStatus.getCssClass(order.status)"> {{orderStatus.getDisplayName(order.status)}}</span></h3>
					<div class="form-columns form-columns--top">
						<div class="form-line">
							<table class="order-table">
								<thead>
								<tr>
									<th colspan="2" i18n="Other table on profile order page@@tableOrderOtherHeaderShipingBilling">Shipping/Billing address</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<th class="text-right" i18n="Other table on profile order page@@tableOrderOtherHeaderNumber">Name</th>
									<td>{{order.firstname}} {{order.lastname}}</td>
								</tr>
								<tr>
									<th class="text-right" i18n="Other table on profile order page@@tableOrderOtherHeaderAddress">Address</th>
									<td>{{order.address}}</td>
								</tr>
								<tr>
									<th class="text-right" i18n="Other table on profile order page@@tableOrderOtherHeaderZip">ZIP, Ort</th>
									<td>{{order.zip}} {{order.ort}}</td>
								</tr>
								<tr>
									<th class="text-right" i18n="Other table on profile order page@@tableOrderOtherHeaderCountry">Country</th>
									<td>{{order.country.label}}</td>
								</tr>
								<tr *ngIf="order.warehouse">
									<th class="text-right" i18n="Other table on profile order page@@tableOrderOtherHeaderWarehouse">Warehouse</th>
									<td>{{order.warehouse}}</td>
								</tr>
								<tr>
									<th class="text-right" i18n="Other table on profile order page@@tableOrderOtherHeaderPhone">Phone</th>
									<td>{{order.phone}}</td>
								</tr>
								<tr>
									<th class="text-right" i18n="Other table on profile order page@@tableOrderOtherHeaderEmail">E-mail</th>
									<td>{{order.email}}</td>
								</tr>
								<tr *ngIf="order.comment">
									<th class="text-right" i18n="Other table on profile order page@@tableOrderOtherHeaderComment">Comment</th>
									<td>{{order.comment}}</td>
								</tr>
								</tbody>
							</table>
						</div>
						<div class="form-line">
							<table>
								<thead>
								<tr>
									<th colspan="2" i18n="Other table on profile order page@@tableOrderAmountToPay:">Order amount to pay:</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<th class="text-right" i18n="Other table on profile order page@@tableOrderOtherHeaderSubtotal">Subtotal</th>
									<td class="text-center">{{currencyService.currencyExchange(order.subtotal) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}</td>
								</tr>
								<tr>
									<th class="text-right">
										<span i18n="Other table on profile order page@@tableOrderOtherHeaderDelivery">Delivery</span>
										<div class="text-small">{{order.delivery.label}}</div>
									</th>
									<td class="text-center" *ngIf="order.status !== orderStatus.PENDING && order.status !== orderStatus.ON_HOLD">{{currencyService.currencyExchange(order.delivery_cost) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}</td>
									<td class="text-center text-danger" *ngIf="order.status === orderStatus.PENDING || order.status === orderStatus.ON_HOLD" i18n="@@tableOrderOtherDeliveryCost">Is calculated</td>
								</tr>
								<tr>
									<th class="text-right">
										<span i18n="Other table on profile order page@@tableOrderOtherHeaderPayment">Payment</span>
										<div class="text-small">{{order.payment.label}}</div>
									</th>
									<td class="text-center">{{currencyService.currencyExchange(order.extra_pay) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}</td>
								</tr>
								<tr>
									<th class="text-right" i18n="Other table on profile order page@@tableOrderOtherHeaderTotal">Total</th>
									<td class="text-center">{{currencyService.currencyExchange(order.total) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}</td>
								</tr>
								<tr *ngIf="paymentInfo">
									<th class="text-right" i18n="@@checkoutConfirmPagePaymentStatus">Payment status:</th>
									<td class="text-center">
										<div class="text-uppercase" [ngClass]="paymentStatus.getCssClass(paymentInfo.status)">{{paymentStatus.getDisplayName(paymentInfo.status)}}</div>
										<div class="text-small">{{paymentInfo.amount | currency:currentCurrency:currency.getSymbol(paymentInfo.currency):currency.getFormat(paymentInfo.currency)}}</div>
									</td>
								</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="text-center" *ngIf="!paymentInfo">
						<button type="button" class="button button--danger button--inline" (click)="confirmOrder()">
							<ng-container *ngIf="order.payment.value !== payment.PRIVAT_BANK && order.payment.value !== payment.VISA_MASTERCARD" i18n="@@checkoutConfirmPageButtonConfirmOrderBill">CONFIRM your order and get a BILL</ng-container>
							<ng-container *ngIf="order.payment.value === payment.PRIVAT_BANK || order.payment.value === payment.VISA_MASTERCARD" i18n="@@checkoutConfirmPageButtonConfirmOrderOnline">CONFIRM your order and get link for PAY ORDER ONLINE</ng-container>
						</button>
					</div>
					<div class="text-center" *ngIf="paymentInfo">
						This will be a button to pay online or pay details via Bank or nothing if selected pay via cash
					</div>
				</div>
			</div>
		</div>
	`
})
export class CheckoutConfirmComponent implements OnInit, OnDestroy {
	
	order: Order;
	paymentInfo: PaymentInfo;
	orderStatus = OrderStatus;
	currentCurrency: Currency;
	currency = Currency;
	payment = Payment;
	paymentStatus = PaymentStatus;
	isDestroyed$: Subject<void> = new Subject();
	
	constructor(private logger: LoggerService,
	            private route: ActivatedRoute,
	            private router: Router,
	            private paymentService: PaymentService,
	            public currencyService: CurrencyService) {
		this.logger.debug(this.constructor.name + " is created.");
	}
	
	ngOnInit(): void {
		this.route.data
		.pipe(
			tap((data: { order: Order, paymentInfo: PaymentInfo }) => {
				this.order = data.order;
				this.paymentInfo = data.paymentInfo;
			}),
			takeUntil(this.isDestroyed$)
		)
		.subscribe();
		
		this.currencyService.currentCurrency$
		.pipe(
			tap((currency: Currency) => {
				this.currentCurrency = currency;
			}),
			takeUntil(this.isDestroyed$)
		)
		.subscribe();
	}
	
	confirmOrder() {
		switch (this.order.payment.value) {
			case Payment.PRIVAT_BANK:
				// this.privatbankSystem();
				this.invoiceSystem();
				break;
			case Payment.VISA_MASTERCARD:
				// this.visamastercardSystem();
				this.invoiceSystem();
				break;
			default:
				this.invoiceSystem();
		}
	}
	
	privatbankSystem() {
	
	}
	
	visamastercardSystem() {
	
	}
	
	invoiceSystem() {
		const request: PaymentRequest = new PaymentRequest();
		request.orderId = this.order.ID;
		request.type = Payment.INVOICE;
		request.status = PaymentStatus.WAITING;
		request.uid = this.order.uid;
		request.currency = this.currentCurrency;
		request.amount = this.currencyService.currencyExchange(this.order.total);
		request.systemId = null;
		request.data = this.order;
		this.paymentService.createBillPayment(request)
		.pipe(
			tap((id) => {
				if (id) {
					this.router.navigateByUrl("/thank-you", {state: {paymentId: id, ty: true, orderId: this.order.ID}});
				}
			}),
			takeUntil(this.isDestroyed$)
		)
		.subscribe();
	}
	
	ngOnDestroy(): void {
		this.isDestroyed$.next();
		this.isDestroyed$.complete();
	}
}
