import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve} from "@angular/router";
import {EMPTY, Observable} from "rxjs";
import {catchError, map} from "rxjs/operators";
import {HttpCachingClient} from "../../_core/services/http-caching-client.service";
import {ExceptionService} from "../../_core/services/exception.service";
import {LoggerService} from "../../_core/services/logger.service";
import {CONFIG} from "../../_core/config";
import {HttpParams} from "@angular/common/http";
import {SelectOption} from "../../_shared/components/select/select-option.enum";
import {DeliveryType} from "../../_core/models/enum/delivery-type.enum";
import {ShopBackendClient} from "../../_core/services/shop/shop-backend.service";
import {ShopSetting} from "../../_core/models/dto/shop/shop-setting.model";

@Injectable()
export class CheckoutDeliveryResolver implements Resolve<SelectOption[]> {
    
    constructor(private logger: LoggerService,
                private exceptionService: ExceptionService,
                private httpClient: HttpCachingClient,
                private shopBackendClient: ShopBackendClient) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    resolve(route: ActivatedRouteSnapshot): Observable<SelectOption[]> {
        let keysString: string;
        DeliveryType.values().forEach((type: DeliveryType) => {
            keysString = keysString ? keysString + ",'" + type + "_cost'" : "'" + type + "_cost'";
        });
        let params: HttpParams = new HttpParams();
        if (keysString) {
            params = params.set("settings_name", keysString);
        }
        return this.httpClient.get(CONFIG.apiUrl + this.shopBackendClient.SHOP_SETTINGS_URL, params)
        .pipe(
            map((settings: ShopSetting[]) => {
                return DeliveryType.values().map<SelectOption>((type: DeliveryType) => {
                    let valueDelivery: any = settings.find((setting: ShopSetting) => setting.setting === type + "_cost") ? settings.find((setting: ShopSetting) => setting.setting === type + "_cost").value : 0;
                    valueDelivery          = parseFloat(valueDelivery) >= 0 ? parseFloat(valueDelivery) : 0;
                    return {label: DeliveryType.getDisplayName(type), value: type, labelSecond: DeliveryType.getDescription(type), valueSecond: valueDelivery};
                });
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            })
        );
    }
}
