import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, Router} from "@angular/router";
import {EMPTY, Observable} from "rxjs";
import {catchError} from "rxjs/operators";
import {HttpCachingClient} from "../../_core/services/http-caching-client.service";
import {ExceptionService} from "../../_core/services/exception.service";
import {LoggerService} from "../../_core/services/logger.service";
import {CONFIG} from "../../_core/config";
import {Order} from "../../_core/models/dto/order/order.model";
import {OrdersBackendClient} from "../../_core/services/order/orders-backend.service";
import {CartService} from "../../_core/services/cart.service";
import {HttpParams} from "@angular/common/http";

@Injectable()
export class CheckoutConfirmOrderResolver implements Resolve<Order> {
  
  constructor(private logger: LoggerService,
              private exceptionService: ExceptionService,
              private httpClient: HttpCachingClient,
              private ordersBackendClient: OrdersBackendClient,
              private cartService: CartService,
              private router: Router) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  resolve(route: ActivatedRouteSnapshot): Observable<Order> {
    const id = route.params["id"];
    let params = new HttpParams();
    const uid = this.cartService.getTempUser();
    if (uid) params = params.set("uid", uid.toString());
    if (id) {
      return this.httpClient.get<Order>(CONFIG.apiUrl + this.ordersBackendClient.ORDER_URL.replace("{id}", id.toString()), params)
      .pipe(
        catchError(errorInfo => {
          this.exceptionService.handleRequestErrorInfo(errorInfo.error);
          this.router.navigate(["/404"]);
          return EMPTY;
        })
      );
    } else {
      this.router.navigate(["/404"]);
    }
  }
}
