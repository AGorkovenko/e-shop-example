import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, Router} from "@angular/router";
import {EMPTY, Observable} from "rxjs";
import {catchError, tap} from "rxjs/operators";
import {HttpCachingClient} from "../../_core/services/http-caching-client.service";
import {ExceptionService} from "../../_core/services/exception.service";
import {Cart} from "../../_core/models/dto/cart/cart.model";
import {LoggerService} from "../../_core/services/logger.service";
import {CartService} from "../../_core/services/cart.service";
import {CONFIG} from "../../_core/config";
import {HttpParams} from "@angular/common/http";

@Injectable()
export class CheckoutCartResolver implements Resolve<Cart> {
	
	constructor(private logger: LoggerService,
	            private exceptionService: ExceptionService,
	            private httpClient: HttpCachingClient,
	            private cartService: CartService,
	            private router: Router) {
		this.logger.debug(this.constructor.name + " is created.");
	}
	
	resolve(route: ActivatedRouteSnapshot): Observable<Cart> {
		let params = new HttpParams();
		const uid = this.cartService.getTempUser();
		if (uid) params = params.set("uid", uid.toString());
		return this.httpClient.get<Cart>(CONFIG.apiUrl + this.cartService.CART_CR_URL, params)
		.pipe(
			tap((cart: Cart) => {
				if(cart.items && cart.items.length <= 0 ) {
					this.router.navigate(["/shop"]);
				}
			}),
			catchError(errorInfo => {
				this.exceptionService.handleRequestErrorInfo(errorInfo.error);
				this.router.navigate(["/shop"]);
				return EMPTY;
			})
		);
	}
}
