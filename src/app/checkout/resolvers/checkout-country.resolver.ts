import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve} from "@angular/router";
import {Observable} from "rxjs";
import {HttpCachingClient} from "../../_core/services/http-caching-client.service";
import {ExceptionService} from "../../_core/services/exception.service";
import {LoggerService} from "../../_core/services/logger.service";
import {CountryService} from "../../_core/services/country.service";
import {SelectOption} from "../../_shared/components/select/select-option.enum";

@Injectable()
export class CheckoutCountryResolver implements Resolve<SelectOption[]> {
	
	constructor(private logger: LoggerService,
	            private exceptionService: ExceptionService,
	            private httpClient: HttpCachingClient,
	            private countryService: CountryService) {
		this.logger.debug(this.constructor.name + " is created.");
	}
	
	resolve(route: ActivatedRouteSnapshot): Observable<SelectOption[]> {
		return this.countryService.getCountryOptions();
	}
}
