import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve} from "@angular/router";
import {EMPTY, Observable} from "rxjs";
import {catchError} from "rxjs/operators";
import {HttpCachingClient} from "../../_core/services/http-caching-client.service";
import {ExceptionService} from "../../_core/services/exception.service";
import {LoggerService} from "../../_core/services/logger.service";
import {CONFIG} from "../../_core/config";
import {CartService} from "../../_core/services/cart.service";
import {HttpParams} from "@angular/common/http";
import {PaymentService} from "../../_core/services/payment.service";
import {PaymentInfo} from "../../_core/models/dto/payment/payment-info.model";

@Injectable()
export class CheckoutConfirmPaymentResolver implements Resolve<PaymentInfo> {
  
  constructor(private logger: LoggerService,
              private exceptionService: ExceptionService,
              private httpClient: HttpCachingClient,
              private paymentService: PaymentService,
              private cartService: CartService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  resolve(route: ActivatedRouteSnapshot): Observable<PaymentInfo> {
    const id = route.params["id"];
    let params = new HttpParams();
    const uid = this.cartService.getTempUser();
    if (id) params = params.set("orderId", id.toString());
    if (uid) params = params.set("uid", uid.toString());
    return this.httpClient.get<PaymentInfo>(CONFIG.apiUrl + this.paymentService.PAYMENT_URL, params)
    .pipe(
      catchError(errorInfo => {
        return EMPTY;
      })
    );
  }
}
