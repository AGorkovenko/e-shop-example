import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve} from "@angular/router";
import {EMPTY, Observable} from "rxjs";
import {catchError, map} from "rxjs/operators";
import {HttpCachingClient} from "../../_core/services/http-caching-client.service";
import {ExceptionService} from "../../_core/services/exception.service";
import {LoggerService} from "../../_core/services/logger.service";
import {CONFIG} from "../../_core/config";
import {HttpParams} from "@angular/common/http";
import {SelectOption} from "../../_shared/components/select/select-option.enum";
import {ShopBackendClient} from "../../_core/services/shop/shop-backend.service";
import {ShopSetting} from "../../_core/models/dto/shop/shop-setting.model";
import {Payment} from "../../_core/models/dto/payment/payment.enum";

@Injectable()
export class CheckoutPaymentResolver implements Resolve<SelectOption[]> {
    
    constructor(private logger: LoggerService,
                private exceptionService: ExceptionService,
                private httpClient: HttpCachingClient,
                private shopBackendClient: ShopBackendClient) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    resolve(route: ActivatedRouteSnapshot): Observable<SelectOption[]> {
        let keysString: string;
        Payment.values().forEach((type: Payment) => {
            keysString = keysString ? keysString + ",'" + type + "_cost'" : "'" + type + "_cost'";
        });
        let params: HttpParams = new HttpParams();
        if (keysString) {
            params = params.set("settings_name", keysString);
        }
        return this.httpClient.get(CONFIG.apiUrl + this.shopBackendClient.SHOP_SETTINGS_URL, params)
        .pipe(
            map((settings: ShopSetting[]) => {
                return Payment.values().map<SelectOption>((type: Payment) => {
                    let valuePayment: any = settings.find((setting: ShopSetting) => setting.setting === type + "_cost") ? settings.find((setting: ShopSetting) => setting.setting === type + "_cost").value : 0;
                    valuePayment          = parseFloat(valuePayment) >= 0 ? parseFloat(valuePayment) : 0;
                    return {label: Payment.getDisplayName(type), value: type, labelSecond: Payment.getDescription(type), valueSecond: valuePayment};
                });
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            })
        );
    }
}
