import {NgModule} from "@angular/core";
import {SharedModule} from "../_shared/shared.module";
import {CheckoutRoutingModule} from "./checkout-routing.module";
import {CheckoutComponent} from "./components/checkout.component";
import {CheckoutConfirmComponent} from "./components/checkout-confirm.component";

@NgModule({
  imports: [
    SharedModule,
    CheckoutRoutingModule
  ],
  declarations: [
    CheckoutComponent,
    CheckoutConfirmComponent
  ]
})
export class CheckoutModule {
}
