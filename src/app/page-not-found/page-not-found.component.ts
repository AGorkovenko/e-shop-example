import {Component, OnInit} from "@angular/core";

@Component({
    selector: "app-page-not-found",
    template: `
        <div class="container">
            <div class="page page--full page--404">
                <div class="page__content">
                    <div class="error-404">
                        <h1 i18n="Title for 404 page@@error404title">Error 404</h1>
                        <div i18n="Description for 404 page@@error404description" class="error-404__description">Uh oh, we can’t seem to find the page you’re looking for.<br>Try going back to the previous page.</div>
                        <button i18n="Back button on 404 page@@error404button" class="button button--uppercase button--danger button--inline">Back to home</button>
                    </div>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["./page-not-found.component.scss"]
})
export class PageNotFoundComponent implements OnInit {
    
    constructor() {
    }
    
    ngOnInit() {
    }
    
}
