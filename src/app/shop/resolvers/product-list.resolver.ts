import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Params, Resolve} from "@angular/router";
import {EMPTY, forkJoin, Observable, of} from "rxjs";
import {catchError, map, switchMap, tap} from "rxjs/operators";
import {LoggerService} from "../../_core/services/logger.service";
import {ExceptionService} from "../../_core/services/exception.service";
import {ProductsBackendClient} from "../../_core/services/product/products-backend.service";
import {Product} from "../../_core/models/dto/product/product.model";
import {ProductListData} from "../../_core/services/product/products.service";
import {ProductViewRequest} from "../../_core/models/dto/product/product-view-request.model";
import {ProductsCategoriesBackendClient} from "../../_core/services/product/products-categories-backend.service";
import {ProductCategory} from "../../_core/models/dto/product/product-category.model";

@Injectable()
export class ProductListResolver implements Resolve<ProductListData> {
    
    constructor(private logger: LoggerService,
                private exceptionService: ExceptionService,
                private productsBackendClient: ProductsBackendClient,
                private productsCategoriesBackendClient: ProductsCategoriesBackendClient) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    resolve(route: ActivatedRouteSnapshot): Observable<ProductListData> {
        const categorySlug: string             = route.params["slug"];
        const viewRequest: ProductViewRequest  = ProductListResolver.paramsToViewRequest(route.queryParams);
        const productListData: ProductListData = new ProductListData();
        
        if (categorySlug) {
            return this.productsCategoriesBackendClient.loadCategoryBySlug(categorySlug)
            .pipe(
                catchError(errorInfo => {
                    this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                    return EMPTY;
                }),
                switchMap((category: ProductCategory) => {
                    viewRequest.category       = category.ID;
                    productListData.categories = category.children ? category.children : [];
                    if (productListData.categories.length) {
                        return of(null);
                    } else {
                        return forkJoin(
                            this.productsBackendClient.loadProducts(viewRequest),
                            this.productsBackendClient.loadCount(viewRequest),
                            this.productsBackendClient.loadPrices(viewRequest)
                        );
                    }
                }),
                map((data: [Product[], number, { min: number, max: number }]) => {
                    if (data) {
                        viewRequest.minPrice        = viewRequest.minPrice && viewRequest.minPrice <= data[2].min ? data[2].min : viewRequest.minPrice;
                        viewRequest.maxPrice        = viewRequest.maxPrice && viewRequest.maxPrice >= data[2].max ? data[2].max : viewRequest.maxPrice;
                        productListData.list        = data[0];
                        productListData.count       = data[1];
                        productListData.viewRequest = viewRequest;
                        productListData.minValue    = data[2].min;
                        productListData.maxValue    = data[2].max;
                    }
                    return productListData;
                })
            );
        } else {
            return this.productsCategoriesBackendClient.loadCategories()
            .pipe(
                catchError(errorInfo => {
                    this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                    return EMPTY;
                }),
                switchMap((categories: ProductCategory[]) => {
                    productListData.categories = categories ? categories : [];
                    if (productListData.categories.length) {
                        return of(null);
                    } else {
                        return forkJoin(
                            this.productsBackendClient.loadProducts(viewRequest),
                            this.productsBackendClient.loadCount(viewRequest),
                            this.productsBackendClient.loadPrices(viewRequest)
                        );
                    }
                }),
                map((data: [Product[], number, { min: number, max: number }]) => {
                    if (data) {
                        viewRequest.minPrice        = viewRequest.minPrice && viewRequest.minPrice <= data[2].min ? data[2].min : viewRequest.minPrice;
                        viewRequest.maxPrice        = viewRequest.maxPrice && viewRequest.maxPrice >= data[2].max ? data[2].max : viewRequest.maxPrice;
                        productListData.list        = data[0];
                        productListData.count       = data[1];
                        productListData.viewRequest = viewRequest;
                        productListData.minValue    = data[2].min;
                        productListData.maxValue    = data[2].max;
                    }
                    return productListData;
                })
            );
        }
    }
    
    private static paramsToViewRequest(queryParams: Params): ProductViewRequest {
        const viewRequest: ProductViewRequest = new ProductViewRequest();
        viewRequest.order                     = queryParams["order"] ? queryParams["order"] : null;
        viewRequest.sort                      = queryParams["sort"] ? queryParams["sort"] : null;
        if (queryParams["page"]) {
            viewRequest.page = parseInt(queryParams["page"]);
        }
        if (queryParams["q"]) {
            viewRequest.q = queryParams["q"];
        }
        if (queryParams["minPrice"]) {
            viewRequest.minPrice = parseFloat(queryParams["minPrice"]);
        }
        if (queryParams["maxPrice"]) {
            viewRequest.maxPrice = parseFloat(queryParams["maxPrice"]);
        }
        // if (queryParams["category"]) {
        //     viewRequest.category = queryParams["category"];
        // }
        return viewRequest;
    }
}
