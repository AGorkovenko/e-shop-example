import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, Router} from "@angular/router";
import {LoggerService} from "../../_core/services/logger.service";
import {ExceptionService} from "../../_core/services/exception.service";
import {EMPTY, Observable} from "rxjs";
import {catchError, tap} from "rxjs/operators";
import {ProductsBackendClient} from "../../_core/services/product/products-backend.service";
import {Product} from "../../_core/models/dto/product/product.model";

@Injectable()
export class ProductResolver implements Resolve<Product> {
  
  constructor(private logger: LoggerService,
              private exceptionService: ExceptionService,
              private productsBackendClient: ProductsBackendClient,
              private router: Router) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  resolve(route: ActivatedRouteSnapshot): Observable<Product> {
    const slug: string = route.params["slug"];
    if (slug) {
      return this.productsBackendClient.loadProductBySlug(slug)
      .pipe(
        catchError(errorInfo => {
          this.exceptionService.handleRequestErrorInfo(errorInfo.error);
          this.router.navigate(["/shop"]);
          return EMPTY;
        })
      );
    } else {
      this.router.navigate(["/shop"]);
    }
  }
  
}
