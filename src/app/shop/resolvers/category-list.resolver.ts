import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve} from "@angular/router";
import {LoggerService} from "../../_core/services/logger.service";
import {ExceptionService} from "../../_core/services/exception.service";
import {ProductsCategoriesBackendClient} from "../../_core/services/product/products-categories-backend.service";
import {EMPTY, Observable} from "rxjs";
import {ProductCategory} from "../../_core/models/dto/product/product-category.model";
import {catchError} from "rxjs/operators";

@Injectable()
export class CategoryListResolver implements Resolve<ProductCategory[]> {
  
  constructor(private logger: LoggerService,
              private exceptionService: ExceptionService,
              private productsCategoriesBackendClient: ProductsCategoriesBackendClient) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  resolve(route: ActivatedRouteSnapshot): Observable<ProductCategory[]> {
    return this.productsCategoriesBackendClient.loadCategories()
    .pipe(
      catchError(errorInfo => {
        this.exceptionService.handleRequestErrorInfo(errorInfo.error);
        return EMPTY;
      })
    );
  }
  
}
