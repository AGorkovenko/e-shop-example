import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, Router} from "@angular/router";
import {LoggerService} from "../../_core/services/logger.service";
import {ExceptionService} from "../../_core/services/exception.service";
import {ProductsCategoriesBackendClient} from "../../_core/services/product/products-categories-backend.service";
import {EMPTY, Observable} from "rxjs";
import {ProductCategory} from "../../_core/models/dto/product/product-category.model";
import {catchError, map, tap} from "rxjs/operators";

@Injectable()
export class CategoryListResolver implements Resolve<ProductCategory> {
  
  constructor(private logger: LoggerService,
              private exceptionService: ExceptionService,
              private productsCategoriesBackendClient: ProductsCategoriesBackendClient,
              private router: Router) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  resolve(route: ActivatedRouteSnapshot): Observable<ProductCategory> {
    const categorySlug: string = route.params["slug"];
    if (categorySlug) {
      return this.productsCategoriesBackendClient.loadCategoryBySlug(categorySlug)
      .pipe(
        catchError(errorInfo => {
          this.exceptionService.handleRequestErrorInfo(errorInfo.error);
          this.router.navigate(["/shop"]);
          return EMPTY;
        }),
        tap((category: ProductCategory) => {
          this.logger.debug("Loaded children of category", category);
          return category;
        })
      );
    } else {
      return this.productsCategoriesBackendClient.loadCategories()
      .pipe(
        catchError(errorInfo => {
          this.exceptionService.handleRequestErrorInfo(errorInfo.error);
          return EMPTY;
        }),
        map((categories: ProductCategory[]) => {
          const category: ProductCategory = new ProductCategory();
          category.children = categories;
          return category;
        })
      );
    }
  }
  
}
