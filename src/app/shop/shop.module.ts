import {NgModule} from "@angular/core";
import {ShopRoutingModule} from "./shop-routing.module";
import {SharedModule} from "../_shared/shared.module";
import {ShopComponent} from "./components/shop.component";
import {CategoryListComponent} from "./components/categories/category-list.component";
import {CategoryPanelComponent} from "./components/categories/category-panel.component";
import {ProductListComponent} from "./components/products/product-list.component";
import {ProductsSortComponent} from "./components/products/products-sort.conponent";
import {ProductPanelComponent} from "./components/products/product-panel.component";
import {ShopSidebarComponent} from "./components/sidebar/shop-sidebar.component";
import {ShopFilterCategoriesComponent} from "./components/sidebar/shop-filter-categories.component";
import {ShopFilterCategoriesItemComponent} from "./components/sidebar/shop-filter-categories-item.component";
import {ShopFilterPriceComponent} from "./components/sidebar/shop-filter-price.component";
import {ProductComponent} from "./components/product/product.component";
import {ProductGalleryComponent} from "./components/product/product-gallery.component";
import {ProductPriceComponent} from "./components/product/product-price.component";
import {ProductStockComponent} from "./components/product/product-stock.component";
import {ProductSkuComponent} from "./components/product/product-sku.component";
import {ProductBuyButtonComponent} from "./components/product/product-buy-button.component";
import {ProductVariationComponent} from "./components/product/product-variation.component";
import {ProductAddsComponent} from "./components/product/product-adds.component";
import {ProductShareComponent} from "./components/product/product-share.component";
import {ProductTabsComponent} from "./components/product/product-tabs.component";
import {ProductReviewsComponent} from "./components/product/product-reviews.component";

@NgModule({
  declarations: [
    ShopComponent,
    ProductListComponent,
    CategoryListComponent,
    CategoryPanelComponent,
    ProductPanelComponent,
    ShopSidebarComponent,
    ProductsSortComponent,
    ShopFilterCategoriesComponent,
    ShopFilterCategoriesItemComponent,
    ShopFilterPriceComponent,
    ProductComponent,
    ProductGalleryComponent,
    ProductPriceComponent,
    ProductStockComponent,
    ProductSkuComponent,
    ProductBuyButtonComponent,
    ProductVariationComponent,
    ProductAddsComponent,
    ProductShareComponent,
    ProductTabsComponent,
    ProductReviewsComponent
  ],
  imports: [
    SharedModule,
    ShopRoutingModule
  ]
})
export class ShopModule {
}
