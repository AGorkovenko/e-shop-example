import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {ProductListResolver} from "./resolvers/product-list.resolver";
import {ProductComponent} from "./components/product/product.component";
import {CategoryListResolver} from "./resolvers/category-list.resolver";
import {ShopComponent} from "./components/shop.component";
import {ProductResolver} from "./resolvers/product.resolver";

const routes: Routes = [
  {
    path: "",
    component: ShopComponent,
    data: {
      title: "Shop"
    },
    resolve: {
      productListData: ProductListResolver
    },
    runGuardsAndResolvers: "paramsOrQueryParamsChange",
  },
  {
    path: "product/:slug",
    component: ProductComponent,
    resolve: {
      product: ProductResolver
    },
    data: {
      title: "Product"
    }
  },
  {
    path: ":slug",
    loadChildren: "./shop.module#ShopModule",
    runGuardsAndResolvers: "paramsOrQueryParamsChange"
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    ProductListResolver,
    CategoryListResolver,
    ProductResolver
  ]
})
export class ShopRoutingModule {
}
