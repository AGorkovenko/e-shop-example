import {Component, OnDestroy, OnInit} from "@angular/core";
import {LoggerService} from "../../_core/services/logger.service";
import {ProductListData, ProductsService} from "../../_core/services/product/products.service";
import {Observable, Subject} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {takeUntil, tap} from "rxjs/operators";
import {ProductCategory} from "../../_core/models/dto/product/product-category.model";

@Component({
  selector: "app-shop",
  template: `
    <div class="container">
      <div class="catalog">
        <div class="catalog__sidebar">
          
          <app-shop-sidebar></app-shop-sidebar>
        
        </div>
        <div class="catalog__content" *ngIf="productListData$ | async as productListData">
          
          <div *ngIf="productListData.categories.length">
            <div class="catalog-head">
              <div class="catalog-head__category">
                <h2 i18n="Head for categories@@shopHeadCategories">Categories</h2>
              </div>
            </div>
            <app-category-list [categories]="productListData.categories"></app-category-list>
          </div>
          
          <div *ngIf="!productListData.categories.length">
            <app-product-list [productListData]="productListData"></app-product-list>
          </div>
        
        </div>
      </div>
    </div>
  `
})
export class ShopComponent implements OnInit, OnDestroy {
  
  productListData$: Observable<ProductListData>;
  category: ProductCategory;
  
  isDestroyed$: Subject<void> = new Subject();
  
  constructor(private logger: LoggerService,
              private route: ActivatedRoute,
              private productsService: ProductsService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  ngOnInit() {
    this.productListData$ = this.productsService.productListData$;
    
    this.route.data
    .pipe(
      tap((data: { productListData: ProductListData }) => {
        this.productsService.initProductList(data.productListData);
      }),
      takeUntil(this.isDestroyed$)
    )
    .subscribe();
  }
  
  ngOnDestroy(): void {
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }
}
