import {Component, OnInit} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";

@Component({
  selector: "app-shop-sidebar",
  template: `
    <div class="sidebar">
      <app-shop-filter-categories></app-shop-filter-categories>
      
      <app-shop-filter-price></app-shop-filter-price>
      
      <!--    <div class="filter">-->
      <!--      <div class="filter__name">Parts</div>-->
      <!--      <div class="filter__block">-->
      <!--        <div class="filter-links">-->
      <!--          <div class="filter-link"><a href="">Accessories</a></div>-->
      <!--        </div>-->
      <!--        <div class="filter-links">-->
      <!--          <div class="filter-link"><a href="">Brakes</a></div>-->
      <!--        </div>-->
      <!--        <div class="filter-links">-->
      <!--          <div class="filter-link"><a href="">Body</a></div>-->
      <!--        </div>-->
      <!--        <div class="filter-links">-->
      <!--          <div class="filter-link"><a href="">Lights</a></div>-->
      <!--        </div>-->
      <!--      </div>-->
      <!--    </div>-->
      
      <!--    <div class="filter">-->
      <!--      <div class="filter__name">Types</div>-->
      <!--      <div class="filter__block">-->
      <!--        <div class="filter__checkbox">-->
      <!--          <div class="checkbox">-->
      <!--            <input type="checkbox">-->
      <!--            <label>4 Wheel Drive</label>-->
      <!--          </div>-->
      <!--          <div class="checkbox checkbox&#45;&#45;checked">-->
      <!--            <input type="checkbox">-->
      <!--            <label>Coupe</label>-->
      <!--          </div>-->
      <!--          <div class="checkbox">-->
      <!--            <input type="checkbox">-->
      <!--            <label>Hatchback</label>-->
      <!--          </div>-->
      <!--          <div class="checkbox">-->
      <!--            <input type="checkbox">-->
      <!--            <label>Sedan</label>-->
      <!--          </div>-->
      <!--        </div>-->
      <!--      </div>-->
      <!--    </div>-->
      
      <!--    <div class="filter">-->
      <!--      <div class="filter__name">Brand</div>-->
      <!--      <div class="filter__block">-->
      <!--        <div class="checkbox">-->
      <!--          <input type="checkbox">-->
      <!--          <label>-->
      <!--            <img align="" src="">-->
      <!--          </label>-->
      <!--        </div>-->
      <!--      </div>-->
      <!--    </div>-->
    </div>
  `
})
export class ShopSidebarComponent implements OnInit {
  
  constructor(private logger: LoggerService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  ngOnInit() {
  }
}
