import {Component, OnDestroy, OnInit} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";
import {ProductsCategoriesService} from "../../../_core/services/product/products-categories.service";
import {ProductCategory} from "../../../_core/models/dto/product/product-category.model";
import {takeUntil, tap} from "rxjs/operators";
import {Subject} from "rxjs";

@Component({
    selector: "app-shop-filter-categories",
    template: `
        <div class="filter" *ngIf="categories">
            <div class="filter__name" i18n="Filter in shop sidebar@@filterSidebarTitleCategories">Categories</div>
            <div class="filter__block">

                <div class="filter-links">
                    <div class="filter-link" *ngFor="let category of categories" [ngClass]="{'filter-link--parent': category.children.length}" routerLinkActive="filter-link--active">
                        <a [routerLink]="['/shop', category.slug]">{{category.name}}</a>
                        <app-shop-categories-item [items]="category.children" [parent]="category.slug" *ngIf="category.children.length"></app-shop-categories-item>
                    </div>
                </div>

            </div>
        </div>
    `
})
export class ShopFilterCategoriesComponent implements OnInit, OnDestroy {
    
    categories: ProductCategory[];
    
    isDestroyed$: Subject<void> = new Subject();
    
    constructor(private logger: LoggerService,
                private productsCategoriesService: ProductsCategoriesService) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    ngOnInit() {
        this.productsCategoriesService.getCategories()
        .pipe(
            tap((categories: ProductCategory[]) => {
                this.categories = categories;
            }),
            takeUntil(this.isDestroyed$)
        )
        .subscribe();
    }
    
    ngOnDestroy(): void {
        this.isDestroyed$.next();
        this.isDestroyed$.complete();
    }
}
