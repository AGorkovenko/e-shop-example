import {Component, Input} from "@angular/core";
import {ProductCategory} from "../../../_core/models/dto/product/product-category.model";
import {LoggerService} from "../../../_core/services/logger.service";

@Component({
  selector: "app-shop-categories-item",
  template: `
    <div class="filter-sublinks">
      <div class="filter-sublink" *ngFor="let item of items" [ngClass]="{'filter-sublink--parent': item.children.length}" routerLinkActive="filter-sublink--active">
        <a [routerLink]="['/shop/' + parent + '/' + item.slug]">{{item.name}}</a>
        <app-shop-categories-item [items]="item.children" [parent]="parent + '/' + item.slug" *ngIf="item.children.length"></app-shop-categories-item>
      </div>
    </div>
  `
})

export class ShopFilterCategoriesItemComponent {
  
  @Input() items: ProductCategory[];
  @Input() parent: string;
  
  sortableOptions: any = {};
  touchMenu: boolean = false;
  
  constructor(private logger: LoggerService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
}
