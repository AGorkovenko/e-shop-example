import {Component, OnDestroy, OnInit} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";
import {Subject} from "rxjs";
import {ChangeContext, Options} from "ng5-slider";
import {CurrencyService} from "../../../_core/services/currency.service";
import {Currency} from "../../../_core/models/enum/currency.enum";
import {switchMap, takeUntil, tap} from "rxjs/operators";
import {ProductViewRequest} from "../../../_core/models/dto/product/product-view-request.model";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {ProductListData, ProductsService} from "../../../_core/services/product/products.service";

@Component({
    selector: "app-shop-filter-price",
    template: `
        <div class="filter" *ngIf="minValue >= 0 && maxValue >= 0">
            <div class="filter__name" i18n="Filter in shop sidebar@@filterSidebarTitlePrice">Price</div>
            <div class="filter__block">
                <div class="filter-slider">
                    <ng5-slider [(value)]="minPrice" [(highValue)]="maxPrice" [options]="options" (userChangeEnd)="changePriceRange($event)"></ng5-slider>
                    <div class="filter-slider__inputs" *ngIf="false">
                        <div class="filter-slider__input filter-slider__input-min">
                            <input class="form-element" type="number" [(ngModel)]="minPrice">
                            <div class="filter-slider__input-symbol">{{currency.getSymbol(this.currentCurrency)}}</div>
                        </div>
                        <div class="filter-slider__input-sep">-</div>
                        <div class="filter-slider__input filter-slider__input-max">
                            <input class="form-element" type="number" [(ngModel)]="maxPrice">
                            <div class="filter-slider__input-symbol">{{currency.getSymbol(this.currentCurrency)}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `,
    styleUrls: ["./shop-filter-price.components.scss"]
})

export class ShopFilterPriceComponent implements OnInit, OnDestroy {
    
    minValue: number;
    maxValue: number;
    minPrice: number;
    maxPrice: number;
    options: Options = new Options();
    
    currentCurrency: Currency;
    currency = Currency;
    
    isDestroyed$: Subject<void> = new Subject();
    
    constructor(private logger: LoggerService,
                private router: Router,
                private route: ActivatedRoute,
                public currencyService: CurrencyService,
                private productsService: ProductsService) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    ngOnInit() {
        this.currencyService.currentCurrency$
        .pipe(
            tap((currency: Currency) => {
                this.currentCurrency = currency;
            }),
            switchMap(() => this.route.data),
            tap((data: { productListData: ProductListData }) => {
                if (data.productListData && data.productListData.minValue && data.productListData.maxValue) {
                    this.minValue = data.productListData.minValue;
                    this.maxValue = data.productListData.maxValue;
                    this.minPrice = data.productListData.viewRequest.minPrice ? data.productListData.viewRequest.minPrice : this.minValue;
                    this.maxPrice = data.productListData.viewRequest.maxPrice ? data.productListData.viewRequest.maxPrice : this.maxValue;
                    this.reloadSlider();
                }
            }),
            takeUntil(this.isDestroyed$)
        )
        .subscribe();
    }
    
    reloadSlider() {
        this.options = {
            floor: this.minValue,
            ceil: this.maxValue,
            step: 1,
            translate: (value: number): string => {
                return Math.round(this.currencyService.currencyExchange(value)) + Currency.getSymbol(this.currentCurrency);
            }
        };
    }
    
    changePriceRange(changeContext: ChangeContext) {
        const productViewRequest: ProductViewRequest = this.productsService.productViewRequest();
        const queryParams: Params                    = {};
        queryParams.minPrice                         = this.minPrice;
        queryParams.maxPrice                         = this.maxPrice;
        this.router.navigate([], {queryParams: queryParams, queryParamsHandling: "merge"});
    }
    
    ngOnDestroy(): void {
        this.isDestroyed$.next();
        this.isDestroyed$.complete();
    }
}
