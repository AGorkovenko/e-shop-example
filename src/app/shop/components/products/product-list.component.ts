import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";
import {ProductListData, ProductsService} from "../../../_core/services/product/products.service";
import {AsyncSubject, Subject} from "rxjs";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: "app-product-list",
  template: `
    <div class="catalog-head">
      <div class="catalog-head__category">
        <h2 i18n="Head product list@@productListHeadProducts">Products</h2>
      </div>
      <div class="catalog-head__count" i18n="Head product list@@productListHeadTotal" *ngIf="productListData.count">Total {{productListData.count}} products</div>
      <div class="catalog-head__sort">
        <app-products-sort></app-products-sort>
      </div>
    </div>
    
    <div class="catalog-products">
      <div class="products">
        <div class="block block--fixed" *ngIf="productListData.list; else notFound">
          <div class="block-row block-row--3">
            <div class="block-column" *ngFor="let product of productListData.list">
              <div class="box">
                <app-product-panel [product]="product"></app-product-panel>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ng-template #notFound>
        <h4 class="text-center mt" i18n="Product list when not find@@productListNoProducts">No Products</h4>
      </ng-template>
    </div>
    
    <div class="catalog-foot">
      <app-paginator *ngIf="productListData.count / productListData.viewRequest.pageSize > 1"
                     [currentPage]="productListData.viewRequest.page" [totalCount]="productListData.count"
                     [pageSize]="productListData.viewRequest.pageSize" [isMoreButtonEnabled]="true"
                     (onShowMore)="loadMoreProducts($event)"></app-paginator>
    </div>
  `
})
export class ProductListComponent implements OnInit, OnDestroy {
  
  @Input() productListData: ProductListData;
  isDestroyed$: Subject<void> = new Subject();
  
  constructor(private logger: LoggerService,
              private route: ActivatedRoute,
              private productsService: ProductsService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  ngOnInit() {
  }
  
  loadMoreProducts(showMoreEvent: { page: number, callbackNotifier: AsyncSubject<void> }) {
    this.productsService.loadAndAppendPage(showMoreEvent.page)
    .subscribe(
      () => showMoreEvent.callbackNotifier.next(null),
      null,
      () => showMoreEvent.callbackNotifier.complete()
    );
  }
  
  ngOnDestroy(): void {
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }
}
