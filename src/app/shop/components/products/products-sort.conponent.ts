import {Component, OnInit} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";
import {SelectOption} from "../../../_shared/components/select/select-option.enum";
import {SortType} from "../../../_core/models/enum/sort-type.enum";
import {ProductViewRequest} from "../../../_core/models/dto/product/product-view-request.model";
import {Params, Router} from "@angular/router";
import {ProductsService} from "../../../_core/services/product/products.service";

@Component({
    selector: "app-products-sort",
    template: `
        <app-select [options]="sortSelectItems" (changed)="sorting()" [(ngModel)]="currentSort"></app-select>
    `
})
export class ProductsSortComponent implements OnInit {
    
    sortSelectItems: SelectOption[];
    currentSort: SelectOption;
    
    constructor(private logger: LoggerService,
                private router: Router,
                private productsService: ProductsService) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    ngOnInit() {
        this.sortSelectItems = this.sortEnumToSelectOption();
    }
    
    sortEnumToSelectOption() {
        const sort: SelectOption[] = [];
        SortType.values().forEach(item => {
            sort.push({
                label: SortType.getDisplayName(item),
                value: item
            });
        });
        return sort;
    }
    
    sorting() {
        const productViewRequest: ProductViewRequest = this.productsService.productViewRequest();
        const queryParams: Params                    = {};
        if (this.currentSort) {
            switch (this.currentSort.value) {
                case SortType.ASC:
                    queryParams.order = "title";
                    queryParams.sort  = this.currentSort.value;
                    break;
                case SortType.DESC:
                    queryParams.order = "title";
                    queryParams.sort  = this.currentSort.value;
                    break;
                case SortType.HIGHEST_PRICE:
                    queryParams.order = "price";
                    queryParams.sort  = SortType.DESC;
                    break;
                case SortType.LOWEST_PRICE:
                    queryParams.order = "price";
                    queryParams.sort  = SortType.ASC;
                    break;
                case SortType.NEWEST:
                    queryParams.order = "date_created";
                    queryParams.sort  = SortType.DESC;
                    break;
                case SortType.OLDEST:
                    queryParams.order = "date_created";
                    queryParams.sort  = SortType.ASC;
                    break;
            }
        }
        this.router.navigate([], {queryParams: queryParams, queryParamsHandling: "merge"});
    }
}
