import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";
import {Product} from "../../../_core/models/dto/product/product.model";
import {Currency} from "../../../_core/models/enum/currency.enum";
import {CurrencyService} from "../../../_core/services/currency.service";
import {takeUntil, tap} from "rxjs/operators";
import {Subject} from "rxjs";
import {ProductDiscount, ShopService} from "../../../_core/services/shop/shop.service";
import {CartService} from "../../../_core/services/cart.service";
import {CartProductRequest} from "../../../_core/models/dto/cart/cart-product-request.model";

@Component({
  selector: "app-product-panel",
  template: `
    <div class="product-card">
      
      <div class="product-card__thumb">
        <a class="image" [routerLink]="['product/' + product.slug]">
          <img [alt]="product.title" [src]="product.photo + '/size/400'" *ngIf="product.photo">
          <img [alt]="product.title" src="assets/images/placeholder-piston.svg" *ngIf="!product.photo">
        </a>
      </div>
      
      <div class="product-card__info">
        <h4 class="product-card-name">
          <a [routerLink]="['product/' + product.slug]">{{product.title}}</a>
        </h4>
        <div class="product-card-review">
          <div class="product-card-review__stars">
            <app-stars [rating]="product.rating"></app-stars>
          </div>
          <div class="product-card-review__count" i18n="@@productPanelReviewCount">(<span>{{product.reviews}}</span> {product.reviews, plural, =0 {reviews} =1 {review} other {reviews}})</div>
        </div>
        <div class="product-card__foot">
          <div class="product-card-price">
            <div class="product-card-price__other product-card-price__other--old" *ngIf="_old_price">
              {{currencyService.currencyExchange(_old_price) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}
            </div>
            <div class="product-card-price__regular">
              {{currencyService.currencyExchange(_price) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}
            </div>
          </div>
          <div class="product-card-stock product-card-stock--not-available" *ngIf="!product.stock">
            <span i18n="@@productPanelLabelNotAvailable">Not available</span>
          </div>
          <button class="product-card-cart" *ngIf="product.stock" i18n-title="@@productPanelButtonTitleAddToCart" title="Add to cart">
            <app-icon name="cart" (click)="addToCart()"></app-icon>
          </button>
        </div>
      </div>
    
    </div>
  `
})
export class ProductPanelComponent implements OnInit, OnDestroy {
  
  @Input() product: Product;
  currentCurrency: Currency;
  currency = Currency;
  
  _old_price: number;
  _price: number;
  
  isDestroyed$: Subject<void> = new Subject();
  
  constructor(private logger: LoggerService,
              public currencyService: CurrencyService,
              private shopService: ShopService,
              private cartService: CartService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  ngOnInit() {
    this.shopService.discountSource$
    .pipe(
      tap((discount: ProductDiscount) => {
        // if user can have discount
        if (discount) {
          // type discount in product main than discount user
          if (this.product[discount.type]) {
            this._price = this.product[discount.type];
            this._old_price = this.product.price;
          } else {
            // if user can have discount than check value discount for this type
            if (discount.value) {
              this._price = this.product.price * (1 - discount.value / 100);
              this._old_price = this.product.price;
            } else {
              // without discount prices
              this._price = this.product.price;
              this._old_price = this.product.old_price;
            }
          }
        } else {
          // without discount prices
          this._price = this.product.price;
          this._old_price = this.product.old_price;
        }
      }),
      takeUntil(this.isDestroyed$)
    )
    .subscribe();
    
    this.currencyService.currentCurrency$
    .pipe(
      tap((currency: Currency) => {
        this.currentCurrency = currency;
      }),
      takeUntil(this.isDestroyed$)
    )
    .subscribe();
  }
  
  addToCart() {
    const request: CartProductRequest = new CartProductRequest();
    request.pid = this.product.ID;
    request.quantity = 1;
    this.cartService.addProduct(request)
    .pipe(takeUntil(this.isDestroyed$))
    .subscribe();
  }
  
  ngOnDestroy(): void {
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }
}
