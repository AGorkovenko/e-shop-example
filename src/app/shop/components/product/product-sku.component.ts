import {Component, Input} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";

@Component({
  selector: "app-product-sku",
  template: `
    <div class="product-sku product-sku--gti" *ngIf="sku" i18n="@@productLabelSkuByGTI">SKU by GTI: <span>{{sku}}</span></div>
    <div class="product-sku" *ngIf="original_sku" i18n="@@productLabelOriginalSky">Original SKU: <span>{{original_sku}}</span></div>
  `
})
export class ProductSkuComponent {
  
  @Input() sku: string;
  @Input() original_sku: string;
  
  constructor(private logger: LoggerService,) {
    this.logger.debug(this.constructor.name + " is created.");
  }
}
