import {Component, Input} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";
import {Product} from "../../../_core/models/dto/product/product.model";

@Component({
    selector: "app-product-tabs",
    template: `
        <div class="product-tabs">
            <div class="product-tabs__control">
                <div class="product-tab" i18n="@@productTabsControlDescription" [ngClass]="{'product-tab--active': tab === 0}" (click)="selectTab(0)">Description</div>
                <div class="product-tab" i18n="@@productTabsControlAdditionalInformation" [ngClass]="{'product-tab--active': tab === 1}" (click)="selectTab(1)">Additional information</div>
                <div class="product-tab" [ngClass]="{'product-tab--active': tab === 2}">
                    <app-stars [rating]="product.rating" (click)="selectTab(2)"></app-stars>
                </div>
            </div>
            <div class="product-tabs__content" *ngIf="tab === 0">
                <h4 i18n="@@productTabsContentTitleDescription">DESCRIPTION</h4>
                <div [innerHTML]="product.description"></div>
            </div>
            <div class="product-tabs__content" *ngIf="tab === 1">
                <h4 i18n="@@productTabsContentTitleAdditionalInformation">ADDITIONAL INFORMATION</h4>
                <table *ngIf="product.attributes && product.attributes.length > 0">
                    <thead>
                    <tr>
                        <th i18n="@@productTabsContentTableAttributesHeadName">Name</th>
                        <th i18n="@@productTabsContentTableAttributesHeadValue">Value</th>
                    </tr>
                    </thead>
                    <tbody>
                    <ng-container *ngFor="let attribute of product.attributes">
                        <tr *ngIf="attribute.value && attribute.value !== ''">
                            <th>{{attribute.name}}</th>
                            <td>{{attribute.value}}</td>
                        </tr>
                    </ng-container>
                    </tbody>
                </table>
            </div>
            <div class="product-tabs__content" *ngIf="tab === 2">
                <h4 i18n="@@productTabsContentTitleAdditionalInformation">{{product.reviews}} {product.reviews, plural, =0 {reviews} =1 {review} other {reviews}} for <span>"{{product.title}}"</span></h4>
                <app-product-reviews [id]="product.ID"></app-product-reviews>
            </div>
        </div>
    `
})
export class ProductTabsComponent {
    
    @Input() product: Product;
    tab: number = 0;
    
    constructor(private logger: LoggerService) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    selectTab(index: number) {
        this.tab = index;
    }
}
