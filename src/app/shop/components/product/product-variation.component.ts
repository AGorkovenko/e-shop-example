import {Component, Input, OnInit} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";
import {SelectOption} from "../../../_shared/components/select/select-option.enum";

@Component({
  selector: "app-product-variation",
  template: `
    <div class="product-variation">
      <div class="product-variation__select">
        <label>Variation</label>
        <app-select [options]="options"></app-select>
      </div>
    </div>
  `
})
export class ProductVariationComponent implements OnInit{
  
  options: SelectOption[];
  
  constructor(private logger: LoggerService,) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  ngOnInit(): void {
    this.options = [
      {label: "First", value: 1},
      {label: "Second", value: 2},
      {label: "Third", value: 3},
      {label: "Fourth", value: 4}
    ];
  }
}
