import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";
import {Product} from "../../../_core/models/dto/product/product.model";
import {Currency} from "../../../_core/models/enum/currency.enum";
import {CurrencyService} from "../../../_core/services/currency.service";
import {takeUntil, tap} from "rxjs/operators";
import {Subject} from "rxjs";
import {ProductDiscount, ShopService} from "../../../_core/services/shop/shop.service";
import {ProfileService} from "../../../_core/services/profile/profile.service";

@Component({
  selector: "app-product-price",
  template: `
    <div class="product-price">
      <div>
        <div class="product-price__other" [ngClass]="{'product-price__other--old': _old_price}" *ngIf="_old_price">
          {{currencyService.currencyExchange(_old_price) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}
          <div class="product-price-info" *ngIf="this.saleInPercent">
            <app-icon name="info"></app-icon>
            <div class="product-price-info__label" i18n="@@productLabelTooltipOldPriceNowSale" *ngIf="!isDealer">Old price<br>now {{saleInPercent}}% Sale</div>
            <div class="product-price-info__label" i18n="@@productLabelTooltipORegularPriceForCustomers" *ngIf="isDealer">Regular price<br>for customers</div>
          </div>
        </div>
        <div class="product-price__regular">
          {{currencyService.currencyExchange(_price) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}
        </div>
      </div>
    </div>
  `
})
export class ProductPriceComponent implements OnInit, OnDestroy {
  
  @Input() product: Product;
  currentCurrency: Currency;
  currency = Currency;
  
  saleInPercent: number;
  isDealer: boolean = false;
  _old_price: number;
  _price: number;
  
  isDestroyed$: Subject<void> = new Subject();
  
  constructor(private logger: LoggerService,
              public currencyService: CurrencyService,
              private profileService: ProfileService,
              private shopService: ShopService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  ngOnInit(): void {
    this.shopService.discountSource$
    .pipe(
      tap((discount: ProductDiscount) => {
        if (discount) {
          if (this.product[discount.type]) {
            this._price = this.product[discount.type];
            this._old_price = this.product.price;
          } else {
            if (discount.value) {
              this._price = (this.product.old_price ? this.product.old_price : this.product.price) * (1 - discount.value / 100);
              this._old_price = this.product.price;
            } else {
              this._price = this.product.price;
              this._old_price = this.product.old_price;
            }
          }
        } else {
          this._price = this.product.price;
          this._old_price = this.product.old_price;
        }
        
        if (this._old_price) {
          this.saleInPercent = Math.round((this._old_price - this._price) / (this._old_price / 100));
          this.isDealer = !!discount;
        }
      }),
      takeUntil(this.isDestroyed$)
    )
    .subscribe();
    
    this.currencyService.currentCurrency$
    .pipe(
      tap((currency: Currency) => {
        this.currentCurrency = currency;
      }),
      takeUntil(this.isDestroyed$)
    )
    .subscribe();
  }
  
  ngOnDestroy(): void {
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }
}
