import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {CartService} from "../../../_core/services/cart.service";
import {Subject} from "rxjs";
import {takeUntil} from "rxjs/operators";
import {CartProductRequest} from "../../../_core/models/dto/cart/cart-product-request.model";

@Component({
  selector: "app-product-buy-button",
  template: `
    <form [formGroup]="buyForm" *ngIf="stock">
      <div class="product-buy">
        <div class="product-buy__qty">
          <div class="product-quantity">
            <button class="product-quantity__button" (click)="minus()">-</button>
            <input class="product-quantity__input" type="number" formControlName="stockToCart" (change)="checkedValue()">
            <button class="product-quantity__button" (click)="plus()">+</button>
          </div>
        </div>
        <div class="product-buy__to-cart">
          <button type="button" class="button button--danger button--md" (click)="addToCart()">Add to cart</button>
        </div>
      </div>
    </form>
  `
})
export class ProductBuyButtonComponent implements OnInit, OnDestroy {
  
  @Input() id: number;
  @Input() stock: number;
  buyForm: FormGroup;
  
  isDestroyed$: Subject<void> = new Subject();
  
  constructor(private logger: LoggerService,
              private fb: FormBuilder,
              private cartService: CartService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  ngOnInit(): void {
    if (this.stock) {
      this.initForm();
    }
  }
  
  initForm() {
    this.buyForm = this.fb.group({
      pid: new FormControl(this.id ? this.id : null, Validators.required),
      stockToCart: new FormControl(1, [Validators.required, Validators.max(this.stock)]),
    });
  }
  
  checkedValue() {
    let checkedValue = parseInt(this.buyForm.get("stockToCart").value) ? parseInt(this.buyForm.get("stockToCart").value) : 1;
    checkedValue = checkedValue > this.stock ? this.stock : checkedValue;
    checkedValue = checkedValue <= 0 ? 1 : checkedValue;
    this.buyForm.patchValue({"stockToCart": checkedValue});
  }
  
  plus() {
    this.buyForm.patchValue({"stockToCart": this.buyForm.get("stockToCart").value + 1});
    this.checkedValue();
  }
  
  minus() {
    this.buyForm.patchValue({"stockToCart": this.buyForm.get("stockToCart").value - 1});
    this.checkedValue();
  }
  
  addToCart() {
    if (this.buyForm.valid) {
      const request: CartProductRequest = new CartProductRequest();
      request.pid = this.buyForm.get("pid").value;
      request.quantity = this.buyForm.get("stockToCart").value;
      this.cartService.addProduct(request)
      .pipe(takeUntil(this.isDestroyed$))
      .subscribe();
    }
  }
  
  ngOnDestroy(): void {
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }
}
