import {Component, Inject, OnInit, PLATFORM_ID} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";
import {isPlatformBrowser} from "@angular/common";

@Component({
  selector: "app-product-share",
  template: `
    <div class="share">
      <div class="share__label">Share</div>
      <div class="share__icons">
        <div class="socials socials--color">
          <a class="social-link social-link--facebook" [href]="facebookShareLink" (click)="openSharingWindow(facebookShareLink, $event)">
            <app-icon name="facebook"></app-icon>
          </a>
          <!--          <a class="social-link social-link&#45;&#45;massenger" href="">-->
          <!--            <app-icon name="massenger"></app-icon>-->
          <!--          </a>-->
          <!--          <a class="social-link social-link&#45;&#45;youtube" href="">-->
          <!--            <app-icon name="youtube"></app-icon>-->
          <!--          </a>-->
          <!--          <a class="social-link social-link&#45;&#45;instagram" href="">-->
          <!--            <app-icon name="instagram"></app-icon>-->
          <!--          </a>-->
        </div>
      </div>
    </div>
  `
})
export class ProductShareComponent implements OnInit {
  
  sharedUrl: string;
  
  facebookShareLink: string = "https://www.facebook.com/share.php";
  linkedInShareLink: string = "http://www.linkedin.com/shareArticle";
  twitterShareLink: string = "https://twitter.com/share";
  
  constructor(private logger: LoggerService,
              @Inject(PLATFORM_ID) private platformId: Object,) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.prepareSharingUrls();
    }
  }
  
  prepareSharingUrls() {
    this.sharedUrl = ProductShareComponent.getSharedUrl();
    this.facebookShareLink = "https://www.facebook.com/share.php?u=" + this.sharedUrl;
    this.linkedInShareLink = "http://www.linkedin.com/shareArticle?mini=true&url=" + this.sharedUrl + "&source=" + this.sharedUrl;
    this.twitterShareLink = "https://twitter.com/share?url=" + this.sharedUrl;
  }
  
  openSharingWindow(link: string, event) {
    event.preventDefault();
    const winHeight = 350,
      winWidth = 520,
      winTop = (screen.height / 2) - (winHeight / 2),
      winLeft = (screen.width / 2) - (winWidth / 2);
    window.open(link, "sharer", "top=" + winTop + ",left=" + winLeft + ",toolbar=0,status=0,width=" + winWidth + ",height=" + winHeight);
  }
  
  static getSharedUrl(): string {
    return (window.location !== window.parent.location) ? document.referrer : document.location.href;
  }
}
