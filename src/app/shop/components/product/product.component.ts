import {Component, OnDestroy, OnInit} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";
import {ActivatedRoute} from "@angular/router";
import {takeUntil, tap} from "rxjs/operators";
import {Subject} from "rxjs";
import {Product} from "../../../_core/models/dto/product/product.model";

@Component({
    selector: "app-product",
    template: `
        <div class="container">
            <div class="product" *ngIf="product">
                <div class="product__gallery">
                    <h1>{{product.title}}</h1>
                    <app-product-gallery [title]="product.title" [gallery]="gallery"></app-product-gallery>
                </div>
                <div class="product__info">
                    <div class="box">
                        <div class="product-code">
                            <app-product-sku [sku]="product.sku" [original_sku]="product.original_sku"></app-product-sku>
                        </div>
                        <div class="product-excerpt">{{product.excerpt}}</div>
                        <div class="product-price-stock">
                            <app-product-price [product]="product"></app-product-price>
                            <app-product-stock [stock]="product.stock"></app-product-stock>
                        </div>
                        <!-- <app-product-variation></app-product-variation> -->
                        <app-product-buy-button [stock]="product.stock" [id]="product.ID"></app-product-buy-button>
                    </div>
                    <div class="box">
                        <app-product-adds></app-product-adds>
                    </div>
                    <div class="box">
                        <app-product-share></app-product-share>
                    </div>
                </div>
            </div>
            <app-product-tabs [product]="product"></app-product-tabs>
        </div>
    `
})
export class ProductComponent implements OnInit, OnDestroy {
    
    product: Product;
    gallery: string[]           = [];
    isDestroyed$: Subject<void> = new Subject();
    
    constructor(private logger: LoggerService,
                private route: ActivatedRoute) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    ngOnInit() {
        this.route.data
        .pipe(
            tap((data: { product: Product }) => {
                this.product = data.product;
                this.gallery = this.product.gallery ? this.product.gallery : [];
                if (this.product.photo) {
                    this.gallery.unshift(this.product.photo);
                }
            }),
            takeUntil(this.isDestroyed$)
        )
        .subscribe();
    }
    
    ngOnDestroy(): void {
        this.isDestroyed$.next();
        this.isDestroyed$.complete();
    }
}
