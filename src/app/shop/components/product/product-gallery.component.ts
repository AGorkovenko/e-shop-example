import {Component, Input} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";

@Component({
    selector: "app-product-gallery",
    template: `
        <div class="product-gallery">
            <div class="product-gallery__thumbs" *ngIf="gallery">
                <div class="product-gallery-thumb" *ngFor="let image of gallery; index as i" (click)="showThumb(i)">
                    <img [alt]="title" [src]="image + '/thumb'">
                </div>
            </div>
            <div class="product-gallery__photo">
                <div class="product-gallery-photo-image">
                    <div class="image">
                        <img [alt]="title" [src]="gallery[indexOfGallery] + '/size/1200'" *ngIf="gallery && gallery[indexOfGallery]">
                        <img [alt]="title" src="assets/images/placeholder-piston.svg" *ngIf="!gallery">
                    </div>
                </div>
            </div>
        </div>
    `
})
export class ProductGalleryComponent {
    
    @Input() gallery: string[];
    @Input() title: string;
    
    indexOfGallery: number = 0;
    
    constructor(private logger: LoggerService) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    showThumb(index: number) {
        this.indexOfGallery = index;
    }
}
