import {Component} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";

@Component({
  selector: "app-product-adds",
  template: `
    <div class="product-adds">
      <div class="info-block info-block--add">
        <div class="info-block__icon">
          <app-icon name="delivery"></app-icon>
        </div>
        <div class="info-block__name">
          <div class="info-block-name-first" i18n="@@productAddsFreeDelivery">Free delivery</div>
        </div>
      </div>
      <div class="info-block info-block--add">
        <div class="info-block__icon">
          <app-icon name="boxes"></app-icon>
        </div>
        <div class="info-block__name">
          <div class="info-block-name-first" i18n="@@productAddsTimesBouht">50 times bought</div>
        </div>
      </div>
      <div class="info-block info-block--add">
        <div class="info-block__icon">
          <app-icon name="shield"></app-icon>
        </div>
        <div class="info-block__name">
          <div class="info-block-name-first" i18n="@@productAddsQualityAssurance">Quality assurance</div>
        </div>
      </div>
    </div>
  `
})
export class ProductAddsComponent {
  
  constructor(private logger: LoggerService,) {
    this.logger.debug(this.constructor.name + " is created.");
  }
}
