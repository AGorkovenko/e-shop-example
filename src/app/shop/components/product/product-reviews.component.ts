import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";
import {Review} from "../../../_core/models/dto/review/review.model";
import {TimeService} from "../../../_core/services/time.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ProfileService} from "../../../_core/services/profile/profile.service";
import {Profile} from "../../../_core/models/dto/profile/profile.model";
import {Subject} from "rxjs";
import {takeUntil} from "rxjs/operators";

@Component({
  selector: "app-product-reviews",
  template: `
    <div class="comments">
      
      <div class="comment" *ngFor="let review of reviews">
        <div class="comment__ava">
          <div class="image" [ngStyle]="{'background-image': 'url(' + review.user.ava + ')'}"></div>
        </div>
        <div class="comment__info">
          <div class="comment-head">
            <div class="comment-head__stars">
              <app-stars [rating]="review.rating"></app-stars>
            </div>
            <div class="comment-head__name">{{review.user.firstname + ' ' + review.user.lastname}}</div>
            <div class="comment-head__date">– {{timeService.getTimeStamp(review.date, "MMMM DD, YYYY")}}</div>
          </div>
          <div class="comment-content">{{review.review}}</div>
        </div>
      </div>
      
      <div class="comment-add">
        <h4 i18n="@@productTabsTitleAddReview">ADD A REVIEW</h4>
        <form *ngIf="profile; else notAuth">
          <div class="form-line form-line--inline">
            <label i18n="@@productTabsLabelYourRating">Your rating</label>
            <app-stars [active]="true"></app-stars>
          </div>
          <div class="form-line">
            <label i18n="@@productTabsLabelYourReview">Your review<sup>*</sup></label>
          </div>
          <div class="form-line">
            <textarea class="form-element form-element--textarea"></textarea>
          </div>
          <div class="form-line">
            <button type="button" class="button button--danger button--inline" i18n="@@productTabsButtonSubmit">Submit</button>
          </div>
        </form>
        <ng-template #notAuth i18n="@@productTabsLeaveReviewNotAuth">To leave review, you must login on our site.</ng-template>
      </div>
    
    </div>
  `
})
export class ProductReviewsComponent implements OnInit, OnDestroy {
  
  @Input() id: number;
  reviews: Review[] = [];
  reviewForm: FormGroup;
  profile: Profile;
  isDestroyed$: Subject<void> = new Subject();
  
  constructor(private logger: LoggerService,
              public timeService: TimeService,
              private fb: FormBuilder,
              public profileService: ProfileService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  ngOnInit(): void {
    this.profileService.profile$
    .pipe(takeUntil(this.isDestroyed$))
    .subscribe((profile: Profile) => {
      this.profile = profile;
    });
  }
  
  initForm() {
    this.reviewForm = this.fb.group({
      userId: new FormControl(this.profileService.getCurrentProfile().ID, Validators.required),
      rating: new FormControl(1, [Validators.required, Validators.max(5), Validators.min(1)]),
      review: new FormControl("", [Validators.required]),
    });
  }
  
  ngOnDestroy(): void {
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }
}
