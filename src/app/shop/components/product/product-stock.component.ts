import {Component, Input, OnInit} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";
import {ProductCategory} from "../../../_core/models/dto/product/product-category.model";

@Component({
  selector: "app-product-stock",
  template: `
    <div class="product-stock" *ngIf="stock">
      <app-icon name="check"></app-icon>
      <span i18n="@@productLabelInStock">In Stock</span>
    </div>
    <div class="product-stock product-stock--not-available" *ngIf="!stock">
      <span i18n="@@productLabelNotAvailable">Not available</span>
    </div>
  `
})
export class ProductStockComponent {
  
  @Input() stock: number;
  
  constructor(private logger: LoggerService,) {
    this.logger.debug(this.constructor.name + " is created.");
  }
}
