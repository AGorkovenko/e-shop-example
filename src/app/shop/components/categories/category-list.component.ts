import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";
import {Subject} from "rxjs";
import {ProductCategory} from "../../../_core/models/dto/product/product-category.model";
import {SiteNavigationService} from "../../../_core/services/site-navigation";
import {tap} from "rxjs/operators";

@Component({
  selector: "app-category-list",
  template: `
    <div class="catalog-categories">
      <div class="categories">
        <div class="block block--fixed" *ngIf="categories">
          <div class="block-row block-row--4">
            <div class="block-column mb" *ngFor="let category of categories">
              <app-category-panel [category]="category" [path]="path"></app-category-panel>
            </div>
          </div>
        </div>
      </div>
    </div>
  `
})
export class CategoryListComponent implements OnInit, OnDestroy {
  
  @Input() categories: ProductCategory[];
  path: string;
  isDestroyed$: Subject<void> = new Subject();
  
  constructor(private logger: LoggerService,
              private siteNavigationService: SiteNavigationService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  ngOnInit() {
    this.siteNavigationService.getCurrentPagePath()
    .pipe(
      tap((path: string) => {
        this.path = path;
      })
    )
    .subscribe();
  }
  
  ngOnDestroy(): void {
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }
}
