import {Component, Input, OnInit} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";
import {ProductCategory} from "../../../_core/models/dto/product/product-category.model";

@Component({
  selector: "app-category-panel",
  template: `
    <div class="category-card">
      <div class="category-card__thumb">
        <a class="image" [routerLink]="[path + '/' + category.slug]">
          <img [alt]="category.name" [src]="category.cover + '/size/400'" *ngIf="category.cover">
          <img [alt]="category.name" src="assets/images/placeholder-piston.svg" *ngIf="!category.cover">
        </a>
      </div>
      <div class="category-card__info">
        <h4 class="category-card-name">
          <a [routerLink]="[path + '/' + category.slug]">{{category.name}}</a>
        </h4>
        <div class="category-card__foot"></div>
      </div>
    </div>
  `
})
export class CategoryPanelComponent implements OnInit {
  
  @Input() path: string;
  @Input() category: ProductCategory;
  
  constructor(private logger: LoggerService,) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  ngOnInit() {
    
  }
}
