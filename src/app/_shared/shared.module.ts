import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {IconComponent} from "./components/icon/icon.component";
import {PaginatorComponent} from "./components/paginator/paginator.component";
import {ClickOutsideDirective} from "./directives/click-outside.directive";
import {SelectComponent} from "./components/select/select.component";
import {DropDownDirective} from "./directives/drop-down.directive";
import {UploaderComponent} from "./components/uploader/uploader.component";
import {PreloaderComponent} from "./components/preloader/preloader.component";
import {SelectMultipleComponent} from "./components/select-multiple/select-multiple.component";
import {SelectMultipleOptionsComponent} from "./components/select-multiple/select-multiple-options.component";
import {TagsComponent} from "./components/tags/tags.component";
import {ModalComponent} from "./components/modals/modal.component";
import {BreadcrumbsComponent} from "./components/breadcrumbs/breadcrumbs.component";
import {StarsComponent} from "./components/stars/stars.component";
import {Ng5SliderModule} from "ng5-slider";

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    Ng5SliderModule,
  ],
  exports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    Ng5SliderModule,
    
    IconComponent,
    PaginatorComponent,
    SelectComponent,
    UploaderComponent,
    PreloaderComponent,
    SelectMultipleComponent,
    SelectMultipleOptionsComponent,
    TagsComponent,
    ModalComponent,
    BreadcrumbsComponent,
    StarsComponent,
    
    ClickOutsideDirective,
    DropDownDirective
  ],
  declarations: [
    IconComponent,
    PaginatorComponent,
    SelectComponent,
    UploaderComponent,
    PreloaderComponent,
    SelectMultipleComponent,
    SelectMultipleOptionsComponent,
    TagsComponent,
    ModalComponent,
    BreadcrumbsComponent,
    StarsComponent,
    
    ClickOutsideDirective,
    DropDownDirective
  ]
})
export class SharedModule {
}
