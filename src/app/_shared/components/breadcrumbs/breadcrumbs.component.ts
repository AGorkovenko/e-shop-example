import {ChangeDetectionStrategy, Component} from "@angular/core";

@Component({
  selector: "app-breadcrumbs",
  template: `
    <div class="breadcrumbs-block" *ngIf="false">
      <div class="container">
        <div class="breadcrumbs">
          <div class="breadcrumb">
            <a [routerLink]="['/']" i18n="Breadcrumbs first link to home@@breadcrumbsHome">Home</a>
            <app-icon name="current"></app-icon>
          </div>
          <div class="breadcrumb">
            <span>path</span>
          </div>
        </div>
      </div>
    </div>
    <div class="back" *ngIf="false">
      <div class="container">
        <div class="back__wrap">
          <a class="back-link" [routerLink]="['/']">
            <app-icon name="back"></app-icon>
            <span i18n="Back in breadcrumbs@@linkBack">back</span>
          </a>
        </div>
      </div>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ["./breadcrumbs.component.scss"]
})
export class BreadcrumbsComponent {
  
  constructor() {
  }
}
