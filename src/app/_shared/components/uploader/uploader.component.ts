import {Component, forwardRef, Input, OnDestroy} from "@angular/core";
import {UploaderService} from "../../../_core/services/uploader.service";
import {catchError, takeUntil, tap} from "rxjs/operators";
import {EMPTY, Subject} from "rxjs";
import {NG_VALUE_ACCESSOR} from "@angular/forms";
import {ExceptionService} from "../../../_core/services/exception.service";
import {HttpEventType} from "@angular/common/http";

@Component({
    selector: "app-uploader",
    template: `
        <div class="dropzone" [ngClass]="{'dropzone--multiple': multiple}">

            <ng-container *ngIf="link">
                <div class="dropzone-preview">
                    <img [src]="link + '/thumb'" class="dropzone-preview__img" [alt]="link">
                </div>
                <div class="dropzone-delete" (click)="deleteFile()">
                    <div class="dropzone-delete__icon icon-close"></div>
                </div>
            </ng-container>

            <ng-container *ngIf="links">
                <div class="dropzone-viewport">
                    <div class="dropzone-viewport__image" *ngFor="let link of links; index as i">
                        <div class="dropzone-preview">
                            <img [src]="link + '/thumb'" class="dropzone-preview__img" [alt]="link">
                        </div>
                        <div class="dropzone-delete" (click)="deleteFile(i)">
                            <div class="dropzone-delete__icon icon-close"></div>
                        </div>
                    </div>
                </div>
            </ng-container>

            <div class="dropzone-field" *ngIf="!link" [ngClass]="{'dropzone-field--multiple': links}">
                <div class="dropzone-field__message" *ngIf="!link">
                    <div>Drop files here to upload</div>
                </div>
                <input type="file" class="dropzone-field__input" (change)="handleFilesInput($event)" [multiple]="multiple">
            </div>

            <app-loader [show]="showPreloader" [percent]="percent"></app-loader>
        </div>
    `,
    styleUrls: ["./uploader.component.scss"],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => UploaderComponent),
            multi: true
        }
    ]
})
export class UploaderComponent implements OnDestroy {
    
    @Input() multiple: boolean = false;
    @Input() link: string      = null;
    @Input() links: string[]   = null;
    @Input() folder: string    = null;
    
    showPreloader: boolean      = false;
    percent: string             = null;
    isDestroyed$: Subject<void> = new Subject();
    
    @Input() _value = null;
    
    get value() {
        return this._value;
    }
    
    set value(val) {
        this._value = val;
        this.propagateChange(this._value);
    }
    
    propagateChange = (_: any) => {
    };
    
    registerOnChange(fn) {
        this.propagateChange = fn;
    }
    
    registerOnTouched() {
    }
    
    writeValue(value: any) {
        if (this.multiple) {
            this.links = value;
        } else {
            this.link = value;
        }
    }
    
    constructor(private uploaderService: UploaderService,
                private exceptionService: ExceptionService) {
    }
    
    handleFilesInput(event) {
        const files: FileList = event.target.files;
        if (this.multiple) {
            const _files = [];
            for (let i = 0; i < files.length; i++) {
                _files.push(files.item(i));
            }
            this.uploadFiles(_files);
        } else {
            this.uploadFile(files.item(0));
        }
    }
    
    uploadFiles(files: File[]) {
        this.showPreloader = true;
        this.uploaderService.uploadImages(files, this.folder)
        .pipe(
            tap((event) => {
                if (event.type === HttpEventType.UploadProgress) {
                    this.percent = ((event.loaded / event.total) * 100).toFixed(0);
                }
                
                if (event.type === HttpEventType.Response) {
                    this.percent = null;
                    const res    = event.body;
                    if (res.links) {
                        this.links = this.links ? this.links.concat(res.links) : res.links;
                    }
                    if (res.errors) {
                        res.errors.forEach(error => {
                            this.exceptionService.handleRequestErrorInfo(error);
                        });
                    }
                    this.showPreloader = false;
                    this.propagateChange(this.links);
                }
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                this.showPreloader = false;
                return EMPTY;
            }),
            takeUntil(this.isDestroyed$)
        )
        .subscribe();
    }
    
    uploadFile(file: File) {
        this.showPreloader = true;
        this.uploaderService.uploadImage(file, this.folder)
        .pipe(
            tap((event) => {
                if (event.type === HttpEventType.UploadProgress) {
                    this.percent = ((event.loaded / event.total) * 100).toFixed(0);
                }
                
                if (event.type === HttpEventType.Response) {
                    const res = event.body;
                    if (res.link) {
                        this.link = res.link;
                    }
                    this.percent       = null;
                    this.showPreloader = false;
                    this.propagateChange(this.link);
                }
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                this.showPreloader = false;
                return EMPTY;
            }),
            takeUntil(this.isDestroyed$)
        )
        .subscribe();
    }
    
    deleteFile(index: number = null) {
        if (index !== null) {
            // this.links = null;
            this.links.splice(index, 1);
            this.propagateChange(this.links);
        } else {
            this.link = null;
            this.propagateChange(null);
        }
    }
    
    ngOnDestroy(): void {
        this.isDestroyed$.next();
        this.isDestroyed$.complete();
    }
    
}
