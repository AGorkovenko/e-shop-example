import {Component, Input, OnInit} from "@angular/core";

@Component({
    selector: "app-loader",
    template: `
        <div class="preloader" *ngIf="show">
            <div class="preloader__spinner"></div>
            <div class="preloader__percent" *ngIf="percent">
                <div>{{percent}}%</div>
            </div>
        </div>
    `,
    // changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ["./preloader.component.scss"]
})
export class PreloaderComponent implements OnInit {
    @Input() show: boolean   = true;
    @Input() percent: string = null;
    
    constructor() {
    }
    
    ngOnInit() {
    }
    
}
