import {Component, Input, ChangeDetectionStrategy, OnChanges, SimpleChanges, Output, EventEmitter} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";

@Component({
    selector: "app-stars",
    template: `
      <div class="stars" [ngClass]="{'stars--active': active}">
        <div class="stars__star" *ngFor="let starVariant of stars; index as i;" [ngClass]="{'stars__star--filled': (i+1) <= rating}">
          <app-icon name="star"></app-icon>
        </div>
      </div>
  `
})
export class StarsComponent {
    
    @Input() rating: number = 0;
    @Input() active: boolean = false;
    @Output() changed: EventEmitter<number> = new EventEmitter();
    
    stars: Array<number> = Array(5);
    
    constructor(private logger: LoggerService) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    setRating(rating: number) {
        if (this.active) this.changed.emit(rating);
    }
}
