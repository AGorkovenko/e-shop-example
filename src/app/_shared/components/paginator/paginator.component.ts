import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from "@angular/core";
import {AsyncSubject} from "rxjs";
import {LoggerService} from "../../../_core/services/logger.service";

@Component({
    selector: "app-paginator",
    template: `
        <div class="pagination">
            <div class="pagination__more" *ngIf="isMoreButtonEnabled && isMoreButtonVisible">
                <button type="button" class="button button--outline-default button--uppercase button--inline" (click)="showMore()" i18n="Pagination more button@@paginationButtonMore">Show more ...</button>
            </div>
    
            <div class="pagination-pages">
                <div class="pagination-page pagination-page--first" *ngIf="isToTheFirstPageButtonVisible">
                    <a class="pagination-page-link" routerLink="./" [queryParams]="{page: firstPage}" queryParamsHandling="merge">{{firstPage}}</a>
                </div>

                <div class="pagination-page pagination-page--prev" *ngIf="isToTheFirstPageButtonVisible && prevPageButtons && prevPageButtons[0] === (firstPage + 2)">
                    <a class="pagination-page-link" routerLink="./" [queryParams]="{page: firstPage + 1}" queryParamsHandling="merge">{{firstPage + 1}}</a>
                </div>

                <div *ngIf="isToTheFirstPageButtonVisible && prevPageButtons && prevPageButtons[0] !== (firstPage + 1) && prevPageButtons[0] !== (firstPage + 2)" class="page-item">
                    ...
                </div>

                <div class="pagination-page" *ngFor="let pageNumber of prevPageButtons">
                    <a class="pagination-page-link" routerLink="./" [queryParams]="{page: pageNumber}" queryParamsHandling="merge">{{pageNumber}}</a>
                </div>

                <ng-container *ngIf="shownPages.length <= 3; else cuttedShownPages">
                    <div class="pagination-page pagination-page--active" *ngFor="let shownPage of shownPages">
                        <span class="pagination-page-link">{{shownPage}}</span>
                    </div>
                </ng-container>

                <div class="pagination-page" *ngFor="let pageNumber of nextPageButtons">
                    <a class="pagination-page-link" routerLink="./" [queryParams]="{page: pageNumber}" queryParamsHandling="merge">{{pageNumber}}</a>
                </div>

                <div *ngIf="isToTheLastPageButtonVisible && nextPageButtons && nextPageButtons[nextPageButtons.length - 1] !== (lastPage - 1) && nextPageButtons[nextPageButtons.length - 1] !== (lastPage - 2)"
                    class="pagination-page">
                    ...
                </div>

                <div class="pagination-page pagination-page--next" *ngIf="isToTheLastPageButtonVisible && nextPageButtons && nextPageButtons[nextPageButtons.length - 1] === (lastPage - 2)">
                    <a class="pagination-page-link" routerLink="./" [queryParams]="{page: lastPage - 1}" queryParamsHandling="merge">{{lastPage - 1}}</a>
                </div>

                <div class="pagination-page pagination-page--last" *ngIf="isToTheLastPageButtonVisible">
                    <a class="pagination-page-link" routerLink="./" [queryParams]="{page: lastPage}" queryParamsHandling="merge">{{lastPage}}</a>
                </div>
            </div>
        </div>

        <ng-template #cuttedShownPages>
            <div class="pagination-page pagination-page--active">
                <span class="pagination-page-link">{{shownPages[0]}}</span>
            </div>
            <div class="pagination-page">...</div>
            <div class="pagination-page pagination-page--active">
                <span class="pagination-page-link">{{shownPages.slice(-1)[0]}}</span>
            </div>
        </ng-template>
    `,
    styleUrls: ["./paginator.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaginatorComponent implements OnChanges {
    
    @Input() totalCount: number;
    @Input() currentPage: number;
    @Input() pageSize: number;
    @Input() numberOfNearestPages: number = 2;
    @Input() isMoreButtonEnabled: boolean;
    
    @Output() onShowMore: EventEmitter<{ page: number, callbackNotifier: AsyncSubject<void> }> = new EventEmitter();
    
    firstPage: number = 1;
    lastPage: number;
    shownPages: number[];
    prevPageButtons: number[];
    nextPageButtons: number[];
    isToTheFirstPageButtonVisible: boolean;
    isToTheLastPageButtonVisible: boolean;
    isMoreButtonVisible: boolean;
    
    isMoreButtonLoading: boolean;
    
    constructor(private logger: LoggerService,
                private cdr: ChangeDetectorRef) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    ngOnChanges(changes: SimpleChanges): void {
        this.lastPage   = Math.ceil(this.totalCount / this.pageSize);
        this.shownPages = [this.currentPage];
        
        this.calculatePageButtons();
        
        const nextPage: number   = this.shownPages.slice(-1)[0] + 1;
        this.isMoreButtonVisible = !(nextPage > this.lastPage);
    }
    
    calculatePageButtons() {
        // calculate prev Buttons
        this.prevPageButtons = [];
        for (let i = 1; i <= this.numberOfNearestPages; i++) {
            const page = this.shownPages[0] - i;
            if (page >= this.firstPage) {
                this.prevPageButtons.unshift(page);
            } else {
                break;
            }
        }
        // calculate next Buttons
        this.nextPageButtons = [];
        for (let i = 1; i <= this.numberOfNearestPages; i++) {
            const page = this.shownPages.slice(-1)[0] + i;
            if (page <= this.lastPage) {
                this.nextPageButtons.push(page);
            } else {
                break;
            }
        }
        
        // calculate max and min buttons
        this.isToTheFirstPageButtonVisible = this.prevPageButtons[0] != null && this.prevPageButtons[0] > this.firstPage;
        this.isToTheLastPageButtonVisible  = this.nextPageButtons.slice(-1)[0] != null && this.nextPageButtons.slice(-1)[0] < this.lastPage;
    }
    
    showMore() {
        this.isMoreButtonLoading                     = true;
        const nextPage: number                       = this.shownPages.slice(-1)[0] + 1;
        const onCompleteNotifier: AsyncSubject<void> = new AsyncSubject();
        onCompleteNotifier.subscribe(
            () => {
                this.shownPages.push(nextPage);
                this.calculatePageButtons();
                if (nextPage >= this.lastPage) {
                    this.isMoreButtonVisible = false;
                }
            },
            null,
            () => {
                this.isMoreButtonLoading = false;
                this.cdr.markForCheck();
            }
        );
        
        this.onShowMore.next({page: nextPage, callbackNotifier: onCompleteNotifier});
    }
    
}
