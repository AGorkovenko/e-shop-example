import {DOCUMENT, isPlatformBrowser} from "@angular/common";
import {ChangeDetectionStrategy, ChangeDetectorRef, Component, ContentChild, ElementRef, EventEmitter, HostListener, Inject, Input, OnDestroy, OnInit, Output, PLATFORM_ID} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";
import {LayoutManipulationService} from "../../../_core/services/layout-manipulation.service";

@Component({
    selector: "app-modal",
    template: `
         <div class="window" [ngClass]="{'modal-right': right}" tabindex="-1">
            <div class="window__wrap" [ngClass]="calculateCssClasses()">
                <div class="box">
                    <div *ngIf="header" class="window-head">
                        <ng-content select="appModalHeader"></ng-content>
                    </div>
                    <div *ngIf="body" class="window-body">
                        <ng-content select="appModalBody"></ng-content>
                    </div>
                    <div *ngIf="footer" class="window-footer">
                        <ng-content select="appModalFooter"></ng-content>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="modal-backdrop" [ngClass]="[!isGlobal ? 'modal-backdrop&#45;&#45;local' : '']"></div> -->
    `,
    styleUrls: ["./modal.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModalComponent implements OnInit, OnDestroy {
    
    @ContentChild("appModalHeader") header;
    @ContentChild("appModalBody") body;
    @ContentChild("appModalFooter") footer;
    
    @Input() baseElement: HTMLElement;
    @Input() size: "sm" | "md" | "lg"; // different width of the modal content
    @Input() compact: boolean; // smaller paddings
    @Input() centered: boolean; // vertical centering of the modal
    @Input() right: boolean                         = true; // like a sidebar
    @Output() onOpened: EventEmitter<void>          = new EventEmitter();
    @Output() onBackgroundClick: EventEmitter<void> = new EventEmitter();
    @Output() onEscape: EventEmitter<void>          = new EventEmitter();
    isGlobal: boolean;
    
    constructor(private logger: LoggerService,
                private elementRef: ElementRef,
                private cdr: ChangeDetectorRef,
                private layoutManipulationService: LayoutManipulationService,
                @Inject(PLATFORM_ID) private platformId: Object,
                @Inject(DOCUMENT) private document) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    ngOnInit() {
        this.isGlobal = !this.baseElement;
        
        this.layoutManipulationService.enableScrollbarCompensation(
            this.isGlobal ? this.document.body : this.elementRef.nativeElement
        );
        
        if (this.isGlobal) {
            if (isPlatformBrowser(this.platformId)) {
                this.document.body.appendChild(this.elementRef.nativeElement);
                setTimeout(() => {
                    this.document.body.classList.add("window-open");
                    this.layoutManipulationService.compensateScrollbar(true);
                });
            }
        } else {
            setTimeout(() => {
                this.baseElement.appendChild(this.elementRef.nativeElement);
                setTimeout(() => {
                    this.elementRef.nativeElement.classList.add("window-open");
                    this.layoutManipulationService.compensateScrollbar(true);
                });
            });
        }
        setTimeout(() => this.onOpened.emit());
        
        this.document.body.appendChild(this.elementRef.nativeElement);
    }
    
    @HostListener("click", ["$event"])
    backgroundClick(event) {
        if (event.target.classList.contains("window")) {
            this.onBackgroundClick.next();
        }
    }
    
    @HostListener("document:keydown.escape", ["$event"])
    onEscapeKeydownHandler(event: KeyboardEvent) {
        this.onEscape.next();
    }
    
    calculateCssClasses(): {} {
        return {
            "window--local": !this.isGlobal,
            "window--compact": this.compact,
            "window-dialog-centered": this.centered,
            [`window-${this.size}`]: this.size
        };
    }
    
    ngOnDestroy(): void {
        if (this.isGlobal && isPlatformBrowser(this.platformId)) {
            this.document.body.classList.remove("window-open");
        }
        
        this.layoutManipulationService.compensateScrollbar(false);
        this.elementRef.nativeElement.remove();
    }
    
}
