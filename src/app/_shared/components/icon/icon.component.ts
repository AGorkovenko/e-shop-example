import {ChangeDetectionStrategy, Component, Inject, Input, LOCALE_ID} from "@angular/core";

@Component({
	selector: "app-icon",
	template: `
        <svg [attr.class]="'icon icon--' + name + ' ' + classes">
            <use [attr.xlink:href]="localePath + '/assets/images/store.svg#icon-' + name"/>
        </svg>
        <ng-content></ng-content>
	`,
	changeDetection: ChangeDetectionStrategy.OnPush,
	styleUrls: ["./icon.component.scss"]
})
export class IconComponent {
	@Input() name: string;
	@Input() classes: string = "";
	
	localePath: string;
	
	constructor(@Inject(LOCALE_ID) locale: string) {
		this.localePath = locale ? "/" + locale : "";
	}
}
