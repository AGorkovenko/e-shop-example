export interface TagsOption {
    label: string;
    labelSecond?: string;
    value: any;
}
