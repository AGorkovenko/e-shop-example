import {Component, EventEmitter, forwardRef, Input, Output} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";
import {TagsOption} from "./tags-option.enum";

@Component({
    selector: "app-tags",
    template: `
        <div class="form-control select-wrap" [ngClass]="{'select-wrap--open': open}" (appClickOutside)="closeList()">
            <div class="select-wrap__value" (click)="openList()">
                <span class="select-tag" *ngFor="let selectedItem of selected;">
	                {{selectedItem.label}}
                    <span *ngIf="selectedItem.labelSecond">({{selectedItem.labelSecond}})</span>
	                <span class="iconsmind-Close" (click)="select(selectedItem)"></span>
                </span>
                <span *ngIf="selected.length <= 0">{{placeholder}}</span>
            </div>
            <div class="select-wrap__caret">
                <span class="icon-arrow-down"></span>
            </div>
            <div class="select-wrap__search">
                <input type="text" [(ngModel)]="querySearch" class="select-wrap__search-field" [placeholder]="searchPlaceholder" (keyup)="querySearchChanged()">
            </div>
            <div class="select-wrap__options select-wrap__options--lifesearch">
                <div class="select-wrap__option" *ngFor="let option of options; index as index" (click)="select(option)">
                    {{option.label}}
                    <div *ngIf="option.labelSecond" class="select-wrap__option-second">{{option.labelSecond}}</div>
                </div>
                <div class="select-wrap__default" *ngIf="!options || (options && options.length <= 0)">{{default}}</div>
            </div>
        </div>
    `,
    styleUrls: ["./tags.component.scss"],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => TagsComponent),
            multi: true
        }
    ]
})
export class TagsComponent implements ControlValueAccessor {
    
    querySearch: any = null;
    
    @Input() options: TagsOption[];
    @Input() placeholder: string       = "Select your option";
    @Input() searchPlaceholder: string = "Input some symbols";
    @Input() default: string           = "No options for you";
    @Input() fullOption: boolean       = false;
    
    selected: TagsOption[] = [];
    @Input() _value;
    
    open: boolean          = false;
    @Output() opened       = new EventEmitter<boolean>();
    @Output() changed      = new EventEmitter<any>();
    @Output() queryChanged = new EventEmitter<any>();
    
    get value() {
        return this._value;
    }
    
    set value(val) {
        this._value = val;
        this.propagateChange(this._value);
    }
    
    propagateChange = (_: any) => {
    };
    
    registerOnChange(fn) {
        this.propagateChange = fn;
    }
    
    registerOnTouched() {
    }
    
    writeValue(value: any) {
        this.selected = value ? value : [];
    }
    
    constructor(private logger: LoggerService) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    select(event: TagsOption) {
        this.selected = this.addInSelect(event);
        this.value    = this.selected;
        
        this.propagateChange(this.selected);
        this.openList();
        this.opened.emit(this.open = false);
        this.changed.emit(this.selected);
    }
    
    openList() {
        this.open = !this.open;
        this.opened.emit(this.open);
    }
    
    closeList() {
        this.opened.emit(this.open = false);
    }
    
    querySearchChanged() {
        this.queryChanged.emit(this.querySearch);
    }
    
    addInSelect(option: TagsOption) {
        let _selected = this.selected;
        if (_selected.find((item: TagsOption) => item.value === option.value)) {
            _selected = _selected.filter((item: TagsOption) => item.value !== option.value);
        } else {
            _selected.push(option);
        }
        return _selected;
    }
}
