export interface SelectMultipleOption {
    label: string;
    value: any;
    children?: SelectMultipleOption[];
}
