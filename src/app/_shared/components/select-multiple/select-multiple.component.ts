import {Component, EventEmitter, forwardRef, Input, OnInit, Output} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";
import {SelectMultipleOption} from "./select-multiple-option.enum";
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
    selector: "app-select-multiple",
    template: `
        <div class="form-control select-multiple-wrap">
            <div class="select-multiple__options" *ngIf="options">
                <app-select-multiple-options [options]="options" (select)="selectOption($event)" [selected]="selected"></app-select-multiple-options>
            </div>
        </div>
    `,
    styleUrls: ["./select-multiple.component.scss"],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => SelectMultipleComponent),
            multi: true
        }
    ]
})
export class SelectMultipleComponent implements ControlValueAccessor {
    @Input() options: SelectMultipleOption[] = [];
    @Input() _value;
    selected: any[]                          = [];
    @Output() changed                        = new EventEmitter<SelectMultipleOption>();
    
    constructor(private logger: LoggerService) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    get value() {
        return this._value;
    }
    
    set value(val) {
        this._value = val;
        this.propagateChange(this._value);
    }
    
    propagateChange = (_: any) => {
    };
    
    registerOnChange(fn) {
        this.propagateChange = fn;
    }
    
    registerOnTouched() {
    }
    
    writeValue(value: any) {
        this.selected = value ? value : [];
    }
    
    selectOption(event: SelectMultipleOption) {
        this.selected = this.addInSelect(event.value);
        this.value    = this.selected;
        this.propagateChange(this.value);
        this.changed.emit(event);
    }
    
    addInSelect(option: any) {
        let _selected = this.selected;
        if (_selected.find(item => item === option)) {
            _selected = _selected.filter(item => item !== option);
        } else {
            _selected.push(option);
        }
        return _selected;
    }
}
