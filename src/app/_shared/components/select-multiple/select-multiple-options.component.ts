import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {SelectMultipleOption} from "./select-multiple-option.enum";

@Component({
    selector: "app-select-multiple-options",
    template: `
        <div class="select-multiple-option" *ngFor="let option of options">
            <div class="select-multiple-option__name" [ngClass]="{'select-multiple-option__name--selected': checkOption(option.value)}" (click)="selectOption(option)">{{option.label}}</div>
            <app-select-multiple-options [options]="option.children" (select)="selectOption($event)" *ngIf="option.children" [selected]="selected"></app-select-multiple-options>
        </div>
    `,
    styleUrls: ["./select-multiple-options.component.scss"]
})

export class SelectMultipleOptionsComponent {
    
    @Input() selected: any = [];
    @Input() options: SelectMultipleOption[];
    @Output() select       = new EventEmitter<SelectMultipleOption>();
    
    constructor() {
    }
    
    checkOption(value): boolean {
        return this.selected.find(item => item === value);
    }
    
    selectOption(option: SelectMultipleOption) {
        this.select.emit(option);
    }
}
