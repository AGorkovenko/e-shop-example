export interface SelectOption {
  label: string;
  value: any;
  labelSecond?: string;
  valueSecond?: any;
}
