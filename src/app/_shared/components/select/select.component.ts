import {Component, EventEmitter, forwardRef, Input, Output} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";
import {SelectOption} from "./select-option.enum";
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
  selector: "app-select",
  template: `
    <div class="form-element select" [ngClass]="{'select--open': open, 'search--life': lifeSearch}" appClickOutside (clickOutside)="closeList($event)">
      <div class="select__value" *ngIf="fullOption" (click)="openList()">
        <div class="select__value-first">{{selected ? selected.label : placeholder}}</div>
        <div class="select__value-second" *ngIf="selected.labelSecond">{{selected.labelSecond}}</div>
      </div>
      <div class="select__value" *ngIf="!fullOption" (click)="openList()">
        <div class="select__value-first">{{selected ? selected.label : placeholder}}</div>
      </div>
      <div class="select__caret">
        <app-icon name="toggle"></app-icon>
      </div>
      <div class="select__search" *ngIf="lifeSearch">
        <input class="form-element" type="text" [(ngModel)]="querySearch" [placeholder]="searchPlaceholder" (keyup)="querySearchChanged()" [disabled]="disabled">
      </div>
      <div class="select__options" [ngClass]="{'select__options--lifesearch': lifeSearch}">
        <div class="select__option" *ngFor="let option of options; index as index" (click)="select(index)" [ngClass]="{'select__option--active': option === selected}">
          <div class="select__option-fisrt">{{option.label}}</div>
          <div class="select__option-second" *ngIf="option.labelSecond">{{option.labelSecond}}</div>
        </div>
        <div class="select__default" *ngIf="!options || (options && options.length <= 0)">{{default}}</div>
      </div>
    </div>
  `,
  styleUrls: ["./select.component.scss"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectComponent),
      multi: true
    }
  ]
})
export class SelectComponent implements ControlValueAccessor {
  
  @Input() lifeSearch: boolean = false;
  querySearch: any = null;
  
  disabled: boolean = false;
  
  @Input() options: SelectOption[];
  @Input() placeholder: string = "Select your option";
  @Input() searchPlaceholder: string = "Input some symbols";
  @Input() default: string = "No options for you";
  @Input() fullOption: boolean = false;
  
  selected: SelectOption = null;
  @Input() _value = null;
  
  open: boolean = false;
  @Output() opened = new EventEmitter<boolean>();
  @Output() changed = new EventEmitter<SelectOption>();
  @Output() queryChanged = new EventEmitter<any>();
  
  get value() {
    return this._value;
  }
  
  set value(val) {
    this._value = val;
    this.propagateChange(this._value);
  }
  
  propagateChange = (_: any) => {
  };
  
  registerOnChange(fn) {
    this.propagateChange = fn;
  }
  
  registerOnTouched() {
  }
  
  writeValue(value: any) {
    this.selected = value ? value : {label: this.placeholder, value: null};
  }
  
  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
  
  constructor(private logger: LoggerService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  select(i: number) {
    this.selected = this.options[i];
    this.value = this.options[i];
    this.propagateChange(this.selected);
    this.openList();
    this.opened.emit(this.open = false);
    this.changed.emit(this.selected);
  }
  
  openList() {
    if (!this.disabled) {
      this.open = !this.open;
      this.opened.emit(this.open);
    }
  }
  
  closeList(event) {
    this.opened.emit(this.open = false);
  }
  
  querySearchChanged() {
    this.queryChanged.emit(this.querySearch);
  }
  
}
