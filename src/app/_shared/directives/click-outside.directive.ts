import {Directive, ElementRef, EventEmitter, HostListener, Output} from "@angular/core";

@Directive({
  selector: "[appClickOutside]",
  exportAs: "appClickOutside"
})
export class ClickOutsideDirective {

  @Output() clickOutside = new EventEmitter();

  @HostListener("document:click", ["$event.target"])
  onClick(targetElement) {
    const clickedInside = this._elementRef.nativeElement.contains(targetElement);
    if (!clickedInside) {
      this.clickOutside.emit(null);
    }
  }

  constructor(private _elementRef: ElementRef) {
  }

}
