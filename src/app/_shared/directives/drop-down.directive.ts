import {Directive, ElementRef, HostBinding, HostListener, Input} from "@angular/core";

@Directive({
    selector: "[appDropDown]",
    exportAs: "appDropDown"
})
export class DropDownDirective {
    
    isOpen: boolean                       = false;
    @Input("class") defClass: string;
    @Input("openClass") openClass: string = "open";
    
    constructor(private _elementRef: ElementRef) {
    }
    
    @HostBinding("class") get class(): string {
        return this.isOpen ? this.defClass + " " + this.openClass : this.defClass;
    }
    
    @HostListener("document:closeDropDown", ["$event", "$event.detail"]) closeDropDown(event: Event, detail) {
        if (detail) {
            this.isOpen = false;
        }
    }
    
    @HostListener("document:click", ["$event", "$event.target"]) onClick(event, targetElement) {
        this.isOpen = this._elementRef.nativeElement.contains(targetElement);
    }
}
