import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";

const routes: Routes = [
    {
        path: "logout",
        loadChildren: "./logout/logout.module#LogoutModule",
        data: {
            title: "User logout"
        }
    },
    {
        path: "email",
        loadChildren: "./email/email.module#EmailModule",
        data: {
          title: "Verification E-mail"
        }
     },
    {
        path: "",
        loadChildren: "./main/main.module#MainModule",
        data: {
            title: "GTI Power Sport Parts"
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {initialNavigation: "enabled"})
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {
}
