import {Component, OnDestroy, OnInit} from "@angular/core";
import {LoggerService} from "../_core/services/logger.service";
import {CartService} from "../_core/services/cart.service";
import {Observable, Subject} from "rxjs";
import {Cart} from "../_core/models/dto/cart/cart.model";
import {Currency} from "../_core/models/enum/currency.enum";
import {CurrencyService} from "../_core/services/currency.service";
import {takeUntil, tap} from "rxjs/operators";

@Component({
    selector: "app-cart",
    template: `
        <div class="container">
            <div class="box">
                <div class="cart">
                    <h1 i18n="@@cartPageTitle">Cart</h1>

                    <div *ngIf="cart$ | async as cart">
                        <table class="cart-table" *ngIf="cart.items.length; else emptyCart">
                            <thead>
                            <tr>
                                <th i18n="@@cartTableHeadProduct">Product</th>
                                <th i18n="@@cartTableHeadSku">SKU</th>
                                <th i18n="@@cartTableHeadPrice">Price</th>
                                <th i18n="@@cartTableHeadVat">VAT (19%)</th>
                                <th i18n="@@cartTableHeadQuantity">Quantity</th>
                                <th i18n="@@cartTableHeadTotal">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr *ngFor="let item of cart.items">
                                <td class="cart-table__product">
                                    <div class="cart-product">
                                        <div class="cart-product__delete" (click)="delete(item.ID)">
                                            <app-icon name="delete"></app-icon>
                                        </div>
                                        <div class="cart-product__thumb">
                                            <div class="image">
                                                <img [alt]="item.title" *ngIf="item.photo" [src]="item.photo + '/thumb'">
                                                <img [alt]="item.title" *ngIf="!item.photo" src="assets/images/placeholder.jpg">
                                            </div>
                                        </div>
                                        <div class="cart-product__info">
                                            <a class="cart-product-name" [routerLink]="['/shop/product/' + item.slug]">{{item.title}}</a>
                                            <div class="cart-product-sku" i18n="@@cartTableBodySku" *ngIf="item.sku">SKU by GTI: {{item.sku}}</div>
                                        </div>
                                    </div>
                                </td>
                                <td class="cart-table__sku">{{item.original_sku}}</td>
                                <td class="cart-table__price">{{currencyService.currencyExchange(item.new_price) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}</td>
                                <td class="cart-table__vat">{{currencyService.currencyExchange(item.new_price * vat) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}</td>
                                <td class="cart-table__quantity">
                                    <div class="product-quantity">
                                        <button class="product-quantity__button" (click)="update(item.ID, item.quantity - 1, item.pid)">-</button>
                                        <input class="product-quantity__input" type="number" disabled [value]="item.quantity">
                                        <button class="product-quantity__button" (click)="update(item.ID, item.quantity + 1, item.pid)">+</button>
                                    </div>
                                </td>
                                <td class="cart-table__price">{{currencyService.currencyExchange(item.total) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}</td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td class="cart-table__subtotal" colspan="4" i18n="@@cartTableFooterSubtotal">Subtotal</td>
                                <td></td>
                                <td class="cart-table__price cart-table__price--subtotal">{{currencyService.currencyExchange(cart.subtotal) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}</td>
                            </tr>
                            <tr>
                                <td class="cart-table__subtotal" colspan="4" i18n="@@cartTableFooterVat">VAT (19%)</td>
                                <td></td>
                                <td class="cart-table__price cart-table__price--subtotal">{{currencyService.currencyExchange(cart.subtotal * vat) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}</td>
                            </tr>
                            <tr>
                                <td class="cart-table__total" colspan="4" i18n="@@cartTableFooterTotal">Total</td>
                                <td></td>
                                <td class="cart-table__price cart-table__price--total">{{currencyService.currencyExchange(cart.total) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}</td>
                            </tr>
                            </tfoot>
                        </table>
                        <div class="cart-next">
                            <a class="button button--inline button--outline-dark" [routerLink]="['/shop']" i18n="@@cartPageBackToShoping">Back to shoping</a>
                            <a class="button button--inline button--danger" [routerLink]="['/checkout']" i18n="@@cartPageProceedToCheckout" *ngIf="cart.items.length">Proceed to checkout</a>
                        </div>
                    </div>
                    <ng-template #emptyCart>
                        <div class="text-center pt pb" i18n="@@cartPageYourCartIsEmpty">Your cart is empty</div>
                    </ng-template>
                </div>
            </div>
        </div>
    `
})
export class CartComponent implements OnInit, OnDestroy {
    
    cart$: Observable<Cart>;
    currentCurrency: Currency;
    currency = Currency;
    
    vat: number = 0.19;
    
    isDestroyed$: Subject<void> = new Subject();
    
    constructor(private logger: LoggerService,
                private cartService: CartService,
                public currencyService: CurrencyService) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    ngOnInit() {
        this.cart$ = this.cartService.cart$;
        
        this.currencyService.currentCurrency$
        .pipe(
            tap((currency: Currency) => {
                this.currentCurrency = currency;
            }),
            takeUntil(this.isDestroyed$)
        )
        .subscribe();
    }
    
    delete(id: number) {
        this.cartService.deleteFromCart(id)
        .pipe(takeUntil(this.isDestroyed$))
        .subscribe();
    }
    
    update(id: number, quantity: number, pid: number) {
        this.cartService.updateCart(id, quantity, pid)
        .pipe(takeUntil(this.isDestroyed$))
        .subscribe();
    }
    
    ngOnDestroy(): void {
        this.isDestroyed$.next();
        this.isDestroyed$.complete();
    }
}
