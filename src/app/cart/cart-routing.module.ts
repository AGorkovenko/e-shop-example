import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {CartComponent} from "./cart.component";

const routes: Routes = [
    {
        path: "",
        component: CartComponent,
        resolve: {
            // email: EmailResolver
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [
    ]
})
export class CartRoutingModule {
}
