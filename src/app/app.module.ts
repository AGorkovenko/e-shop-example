import {BrowserModule, BrowserTransferStateModule} from "@angular/platform-browser";
import {LOCALE_ID, NgModule} from "@angular/core";

import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "./app.component";
import {CoreModule} from "./_core/core.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

@NgModule({
    imports: [
        BrowserModule.withServerTransition({appId: "app-site"}),
        BrowserTransferStateModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        CoreModule
    ],
    declarations: [
        AppComponent
    ],
    providers: [
        {
            provide: LOCALE_ID,
            useValue: null
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
