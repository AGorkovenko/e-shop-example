import {Injectable} from "@angular/core";
import {LoggerService} from "./logger.service";
import {BehaviorSubject, EMPTY, Observable, Subscription} from "rxjs";
import {HttpCachingClient} from "./http-caching-client.service";
import {CONFIG} from "../config";
import {catchError, map, tap} from "rxjs/operators";
import {Country} from "../models/dto/country.model";
import {SelectOption} from "../../_shared/components/select/select-option.enum";
import {DeliveryType} from "../models/enum/delivery-type.enum";
import {Payment} from "../models/dto/payment/payment.enum";

@Injectable()
export class CountryService {
	
	COUNTRY_URL: string = "/country";
	
	availableCountries: any[] = [
		{code: "BY", delivery: [DeliveryType.EXPRESS_RU, DeliveryType.PONY_EXPRESS], payment: [Payment.INVOICE, Payment.VISA_MASTERCARD]},
		{code: "GE", delivery: [DeliveryType.EXPRESS_RU, DeliveryType.PONY_EXPRESS], payment: [Payment.INVOICE, Payment.VISA_MASTERCARD]},
		{code: "KZ", delivery: [DeliveryType.EXPRESS_RU, DeliveryType.PONY_EXPRESS], payment: [Payment.INVOICE, Payment.VISA_MASTERCARD]},
		{code: "KG", delivery: [DeliveryType.EXPRESS_RU, DeliveryType.PONY_EXPRESS], payment: [Payment.INVOICE, Payment.VISA_MASTERCARD]},
		{code: "RU", delivery: [DeliveryType.EXPRESS_RU, DeliveryType.PONY_EXPRESS], payment: [Payment.INVOICE, Payment.VISA_MASTERCARD]},
		{code: "UA", delivery: [DeliveryType.PICKUP, DeliveryType.NOVA_POSHTA], payment: [Payment.CASH, Payment.PRIVAT_BANK]},
		{code: "UZ", delivery: [DeliveryType.EXPRESS_RU, DeliveryType.PONY_EXPRESS], payment: [Payment.INVOICE, Payment.VISA_MASTERCARD]}
	];
	
	private countriesSource: BehaviorSubject<Country[]> = new BehaviorSubject([]);
	countries$: Observable<Country[]> = this.countriesSource.asObservable();
	
	constructor(private logger: LoggerService,
	            private httpClient: HttpCachingClient) {
		this.logger.debug(this.constructor.name + " is created.");
	}
	
	getCountries(): Observable<Country[]> {
		const url = CONFIG.apiUrl + this.COUNTRY_URL;
		return this.httpClient.get<Country[]>(url)
			.pipe(
				tap(countries => {
					this.logger.debug("Countries loaded");
					this.countriesSource.next(countries);
				}),
				catchError(errorInfo => {
					this.logger.debug("Error of loading countries", errorInfo);
					return EMPTY;
				})
			);
	}
	
	loadCountries(): Subscription {
		const url = CONFIG.apiUrl + this.COUNTRY_URL;
		return this.httpClient.get<Country[]>(url)
			.pipe(
				tap(countries => {
					this.logger.debug("Countries loaded");
					this.countriesSource.next(countries);
				}),
				catchError(errorInfo => {
					this.logger.debug("Error of loading countries", errorInfo);
					return EMPTY;
				})
			)
			.subscribe();
	}
	
	getCountryOptions(): Observable<SelectOption[]> {
		return this.httpClient.get(CONFIG.apiUrl + this.COUNTRY_URL)
			.pipe(
				map((countries: Country[]) => {
					return countries
						.filter((country: Country) => {
							return this.availableCountries.some((availableCountry) => country.code === availableCountry.code);
						})
						.map<SelectOption>((country: Country) => {
							return {label: country.name, value: country.code};
						});
				}),
				catchError(errorInfo => {
					this.logger.debug("Error of loading countries", errorInfo);
					return EMPTY;
				})
			);
	}
	
	availableDeliveryPayment(deliveryCode: string, countryCode: string): boolean {
		const availableCountryDelivery = this.availableCountries.find((availableCountry) => countryCode === availableCountry.code).delivery;
		return availableCountryDelivery.some(availableDelivery => deliveryCode === availableDelivery);
	}
	
	availableCountryPayment(paymentCode: string, countryCode: string): boolean {
		const availableCountryPayment = this.availableCountries.find((availableCountry) => countryCode === availableCountry.code).payment;
		return availableCountryPayment.some(availablePayment => paymentCode === availablePayment);
	}
	
	// getCountryName(code: string): string {
	// 	const currentLanguage = this.countriesSource.getValue() ? this.countriesSource.getValue().filter((language: Language) => code == language.code).pop() : null;
	// 	return currentLanguage ? currentLanguage.name : null;
	// }
}
