import {Injectable} from "@angular/core";
import {HttpCachingClient} from "../http-caching-client.service";
import {LoggerService} from "../logger.service";
import {Observable} from "rxjs";
import {HttpParams} from "@angular/common/http";
import {CONFIG} from "../../config";
import {ContentViewRequest} from "../../models/dto/content/content-view-request.model";
import {Menu} from "../../models/dto/menu/menu.model";
import {MenuViewRequest} from "../../models/dto/menu/menu-view-request.model";
import {MenuItem} from "../../models/dto/menu/menu-item.model";
import {MenuItemRequest} from "../../models/dto/menu/menu-item-request.model";
import {MenuRequest} from "../../models/dto/menu/menu-view.model";
import {ItemOrder} from "../../models/dto/item-order.model";

@Injectable()
export class MenuBackendClient {
    
    MENUS_URL            = "/menu";
    MENUS_COUNT_URL      = "/menu/count";
    MENU_URL             = "/menu/{id}";
    MENU_SLUG_URL             = "/menu/slug/{slug}";
    MENU_URL_TRANSLATION = "/menu/{id}/translation";
    MENU_ITEM_URL        = "/menu/{id}/item";
    MENU_SLUG_ITEM_URL        = "/menu/slug/{slug}/item";
    MENU_SORT_URL        = "/menu/{id}/sort";
    MENU_ITEM_EDIT_URL   = "/menu/{id}/item/{itemId}";
    
    constructor(private logger: LoggerService,
                private httpClient: HttpCachingClient) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    loadMenus(viewRequest?: MenuViewRequest): Observable<Menu[]> {
        const url              = CONFIG.apiUrl + this.MENUS_URL;
        let params: HttpParams = new HttpParams();
        if (!viewRequest) {
            viewRequest = new ContentViewRequest();
        }
        const offset = (viewRequest.page - 1) * viewRequest.pageSize;
        params       = params.set("offset", offset.toString());
        params       = params.set("limit", viewRequest.pageSize.toString());
        
        return this.httpClient.get(url, params);
    }
    
    loadCount(viewRequest: MenuViewRequest): Observable<number> {
        const url = CONFIG.apiUrl + this.MENUS_COUNT_URL;
        return this.httpClient.get<number>(url);
    }
    
    loadMenu(id: number): Observable<Menu> {
        const url = CONFIG.apiUrl + this.MENU_SLUG_URL.replace("{id}", id.toString());
        return this.httpClient.get<Menu>(url);
    }
    
    loadMenuBySlug(slug: string): Observable<Menu> {
        const url = CONFIG.apiUrl + this.MENU_URL.replace("{slug}", slug);
        return this.httpClient.get<Menu>(url);
    }
    
    loadMenuTranslations(id: number): Observable<any> {
        const url = CONFIG.apiUrl + this.MENU_URL_TRANSLATION.replace("{id}", id.toString());
        return this.httpClient.get<any>(url);
    }
    
    loadMenuItems(id: number, lang?: string): Observable<MenuItem[]> {
        let params = new HttpParams();
        if (lang) {
            params = params.set("lang", lang);
        }
        const url = CONFIG.apiUrl + this.MENU_ITEM_URL.replace("{id}", id.toString());
        return this.httpClient.get<MenuItem[]>(url, params);
    }
    
    loadMenuItemsBySlug(slug: string, lang?: string): Observable<MenuItem[]> {
        let params = new HttpParams();
        if (lang) {
            params = params.set("lang", lang);
        }
        const url = CONFIG.apiUrl + this.MENU_SLUG_ITEM_URL.replace("{slug}", slug);
        return this.httpClient.get<MenuItem[]>(url, params);
    }
}
