import {Injectable} from "@angular/core";
import {LoggerService} from "../logger.service";
import {Menu} from "../../models/dto/menu/menu.model";
import {HttpCachingClient} from "../http-caching-client.service";
import {BehaviorSubject, EMPTY, Observable} from "rxjs";
import {catchError, map, tap} from "rxjs/operators";
import {MenuViewRequest} from "../../models/dto/menu/menu-view-request.model";
import iassign from "immutable-assign";
import {ExceptionService} from "../exception.service";
import {MenuBackendClient} from "./menu-backend.service";
import {NotificationService} from "../notification.service";
import {MenuItem} from "../../models/dto/menu/menu-item.model";

export class MenuListData {
    viewRequest: MenuViewRequest;
    list: Menu[];
    count: number;
}

@Injectable()
export class MenuService {
    
    private menuListDataSource: BehaviorSubject<MenuListData> = new BehaviorSubject(null);
    menuListData$: Observable<MenuListData>                   = this.menuListDataSource.asObservable();
    
    constructor(private logger: LoggerService,
                private httpClient: HttpCachingClient,
                private notificationService: NotificationService,
                private exceptionService: ExceptionService,
                private menuBackend: MenuBackendClient) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    initMenuList(menuListData: MenuListData) {
        this.menuListDataSource.next(menuListData);
    }
    
    loadAndAppendPage(page: number): Observable<void> {
        const viewRequest = Object.assign({}, this.menuListDataSource.getValue().viewRequest);
        viewRequest.page  = page;
        
        return this.menuBackend.loadMenus(viewRequest)
        .pipe(
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            }),
            tap((items: Menu[]) => {
                const updatedMenuListData: MenuListData = iassign(
                    this.menuListDataSource.getValue(),
                    listData => listData.list,
                    list => {
                        list.push(...items);
                        return list;
                    }
                );
                this.menuListDataSource.next(updatedMenuListData);
            }),
            map(() => {
            })
        );
    }
    
    getMenu(id: number): Observable<Menu> {
        return this.menuBackend.loadMenu(id)
        .pipe(
            tap((menu: Menu) => {
                this.notificationService.showSuccess("Menu was loaded", "Load menu");
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            })
        );
    }
    
    getMenuBySlug(slug: string): Observable<Menu> {
        return this.menuBackend.loadMenuBySlug(slug)
        .pipe(
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            })
        );
    }
    
    getMenuItems(id: number, lang?: string): Observable<MenuItem[]> {
        return this.menuBackend.loadMenuItems(id, lang)
        .pipe(
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            })
        );
    }
    
    getMenuItemsBySlug(slug: string, lang?: string): Observable<MenuItem[]> {
        return this.menuBackend.loadMenuItemsBySlug(slug, lang)
        .pipe(
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            })
        );
    }
}
