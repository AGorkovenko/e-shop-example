import {Injectable} from "@angular/core";
import {HttpCachingClient} from "../http-caching-client.service";
import {LoggerService} from "../logger.service";
import {Observable} from "rxjs";
import {HttpParams} from "@angular/common/http";
import {CONFIG} from "../../config";
import {Site} from "../../models/dto/site/site.model";
import {SiteViewRequest} from "../../models/dto/site/site-view-request.model";

@Injectable()
export class SiteBackendClient {
  
  SITE_URL       = "/site";
  SITE_ID_URL    = "/site/{id}";
  SITE_COUNT_URL = "/site/count";
  
  constructor(private logger: LoggerService,
              private httpClient: HttpCachingClient) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  loadSites(viewRequest?: SiteViewRequest): Observable<Site[]> {
    const url              = CONFIG.apiUrl + this.SITE_URL;
    let params: HttpParams = new HttpParams();
    if (!viewRequest) {
      viewRequest = new SiteViewRequest();
    }
    if (viewRequest.order != null) {
      params = params.set("order", viewRequest.order);
    }
    if (viewRequest.sort != null) {
      params = params.set("sort", viewRequest.sort);
    }
    const offset = (viewRequest.page - 1) * viewRequest.pageSize;
    params       = params.set("offset", offset.toString());
    params       = params.set("limit", viewRequest.pageSize.toString());
    
    return this.httpClient.get(url, params);
  }
  
  loadCount(): Observable<number> {
    const url = CONFIG.apiUrl + this.SITE_COUNT_URL;
    return this.httpClient.get<number>(url);
  }
  
  loadSite(id: number): Observable<Site> {
    const url = CONFIG.apiUrl + this.SITE_ID_URL.replace("{id}", id.toString());
    return this.httpClient.get(url);
  }
  
  addSite(request): Observable<number> {
    const url  = CONFIG.apiUrl + this.SITE_URL;
    const body = {
      name: request.name,
      url: request.url,
      lang: request.lang
    };
    
    return this.httpClient.post<number>(url, body);
  }
  
  updateSite(request): Observable<Site> {
    const url  = CONFIG.apiUrl + this.SITE_ID_URL.replace("{id}", request.id.toString());
    const body = {
      name: request.name,
      url: request.url,
      lang: request.lang
    };
    return this.httpClient.post<Site>(url, body);
  }
  
  deleteSite(id: number): Observable<boolean> {
    const url = CONFIG.apiUrl + this.SITE_ID_URL.replace("{id}", id.toString());
    return this.httpClient.delete<boolean>(url);
  }
}
