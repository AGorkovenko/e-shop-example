import {Injectable} from "@angular/core";
import {BehaviorSubject, EMPTY, Observable, Subscription} from "rxjs";
import {catchError, map, tap} from "rxjs/operators";
import {LoggerService} from "../logger.service";
import {ExceptionService} from "../exception.service";
import iassign from "immutable-assign";
import {NotificationService} from "../notification.service";
import {Site} from "../../models/dto/site/site.model";
import {SiteViewRequest} from "../../models/dto/site/site-view-request.model";
import {SiteRequest} from "../../models/dto/site/site-request.model";
import {SiteBackendClient} from "./site-backend.service";
import {CONFIG} from "../../config";
import {CookieService} from "../cookie.service";
import {Settings} from "../../models/dto/settings.model";
import {SelectOption} from "../../../_shared/components/select/select-option.enum";

export class SiteListData {
  viewRequest: SiteViewRequest;
  list: Site[];
  count: number;
}

@Injectable()
export class SiteService {
  
  private siteListDataSource: BehaviorSubject<SiteListData> = new BehaviorSubject(undefined);
  siteListData$: Observable<SiteListData>                   = this.siteListDataSource.asObservable();
  
  private allSitesSource: BehaviorSubject<Site[]> = new BehaviorSubject([]);
  allSites$: Observable<Site[]>                   = this.allSitesSource.asObservable();
  
  constructor(private logger: LoggerService,
              private exceptionService: ExceptionService,
              private siteBackendClient: SiteBackendClient,
              private notificationService: NotificationService,
              private cookieService: CookieService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  initSiteList(siteListData: SiteListData) {
    this.siteListDataSource.next(siteListData);
  }
  
  SiteViewRequest(): SiteViewRequest {
    return this.siteListDataSource.value.viewRequest;
  }
  
  loadAndAppendPage(page: number): Observable<void> {
    const viewRequest = Object.assign({}, this.siteListDataSource.getValue().viewRequest);
    viewRequest.page  = page;
    
    return this.siteBackendClient.loadSites(viewRequest)
    .pipe(
      catchError(errorInfo => {
        this.exceptionService.handleRequestErrorInfo(errorInfo.error);
        return EMPTY;
      }),
      tap(sites => {
        const updatedSiteListData: SiteListData = iassign(
          this.siteListDataSource.getValue(),
          siteListData => siteListData.list,
          siteList => {
            siteList.push(...sites);
            return siteList;
          }
        );
        this.siteListDataSource.next(updatedSiteListData);
      }),
      map(() => {
      })
    );
  }
  
  editSite(formData: SiteRequest): Observable<Site> {
    const request: SiteRequest = new SiteRequest();
    request.id                 = formData.id ? formData.id : null;
    request.name               = formData.name ? formData.name : null;
    request.url                = formData.url ? formData.url : "";
    request.lang               = formData.lang ? formData.lang.value : null;
    return this.siteBackendClient.updateSite(request)
    .pipe(
      tap((site: Site) => {
        if (site) {
          this.notificationService.showSuccess("All data for Site updated", "Edit Site");
        }
      }),
      catchError(errorInfo => {
        this.exceptionService.handleRequestErrorInfo(errorInfo.error);
        return EMPTY;
      })
    );
  }
  
  createSite(formData: SiteRequest): Observable<number> {
    const request: SiteRequest = new SiteRequest();
    request.id                 = formData.id ? formData.id : null;
    request.name               = formData.name ? formData.name : null;
    request.url                = formData.url ? formData.url : null;
    request.lang               = formData.lang ? formData.lang.value : null;
    return this.siteBackendClient.addSite(request)
    .pipe(
      tap((id: number) => {
        if (id) {
          this.notificationService.showSuccess("New Site created", "Create Site");
        }
      }),
      catchError(errorInfo => {
        this.exceptionService.handleRequestErrorInfo(errorInfo.error);
        return EMPTY;
      })
    );
  }
  
  deleteSite(id: number): Observable<boolean> {
    return this.siteBackendClient.deleteSite(id)
    .pipe(
      tap(() => {
        this.notificationService.showSuccess("Site deleted", "Delete Site");
      }),
      catchError(errorInfo => {
        this.exceptionService.handleRequestErrorInfo(errorInfo.error);
        return EMPTY;
      })
    );
  }
  
  // functions for site system
  loadAllSites(): Subscription {
    const viewRequest: SiteViewRequest = new SiteViewRequest();
    viewRequest.page                   = 1;
    viewRequest.pageSize               = 100; // like all
    return this.siteBackendClient.loadSites(viewRequest)
    .pipe(
      tap((sites: Site[]) => {
        this.allSitesSource.next(sites);
      }),
      catchError(errorInfo => {
        this.exceptionService.handleRequestErrorInfo(errorInfo.error);
        return EMPTY;
      })
    ).subscribe();
  }
  
  setCurrentSite(site: SelectOption) {
    const newSettings: Settings = new Settings();
    const settings: Settings  = this.cookieService.getJson(CONFIG.settingsCookie);
    newSettings.site          = site;
    this.cookieService.set(CONFIG.settingsCookie, JSON.stringify(newSettings), 30, "/");
    window.location.reload();
  }
  
  getCurrentSite() {
    const settings: Settings = this.cookieService.getJson(CONFIG.settingsCookie);
    const currentSite        = settings.site ? settings.site : null;
    return currentSite;
  }
  
  getSiteById(id: number): Site {
    const allSites: Site[] = this.allSitesSource.getValue();
    return allSites.filter((site: Site) => site.ID === id).pop();
  }
  
  getSiteNameById(id: number): string {
    const site: Site = this.getSiteById(id);
    return site ? site.name : "";
  }
  
  getSiteUrlById(id: number): string {
    const site: Site = this.getSiteById(id);
    return site ? site.url : "";
  }
}
