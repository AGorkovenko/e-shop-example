import {Injectable} from "@angular/core";
import {LoggerService} from "./logger.service";
import {NotificationService} from "./notification.service";
import {ErrorInfo} from "../models/dto/error/error-info.model";
import {CATALOG_ERROR_MESSAGES} from "../models/dto/error/error-localization-message.catalog";

@Injectable()
export class ExceptionService {
    
    constructor(private logger: LoggerService,
                private notificationService: NotificationService) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    handleRequestErrorInfo(errorInfo: ErrorInfo, errorTitle?: string, errorMessage?: string) {
        this.logger.debug("Server return error: ", errorInfo);
        if (!errorMessage) {
            errorMessage = CATALOG_ERROR_MESSAGES[errorInfo.code] ? CATALOG_ERROR_MESSAGES[errorInfo.code] : null;
            if (!errorTitle) {
                errorTitle = errorInfo.code;
            }
            if (!errorMessage) {
                errorMessage = errorInfo.message;
            }
        }
        this.notificationService.showError(errorMessage, errorTitle);
    }
    
    raiseUnknownError(message: string, error: any) {
        this.logger.error(message, error);
    }
}
