import {Injectable} from "@angular/core";
import {HttpCachingClient} from "../http-caching-client.service";
import {ProfileUpdateBackendClient} from "./profile-update-backend-client.service";
import {catchError, map, tap} from "rxjs/operators";
import {EMPTY, Observable, of} from "rxjs";
import {ProfileDelivaryRequest} from "../../models/dto/profile/profile-delivery-request.model";
import {LoggerService} from "../logger.service";
import {ExceptionService} from "../exception.service";
import {NotificationService} from "../notification.service";
import {ProfileService} from "./profile.service";
import {CONFIG} from "../../config";
import {Profile} from "../../models/dto/profile/profile.model";
import {ProfileRequest} from "../../models/dto/profile/profile-request.model";
import {ProfileDelivery} from "../../models/dto/profile/profile-delivery.model";

@Injectable()
export class ProfileUpdateService {
    
    constructor(private logger: LoggerService,
                private exceptionService: ExceptionService,
                private notificationService: NotificationService,
                private profileUpdateBackendClient: ProfileUpdateBackendClient,
                private profileService: ProfileService,
                private httpClient: HttpCachingClient) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    updateProfile(formData: any): Observable<Profile> {
        const request: ProfileRequest = new ProfileRequest();
        request.firstname             = formData.firstname ? formData.firstname : null;
        request.lastname              = formData.lastname ? formData.lastname : null;
        request.login                 = formData.login ? formData.login : null;
        return this.profileUpdateBackendClient.loadProfileUpdate(request)
        .pipe(
            tap((profile: Profile) => {
                this.logger.debug("Profile was updated", profile);
            }),
            catchError(err => {
                this.exceptionService.handleRequestErrorInfo(err.error, "Error update profile");
                return of(null);
            })
        );
    }
    
    profileDeliveryUpdate(formData: any): Observable<ProfileDelivery> {
        const request: ProfileDelivaryRequest = new ProfileDelivaryRequest();
        request.ID                            = formData.id ? formData.id : null;
        request.firstname                     = formData.firstname ? formData.firstname : null;
        request.lastname                      = formData.lastname ? formData.lastname : null;
        request.country                       = formData.country ? formData.country : null;
        request.zip                           = formData.zip ? formData.zip : null;
        request.ort                           = formData.ort ? formData.ort : null;
        request.address                       = formData.address ? formData.address : null;
        request.phone                         = formData.phone ? formData.phone : null;
        return this.profileUpdateBackendClient.loadProfileDeliveryUpdate(request)
        .pipe(
            tap((profileDelivery: ProfileDelivery) => {
                this.logger.debug("Profile was updated", profileDelivery);
                this.profileService.profileDeliveryInit(profileDelivery);
            }),
            catchError(err => {
                this.exceptionService.handleRequestErrorInfo(err.error, "Error update profile delivery data");
                return EMPTY;
            })
        );
    }
    
    passwordChange(password: string, rePassword: string, oldPasswordOrCode: string, uid?: number): Observable<boolean> {
        if (uid && uid > 0) {
            return this.profileUpdateBackendClient.loadRestoreProfilePasswordUpdate(password, rePassword, oldPasswordOrCode, uid)
            .pipe(
                tap(res => {
                    this.logger.debug("Password was recovered");
                }),
                catchError(err => {
                    this.exceptionService.handleRequestErrorInfo(err.error);
                    return of(false);
                })
            );
        } else {
            return this.profileUpdateBackendClient.loadProfilePasswordUpdate(oldPasswordOrCode, password, rePassword)
            .pipe(
                tap(token => {
                    this.logger.debug("Profile password was updated", token);
                    localStorage.setItem(CONFIG.profileLocalStorage,
                        JSON.stringify({
                            token: token
                        })
                    );
                }),
                map(token => {
                    return true;
                }),
                catchError(err => {
                    this.exceptionService.handleRequestErrorInfo(err.error);
                    return of(false);
                })
            );
        }
    }
    
    passwordRecoveryRequest(email: string): Observable<boolean> {
        return this.profileUpdateBackendClient.loadPasswordRecoveryRequest(email)
        .pipe(
            tap(res => {
                this.logger.debug("Request for password recovery sent");
            }),
            catchError(err => {
                this.exceptionService.handleRequestErrorInfo(err.error);
                return of(false);
            })
        );
    }
    
    sendRequestNewEmail(email: string): Observable<boolean> {
        return this.profileUpdateBackendClient.loadProfileRequestEmail(email)
        .pipe(
            tap(res => {
                this.logger.debug("Request for change E-mail was sent");
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            })
        );
    }
}
