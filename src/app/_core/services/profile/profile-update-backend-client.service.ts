import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {ProfileDelivery} from "../../models/dto/profile/profile-delivery.model";
import {LoggerService} from "../logger.service";
import {HttpCachingClient} from "../http-caching-client.service";
import {Profile} from "../../models/dto/profile/profile.model";
import {CONFIG} from "../../config";
import {ProfileDelivaryRequest} from "../../models/dto/profile/profile-delivery-request.model";
import {ProfileRequest} from "../../models/dto/profile/profile-request.model";

@Injectable({
    providedIn: "root"
})
export class ProfileUpdateBackendClient {
    
    PROFILE_URL               = "/profile";
    PROFILE_DELIVERY_URL      = "/profile/delivery";
    PROFILE_PASSWORD_URL      = "/profile/password";
    PROFILE_EMAIL_URL         = "/profile/email";
    USER_PASSWORD_RESTORE_URL = "/user/restore";
    USER_PASSWORD_REQUEST_URL = "/user/request";
    
    constructor(private logger: LoggerService,
                private httpClient: HttpCachingClient) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    loadProfileUpdate(profileRequest: ProfileRequest): Observable<Profile> {
        const url  = CONFIG.apiUrl + this.PROFILE_URL;
        const body = {
            profile: profileRequest
        };
        return this.httpClient.post(url, body);
    }
    
    loadProfileDeliveryUpdate(profileDeliveryRequest: ProfileDelivaryRequest): Observable<ProfileDelivery> {
        const url  = CONFIG.apiUrl + this.PROFILE_DELIVERY_URL;
        const body = {
            profileDelivery: profileDeliveryRequest
        };
        return this.httpClient.post(url, body);
    }
    
    loadProfilePasswordUpdate(oldPassword: string, password: string, rePassword: string): Observable<string> {
        const url  = CONFIG.apiUrl + this.PROFILE_PASSWORD_URL;
        const body = {
            oldpassword: oldPassword,
            newpassword: password,
            renewpassword: rePassword
        };
        return this.httpClient.post(url, body);
    }
    
    loadRestoreProfilePasswordUpdate(password: string, rePassword: string, code: string, uid: number): Observable<boolean> {
        const url  = CONFIG.apiUrl + this.USER_PASSWORD_RESTORE_URL;
        const body = {
            newpassword: password,
            renewpassword: rePassword,
            activationCode: code,
            uid: uid
        };
        return this.httpClient.post(url, body);
    }
    
    loadPasswordRecoveryRequest(email: string): Observable<boolean> {
        const url  = CONFIG.apiUrl + this.USER_PASSWORD_REQUEST_URL;
        const body = {
            email: email
        };
        return this.httpClient.post(url, body);
    }
    
    loadProfileRequestEmail(email: string): Observable<boolean> {
        const url  = CONFIG.apiUrl + this.PROFILE_EMAIL_URL;
        const body = {
            email: email
        };
        return this.httpClient.post(url, body);
    }
}
