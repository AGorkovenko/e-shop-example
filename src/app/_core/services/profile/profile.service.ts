import {Injectable, OnInit} from "@angular/core";
import {BehaviorSubject, EMPTY, Observable, of} from "rxjs";
import {catchError, map, switchMap, take, tap} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";
import {Profile} from "../../models/dto/profile/profile.model";
import {LoggerService} from "../logger.service";
import {environment} from "../../../../environments/environment";
import {CONFIG} from "../../config";
import {HistoryNavigationService} from "../history-navigation.service";
import {ExceptionService} from "../exception.service";
import {ProfileDelivery} from "../../models/dto/profile/profile-delivery.model";
import {ShopService} from "../shop/shop.service";

@Injectable()
export class ProfileService {
  
  PROFILE_URL = "/profile";
  PROFILE_CHECK_TOKEN_URL = "/profile/check";
  PROFILE_DELIVERY_URL = "/profile/delivery";
  PROFILE_EMAIL_URL = "/profile/email";
  USER_URL = "/user";
  LOGIN_URL = "/user/auth";
  LOGOUT_URL = "/user/logout";
  USER_ACODE_URL = "/user/acode";
  USER_EMAIL_URL = "/user/email";
  
  private profileSource: BehaviorSubject<Profile> = new BehaviorSubject(null);
  profile$: Observable<Profile> = this.profileSource.asObservable();
  
  private profileDeliverySource: BehaviorSubject<ProfileDelivery> = new BehaviorSubject(null);
  profileDelivery$: Observable<ProfileDelivery> = this.profileDeliverySource.asObservable();
  
  constructor(private logger: LoggerService,
              private httpClient: HttpClient,
              private navigationService: HistoryNavigationService,
              private exceptionService: ExceptionService) {
    this.logger.debug(this.constructor.name + " is created.");
    this.loadProfileSource();
  }
  
  getCurrentProfile(): Profile | null {
    return this.profileSource.getValue();
  }
  
  checkUserToken(): Observable<boolean> {
    return this.httpClient.get<string>(environment.serverAddress + CONFIG.apiUrl + this.PROFILE_CHECK_TOKEN_URL)
    .pipe(
      catchError(errorInfo => {
        this.exceptionService.handleRequestErrorInfo(errorInfo.error, "Check token");
        this.clearProfile();
        return of(null);
      })
    );
  }
  
  isUserAuthenticated(): Observable<boolean> {
    return this.profile$.pipe(
      take(1),
      map(meProfile => !!meProfile)
    );
  }
  
  loadProfileSource() {
    const tokenJson = JSON.parse(localStorage.getItem(CONFIG.profileLocalStorage));
    if (tokenJson && tokenJson.token) {
      this.httpClient.get<Profile>(environment.serverAddress + CONFIG.apiUrl + this.PROFILE_URL)
      .pipe(
        tap(profile => {
          if (profile && profile.role >= 2) {
            this.logger.info("Create profile: ", profile);
            this.profileSource.next(profile);
          } else {
            this.clearProfile();
          }
        }),
        catchError(errorInfo => {
          this.exceptionService.handleRequestErrorInfo(errorInfo.error, "Error creating user profile");
          // this.clearProfile();
          return of(null);
        }),
        take(1)
      )
      .subscribe();
    } else {
      return of(null);
    }
  }
  
  profileInit(profile: Profile) {
    this.profileSource.next(profile);
  }
  
  login(login: string, pass: string): Observable<{ [key: string]: string }> {
    const authBody = {
      login: login,
      pass: pass
    };
    return this.httpClient.post<string>(environment.serverAddress + CONFIG.apiUrl + this.LOGIN_URL, authBody)
    .pipe(
      catchError(err => {
        this.exceptionService.handleRequestErrorInfo(err.error);
        this.logger.debug("Problem with authentication", err);
        return of(null);
      }),
      tap(tokenJson => {
        this.logger.info("User successfully authorized with token", tokenJson);
        if (tokenJson) {
          localStorage.setItem(CONFIG.profileLocalStorage, JSON.stringify(tokenJson));
          this.loadProfileSource();
        }
      })
    );
  }
  
  logout(): Observable<boolean> {
    // thinking about this code next
    return this.httpClient.get<boolean>(environment.serverAddress + CONFIG.apiUrl + this.LOGOUT_URL)
    .pipe(
      tap(logout => {
        this.clearProfile();
        this.logger.info("User successfully logged out");
      }),
      catchError(err => {
        this.logger.debug("Problem with authentication logged out", err);
        return of(false);
      })
    );
  }
  
  singup(formData: any): Observable<boolean> {
    const body = {
      login: formData.login,
      email: formData.email,
      pass: formData.password === formData.repassword ? formData.password : null,
      repass: formData.password === formData.repassword ? formData.repassword : null,
      flag: formData.flag,
      agreement: formData.agreement
    };
    return this.httpClient.post<boolean>(environment.serverAddress + CONFIG.apiUrl + this.USER_URL, body)
    .pipe(
      tap(tokenJson => {
        if (tokenJson) {
          this.logger.info("User successfully created and get new token", tokenJson);
          localStorage.setItem(CONFIG.profileLocalStorage, JSON.stringify(tokenJson));
          this.loadProfileSource();
          this.navigationService.goToBackPage("/");
        }
      }),
      catchError(err => {
        this.logger.debug("Problem with creating user", err);
        this.exceptionService.handleRequestErrorInfo(err.error);
        return of(false);
      })
    );
  }
  
  clearProfile() {
    localStorage.removeItem(CONFIG.profileLocalStorage);
    this.profileSource.next(null);
  }
  
  profileDeliveryInit(profileDelivery: ProfileDelivery) {
    this.profileDeliverySource.next(profileDelivery);
  }
  
  loadProfileDelivery(): Observable<ProfileDelivery> {
    return this.httpClient.get<ProfileDelivery>(environment.serverAddress + CONFIG.apiUrl + this.PROFILE_DELIVERY_URL)
    .pipe(
      tap(profileDelivery => {
        this.logger.info("Delivery data received", profileDelivery);
        this.profileDeliveryInit(profileDelivery);
      }),
      catchError(err => {
        this.logger.debug("Problem with getting delivery data", err);
        this.exceptionService.handleRequestErrorInfo(err.error, "Error getting delivery data");
        return EMPTY;
      })
    );
  }
  
  confirmationEmail(email: string, confirmationCode: string): Observable<any> {
    const body = {
      email: email,
      confirmationCode: confirmationCode
    };
    return this.httpClient.post<any>(environment.serverAddress + CONFIG.apiUrl + this.USER_EMAIL_URL, body)
    .pipe(
      catchError(errorInfo => {
        this.exceptionService.handleRequestErrorInfo(errorInfo.error, "Confirmation of mail");
        this.navigationService.goToBackPage();
        return of(false);
      })
    );
  }
  
  checkActivationCode(code: string): Observable<number> {
    const body = {
      activationCode: code
    };
    return this.httpClient.post<number>(environment.serverAddress + CONFIG.apiUrl + this.USER_ACODE_URL, body)
    .pipe(
      catchError(errorInfo => {
        this.logger.debug("Error resolved activation code: " + code, errorInfo);
        this.exceptionService.handleRequestErrorInfo(errorInfo.error);
        return of(null);
      })
    );
  }
  
  loadRequestedEmail(): Observable<string> {
    return this.httpClient.get<string>(environment.serverAddress + CONFIG.apiUrl + this.PROFILE_EMAIL_URL);
  }
}
