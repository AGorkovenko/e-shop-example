import {Inject, Injectable, PLATFORM_ID} from "@angular/core";
import {ToastrService} from "ngx-toastr";
import {isPlatformBrowser} from "@angular/common";

@Injectable()
export class NotificationService {
    
    constructor(private toastrService: ToastrService,
                @Inject(PLATFORM_ID) private platformId: Object) {
    }
    
    showError(errorMessage: string, errorTitle?: string) {
        if (isPlatformBrowser(this.platformId)) {
            this.toastrService.error(errorMessage, errorTitle);
        }
    }
    
    showWarning(warningMessage: string, warningTitle?: string) {
        if (isPlatformBrowser(this.platformId)) {
            this.toastrService.warning(warningMessage, warningTitle);
        }
    }
    
    showSuccess(text: string, title?: string) {
        if (isPlatformBrowser(this.platformId)) {
            this.toastrService.success(text, title);
        }
    }
}
