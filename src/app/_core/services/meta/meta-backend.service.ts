import {Injectable} from "@angular/core";
import {HttpCachingClient} from "../http-caching-client.service";
import {LoggerService} from "../logger.service";
import {ContentMetaViewRequest} from "../../models/dto/meta/content-meta-view-request.model";
import {Observable} from "rxjs";
import {HttpParams} from "@angular/common/http";
import {CONFIG} from "../../config";
import {ContentMeta} from "../../models/dto/meta/content-meta.model";
import {ContentMetaTranslation} from "../../models/dto/meta/content-meta-translation.model";

@Injectable()
export class ContentMetaBackendClient {
    
    META_URL              = "/meta";
    META_ID_URL           = "/meta/{id}";
    META_TRANSLATIOMS_URL = "/meta/{id}/translation";
    META_COUNT_URL        = "/meta/count";
    
    constructor(private logger: LoggerService,
                private httpClient: HttpCachingClient) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    loadMetaFields(viewRequest?: ContentMetaViewRequest): Observable<ContentMeta[]> {
        const url              = CONFIG.apiUrl + this.META_URL;
        let params: HttpParams = new HttpParams();
        if (!viewRequest) {
            viewRequest = new ContentMetaViewRequest();
        }
        let keysString: string;
        if (viewRequest.keys) {
            viewRequest.keys.forEach(key => {
                keysString = keysString ? keysString + ",'" + key + "'" : "'" + key + "'";
            });
        }
        if (keysString) {
            params = params.set("keys", keysString);
        }
        if (viewRequest.order != null) {
            params = params.set("order", viewRequest.order);
        }
        if (viewRequest.sort != null) {
            params = params.set("sort", viewRequest.sort);
        }
        const offset = (viewRequest.page - 1) * viewRequest.pageSize;
        params       = params.set("offset", offset.toString());
        params       = params.set("limit", viewRequest.pageSize.toString());
        
        return this.httpClient.get(url, params);
    }
    
    loadCount(viewRequest: ContentMetaViewRequest): Observable<number> {
        const url              = CONFIG.apiUrl + this.META_COUNT_URL;
        let params: HttpParams = new HttpParams();
        let keysString: string;
        if (viewRequest.keys) {
            viewRequest.keys.forEach(key => {
                keysString = keysString ? keysString + ",'" + key + "'" : "'" + key + "'";
            });
        }
        if (keysString) {
            params = params.set("keys", keysString);
        }
        
        return this.httpClient.get<number>(url, params);
    }
    
    loadMeta(id: number, lang?: string): Observable<ContentMeta> {
        let params: HttpParams = new HttpParams();
        if (lang) {
            params = params.set("lang", lang);
        }
        const url = CONFIG.apiUrl + this.META_ID_URL.replace("{id}", id.toString());
        return this.httpClient.get(url, params);
    }
    
    loadTranslations(id: number): Observable<ContentMetaTranslation[]> {
        const url = CONFIG.apiUrl + this.META_TRANSLATIOMS_URL.replace("{id}", id.toString());
        return this.httpClient.get<ContentMetaTranslation[]>(url);
    }
}
