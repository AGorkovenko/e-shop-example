import {Injectable} from "@angular/core";
import {BehaviorSubject, EMPTY, Observable, of} from "rxjs";
import {catchError, map, tap} from "rxjs/operators";
import {LoggerService} from "../logger.service";
import {ExceptionService} from "../exception.service";
import {ContentMeta} from "../../models/dto/meta/content-meta.model";
import {ContentMetaViewRequest} from "../../models/dto/meta/content-meta-view-request.model";
import iassign from "immutable-assign";
import {ContentMetaBackendClient} from "./meta-backend.service";
import {NotificationService} from "../notification.service";

export class ContentMetaListData {
    viewRequest: ContentMetaViewRequest;
    list: ContentMeta[];
    count: number;
}

@Injectable()
export class MetaService {
    
    private contentMetaListDataSource: BehaviorSubject<ContentMetaListData> = new BehaviorSubject(undefined);
    contentMetaListData$: Observable<ContentMetaListData>                   = this.contentMetaListDataSource.asObservable();
    
    constructor(private logger: LoggerService,
                private exceptionService: ExceptionService,
                private contentMetaBackendClient: ContentMetaBackendClient,
                private notificationService: NotificationService) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    initContentMetaList(contentMetaListData: ContentMetaListData) {
        this.contentMetaListDataSource.next(contentMetaListData);
    }
    
    ContentMetaViewRequest(): ContentMetaViewRequest {
        return this.contentMetaListDataSource.value.viewRequest;
    }
    
    loadAndAppendPage(page: number): Observable<void> {
        const viewRequest = Object.assign({}, this.contentMetaListDataSource.getValue().viewRequest);
        viewRequest.page  = page;
        
        return this.contentMetaBackendClient.loadMetaFields(viewRequest)
        .pipe(
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            }),
            tap(metas => {
                const updatedContentMetaListData: ContentMetaListData = iassign(
                    this.contentMetaListDataSource.getValue(),
                    contentMetsListData => contentMetsListData.list,
                    contentMetsList => {
                        contentMetsList.push(...metas);
                        return contentMetsList;
                    }
                );
                this.contentMetaListDataSource.next(updatedContentMetaListData);
            }),
            map(() => {
            })
        );
    }
    
    getMetas(keys: string[]): Observable<ContentMeta[]> {
        const viewRequest: ContentMetaViewRequest = new ContentMetaViewRequest();
        if (keys) {
            viewRequest.keys     = keys;
            viewRequest.pageSize = keys.length;
        }
        return this.contentMetaBackendClient.loadMetaFields(viewRequest)
        .pipe(
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return of(null);
            })
        );
    }
    
    getMeta(id: number, lang?: string): Observable<ContentMeta> {
        return this.contentMetaBackendClient.loadMeta(id, lang)
        .pipe(
            catchError(errorInfo => {
                // this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return of(null);
            }),
            tap((meta: ContentMeta) => {
                if (meta) {
                    this.notificationService.showSuccess("Loaded content meta for language", "Load Meta");
                }
            })
        );
    }
}
