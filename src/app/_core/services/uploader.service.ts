import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {HttpCachingClient} from "./http-caching-client.service";
import {CONFIG} from "../config";

@Injectable()
export class UploaderService {
	
	UPLOAD_IMAGE_API: string = "/image";
	
	constructor(private httpClient: HttpCachingClient) {
	}
	
	uploadImage(file: File, folder: string): Observable<any> {
		const url                = CONFIG.apiUrl + this.UPLOAD_IMAGE_API;
		const formData: FormData = new FormData();
		formData.append("image", file, file.name);
		formData.append("folder", folder ? folder : "cover");
		return this.httpClient.upload(url, formData);
	}
	
	uploadImages(files: File[], folder: string): Observable<any> {
		const url                = CONFIG.apiUrl + this.UPLOAD_IMAGE_API;
		const formData: FormData = new FormData();
		files.forEach((file: File, index: number) => {
			formData.append("images[]", file, file.name);
		});
		formData.append("folder", folder ? folder : "cover");
		return this.httpClient.upload(url, formData);
	}
}
