import {Inject, Injectable, Renderer2, RendererFactory2} from "@angular/core";
import {DOCUMENT} from "@angular/common";

@Injectable()
export class LoaderService {
    
    private renderer: Renderer2;
    
    constructor(@Inject(DOCUMENT) private document: Document,
                private rendererFactory: RendererFactory2) {
        this.renderer = rendererFactory.createRenderer(null, null);
    }
    
    onLoader() {
        this.renderer.addClass(this.document.body, "show-spinner");
    }
    
    offLoader() {
        this.renderer.removeClass(this.document.body, "show-spinner");
    }
    
}
