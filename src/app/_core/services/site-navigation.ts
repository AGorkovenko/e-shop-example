import {DOCUMENT, isPlatformBrowser, Location} from "@angular/common";
import {Inject, Injectable, OnDestroy, PLATFORM_ID} from "@angular/core";
import {ActivatedRoute, NavigationEnd, NavigationStart, Params, Router, UrlTree} from "@angular/router";
import {ConnectableObservable, EMPTY, from, Observable, of, Subject, Subscription, timer} from "rxjs";
import {filter, map, publish, publishReplay, shareReplay, switchMap, take, takeUntil, tap} from "rxjs/operators";
import {LoggerService} from "./logger.service";
import {PageNavigationService} from "./page-navigation";
import {LoaderService} from "./loader.service";
import {SignInUpService} from "./sign-in-up.service";

@Injectable()
export class SiteNavigationService implements OnDestroy {
    
    navigationEndEvents$: Observable<NavigationEnd>;
    navigationStartEvents$: Observable<NavigationStart>;
    
    pageTitle$: Observable<string>;
    
    isFirstNavigation: boolean = true;
    
    private urlScrollPositions: Map<string, { x: number, y: number }> = new Map();
    
    isDestroyed$: Subject<boolean> = new Subject();
    
    constructor(private logger: LoggerService,
                @Inject(PLATFORM_ID) private platformId: Object,
                private router: Router,
                private activatedRoute: ActivatedRoute,
                private location: Location,
                private pageNavigationService: PageNavigationService,
                @Inject(DOCUMENT) private document: Document,
                private loaderService: LoaderService,
                private signInUpService: SignInUpService) {
        this.navigationEndEvents$   = this.createNavigationEndNotifier();
        this.navigationStartEvents$ = this.createNavigationStartNotifier();
        this.enableFirstNavigationCheck();
    }
    
    enableFirstNavigationCheck(): void {
        this.navigationStartEvents$
        .pipe(
            take(1),
            takeUntil(this.isDestroyed$)
        )
        .subscribe(() => this.isFirstNavigation = false);
    }
    
    getCurrentPageUrl(): string {
        return this.router.url;
    }
    
    setQueryParams(params: Params) {
        this.getCurrentPagePath().subscribe(path => {
            this.router.navigate([path], {
                relativeTo: this.activatedRoute,
                queryParams: params
            });
        });
    }
    
    getCurrentPageUrlWithoutHash(): string {
        return this.router.url.replace(/#.*$/, "");
    }
    
    getCurrentPagePath(): Observable<string> {
        return this.navigationEndEvents$
        .pipe(
            map(navigationEnd => {
                const urlTree: UrlTree = this.router.parseUrl(navigationEnd.urlAfterRedirects);
                let currentUrlWithoutParams;
                if (urlTree.root.children["primary"]) {
                    currentUrlWithoutParams = "/" + urlTree.root.children["primary"].segments.map(segment => segment.path).join("/");
                } else {
                    currentUrlWithoutParams = "/";
                }
                return currentUrlWithoutParams;
            })
        );
    }
    
    getCurrentParentSegment(): Observable<string> {
        return this.navigationEndEvents$
        .pipe(
            map(navigationEnd => {
                const urlTree: UrlTree = this.router.parseUrl(navigationEnd.urlAfterRedirects);
                let currentUrlWithoutParams;
                if (urlTree.root.children["primary"]) {
                    currentUrlWithoutParams = urlTree.root.children["primary"].segments[0].toString();
                } else {
                    currentUrlWithoutParams = "/";
                }
                return currentUrlWithoutParams;
            })
        );
    }
    
    startPageTitleGeneration() {
        if (!this.pageTitle$) {
            this.pageTitle$ = this.navigationEndEvents$
            .pipe(
                map(() => this.activatedRoute),
                switchMap(route => {
                    const allRoutes = [];
                    allRoutes.push(route);
                    
                    let currentRoute = route;
                    while (currentRoute.firstChild) {
                        currentRoute = currentRoute.firstChild;
                        allRoutes.push(currentRoute);
                    }
                    return from(allRoutes);
                }),
                filter(route => route.outlet === "primary"),
                switchMap(route => route.data),
                map(data => data["title"]),
                shareReplay(1)
            );
        }
    }
    
    scrollToTop(): void {
        if (isPlatformBrowser(this.platformId)) {
            window.scrollTo(0, 0);
        }
    }
    
    enableHashRoutingBehaviour(): Subscription {
        if (isPlatformBrowser(this.platformId)) {
            return this.navigationEndEvents$
            .pipe(
                switchMap(event => {
                    const urlTree: UrlTree = this.router.parseUrl(event.url);
                    return urlTree.fragment ? of(urlTree.fragment) : EMPTY;
                }),
                switchMap(fragment => {
                    return timer(0, 100)
                    .pipe(
                        take(30),
                        map(() => fragment),
                        filter(fragmentName => this.pageNavigationService.isAnchorPresentOnPage(fragmentName)),
                        take(1)
                    );
                })
            )
            .subscribe(fragment => this.pageNavigationService.scrollToAnchor(fragment, 500));
        }
        return Subscription.EMPTY;
    }
    
    enableScrollPositionBehaviour(): Subscription[] {
        if (isPlatformBrowser(this.platformId)) {
            if (history.scrollRestoration) {
                history.scrollRestoration = "manual";
            }
            let isPopStateTransition: boolean;
            let lastUrl: string;
            
            const openedSubscription: Subscription[] = [];
            // Save position of current scroll before navigating out
            const subscription1: Subscription        = this.router.events
            .pipe(
                filter(event => event instanceof NavigationStart)
            )
            .subscribe(() => {
                this.urlScrollPositions.set(this.router.url, {x: window.scrollX, y: window.scrollY});
            });
            openedSubscription.push(subscription1);
            
            // If transition is ordinary and on another content - move screen to top
            const subscription2: Subscription = this.navigationEndEvents$.subscribe(navigationEnd => {
                // initial navigation shoudn't trigger scrolling
                if (navigationEnd.id > 1) {
                    const hashRegex = /#.*/;
                    
                    if (isPopStateTransition) {
                        isPopStateTransition = false;
                    } else if (lastUrl && lastUrl.replace(hashRegex, "") !== navigationEnd.urlAfterRedirects.replace(hashRegex, "")) {
                        this.scrollToTop();
                    }
                }
                lastUrl = navigationEnd.urlAfterRedirects;
            });
            openedSubscription.push(subscription2);
            
            // If transition from popState - restore position from cache
            const subscription3: Subscription = <Subscription>this.location.subscribe(() => {
                isPopStateTransition = true;
                this.makeAttemptToRestorePosition();
            });
            openedSubscription.push(subscription3);
            
            return openedSubscription;
        }
        return [];
    }
    
    private createNavigationStartNotifier(): Observable<NavigationStart> {
        const navigationStartEvents = this.router.events
        .pipe(
            filter(event => event instanceof NavigationStart),
            map(event => <NavigationStart>event),
            publish()
        ) as ConnectableObservable<NavigationStart>;
        navigationStartEvents.connect();
        return navigationStartEvents;
    }
    
    private createNavigationEndNotifier(): Observable<NavigationEnd> {
        const navigationEndEvents = this.router.events
        .pipe(
            filter(event => event instanceof NavigationEnd),
            map(event => <NavigationEnd>event),
            tap(event => this.logger.debug("NavigationEnd event occurred", event)),
            publishReplay(1)
        ) as ConnectableObservable<NavigationEnd>;
        navigationEndEvents.connect();
        return navigationEndEvents;
    }
    
    private makeAttemptToRestorePosition() {
        this.router.events
        .pipe(
            filter(event => event instanceof NavigationEnd),
            take(1),
            switchMap((event: NavigationEnd) => {
                const position = this.urlScrollPositions.get(event.url);
                return position ? of(position) : EMPTY;
            }),
            switchMap(pagePosition => {
                return timer(0, 100)
                .pipe(
                    take(30),
                    map(() => pagePosition)
                );
            }),
            filter((pagePosition: { x: number, y: number }) => {
                const body = document.body;
                const html = document.documentElement;
                
                const documentHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
                return documentHeight - window.innerHeight >= pagePosition.y;
            }),
            take(1)
        )
        .subscribe((position: { x: number, y: number }) => {
            window.scrollTo(position.x, position.y);
        });
    }
    
    enableLoader() {
        this.navigationStartEvents$.subscribe(() => {
            this.loaderService.onLoader();
        });
        this.navigationEndEvents$.subscribe(() => {
            this.loaderService.offLoader();
        });
    }
    
    enableCloser() {
        this.navigationEndEvents$.subscribe(() => {
           this.signInUpService.closeModal();
        });
    }
    
    ngOnDestroy(): void {
        this.isDestroyed$.next();
        this.isDestroyed$.complete();
    }
}
