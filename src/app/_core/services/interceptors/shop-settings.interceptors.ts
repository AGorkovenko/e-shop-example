import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {Settings} from "../../models/dto/settings.model";
import {CONFIG} from "../../config";
import {CookieService} from "../cookie.service";

@Injectable()
export class ShopSettingsInterceptors implements HttpInterceptor {
    constructor(private cookieService: CookieService) {
    }
    
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const settings: Settings = this.cookieService.getJson(CONFIG.settingsCookie);
        if (settings.site || settings.lang || settings.currency) {
            request = request.clone({
                params: request.params.set("shopSettings", JSON.stringify(settings))
            });
        }
        
        return next.handle(request);
    }
}
