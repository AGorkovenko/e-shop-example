import {Inject, Injectable, LOCALE_ID} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable()
export class TranslateInterceptors implements HttpInterceptor {
    constructor(@Inject(LOCALE_ID) private locale: string) {
    }
    
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.locale) {
            request = request.clone({
                params: request.params.set("lang", this.locale)
            });
        }
        return next.handle(request);
    }
}
