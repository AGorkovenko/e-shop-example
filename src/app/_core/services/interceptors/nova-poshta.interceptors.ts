import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {CONFIG} from "../../config";

@Injectable()
export class NovaPoshtaInterceptors implements HttpInterceptor {
	constructor() {
	}
	
	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		const tokenJson = JSON.parse(localStorage.getItem(CONFIG.profileLocalStorage));
		
		if (request.params.get("NovaPoshta")) {
			request = request.clone({
				headers: request.headers.delete("Authorization"),
				params: request.params.delete("NovaPoshta"),
				withCredentials: false
			});
		}
		
		return next.handle(request);
	}
}
