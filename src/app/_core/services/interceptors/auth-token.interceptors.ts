import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {CONFIG} from "../../config";
import {Observable} from "rxjs";

@Injectable()
export class AuthTokenInterceptors implements HttpInterceptor {
    constructor() {
    }
    
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const tokenJson = JSON.parse(localStorage.getItem(CONFIG.profileLocalStorage));
        
        if (tokenJson && tokenJson.token) {
            request = request.clone({
                setHeaders: {
                    Authorization: tokenJson.token
                }
            });
        }
        
        request = request.clone({
            withCredentials: environment.production
        });
        
        return next.handle(request);
    }
}
