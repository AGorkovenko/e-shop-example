import {Injectable} from "@angular/core";
import {NavigationEnd, Router} from "@angular/router";
import {filter} from "rxjs/operators";
import {LoggerService} from "./logger.service";

@Injectable()
export class HistoryNavigationService {
    
    private history = [];
    
    constructor(private logger: LoggerService,
                private router: Router) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    public loadRouting(): void {
        this.router.events
        .pipe(
            filter(event => event instanceof NavigationEnd)
        )
        .subscribe(({urlAfterRedirects}: NavigationEnd) => {
            this.history = [...this.history, urlAfterRedirects];
        });
    }
    
    public getHistory(): string[] {
        return this.history;
    }
    
    public getPreviousUrl(): string {
        return this.history[this.history.length - 2] || "/index";
    }
    
    public goToBack(): void {
        this.router.navigate([this.getPreviousUrl()]);
    }
    
    public goToBackPage(defUrl: string = "/"): void {
        const clearHistory     = this.history
        .filter(item => item !== "/logout")
        .filter(item => item !== "/login")
        .filter(item => item !== "/login/signup")
        .filter(item => item !== "/login/password-recovery");
        const clearPreviousUrl = clearHistory[clearHistory.length - 1] || defUrl;
        this.router.navigate([clearPreviousUrl]);
    }
}
