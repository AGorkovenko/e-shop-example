import {Injectable} from "@angular/core";
import {BehaviorSubject, Observable} from "rxjs";
import {LoggerService} from "./logger.service";
import {MetaService} from "./meta/meta.service";
import {tap} from "rxjs/operators";
import {ContentMeta} from "../models/dto/meta/content-meta.model";

export class SiteAppData {
    metas: ContentMeta[];
}

@Injectable()
export class SiteAppService {
    
    private siteAppDataSource: BehaviorSubject<SiteAppData> = new BehaviorSubject(null);
    siteAppData$: Observable<SiteAppData>                   = this.siteAppDataSource.asObservable();
    
    siteAppData: SiteAppData = new SiteAppData();
    
    constructor(private logger: LoggerService,
                private metaService: MetaService) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    initData() {
        this.metaService.getMetas([
            "email-in-top",
            "phone-in-top",
            "facebook",
            "massenger",
            "youtube",
            "instagram",
            "address",
            "phone-in-footer",
            "open-time",
            "email-in-footer"
        ])
        .pipe(
            tap((metas: ContentMeta[]) => {
                this.siteAppData.metas = metas;
                this.siteAppDataSource.next(this.siteAppData);
            })
        )
        .subscribe();
    }
    
    getMetaValue(key: string) {
        const metas = this.siteAppDataSource.getValue();
        return metas ? metas.metas.find((meta: ContentMeta) => meta.meta_key === key).meta_value : null;
    }
}
