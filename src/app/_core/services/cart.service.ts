import {Injectable} from "@angular/core";
import {BehaviorSubject, Observable, of, Subscription} from "rxjs";
import {LoggerService} from "./logger.service";
import {HttpParams} from "@angular/common/http";
import {ExceptionService} from "./exception.service";
import {Cart} from "../models/dto/cart/cart.model";
import {catchError, map, switchMap, take, tap} from "rxjs/operators";
import {CartProductRequest} from "../models/dto/cart/cart-product-request.model";
import {CONFIG} from "../config";
import {CookieService} from "./cookie.service";
import {HttpCachingClient} from "./http-caching-client.service";
import {ProfileService} from "./profile/profile.service";
import {Profile} from "../models/dto/profile/profile.model";
import {ProductDiscount, ShopService} from "./shop/shop.service";
import {CartItem} from "../models/dto/cart/cart-item.model";

@Injectable()
export class CartService {
  
  CART_CR_URL = "/cart";
  CART_UD_URL = "/cart/{id}";
  CART_MERGE_URL = "/cart/merge";
  
  private cartSource: BehaviorSubject<Cart> = new BehaviorSubject(null);
  cart$: Observable<Cart> = this.cartSource.asObservable();
  
  constructor(private logger: LoggerService,
              private httpClient: HttpCachingClient,
              private cookieService: CookieService,
              private exceptionService: ExceptionService,
              private profileService: ProfileService,
              private shopService: ShopService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  checkingCart(): Subscription {
    return this.profileService.profile$
    .pipe(
      switchMap((profile: Profile) => this.shopService.getSettingByName((profile ? profile.flag : null) + "_discount"), (profile, discount) => [profile ? profile.flag : null, parseFloat(discount)]),
      tap(([flag, discount]: [string, number]) => {
        if (flag) {
          this.shopService.initDiscount(discount, flag + "_price");
        } else {
          this.shopService.resetDiscount();
        }
      }),
      switchMap(([flag, discount]: [string, number]) => {
        if (flag) {
          return this.mergeCartWithTempUser();
        } else {
          return this.loadCart();
        }
      })
    )
    .subscribe();
  }
  
  getTempUser(): number {
    const tokenJson = JSON.parse(localStorage.getItem(CONFIG.profileLocalStorage));
    if (!tokenJson || !tokenJson.token) {
      let tempUser = parseInt(this.cookieService.get(CONFIG.temporaryUserCoockie));
      if (tempUser) {
        return tempUser;
      } else {
        tempUser = Math.round((new Date()).getTime() / 1000);
        this.cookieService.set(CONFIG.temporaryUserCoockie, tempUser.toString());
        return tempUser;
      }
    } else {
      return 0;
    }
  }
  
  loadCart(): Observable<boolean> {
    let params = new HttpParams();
    const uid = this.getTempUser();
    if (uid) params = params.set("uid", uid.toString());
    return this.httpClient.get(CONFIG.apiUrl + this.CART_CR_URL, params)
    .pipe(
      tap((cart: Cart) => {
        const discount: ProductDiscount = this.shopService.getDiscount();
        let cartTotalDiscount = 0;
        cart.items.map((item: CartItem) => {
          if (discount) {
            if (item[discount.type]) {
              item.new_price = item[discount.type];
            } else {
              if (discount.value) {
                item.new_price = (item.old_price ? item.old_price : item.price) * (1 - discount.value / 100);
              } else {
                item.new_price = item.price;
              }
            }
          } else {
            item.new_price = item.price;
          }
          item.total = item.quantity * item.new_price;
          cartTotalDiscount = cartTotalDiscount + item.total;
          return item;
        });
        cart.subtotal = cartTotalDiscount;
        cart.total = cartTotalDiscount;
        this.cartSource.next(cart);
      }),
      map(() => true),
      catchError(errorInfo => {
        this.exceptionService.handleRequestErrorInfo(errorInfo.error, "Error loaded cart");
        return of(false);
      })
    );
  }
  
  getCart() {
    this.loadCart()
    .pipe(
      take(1)
    )
    .subscribe();
  }
  
  addProduct(request: CartProductRequest): Observable<boolean> {
    const body = new CartProductRequest();
    body.uid = request.uid ? request.uid : this.getTempUser();
    body.pid = request.pid ? request.pid : 0;
    body.pvid = request.pvid ? request.pvid : null;
    body.quantity = request.quantity ? request.quantity : 1;
    body.comment = request.comment ? request.comment : null;
    return this.httpClient.post<boolean>(CONFIG.apiUrl + this.CART_CR_URL, body)
    .pipe(
      switchMap(() => this.loadCart()),
      catchError(errorInfo => {
        this.exceptionService.handleRequestErrorInfo(errorInfo.error, "Adding product to cart");
        return of(false);
      })
    );
  }
  
  updateCart(id: number, quantity: number, pid?: number): Observable<boolean> {
    const uid = this.getTempUser();
    const body = {
      quantity: quantity,
      uid: uid,
      pid: pid
    };
    return this.httpClient.post<boolean>(CONFIG.apiUrl + this.CART_UD_URL.replace("{id}", id.toString()), body)
    .pipe(
      switchMap(() => this.loadCart()),
      catchError(errorInfo => {
        this.exceptionService.handleRequestErrorInfo(errorInfo.error, "Refreshing products in cart");
        return of(false);
      })
    );
  }
  
  deleteFromCart(id: number): Observable<boolean> {
    const uid = this.getTempUser();
    let body;
    if (uid) body = {"uid": uid};
    return this.httpClient.delete<boolean>(CONFIG.apiUrl + this.CART_UD_URL.replace("{id}", id.toString()), body)
    .pipe(
      switchMap(() => this.loadCart()),
      catchError(errorInfo => {
        this.exceptionService.handleRequestErrorInfo(errorInfo.error, "Removing product from cart");
        return of(false);
      })
    );
  }
  
  mergeCartWithTempUser(): Observable<boolean> {
    let tempUser = parseInt(this.cookieService.get(CONFIG.temporaryUserCoockie));
    if (tempUser) {
      const body = {
        "tempUser": tempUser
      };
      return this.httpClient.post<boolean>(CONFIG.apiUrl + this.CART_MERGE_URL, body)
      .pipe(
        tap((res) => {
          this.logger.debug("Temp user's cart merged with logged user", res);
        }),
        switchMap(() => this.loadCart()),
        catchError(errorInfo => {
          this.exceptionService.handleRequestErrorInfo(errorInfo.error, "Merging carts");
          return of(false);
        })
      );
    } else {
      return this.loadCart();
    }
  }
  
  clearCart(): Observable<boolean> {
    const uid = this.getTempUser();
    const body = uid ? {uid: uid} : {};
    return this.httpClient.delete<boolean>(CONFIG.apiUrl + this.CART_CR_URL, body)
    .pipe(
      tap((res) => {
        this.logger.debug("Cart already clear");
        this.getCart();
      }),
      catchError(errorInfo => {
        this.exceptionService.handleRequestErrorInfo(errorInfo.error, "Clearing cart");
        return of(false);
      })
    );
  }
}
