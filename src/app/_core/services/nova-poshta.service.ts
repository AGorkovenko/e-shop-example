import {Injectable} from "@angular/core";
import {LoggerService} from "./logger.service";
import {HttpClient, HttpParams} from "@angular/common/http";
import {EMPTY, Observable} from "rxjs";
import {catchError, map, tap} from "rxjs/operators";
import {ExceptionService} from "./exception.service";
import {ErrorInfo} from "../models/dto/error/error-info.model";

export interface NPCity {
    Area: string;
    CityID: string;
    Conglomerates: null;
    Delivery1: string;
    Delivery2: string;
    Delivery3: string;
    Delivery4: string;
    Delivery5: string;
    Delivery6: string;
    Delivery7: string;
    Description: string;
    DescriptionRu: string;
    IsBranch: string;
    PreventEntryNewStreetsUser: null;
    Ref: string;
    SettlementType: string;
    SettlementTypeDescription: string;
    SettlementTypeDescriptionRu: string;
    SpecialCashCheck: number;
}

export interface NPSettlement {
    Area: string;
    CityID: string;
    Conglomerates: null;
    Delivery1: string;
    Delivery2: string;
    Delivery3: string;
    Delivery4: string;
    Delivery5: string;
    Delivery6: string;
    Delivery7: string;
    Description: string;
    DescriptionRu: string;
    IsBranch: string;
    PreventEntryNewStreetsUser: null;
    Ref: string;
    SettlementType: string;
    SettlementTypeDescription: string;
    SettlementTypeDescriptionRu: string;
    SpecialCashCheck: number;
    Latitude: string;
    Longitude: string;
    Region: string;
    RegionsDescription: string;
    RegionsDescriptionRu: string;
    AreaDescription: string;
    AreaDescriptionRu: string;
    Index1: string;
    Index2: string;
    IndexCOATSU1: string;
    Warehouse: string;
}

export class NPWeek {
    Monday: string;
    Tuesday: string;
    Wednesday: string;
    Thursday: string;
    Friday: string;
    Saturday: string;
    Sunday: string;
}

export class NPWarehouse {
    SiteKey: string;
    Description: string;
    DescriptionRu: string;
    Phone: string;
    TypeOfWarehouse: string;
    Ref: string;
    Number: string;
    CityRef: string;
    CityDescription: string;
    CityDescriptionRu: string;
    Longitude: string;
    Latitude: string;
    PostFinance: string;
    BicycleParking: string;
    POSTerminal: string;
    InternationalShipping: string;
    TotalMaxWeightAllowed: number;
    PlaceMaxWeightAllowed: number;
    Reception: NPWeek;
    Delivery: NPWeek;
    Schedule: NPWeek;
}

export interface NPResponse {
    data: NPSettlement[] | NPWarehouse[];
    info: {
        totalCount: number;
    };
    success: boolean;
    errors: any[];
    messageCodes: any[];
    errorCodes: any[];
    warningCodes: any[];
    infoCodes: any[];
}

export interface NPResponseCities {
    data: NPCity[];
    info: {
        totalCount: number;
    };
    success: boolean;
    errors: any[];
    messageCodes: any[];
    errorCodes: any[];
    warningCodes: any[];
    infoCodes: any[];
}

export interface NPAddresses {
    Warehouses: number;
    MainDescription: string;
    Area: string;
    Region: string;
    Present: string
    SettlementTypeCode: string;
    Ref: string;
    DeliveryCity: string;
}

export interface NPData {
    TotalCount: number;
    Addresses: NPAddresses[] | NPCity[];
}

export interface NPSearchResponse {
    success: boolean;
    data: NPData[];
    errors: any[];
    warnings: any[];
    info: any[];
    messageCodes: any[];
    errorCodes: any[];
    warningCodes: any[];
    infoCodes: any[];
}

@Injectable()
export class NovaPoshtaService {
    
    API_KEY = "5defdce65f20955170fd0548d1973045";
    
    NOVA_POSHTA_API_URL       = "https://api.novaposhta.ua/v2.0/json/";
    ADDRESS_GET_CITIES        = "Address/getCities";
    ADDRESS_GET_SETTLEMETS    = "AddressGeneral/getSettlements";
    ADDRESS_GET_WAREHOUSES    = "AddressGeneral/getWarehouses";
    ADDRESS_SEARCH_SETTLEMETS = "Address/searchSettlements";
    
    params: HttpParams = new HttpParams();
    
    constructor(private logger: LoggerService,
                private httpClient: HttpClient,
                private exceptionService: ExceptionService) {
        this.logger.debug(this.constructor.name + " is created.");
        this.params = this.params.set("NovaPoshta", "true");
    }
    
    getCities(query?: string): Observable<any> {
        const url = this.NOVA_POSHTA_API_URL + this.ADDRESS_GET_CITIES;
        
        const body = JSON.stringify({
            "modelName": "Address",
            "calledMethod": "getCities",
            "apiKey": this.API_KEY,
            "methodProperties": {
                "Ref": "", // Идентификатор адреса
                "Page": "1", // Номер страницы
                "FindByString": query, // Поиск по строкам Украинский либо Русский язык
                "language": "ua"
            }
        });
        
        return this.httpClient.post(url, body, {params: this.params})
        .pipe(
            tap((response: NPResponseCities) => {
                if ((response.errorCodes && response.errorCodes[0]) || (response.errors && response.errors[0])) {
                    const errorInfo: ErrorInfo = {
                        code: response.errorCodes[0],
                        message: response.errors[0],
                        data: null
                    };
                    this.exceptionService.handleRequestErrorInfo(errorInfo, "Помилка при пошуку населеного пункту");
                }
            }),
            map((response: NPResponseCities) => {
                return response.data;
            }),
            catchError(err => {
                this.exceptionService.handleRequestErrorInfo(err, "Помилка отримання списку населених пунктів");
                return EMPTY;
            })
        );
    }
    
    getSettlements(query?: string): Observable<any> {
        const url = this.NOVA_POSHTA_API_URL + this.ADDRESS_GET_SETTLEMETS;
        
        const body = JSON.stringify({
            "modelName": "AddressGeneral",
            "calledMethod": "getSettlements",
            "apiKey": this.API_KEY,
            "methodProperties": {
                "AreaRef": "",
                "Ref": "", // Идентификатор адреса
                "RegionRef": "", // Фильтр по идентификатору района
                "Page": "1", // Номер страницы
                "Warehouse": "", // Фильтр наличия отделений
                "FindByString": query // Поиск по строкам Украинский либо Русский язык
            },
            "language": "ua"
        });
        
        return this.httpClient.post(url, body, {params: this.params})
        .pipe(
            map((response: NPResponse) => {
                return response.data;
            }),
            catchError(err => {
                this.exceptionService.handleRequestErrorInfo(err, "Помилка отримання списку населених пунктів");
                return EMPTY;
            })
        );
    }
    
    getWarehouses(query?: string): Observable<any> {
        const url  = this.NOVA_POSHTA_API_URL + this.ADDRESS_GET_WAREHOUSES;
        const body = JSON.stringify({
            "modelName": "AddressGeneral",
            "calledMethod": "getWarehouses",
            "apiKey": this.API_KEY,
            "methodProperties": {
                "CityName": "", // Дополнительный фильтр по имени города
                "CityRef": query, // Дополнительный фильтр по идентификатору города
                "Page": 1, // Страница, максимум 500 записей на странице. Работает в связке с параметром Limit
                "Limit": 500, // Количество записей на странице. Работает в связке с параметром Page
                "language": "ua"
            }
        });
        
        return this.httpClient.post(url, body, {params: this.params})
        .pipe(
            tap((response: NPResponse) => {
                if ((response.errorCodes && response.errorCodes[0]) || (response.errors && response.errors[0])) {
                    const errorInfo: ErrorInfo = {
                        code: response.errorCodes[0],
                        message: response.errors[0],
                        data: null
                    };
                    this.exceptionService.handleRequestErrorInfo(errorInfo, "Помилка при пошуку населеного пункту");
                }
            }),
            map((response: NPResponse) => {
                return response.data;
            }),
            catchError(err => {
                this.exceptionService.handleRequestErrorInfo(err, "Помилка отримання списку населених пунктів");
                return EMPTY;
            })
        );
    }
    
    searchSettlements(cityName: string, limit: number = 50): Observable<NPSearchResponse> {
        const url = this.NOVA_POSHTA_API_URL + this.ADDRESS_SEARCH_SETTLEMETS;
        
        const body = JSON.stringify({
            "modelName": "Address",
            "calledMethod": "searchSettlements",
            "apiKey": this.API_KEY,
            "methodProperties": {
                "CityName": cityName,
                "Limit": limit,
                "language": "ua"
            }
        });
        
        return this.httpClient.post<NPSearchResponse>(url, body, {params: this.params})
        .pipe(
            tap((response: NPSearchResponse) => {
                if ((response.errorCodes && response.errorCodes[0]) || (response.errors && response.errors[0])) {
                    const errorInfo: ErrorInfo = {
                        code: response.errorCodes[0],
                        message: response.errors[0],
                        data: null
                    };
                    this.exceptionService.handleRequestErrorInfo(errorInfo, "Помилка при пошуку населеного пункту");
                }
            }),
            map((response: NPSearchResponse) => {
                return response;
            }),
            catchError(err => {
                this.exceptionService.handleRequestErrorInfo(err, "Помилка при пошуку населеного пункту");
                return EMPTY;
            })
        );
    }
    
    getCityName(ref: string): Observable<string> {
        const url = this.NOVA_POSHTA_API_URL + this.ADDRESS_GET_SETTLEMETS;
        
        const body = JSON.stringify({
            "modelName": "AddressGeneral",
            "calledMethod": "getSettlements",
            "apiKey": this.API_KEY,
            "methodProperties": {
                "Ref": ref,
                "Page": "1",
                "language": "ua"
            }
        });
        
        return this.httpClient.post(url, body, {params: this.params})
        .pipe(
            map((response: NPResponse): string => {
                return response.data[0].Description;
            }),
            catchError(err => {
                this.exceptionService.handleRequestErrorInfo(err, "Помилка отримання назви міста");
                return EMPTY;
            })
        );
    }
}
