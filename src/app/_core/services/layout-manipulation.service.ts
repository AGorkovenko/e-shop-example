import {DOCUMENT, isPlatformServer} from "@angular/common";
import {Inject, Injectable, PLATFORM_ID} from "@angular/core";
import {BehaviorSubject, Observable} from "rxjs";
import {LoggerService} from "./logger.service";

@Injectable()
export class LayoutManipulationService {
    
    private compensateScrollbarSource: BehaviorSubject<boolean> = new BehaviorSubject(undefined);
    public compensateScrollbar$: Observable<boolean>            = this.compensateScrollbarSource.asObservable();
    
    public hideSiteHeaderSource: BehaviorSubject<boolean> = new BehaviorSubject(false);
    public hideSiteHeader$: Observable<boolean>           = this.hideSiteHeaderSource.asObservable();
    
    public hideSiteSidebarSource: BehaviorSubject<boolean> = new BehaviorSubject(false);
    public hideSiteSidebar$: Observable<boolean>           = this.hideSiteSidebarSource.asObservable();
    
    public scrollbarWidth: number;
    
    constructor(private logger: LoggerService,
                @Inject(DOCUMENT) private document,
                @Inject(PLATFORM_ID) private platformId: Object) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    enableScrollbarCompensation(container: HTMLElement): void {
        if (isPlatformServer(this.platformId) || !this.isScrollbarPresent(container)) {
            return;
        }
        
        this.scrollbarWidth = this.getScrollbarWidth(container);
        
        const userSetPadding = container.style.paddingRight;
        const paddingAmount  = parseFloat(window.getComputedStyle(container)["padding-right"]);
        
        this.compensateScrollbar$.subscribe((compensate: boolean) => {
            this.adjustContainer(container, userSetPadding, paddingAmount, compensate);
        });
    }
    
    compensateScrollbar(compensate: boolean) {
        this.compensateScrollbarSource.next(compensate);
    }
    
    toggleSiteHeader(isHidden: boolean): void {
        this.hideSiteHeaderSource.next(isHidden);
    }
    
    toggleSiteSidebar(isHidden: boolean): void {
        this.hideSiteSidebarSource.next(isHidden);
    }
    
    private adjustContainer(container: HTMLElement, userSetPadding: string, paddingAmount: number, compensate: boolean = false): void {
        if (compensate) {
            container.style["padding-right"] = `${paddingAmount + this.scrollbarWidth}px`;
        } else {
            container.style["padding-right"] = userSetPadding;
        }
    }
    
    /**
     * Tells whether a scrollbar is currently present on the body.
     *
     * @return true if scrollbar is present, false otherwise
     */
    private isScrollbarPresent(container: HTMLElement): boolean {
        const rect = container.getBoundingClientRect();
        return rect.left + rect.right < window.innerWidth;
    }
    
    /**
     * Calculates and returns the width of a scrollbar.
     *
     * @return the width of a scrollbar on this page
     */
    private getScrollbarWidth(container: HTMLElement): number {
        const measurer     = this.document.createElement("div");
        measurer.className = "modal-scrollbar-measure";
        
        const body = this.document.body;
        body.appendChild(measurer);
        const width = measurer.getBoundingClientRect().width - measurer.clientWidth;
        body.removeChild(measurer);
        
        return width;
    }
}
