import {Injectable} from "@angular/core";
import {HttpCachingClient} from "../http-caching-client.service";
import {LoggerService} from "../logger.service";
import {Observable} from "rxjs";
import {HttpParams} from "@angular/common/http";
import {CONFIG} from "../../config";
import {ContentViewRequest} from "../../models/dto/content/content-view-request.model";
import {ContentType} from "../../models/dto/content/content-type.enum";
import {Content} from "../../models/dto/content/content.model";

@Injectable()
export class ContentBackendClient {
  
  PAGE_URL = "/page";
  PAGE_COUNT_URL = "/page/count";
  PAGE_ID_URL = "/page/{id}";
  PAGE_SLUG_URL = "/page/slug/{slug}";
  POST_URL = "/post";
  POST_COUNT_URL = "/post/count";
  POST_ID_URL = "/post/{id}";
  POST_SLUG_URL = "/post/slug/{slug}";
  
  constructor(private logger: LoggerService,
              private httpClient: HttpCachingClient) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  loadContents(viewRequest?: ContentViewRequest): Observable<Content[]> {
    const url = CONFIG.apiUrl + (viewRequest.type === ContentType.PAGE ? this.PAGE_URL : this.POST_URL);
    let params: HttpParams = new HttpParams();
    if (!viewRequest) viewRequest = new ContentViewRequest();
    if (viewRequest.q != null) params = params.set("q", viewRequest.q);
    if (viewRequest.order != null) params = params.set("order", viewRequest.order);
    if (viewRequest.sort != null) params = params.set("sort", viewRequest.sort);
    const offset = (viewRequest.page - 1) * viewRequest.pageSize;
    params = params.set("offset", offset.toString());
    params = params.set("limit", viewRequest.pageSize.toString());
    
    return this.httpClient.get(url, params);
  }
  
  loadCount(viewRequest: ContentViewRequest): Observable<number> {
    const url = CONFIG.apiUrl + (viewRequest.type === ContentType.PAGE ? this.PAGE_COUNT_URL : this.POST_COUNT_URL);
    let params: HttpParams = new HttpParams();
    if (viewRequest.q != null) params = params.set("q", viewRequest.q);
    return this.httpClient.get<number>(url, params);
  }
  
  loadContent(id: number, type: ContentType, lang?: string): Observable<Content> {
    let params: HttpParams = new HttpParams();
    if (lang) params = params.set("lang", lang);
    const url = CONFIG.apiUrl + (type === ContentType.PAGE ? this.PAGE_ID_URL.replace("{id}", id.toString()) : this.POST_ID_URL.replace("{id}", id.toString()));
    return this.httpClient.get(url, params);
  }
  
  loadContentBySlug(slug: string, type: ContentType, lang?: string): Observable<Content> {
    let params: HttpParams = new HttpParams();
    if (lang) params = params.set("lang", lang);
    const url = CONFIG.apiUrl + (type === ContentType.PAGE ? this.PAGE_SLUG_URL.replace("{slug}", slug) : this.POST_SLUG_URL.replace("{slug}", slug));
    return this.httpClient.get(url, params);
  }
}
