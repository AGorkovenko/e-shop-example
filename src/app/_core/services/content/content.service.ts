import {Injectable} from "@angular/core";
import {BehaviorSubject, EMPTY, Observable, of} from "rxjs";
import {catchError, map, tap} from "rxjs/operators";
import {LoggerService} from "../logger.service";
import {ExceptionService} from "../exception.service";
import iassign from "immutable-assign";
import {NotificationService} from "../notification.service";
import {Content} from "../../models/dto/content/content.model";
import {ContentViewRequest} from "../../models/dto/content/content-view-request.model";
import {ContentBackendClient} from "./content-backend.service";
import {ContentType} from "../../models/dto/content/content-type.enum";

export class ContentListData {
  viewRequest: ContentViewRequest;
  list: Content[];
  count: number;
}

@Injectable()
export class ContentService {
  
  private contentListDataSource: BehaviorSubject<ContentListData> = new BehaviorSubject(undefined);
  contentListData$: Observable<ContentListData> = this.contentListDataSource.asObservable();
  
  constructor(private logger: LoggerService,
              private exceptionService: ExceptionService,
              private contentBackendClient: ContentBackendClient,
              private notificationService: NotificationService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  initContentList(contentListData: ContentListData) {
    this.contentListDataSource.next(contentListData);
  }
  
  ContentViewRequest(): ContentViewRequest {
    return this.contentListDataSource.value.viewRequest;
  }
  
  loadAndAppendContent(page: number): Observable<void> {
    const viewRequest = Object.assign({}, this.contentListDataSource.getValue().viewRequest);
    viewRequest.page = page;
    
    return this.contentBackendClient.loadContents(viewRequest)
      .pipe(
        catchError(errorInfo => {
          this.exceptionService.handleRequestErrorInfo(errorInfo.error);
          return EMPTY;
        }),
        tap(content => {
          const updatedContentMetaListData: ContentListData = iassign(
            this.contentListDataSource.getValue(),
            contentListData => contentListData.list,
            contentList => {
              contentList.push(...content);
              return contentList;
            }
          );
          this.contentListDataSource.next(updatedContentMetaListData);
        }),
        map(() => {
        })
      );
  }
  
  getContent(id: number, type: ContentType, lang?: string): Observable<Content> {
    return this.contentBackendClient.loadContent(id, type, lang)
      .pipe(
        catchError(errorInfo => {
          // this.exceptionService.handleRequestErrorInfo(errorInfo.error);
          return of(null);
        }),
        tap((content: Content) => {
          if (content) this.notificationService.showSuccess("Loaded content for language", "Load Content");
        })
      );
  }
  
  getContentBySlug(slug: string, type: ContentType, lang?: string): Observable<Content> {
    return this.contentBackendClient.loadContentBySlug(slug, type, lang)
      .pipe(
        catchError(errorInfo => {
          // this.exceptionService.handleRequestErrorInfo(errorInfo.error);
          return of(null);
        }),
        tap((content: Content) => {
          if (content) this.notificationService.showSuccess("Loaded content for language", "Load Content");
        })
      );
  }
}
