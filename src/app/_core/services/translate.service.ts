import {Injectable} from "@angular/core";
import {LoggerService} from "./logger.service";
import {NotificationService} from "./notification.service";

@Injectable()
export class TranslateService {
	
	constructor(private logger: LoggerService,
	            private notificationService: NotificationService) {
		this.logger.debug(this.constructor.name + " is created.");
	}
	
	get_translate(name: string, parent: number, lang: string) {
		return true;
	}
}
