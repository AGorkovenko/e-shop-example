import {isPlatformServer} from "@angular/common";
import {Inject, Injectable, OnDestroy, PLATFORM_ID} from "@angular/core";
import {Meta, Title} from "@angular/platform-browser";
import {ActivatedRoute, Router, UrlTree} from "@angular/router";
import {from, Observable, Subscription} from "rxjs";
import {filter, map, switchMap, take} from "rxjs/operators";
import {CONFIG} from "../config";
import {LoggerService} from "./logger.service";
import {MetaLinkService} from "./meta-link.service";
import {SiteNavigationService} from "./site-navigation";
import {HttpCachingClient} from "./http-caching-client.service";

@Injectable()
export class SiteMetaDataService implements OnDestroy {
    
    private TITLE_TEMPLATE: string = "{pageTitle}";
    private ogTitleElement: HTMLMetaElement;
    
    constructor(private logger: LoggerService,
                private httpCachingClient: HttpCachingClient,
                @Inject(PLATFORM_ID) private platformId: Object,
                private siteNavigationService: SiteNavigationService,
                private activatedRoute: ActivatedRoute,
                private router: Router,
                private metaService: Meta,
                private metaLinkService: MetaLinkService,
                private titleService: Title) {
        this.logger.debug(this.constructor.name + " is created.");
        
        this.ogTitleElement = this.metaService.addTag({property: "og:title"});
        this.setGeneralMetaData();
    }
    
    enableSiteTitleSetting(): Subscription {
        return this.siteNavigationService.pageTitle$.subscribe(newPageTitle => {
            const siteTitle = this.TITLE_TEMPLATE.replace("{pageTitle}", newPageTitle);
            this.titleService.setTitle(siteTitle);
            this.ogTitleElement.content = siteTitle;
        });
    }
    
    specifyTitleWithAdditionalText(text: string) {
        this.siteNavigationService.pageTitle$
        .pipe(take(1))
        .subscribe(currentPageTitle => {
            const siteTitle = this.TITLE_TEMPLATE.replace("{pageTitle}", currentPageTitle + " - " + text);
            this.titleService.setTitle(siteTitle);
            this.ogTitleElement.content = siteTitle;
        });
    }
    
    setCustomTitle(text: string): void {
        this.titleService.setTitle(text);
        this.ogTitleElement.content = text;
    }
    
    setDescription(description: string): Observable<void> {
        return new Observable<void>(observer => {
            let descriptionElement = this.metaService.getTag("name=description");
            
            if (descriptionElement) {
                descriptionElement = this.metaService.updateTag({name: "description", content: description});
            } else {
                descriptionElement = this.metaService.addTag({name: "description", content: description});
            }
            observer.next();
            return () => this.metaService.removeTagElement(descriptionElement);
        });
    }
    
    setAdditionalOgMetaTags(description: string | null = null,
                            imageUrl: string | null    = null,
                            pageUrl: string | null     = null): Observable<void> {
        return new Observable(observer => {
            const metaElements: HTMLMetaElement[] = [];
            if (description) {
                let ogDescriptionElement = this.metaService.getTag("name=description");
                
                if (ogDescriptionElement) {
                    ogDescriptionElement = this.metaService.updateTag({property: "og:description", content: description});
                } else {
                    ogDescriptionElement = this.metaService.addTag({property: "og:description", content: description});
                }
                
                metaElements.push(ogDescriptionElement);
            }
            if (imageUrl) {
                const ogImageUrl = this.metaService.addTag({property: "og:image", content: imageUrl});
                metaElements.push(ogImageUrl);
            }
            if (pageUrl) {
                const ogUrlElement = this.metaService.addTag({property: "og:url", content: pageUrl});
                metaElements.push(ogUrlElement);
            }
            observer.next();
            return () => metaElements.forEach(element => this.metaService.removeTagElement(element));
        });
    }
    
    enableServerCanonicalRefGeneration(): Subscription {
        const excludeUrlPatterns: RegExp[] = [/^(\/users\/\d+)$/];
        
        if (!isPlatformServer(this.platformId)) {
            return Subscription.EMPTY;
        }
        
        return this.siteNavigationService.navigationEndEvents$.subscribe(navigationEnd => {
            const urlTree: UrlTree = this.router.parseUrl(navigationEnd.urlAfterRedirects);
            let currentUrlWithoutParams;
            if (urlTree.root.children["primary"]) {
                currentUrlWithoutParams = "/" + urlTree.root.children["primary"].segments.map(segment => segment.path).join("/");
            } else {
                currentUrlWithoutParams = "/";
            }
            
            let canonicalUrl: string | null = null;
            
            const allRoutes: ActivatedRoute[] = [];
            let currentRoute                  = this.activatedRoute;
            allRoutes.push(currentRoute);
            while (currentRoute.firstChild) {
                currentRoute = currentRoute.firstChild;
                allRoutes.push(currentRoute);
            }
            
            // add canonical url for pages, that can have multiple urls
            from(allRoutes)
            .pipe(
                filter(route => route.outlet === "primary"),
                switchMap(route => route.params)
            )
            .subscribe(routeParams => {
                if (routeParams["postKid"]) {
                    // add canonical url to post with ID in URL
                    if (!isNaN(routeParams["postKid"])) {
                        from(allRoutes)
                        .pipe(
                            filter(route => route.outlet === "primary"),
                            switchMap(route => route.data),
                            filter(data => data["post"]),
                            map(data => data["post"])
                        )
                        .subscribe(post => {
                            canonicalUrl = currentUrlWithoutParams.replace(routeParams["postKid"], post.key);
                        });
                    }
                } else if (routeParams["groupKid"]) {
                    // add canonical url to group with ID in URL
                    if (!isNaN(routeParams["groupKid"])) {
                        from(allRoutes)
                        .pipe(
                            filter(route => route.outlet === "primary"),
                            switchMap(route => route.data),
                            filter(data => data["group"]),
                            map(data => data["group"])
                        )
                        .subscribe(post => {
                            canonicalUrl = currentUrlWithoutParams.replace(routeParams["groupKid"], post.key);
                        });
                    }
                } else if (routeParams["questionId"]) {
                    // add canonical url to question with KEY in URL
                    if (isNaN(routeParams["questionId"])) {
                        from(allRoutes)
                        .pipe(
                            filter(route => route.outlet === "primary"),
                            switchMap(route => route.data),
                            filter(data => data["question"]),
                            map(data => data["question"])
                        )
                        .subscribe(question => {
                            canonicalUrl = currentUrlWithoutParams.replace(routeParams["questionId"], question.id.toString());
                        });
                    }
                }
            });
            
            // if url has query params - add canonical url without them
            if (!canonicalUrl && urlTree.queryParamMap.keys.length > 0) {
                canonicalUrl = currentUrlWithoutParams;
            }
            
            for (let i = 0; i < excludeUrlPatterns.length; i++) {
                if (canonicalUrl && canonicalUrl.match(excludeUrlPatterns[i])) {
                    return Subscription.EMPTY;
                }
            }
            
            if (canonicalUrl) {
                this.metaLinkService.addTag({rel: "canonical", href: canonicalUrl});
            }
        });
    }
    
    private setGeneralMetaData() {
        if (isPlatformServer(this.platformId)) {
            this.metaService.addTag({property: "og:site_name", content: "Admin panel by GA"});
            this.metaService.addTag({property: "og:type", content: "website"});
        }
    }
    
    ngOnDestroy(): void {
        if (this.ogTitleElement) {
            this.metaService.removeTagElement(this.ogTitleElement);
        }
    }
    
}
