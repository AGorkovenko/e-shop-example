import {isPlatformBrowser, DOCUMENT, isPlatformServer} from "@angular/common";
import {Inject, Injectable, PLATFORM_ID} from "@angular/core";
import {Observable} from "rxjs";
import {EMPTY} from "rxjs/internal/observable/empty";
import {fromEvent} from "rxjs/internal/observable/fromEvent";
import {LoggerService} from "./logger.service";

@Injectable()
export class PageNavigationService {
    
    private pageAnchors: Map<string, HTMLElement> = new Map();
    
    constructor(private logger: LoggerService,
                @Inject(DOCUMENT) private document,
                @Inject(PLATFORM_ID) private platformId: Object) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    addVirtualAnchor(name: string, element: HTMLElement) {
        this.pageAnchors.set(name, element);
    }
    
    removeVirtualAnchor(name: string) {
        this.pageAnchors.delete(name);
    }
    
    getHtmlElementByIdOrName(idOrName: string): HTMLElement | null {
        let element: HTMLElement = null;
        element                  = this.document.getElementById(idOrName);
        if (!element) {
            const elementsByName: HTMLElement[] = this.document.getElementsByName(idOrName);
            if (elementsByName.length > 0) {
                element = elementsByName[0];
            }
        }
        return element;
    }
    
    isAnchorPresentOnPage(name: string): boolean {
        if (isPlatformBrowser(this.platformId)) {
            return this.pageAnchors.has(name) || !!this.getHtmlElementByIdOrName(name);
        }
        return this.pageAnchors.has(name);
    }
    
    scrollToAnchor(name: string, duration: number) {
        if (isPlatformBrowser(this.platformId)) {
            let elementTarget: HTMLElement;
            elementTarget = this.pageAnchors.get(name);
            if (!elementTarget) {
                elementTarget = this.getHtmlElementByIdOrName(name);
            }
            
            if (elementTarget) {
                if (window.location.hash !== name) {
                    window.location.hash = name;
                }
                this.scrollTo(elementTarget, duration);
            }
        }
    }
    
    enableOldStyleHashNavigation(element: HTMLElement): Observable<void> {
        if (isPlatformServer(this.platformId)) {
            return EMPTY;
        }
        return new Observable<void>(_ => {
            const globalClickObservable = fromEvent(element, "click")
            .subscribe((event: any) => {
                const hrefAttribute: string = event.target.getAttribute("href");
                if (hrefAttribute && hrefAttribute.startsWith("#")) {
                    const fragment     = hrefAttribute.substr(1);
                    const matchingNode = this.getHtmlElementByIdOrName(fragment);
                    if (matchingNode) {
                        this.scrollTo(matchingNode, 300);
                        window.location.hash = fragment;
                    }
                    event.preventDefault();
                }
            });
            
            return () => globalClickObservable.unsubscribe();
        });
    }
    
    // https://stackoverflow.com/questions/17722497/scroll-smoothly-to-specific-element-on-page
    private scrollTo(element: HTMLElement, duration: number) {
        const startingY = window.pageYOffset;
        const elementY  = window.pageYOffset + element.getBoundingClientRect().top;
        // If element is close to content's bottom then window will scroll only to some position above the element.
        const targetY   = document.body.scrollHeight - elementY < window.innerHeight ? document.body.scrollHeight - window.innerHeight : elementY;
        const diff      = targetY - startingY;
        
        // Easing function: easeInOutCubic
        // From: https://gist.github.com/gre/1650294
        const easing = function (t) {
            return t < .5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1;
        };
        let start;
        
        if (!diff) {
            return;
        }
        
        // Bootstrap our animation - it will get called right before next frame shall be rendered.
        window.requestAnimationFrame(function step(timestamp) {
            if (!start) {
                start = timestamp;
            }
            // Elapsed miliseconds since start of scrolling.
            const time  = timestamp - start;
            // Get percent of completion in range [0, 1].
            let percent = Math.min(time / duration, 1);
            // Apply the easing.
            // It can cause bad-looking slow frames in browser performance tool, so be careful.
            percent = easing(percent);
            
            window.scrollTo(0, startingY + diff * percent);
            
            // Proceed with animation as long as we wanted it to.
            if (time < duration) {
                window.requestAnimationFrame(step);
            }
        });
    }
}
