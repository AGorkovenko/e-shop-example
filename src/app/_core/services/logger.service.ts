import {Injectable} from "@angular/core";
import {environment} from "../../../environments/environment";
import {NotificationService} from "./notification.service";

export enum LogLevel {
  ERROR,
  WARN,
  INFO,
  DEBUG
}

@Injectable()
export class LoggerService {

  private currentLogLevel: LogLevel = LogLevel[environment.logLevel.toUpperCase()];

  constructor(private notificationService: NotificationService) {
    this.debug(this.constructor.name + " is created.");
  }

  error(message: string, ...objectToLog: any[]) {
    this.log(message, LogLevel.ERROR, ...objectToLog);
    this.notificationService.showError(message);
  }

  warn(message: string, ...objectToLog: any[]) {
    this.log(message, LogLevel.WARN, ...objectToLog);
	  this.notificationService.showWarning(message);
  }

  info(message: string, ...objectToLog: any[]) {
    this.log(message, LogLevel.INFO, ...objectToLog);
  }

  debug(message: string, ...objectToLog: any[]) {
    this.log(message, LogLevel.DEBUG, ...objectToLog);
  }

  setLogLevel(logLevel: LogLevel) {
    this.currentLogLevel = logLevel;
  }

  getCurrentLogLevel() {
    return this.currentLogLevel;
  }

  private log(message: string, logLevel: LogLevel, ...objectToLog: any[]) {
    if (logLevel <= this.currentLogLevel) {
      const objects: any[] = objectToLog.map(object => {
        return object instanceof Object ? Object.assign({}, object) : object;
      });
      switch (logLevel) {
        case LogLevel.DEBUG:
          console.debug(message, ...objects);
          break;
        case LogLevel.INFO:
          console.info(message, ...objects);
          break;
        case LogLevel.WARN:
          console.warn(message, ...objects);
          break;
        case LogLevel.ERROR:
          console.error(message, ...objects);
          break;
      }
    }
  }
}
