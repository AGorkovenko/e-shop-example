import {Injectable} from "@angular/core";
import {LoggerService} from "./logger.service";
import * as moment from "moment";

@Injectable()
export class TimeService {
  
  constructor(private logger: LoggerService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  getTimeStamp(date: Date | string, format?: string): string {
    if (!format) {
      format = "DD.MM.YYYY";
    }
    return moment(date, "YYYY-MM-DD HH:mm:ss").format(format).toString();
  }
}
