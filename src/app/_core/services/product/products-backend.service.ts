import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {HttpParams} from "@angular/common/http";
import {CONFIG} from "../../config";
import {LoggerService} from "../logger.service";
import {HttpCachingClient} from "../http-caching-client.service";
import {Product} from "../../models/dto/product/product.model";
import {ProductViewRequest} from "../../models/dto/product/product-view-request.model";
import {ProductRequest} from "../../models/dto/product/product.request";

@Injectable()
export class ProductsBackendClient {
    
    PRODUCTS_URL            = "/products";
    PRODUCTS_COUNT_URL      = "/products/count";
    PRODUCTS_PRICES_URL     = "/products/prices";
    PRODUCTS_MULTIPLE_URL   = "/products/multiple";
    PRODUCT_URL             = "/products/{id}";
    PRODUCT_SLUG_URL        = "/products/slug/{slug}";
    PRODUCT_URL_TRANSLATION = "/products/{id}/translation";
    
    constructor(private logger: LoggerService,
                private httpClient: HttpCachingClient) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    loadProducts(viewRequest?: ProductViewRequest, lang?: string): Observable<Product[]> {
        const url              = CONFIG.apiUrl + this.PRODUCTS_URL;
        let params: HttpParams = new HttpParams();
        if (!viewRequest) {
            viewRequest = new ProductViewRequest();
        }
        if (lang) {
            params = params.set("lang", lang);
        }
        if (viewRequest.order != null) {
            params = params.set("order", viewRequest.order);
        }
        if (viewRequest.sort != null) {
            params = params.set("sort", viewRequest.sort);
        }
        if (viewRequest.q != null) {
            params = params.set("q", viewRequest.q);
        }
        if (viewRequest.minPrice) {
            params = params.set("minPrice", viewRequest.minPrice.toString());
        }
        if (viewRequest.maxPrice) {
            params = params.set("maxPrice", viewRequest.maxPrice.toString());
        }
        if (viewRequest.category) {
            params = params.set("categories", viewRequest.category.toString());
        } // for this module only one category
        const offset = (viewRequest.page - 1) * viewRequest.pageSize;
        params       = params.set("offset", offset.toString());
        params       = params.set("limit", viewRequest.pageSize.toString());
        
        return this.httpClient.get(url, params);
    }
    
    loadCount(viewRequest: ProductViewRequest): Observable<number> {
        const url              = CONFIG.apiUrl + this.PRODUCTS_COUNT_URL;
        let params: HttpParams = new HttpParams();
        if (viewRequest.q != null) {
            params = params.set("q", viewRequest.q);
        }
        if (viewRequest.category) {
            params = params.set("categories", viewRequest.category.toString());
        }
        return this.httpClient.get<number>(url, params);
    }
    
    loadProduct(id: number, lang?: string): Observable<Product> {
        const url              = CONFIG.apiUrl + this.PRODUCT_URL.replace("{id}", id.toString());
        let params: HttpParams = new HttpParams();
        params                 = params.set("lang", lang);
        return this.httpClient.get(url, params);
    }
    
    addProduct(request: ProductRequest): Observable<number> {
        const url  = CONFIG.apiUrl + this.PRODUCTS_URL;
        const body = {
            title: request.title,
            sku: request.sku,
            original_sku: request.original_sku,
            slug: request.slug,
            category: request.category,
            price: request.price,
            old_price: request.old_price,
            dealer_price: request.dealer_price ? request.dealer_price : null,
            distributor_price: request.distributor_price ? request.distributor_price : null,
            stock: request.stock,
            description: request.description,
            photo: request.photo,
            gallery: request.gallery,
            excerpt: request.excerpt,
            upsell: request.upsell,
            crosssell: request.crosssell,
            site_id: request.site ? request.site.value : null
        };
        return this.httpClient.post<number>(url, body);
    }
    
    updateProduct(request: ProductRequest, lang?: string): Observable<Product> {
        const url  = CONFIG.apiUrl + this.PRODUCT_URL.replace("{id}", request.id.toString());
        const body = {
            title: request.title,
            sku: request.sku,
            original_sku: request.original_sku,
            slug: request.slug,
            category: request.category,
            price: request.price,
            old_price: request.old_price,
            dealer_price: request.dealer_price ? request.dealer_price : null,
            distributor_price: request.distributor_price ? request.distributor_price : null,
            stock: request.stock,
            description: request.description,
            photo: request.photo,
            gallery: request.gallery,
            excerpt: request.excerpt,
            upsell: request.upsell,
            crosssell: request.crosssell,
            site_id: request.site ? request.site.value : null,
            lang: lang
        };
        return this.httpClient.post<Product>(url, body);
    }
    
    loadProductTranslations(id: number): Observable<any> {
        const url = CONFIG.apiUrl + this.PRODUCT_URL_TRANSLATION.replace("{id}", id.toString());
        return this.httpClient.get<any>(url);
    }
    
    removeProduct(id: number): Observable<boolean> {
        const url = CONFIG.apiUrl + this.PRODUCT_URL.replace("{id}", id.toString());
        return this.httpClient.delete<boolean>(url);
    }
    
    removeProducts(ids: number[]): Observable<boolean> {
        const url  = CONFIG.apiUrl + this.PRODUCTS_URL;
        const body = {
            "ids": ids
        };
        return this.httpClient.delete<boolean>(url, body);
    }
    
    moveToCategories(productIds: number[], categoryIds: number[]): Observable<boolean> {
        const url  = CONFIG.apiUrl + this.PRODUCTS_MULTIPLE_URL;
        const body = {
            "ids": productIds,
            "categories": categoryIds
        };
        return this.httpClient.post<boolean>(url, body);
    }
    
    loadProductBySlug(slug: string, lang?: string): Observable<Product> {
        const url  = CONFIG.apiUrl + this.PRODUCT_SLUG_URL.replace("{slug}", slug);
        let params = new HttpParams();
        params     = params.append("lang", lang);
        return this.httpClient.get<Product>(url, params);
    }
    
    loadPrices(viewRequest: ProductViewRequest): Observable<{min: number, max: number}> {
        const url              = CONFIG.apiUrl + this.PRODUCTS_PRICES_URL;
        let params: HttpParams = new HttpParams();
        if (viewRequest.q != null) {
            params = params.set("q", viewRequest.q);
        }
        if (viewRequest.category) {
            params = params.set("categories", viewRequest.category.toString());
        }
        return this.httpClient.get<{min: number, max: number}>(url, params);
    }
}
