import {Injectable} from "@angular/core";
import {BehaviorSubject, EMPTY, Observable, of} from "rxjs";
import {catchError, map, tap} from "rxjs/operators";
import iassign from "immutable-assign";
import {Product} from "../../models/dto/product/product.model";
import {LoggerService} from "../logger.service";
import {ExceptionService} from "../exception.service";
import {NotificationService} from "../notification.service";
import {ProductsBackendClient} from "./products-backend.service";
import {ProductViewRequest} from "../../models/dto/product/product-view-request.model";
import {ProductRequest} from "../../models/dto/product/product.request";
import {ProductCategory} from "../../models/dto/product/product-category.model";

export class ProductListData {
    viewRequest: ProductViewRequest;
    list: Product[];
    count: number;
    minValue: number;
    maxValue: number;
    categories: ProductCategory[]
}

@Injectable()
export class ProductsService {
    
    private productListDataSource: BehaviorSubject<ProductListData> = new BehaviorSubject(undefined);
    productListData$: Observable<ProductListData>                   = this.productListDataSource.asObservable();
    
    constructor(private logger: LoggerService,
                private exceptionService: ExceptionService,
                private productsBackendClient: ProductsBackendClient,
                private notificationService: NotificationService) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    initProductList(productListData: ProductListData) {
        this.productListDataSource.next(productListData);
    }
    
    productViewRequest(): ProductViewRequest {
        return this.productListDataSource.value.viewRequest;
    }
    
    loadAndAppendPage(page: number, lang?: string): Observable<void> {
        const viewRequest = Object.assign({}, this.productListDataSource.getValue().viewRequest);
        viewRequest.page  = page;
        
        return this.productsBackendClient.loadProducts(viewRequest, lang)
        .pipe(
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            }),
            tap(products => {
                const updatedProductListData: ProductListData = iassign(
                    this.productListDataSource.getValue(),
                    productListData => productListData.list,
                    productList => {
                        productList.push(...products);
                        return productList;
                    }
                );
                this.productListDataSource.next(updatedProductListData);
            }),
            map(() => {
            })
        );
    }
    
    getProduct(id: number, lang?: string): Observable<Product> {
        return this.productsBackendClient.loadProduct(id)
        .pipe(
            tap((product: Product) => {
                if (product) {
                    this.notificationService.showSuccess("Loaded product", "Load Product");
                }
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return of(null);
            })
        );
    }
    
    editProduct(formData: ProductRequest, lang?: string): Observable<Product> {
        return this.productsBackendClient.updateProduct(formData, lang)
        .pipe(
            tap((product: Product) => {
                if (Product) {
                    this.notificationService.showSuccess("All data for product updated", "Edit product");
                }
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            })
        );
    }
    
    createProduct(formData: ProductRequest): Observable<number> {
        return this.productsBackendClient.addProduct(formData)
        .pipe(
            tap((id: number) => {
                if (id) {
                    this.notificationService.showSuccess("New Product was created", "Create Product");
                }
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            })
        );
    }
    
    deleteProduct(id: number): Observable<boolean> {
        return this.productsBackendClient.removeProduct(id)
        .pipe(
            tap(() => {
                this.notificationService.showSuccess("Product deleted", "Delete Product");
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            })
        );
    }
    
    deleteProducts(ids: number[]): Observable<boolean> {
        return this.productsBackendClient.removeProducts(ids)
        .pipe(
            tap(() => {
                this.notificationService.showSuccess("Products deleted", "Delete Products");
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            })
        );
    }
    
    changeCategories(productIds: number[], categoryIds: number[]) {
        return this.productsBackendClient.moveToCategories(productIds, categoryIds)
        .pipe(
            tap(() => {
                this.notificationService.showSuccess("Products moved to categories", "Move Products");
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            })
        );
    }
    
    getProductBySlug(slug: string, lang?: string): Observable<Product> {
        return this.productsBackendClient.loadProductBySlug(slug, lang)
        .pipe(
            tap((product: Product) => {
                if (product) {
                    this.notificationService.showSuccess("Loaded product", "Load Product");
                }
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return of(null);
            })
        );
    }
    
    getMaxMinPrices(): Observable<{min: number, max: number}> {
        const viewRequest = Object.assign({}, this.productListDataSource.getValue().viewRequest);
        return this.productsBackendClient.loadPrices(viewRequest)
        .pipe(
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            })
        );
    }
}
