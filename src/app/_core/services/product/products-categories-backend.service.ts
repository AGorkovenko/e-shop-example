import {Injectable} from "@angular/core";
import {LoggerService} from "../logger.service";
import {HttpCachingClient} from "../http-caching-client.service";
import {CONFIG} from "../../config";
import {ProductCategory} from "../../models/dto/product/product-category.model";
import {HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {CategoryOrder} from "../../models/dto/product/category-order.model";
import {ProductCategoryRequest} from "../../models/dto/product/product-category.request";

@Injectable()
export class ProductsCategoriesBackendClient {
  
  CATEGORIES_URL = "/products/categories";
  CATEGORY_URL = "/products/categories/{id}";
  CATEGORY_SLUG_URL = "/products/categories/slug/{slug}";
  CATEGORIES_URL_TRANSLATION = "/products/categories/translation";
  
  constructor(private logger: LoggerService,
              private httpClient: HttpCachingClient) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  loadCategories(lang?: string): Observable<ProductCategory[]> {
    const url = CONFIG.apiUrl + this.CATEGORIES_URL;
    let params: HttpParams = new HttpParams();
    if (lang) {
      params = params.append("lang", lang);
    }
    return this.httpClient.get<ProductCategory[]>(url, params);
  }
  
  loadCategory(id: number, lang?: string): Observable<ProductCategory> {
    const url = CONFIG.apiUrl + this.CATEGORY_URL.replace("{id}", id.toString());
    let params = new HttpParams();
    params = params.append("lang", lang);
    return this.httpClient.get<ProductCategory>(url, params);
  }
  
  loadCategoryBySlug(slug: string, lang?: string): Observable<ProductCategory> {
    const url = CONFIG.apiUrl + this.CATEGORY_SLUG_URL.replace("{slug}", slug);
    let params = new HttpParams();
    params = params.append("lang", lang);
    return this.httpClient.get<ProductCategory>(url, params);
  }
  
  loadCategoriesTranslations(): Observable<any> {
    const url = CONFIG.apiUrl + this.CATEGORIES_URL_TRANSLATION;
    return this.httpClient.get<any>(url);
  }
}
