import {Injectable} from "@angular/core";
import {BehaviorSubject, Observable, of} from "rxjs";
import {LoggerService} from "../logger.service";
import {ExceptionService} from "../exception.service";
import {NotificationService} from "../notification.service";
import {ProductCategory} from "../../models/dto/product/product-category.model";
import {ProductsCategoriesBackendClient} from "./products-categories-backend.service";
import {catchError, tap} from "rxjs/operators";
import {CategoryOrder} from "../../models/dto/product/category-order.model";

@Injectable()
export class ProductsCategoriesService {
    
    private productsCategoriesSource: BehaviorSubject<ProductCategory[]> = new BehaviorSubject([]);
    productsCategories$: Observable<ProductCategory[]>                   = this.productsCategoriesSource.asObservable();
    
    constructor(private logger: LoggerService,
                private exceptionService: ExceptionService,
                private productsCategoriesBackendClient: ProductsCategoriesBackendClient,
                private notificationService: NotificationService) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    initCategories(categories: ProductCategory[]) {
        this.productsCategoriesSource.next(categories);
    }
    
    getCategories(lang?: string): Observable<ProductCategory[]> {
        return this.productsCategoriesBackendClient.loadCategories(lang)
        .pipe(
            tap((categories: ProductCategory[]) => {
                this.productsCategoriesSource.next(categories);
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return of(null);
            })
        );
    }
    
    setOrder(categories: ProductCategory[], parentItem: number = 0): CategoryOrder[] {
        return categories.map((category: ProductCategory, index: number) => {
            const sortCategory: CategoryOrder = new CategoryOrder();
            sortCategory.ID                   = category.ID;
            sortCategory.order                = index;
            sortCategory.parent               = parentItem;
            if (category.children) {
                sortCategory.children = this.setOrder(category.children, category.ID);
            }
            return sortCategory;
        });
    }
    
    getCategory(id: number, lang?: string): Observable<ProductCategory> {
        return this.productsCategoriesBackendClient.loadCategory(id, lang)
        .pipe(
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return of(null);
            })
        );
    }
    
    getCategoryBySlug(slug: string, lang?: string): Observable<ProductCategory> {
        return this.productsCategoriesBackendClient.loadCategoryBySlug(slug, lang)
        .pipe(
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return of(null);
            })
        );
    }
}
