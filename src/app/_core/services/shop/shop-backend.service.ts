import {Injectable} from "@angular/core";
import {HttpCachingClient} from "../http-caching-client.service";
import {LoggerService} from "../logger.service";
import {Observable} from "rxjs";
import {CONFIG} from "../../config";
import {HttpParams} from "@angular/common/http";
import {ShopSetting} from "../../models/dto/shop/shop-setting.model";
import {ShopSettingRequest} from "../../models/dto/shop/shop-setting.request";
import {ShopSettingsViewRequest} from "../../models/dto/shop/shop-setting-view-request.model";

@Injectable()
export class ShopBackendClient {
  
  SHOP_SETTINGS_URL = "/shop/settings";
  SHOP_SETTINGS_COUNT_URL = "/shop/settings/count";
  SHOP_SETTING_ID_URL = "/shop/settings/{id}";
  SHOP_SETTING_NAME_URL = "/shop/settings/{name}";
  
  constructor(private logger: LoggerService,
              private httpClient: HttpCachingClient) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  loadSettings(viewRequest?: ShopSettingsViewRequest): Observable<ShopSetting[]> {
    const url = CONFIG.apiUrl + this.SHOP_SETTINGS_URL;
    let params: HttpParams = new HttpParams();
    if (!viewRequest) {
      viewRequest = new ShopSettingsViewRequest();
    }
    let settingKeysString: string;
    if (viewRequest.settings_name) {
      viewRequest.settings_name.forEach(settingKey => {
        settingKeysString = settingKeysString ? settingKeysString + ",'" + settingKey + "'" : "'" + settingKey + "'";
      });
    }
    if (settingKeysString) {
      params = params.set("settings_name", settingKeysString);
    }
    if (viewRequest.order != null) {
      params = params.set("order", viewRequest.order);
    }
    if (viewRequest.sort != null) {
      params = params.set("sort", viewRequest.sort);
    }
    const offset = (viewRequest.page - 1) * viewRequest.pageSize;
    params = params.set("offset", offset.toString());
    params = params.set("limit", viewRequest.pageSize.toString());
    
    return this.httpClient.get(url, params);
  }
  
  loadCount(viewRequest: ShopSettingsViewRequest): Observable<number> {
    const url = CONFIG.apiUrl + this.SHOP_SETTINGS_COUNT_URL;
    let params: HttpParams = new HttpParams();
    let settingKeysString: string;
    if (viewRequest.settings_name) {
      viewRequest.settings_name.forEach(settingKey => {
        settingKeysString = settingKeysString ? settingKeysString + ",'" + settingKey + "'" : "'" + settingKey + "'";
      });
    }
    if (settingKeysString) {
      params = params.set("settings_name", settingKeysString);
    }
    return this.httpClient.get<number>(url, params);
  }
  
  loadSetting(id: number): Observable<ShopSetting> {
    const url = CONFIG.apiUrl + this.SHOP_SETTING_ID_URL.replace("{id}", id.toString());
    return this.httpClient.get<any>(url);
  }
  
  loadSettingByName(name: string): Observable<any> {
    const url = CONFIG.apiUrl + this.SHOP_SETTING_NAME_URL.replace("{name}", name);
    return this.httpClient.get<any>(url);
  }
}
