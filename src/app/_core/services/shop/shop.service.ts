import {Injectable} from "@angular/core";
import {BehaviorSubject, Observable, of} from "rxjs";
import {catchError, tap} from "rxjs/operators";
import {LoggerService} from "../logger.service";
import {ExceptionService} from "../exception.service";
import {NotificationService} from "../notification.service";
import {ShopBackendClient} from "./shop-backend.service";
import {ShopSetting} from "../../models/dto/shop/shop-setting.model";

export class ProductDiscount {
  value: number;
  type: string;
}

@Injectable()
export class ShopService {
  
  private discountSource: BehaviorSubject<ProductDiscount> = new BehaviorSubject(null);
  discountSource$: Observable<ProductDiscount> = this.discountSource.asObservable();
  
  constructor(private logger: LoggerService,
              private exceptionService: ExceptionService,
              private shopBackendClient: ShopBackendClient,
              private notificationService: NotificationService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  initDiscount(value: number, type: string) {
    if (value && value >= 0 && type) {
      const discount: ProductDiscount = new ProductDiscount();
      discount.value = value;
      discount.type = type;
      this.discountSource.next(discount);
    }
  }
  
  getDiscount(): ProductDiscount {
    return this.discountSource.getValue();
  }
  
  resetDiscount() {
    this.discountSource.next(null);
  }
  
  getSettings(): Observable<ShopSetting[]> {
    return this.shopBackendClient.loadSettings()
    .pipe(
      catchError(errorInfo => {
        this.exceptionService.handleRequestErrorInfo(errorInfo.error);
        return of(null);
      }),
      tap(setting => {
        if (setting) {
          this.notificationService.showSuccess("Loaded shop settings", "Load settings");
        }
      })
    );
  }
  
  getSetting(id: number): Observable<ShopSetting> {
    return this.shopBackendClient.loadSetting(id)
    .pipe(
      catchError(errorInfo => {
        this.exceptionService.handleRequestErrorInfo(errorInfo.error);
        return of(null);
      }),
      tap(setting => {
        if (setting) {
          // this.notificationService.showSuccess("Loaded shop setting", "Load setting");
        }
      })
    );
  }
  
  getSettingByName(name: string): Observable<any> {
    return this.shopBackendClient.loadSettingByName(name)
    .pipe(
      catchError(errorInfo => {
        this.exceptionService.handleRequestErrorInfo(errorInfo.error);
        return of(null);
      }),
      tap(setting => {
        if (setting) {
          // this.notificationService.showSuccess("Loaded shop setting", "Load setting");
        }
      })
    );
  }
}
