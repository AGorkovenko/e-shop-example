import {Injectable} from "@angular/core";
import {LoggerService} from "./logger.service";
import {HttpCachingClient} from "./http-caching-client.service";
import {CONFIG} from "../config";
import {PaymentRequest} from "../models/dto/payment/payment-request.model";
import {Observable} from "rxjs";
import {PaymentInfo} from "../models/dto/payment/payment-info.model";
import {HttpParams} from "@angular/common/http";
import {CartService} from "./cart.service";

@Injectable()
export class PaymentService {
  
  PAYMENT_URL: string = "/payment";
  
  constructor(private logger: LoggerService,
              private httpClient: HttpCachingClient,
              private cartService: CartService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  getPayment(data: any): Observable<PaymentInfo> {
    const url = CONFIG.apiUrl + this.PAYMENT_URL;
    let params = new HttpParams();
    const uid = this.cartService.getTempUser();
    if (data.id) params = params.set("id", data.id.toString());
    if (data.uid) params = params.set("uid", data.uid.toString());
    return this.httpClient.get<PaymentInfo>(url, params);
  }
  
  createBillPayment(request: PaymentRequest): Observable<number> {
    const url = CONFIG.apiUrl + this.PAYMENT_URL;
    const body = {
      order_id: request.orderId,
      uid: request.uid,
      type: request.type,
      system_id: request.systemId,
      status: request.status,
      amount: request.amount,
      currency: request.currency,
      data: request.data
    };
    return this.httpClient.post<number>(url, body);
  }
}
