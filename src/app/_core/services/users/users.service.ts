import {Injectable} from "@angular/core";
import {BehaviorSubject, EMPTY, Observable, of} from "rxjs";
import {LoggerService} from "../logger.service";
import {ExceptionService} from "../exception.service";
import {NotificationService} from "../notification.service";
import {catchError, map, tap} from "rxjs/operators";
import iassign from "immutable-assign";
import {User} from "../../models/dto/user/user.model";
import {UserViewRequest} from "../../models/dto/user/user-view-request.model";
import {UsersBackendClient} from "./users-backend.service";
import {UserRequest} from "../../models/dto/user/user.request";
import {UserDelivery} from "../../models/dto/user/user-delivery.model";

export class UserListData {
    viewRequest: UserViewRequest;
    list: User[];
    count: number;
}

@Injectable()
export class UsersService {
    
    private userListDataSource: BehaviorSubject<UserListData> = new BehaviorSubject(undefined);
    userListData$: Observable<UserListData>                   = this.userListDataSource.asObservable();
    
    private allUsersSource: BehaviorSubject<User[]> = new BehaviorSubject([]);
    allUsers$: Observable<User[]>                   = this.allUsersSource.asObservable();
    
    constructor(private logger: LoggerService,
                private exceptionService: ExceptionService,
                private usersBackendClient: UsersBackendClient,
                private notificationService: NotificationService) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    initSiteList(userListData: UserListData) {
        this.userListDataSource.next(userListData);
    }
    
    UserViewRequest(): UserViewRequest {
        return this.userListDataSource.value.viewRequest;
    }
    
    loadAndAppendPage(page: number): Observable<void> {
        const viewRequest = Object.assign({}, this.userListDataSource.getValue().viewRequest);
        viewRequest.page  = page;
        
        return this.usersBackendClient.loadUsers(viewRequest)
        .pipe(
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            }),
            tap(users => {
                const updatedSiteListData: UserListData = iassign(
                    this.userListDataSource.getValue(),
                    userListData => userListData.list,
                    userList => {
                        userList.push(...users);
                        return userList;
                    }
                );
                this.userListDataSource.next(updatedSiteListData);
            }),
            map(() => {
            })
        );
    }
    
    getUser(id: number): Observable<User> {
        return this.usersBackendClient.loadUser(id)
        .pipe(
            tap((user: User) => {
                if (user) {
                    this.notificationService.showSuccess("Loaded user", "Load User");
                }
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return of(null);
            })
        );
    }
    
    editUser(formData: UserRequest): Observable<User> {
        return this.usersBackendClient.updateUser(formData)
        .pipe(
            tap((user: User) => {
                if (user) {
                    this.notificationService.showSuccess("All data for user updated", "Edit user");
                }
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            })
        );
    }
    
    createUser(formData: UserRequest): Observable<number> {
        return this.usersBackendClient.addUser(formData)
        .pipe(
            tap((id: number) => {
                if (id) {
                    this.notificationService.showSuccess("New User created", "Create User");
                }
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            })
        );
    }
    
    deleteUser(id: number): Observable<boolean> {
        return this.usersBackendClient.deleteUser(id)
        .pipe(
            tap(() => {
                this.notificationService.showSuccess("User deleted", "Delete User");
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            })
        );
    }
    
    passwordUser(formData): Observable<boolean> {
        return this.usersBackendClient.passwordUser(formData)
        .pipe(
            tap(pass => {
                if (pass) {
                    this.notificationService.showSuccess("Password changed", "Change password");
                } else {
                    this.notificationService.showError("Password for user not was changed", "Change password");
                }
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            })
        );
    }
    
    getUserDelivery(id: number): Observable<UserDelivery> {
        return this.usersBackendClient.loadUserDelivery(id)
        .pipe(
            tap((delivery: UserDelivery) => {
                if (delivery) {
                    this.notificationService.showSuccess("Loaded user delivery address", "Load Delivery Address");
                }
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return of(null);
            })
        );
    }
    
    editUserDelivery(formData): Observable<UserDelivery> {
        return this.usersBackendClient.updateUserDelivery(formData)
        .pipe(
            tap((delivery: UserDelivery) => {
                if (delivery) {
                    this.notificationService.showSuccess("User delivery address was updated", "Update Delivery Address");
                }
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return of(null);
            })
        );
    }
    
}
