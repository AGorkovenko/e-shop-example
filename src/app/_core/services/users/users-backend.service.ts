import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {HttpParams} from "@angular/common/http";
import {CONFIG} from "../../config";
import {HttpCachingClient} from "../http-caching-client.service";
import {LoggerService} from "../logger.service";
import {User} from "../../models/dto/user/user.model";
import {UserViewRequest} from "../../models/dto/user/user-view-request.model";
import {UserRequest} from "../../models/dto/user/user.request";
import {UserDelivery} from "../../models/dto/user/user-delivery.model";
import {UserDeliveryRequest} from "../../models/dto/user/user-delivery-request.model";

@Injectable()
export class UsersBackendClient {
    
    USERS_URL         = "/users";
    SITE_COUNT_URL    = "/users/count";
    USER_URL          = "/users/{id}";
    USER_PASS_URL     = "/users/{id}/pass";
    USER_DELIVERY_URL = "/users/{id}/delivery";
    
    constructor(private logger: LoggerService,
                private httpClient: HttpCachingClient) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    loadUsers(viewRequest?: UserViewRequest): Observable<User[]> {
        const url              = CONFIG.apiUrl + this.USERS_URL;
        let params: HttpParams = new HttpParams();
        if (!viewRequest) {
            viewRequest = new UserViewRequest();
        }
        if (viewRequest.order != null) {
            params = params.set("order", viewRequest.order);
        }
        if (viewRequest.sort != null) {
            params = params.set("sort", viewRequest.sort);
        }
        if (viewRequest.q != null) {
            params = params.set("q", viewRequest.q);
        }
        if (viewRequest.role != null) {
            params = params.set("role", viewRequest.role.toString());
        }
        const offset = (viewRequest.page - 1) * viewRequest.pageSize;
        params       = params.set("offset", offset.toString());
        params       = params.set("limit", viewRequest.pageSize.toString());
        
        return this.httpClient.get(url, params);
    }
    
    loadCount(viewRequest: UserViewRequest): Observable<number> {
        const url              = CONFIG.apiUrl + this.SITE_COUNT_URL;
        let params: HttpParams = new HttpParams();
        if (viewRequest.q != null) {
            params = params.set("q", viewRequest.q);
        }
        if (viewRequest.role != null) {
            params = params.set("role", viewRequest.role.toString());
        }
        return this.httpClient.get<number>(url);
    }
    
    loadUser(id: number): Observable<User> {
        const url = CONFIG.apiUrl + this.USER_URL.replace("{id}", id.toString());
        return this.httpClient.get(url);
    }
    
    addUser(request: UserRequest): Observable<number> {
        const url  = CONFIG.apiUrl + this.USERS_URL;
        const body = {
            login: request.login,
            firstname: request.firstname,
            lastname: request.lastname,
            email: request.email,
            role: request.role ? request.role.value : null,
            ava: request.ava,
            flag: request.flag
        };
        return this.httpClient.post<number>(url, body);
    }
    
    updateUser(request: UserRequest): Observable<User> {
        const url  = CONFIG.apiUrl + this.USER_URL.replace("{id}", request.id.toString());
        const body = {
            login: request.login,
            firstname: request.firstname,
            lastname: request.lastname,
            email: request.email,
            role: request.role ? request.role.value : null,
            ava: request.ava,
            flag: request.flag
        };
        return this.httpClient.post<User>(url, body);
    }
    
    deleteUser(id: number): Observable<boolean> {
        const url = CONFIG.apiUrl + this.USER_URL.replace("{id}", id.toString());
        return this.httpClient.delete<boolean>(url);
    }
    
    passwordUser(request): Observable<boolean> {
        const url  = CONFIG.apiUrl + this.USER_PASS_URL.replace("{id}", request.id.toString());
        const body = {
            pass: request.pass,
            repass: request.repass
        };
        return this.httpClient.post<boolean>(url, body);
    }
    
    loadUserDelivery(id: number): Observable<UserDelivery> {
        const url = CONFIG.apiUrl + this.USER_DELIVERY_URL.replace("{id}", id.toString());
        return this.httpClient.get(url);
    }
    
    updateUserDelivery(request: UserDeliveryRequest): Observable<UserDelivery> {
        const url  = CONFIG.apiUrl + this.USER_DELIVERY_URL.replace("{id}", request.uid.toString());
        const body = {
            id: request.id,
            uid: request.uid,
            firstname: request.firstname,
            lastname: request.lastname,
            country: request.country,
            zip: request.zip,
            ort: request.ort,
            address: request.address,
            phone: request.phone
        };
        return this.httpClient.post<UserDelivery>(url, body);
    }
}
