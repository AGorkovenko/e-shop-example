import {Injectable} from "@angular/core";
import {LoggerService} from "./logger.service";
import {CONFIG} from "../config";
import {Currency, CurrencyData} from "../models/enum/currency.enum";
import {BehaviorSubject, forkJoin, Observable} from "rxjs";
import {ShopService} from "./shop/shop.service";
import {tap} from "rxjs/operators";
import {Settings} from "../models/dto/settings.model";
import {CookieService} from "./cookie.service";

@Injectable()
export class CurrencyService {
    
    currency = Currency;
    
    currencyExchangeValues = [];
    
    private currentCurrencySource: BehaviorSubject<Currency> = new BehaviorSubject(CONFIG.defCurrency);
    currentCurrency$: Observable<Currency>                   = this.currentCurrencySource.asObservable();
    
    constructor(private logger: LoggerService,
                private shopService: ShopService,
                private cookieService: CookieService) {
        this.logger.debug(this.constructor.name + " is created.");
        this.getCurrencyExchange();
    }
    
    getCurrencyExchange() {
        forkJoin(
            this.shopService.getSettingByName(Currency.EUR),
            this.shopService.getSettingByName(Currency.UAH),
            this.shopService.getSettingByName(Currency.USD),
            this.shopService.getSettingByName(Currency.RUB)
        )
        .pipe(
            tap(([v1, v2, v3, v4]) => {
                this.currencyExchangeValues[Currency.EUR] = parseFloat(v1);
                this.currencyExchangeValues[Currency.UAH] = parseFloat(v2);
                this.currencyExchangeValues[Currency.USD] = parseFloat(v3);
                this.currencyExchangeValues[Currency.RUB] = parseFloat(v4);
                this.initCurrency();
            })
        )
        .subscribe();
    }
    
    initCurrency() {
        const settings: Settings = this.cookieService.getJson(CONFIG.settingsCookie);
        if (settings && settings.currency) {
            this.setCurrency(settings.currency);
        }
    }
    
    setCurrency(currency: Currency) {
        const newSettings: Settings = new Settings();
        Object.assign(newSettings, this.cookieService.getJson(CONFIG.settingsCookie));
        newSettings.currency = currency;
        this.cookieService.set(CONFIG.settingsCookie, JSON.stringify(newSettings), 30, "/");
        this.currentCurrencySource.next(currency);
    }
    
    getCurrentCurrencyData(currency: Currency): CurrencyData {
        return this.currency.getCurrencyData(currency);
    }
    
    currencyExchange(price: number, currency?: Currency): number {
        const currentCurrencyValue = currency ? currency : this.currentCurrencySource.getValue();
        return this.currencyExchangeValues[currentCurrencyValue] ? price * this.currencyExchangeValues[currentCurrencyValue] : price;
    }
}
