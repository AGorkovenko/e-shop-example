import {Inject, Injectable, LOCALE_ID} from "@angular/core";
import {LoggerService} from "./logger.service";
import {BehaviorSubject, EMPTY, Observable, Subscription} from "rxjs";
import {Language} from "../models/dto/lenguage.model";
import {HttpCachingClient} from "./http-caching-client.service";
import {CONFIG} from "../config";
import {catchError, tap} from "rxjs/operators";
import {registerLocaleData} from "@angular/common";
import localeEn from "@angular/common/locales/en";
import localePl from "@angular/common/locales/pl";
import localeRu from "@angular/common/locales/ru";
import localeUk from "@angular/common/locales/uk";

@Injectable()
export class LanguageService {
    
    LANGUAGES_URL: string = "/languages";
    
    private languagesSource: BehaviorSubject<Language[]> = new BehaviorSubject(undefined);
    languages$: Observable<Language[]>                   = this.languagesSource.asObservable();
    
    private currentLanguageSource: BehaviorSubject<Language> = new BehaviorSubject(undefined);
    currentLanguage$: Observable<Language>                   = this.currentLanguageSource.asObservable();
    
    private currentLocaleSource: BehaviorSubject<string> = new BehaviorSubject(undefined);
    currentLocale$: Observable<string>                   = this.currentLocaleSource.asObservable();
    
    constructor(private logger: LoggerService,
                private httpClient: HttpCachingClient,
                @Inject(LOCALE_ID) locale: string) {
        this.logger.debug(this.constructor.name + " is created.");
        this.currentLocaleSource.next(locale);
        this.languagesSource.next([
            {
                ID: 0,
                name: "English",
                code: "en"
            },
            {
                ID: 1,
                name: "Polski",
                code: "pl"
            },
            {
                ID: 2,
                name: "Russian",
                code: "ru"
            }
        ]);
        
        registerLocaleData(localeEn, "en");
        registerLocaleData(localePl, "pl");
        registerLocaleData(localeRu, "ru");
        registerLocaleData(localeUk, "uk");
    }
    
    setCurrentLocale(locale: string) {
        this.currentLocaleSource.next(locale);
    }
    
    getCurrentLocale(): string {
        return this.currentLocaleSource.getValue();
    }
    
    loadLanguages(): Subscription {
        const url = CONFIG.apiUrl + this.LANGUAGES_URL;
        return this.httpClient.get<Language[]>(url)
        .pipe(
            catchError(errorInfo => {
                this.logger.debug("Error of loading languages", errorInfo);
                return EMPTY;
            }),
            tap(languages => {
                this.logger.debug("Languages loaded");
                this.languagesSource.next(languages);
            })
        )
        .subscribe();
    }
    
    getLanguageName(code: string): string {
        const currentLanguage = this.languagesSource.getValue() ? this.languagesSource.getValue().filter((language: Language) => code === language.code).pop() : null;
        return currentLanguage ? currentLanguage.name : null;
    }
}
