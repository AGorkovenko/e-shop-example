import {Injectable} from "@angular/core";
import {LoggerService} from "./logger.service";
import {BehaviorSubject, Observable} from "rxjs";
import {SignInUp} from "../models/enum/sign-in-up.enum";

@Injectable()
export class SignInUpService {
  
  private openModalSource: BehaviorSubject<SignInUp> = new BehaviorSubject(null);
  openModalSource$: Observable<SignInUp> = this.openModalSource.asObservable();
  
  constructor(private logger: LoggerService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  openModal(status: SignInUp = SignInUp.LOGIN) {
    this.openModalSource.next(status);
  }
  
  closeModal() {
    this.openModalSource.next(null);
  }
}
