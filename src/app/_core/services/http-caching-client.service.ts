import {isPlatformServer} from "@angular/common";
import {HttpClient, HttpEvent, HttpParams} from "@angular/common/http";
import {Inject, Injectable, PLATFORM_ID} from "@angular/core";
import {makeStateKey, StateKey, TransferState} from "@angular/platform-browser";
import {tap} from "rxjs/operators";
import {environment} from "../../../environments/environment";
import {LoggerService} from "./logger.service";
import {Observable, of} from "rxjs";

@Injectable()
export class HttpCachingClient {
  
  private SERVER_ADDRESS: string = environment.serverAddress;
  
  constructor(private logger: LoggerService,
              private httpClient: HttpClient,
              @Inject(PLATFORM_ID) private platformId: Object,
              private transferState: TransferState) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  get<T>(path: string, params?: HttpParams): Observable<T> {
    const transferKey: StateKey<T> = makeStateKey(`${path}?${params != null ? params.toString() : ""}`);
    
    if (this.transferState.hasKey(transferKey)) {
      return of(this.transferState.get<any>(transferKey, 0))
      .pipe(
        tap(() => this.transferState.remove(transferKey))
      );
    } else {
      return this.httpClient.get<T>(this.SERVER_ADDRESS + path, {
        observe: "body",
        responseType: "json",
        params: params
      })
      .pipe(
        tap(response => {
          if (isPlatformServer(this.platformId)) {
            this.transferState.set<T>(transferKey, response);
            this.logger.info(`Transferring data to browser from ${transferKey}: ${JSON.stringify(response)}`);
          }
        })
      );
    }
  }
  
  post<T>(path: string, body?: any, params?: HttpParams): Observable<T> {
    return this.httpClient.post<T>(this.SERVER_ADDRESS + path, body, {
      observe: "body",
      responseType: "json",
      params: params
    });
  }
  
  delete<T>(path: string, body?: any, params?: HttpParams): Observable<T> {
    return this.httpClient.request<T>("delete", this.SERVER_ADDRESS + path, {
      body: body,
      observe: "body",
      responseType: "json",
      params: params
    });
  }
  
  upload<T>(path: string, body?: any, params?: HttpParams): Observable<HttpEvent<T>> {
    return this.httpClient.post<T>(this.SERVER_ADDRESS + path, body, {
      observe: "events",
      responseType: "json",
      params: params,
      reportProgress: true
    });
  }
}
