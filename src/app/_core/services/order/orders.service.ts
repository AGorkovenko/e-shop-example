import {Injectable} from "@angular/core";
import {BehaviorSubject, EMPTY, Observable, of} from "rxjs";
import {LoggerService} from "../logger.service";
import {ExceptionService} from "../exception.service";
import {NotificationService} from "../notification.service";
import {CookieService} from "../cookie.service";
import {HttpCachingClient} from "../http-caching-client.service";
import {Order} from "../../models/dto/order/order.model";
import {OrderRequest} from "../../models/dto/order/order-request.model";
import {catchError, map, tap} from "rxjs/operators";
import iassign from "immutable-assign";
import {OrdersBackendClient} from "./orders-backend.service";
import {OrderViewRequest} from "../../models/dto/order/order-view-request.model";
import {Cart} from "../../models/dto/cart/cart.model";

export class OrderListData {
    viewRequest: OrderViewRequest;
    list: Order[];
    count: number;
}

@Injectable()
export class OrdersService {
    
    private orderListDataSource: BehaviorSubject<OrderListData> = new BehaviorSubject(undefined);
    orderListData$: Observable<OrderListData>                   = this.orderListDataSource.asObservable();
    
    constructor(private logger: LoggerService,
                private httpClient: HttpCachingClient,
                private ordersBackendClient: OrdersBackendClient,
                private cookieService: CookieService,
                private exceptionService: ExceptionService,
                private notificationService: NotificationService) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    initOrderList(orderListData: OrderListData) {
        this.orderListDataSource.next(orderListData);
    }
    
    OrderViewRequest(): OrderViewRequest {
        return this.orderListDataSource.value.viewRequest;
    }
    
    loadAndAppendPage(page: number, lang?: string): Observable<void> {
        const viewRequest = Object.assign({}, this.orderListDataSource.getValue().viewRequest);
        viewRequest.page  = page;
        
        return this.ordersBackendClient.loadOrders(viewRequest, lang)
        .pipe(
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            }),
            tap(orders => {
                const updatedOrderListData: OrderListData = iassign(
                    this.orderListDataSource.getValue(),
                    orderListData => orderListData.list,
                    orderList => {
                        orderList.push(...orders);
                        return orderList;
                    }
                );
                this.orderListDataSource.next(updatedOrderListData);
            }),
            map(() => {
            })
        );
    }
    
    getOrder(id: number): Observable<Order> {
        return this.ordersBackendClient.loadOrder(id)
        .pipe(
            tap((order: Order) => {
                if (order) {
                    this.notificationService.showSuccess("Order was loaded", "Load order");
                }
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return of(null);
            })
        );
    }
    
    editOrder(formData: Order): Observable<Order> {
        const orderRequest: OrderRequest = new OrderRequest();
        orderRequest.id                  = formData.ID;
        orderRequest.uid                 = formData.uid;
        orderRequest.firstname           = formData.firstname;
        orderRequest.lastname            = formData.lastname;
        orderRequest.country             = formData.country;
        orderRequest.ort                 = formData.ort;
        orderRequest.zip                 = formData.zip;
        orderRequest.address             = formData.address;
        orderRequest.warehouse           = formData.warehouse;
        orderRequest.phone               = formData.phone;
        orderRequest.email               = formData.email;
        orderRequest.status              = formData.status;
        orderRequest.delivery            = formData.delivery;
        orderRequest.payment             = formData.payment;
        orderRequest.comment             = formData.comment;
        orderRequest.currency            = formData.currency;
        orderRequest.cart                = new Cart();
        orderRequest.cart.couponId       = formData.couponId;
        orderRequest.cart.delivery_cost  = formData.delivery_cost;
        orderRequest.cart.extra_pay      = formData.extra_pay;
        orderRequest.cart.quantity       = formData.quantity;
        orderRequest.cart.subtotal       = formData.subtotal;
        orderRequest.cart.total          = formData.total;
        orderRequest.cart.items          = formData.items;
        return this.ordersBackendClient.updateOrder(orderRequest)
        .pipe(
            tap((order: Order) => {
                if (order) {
                    this.notificationService.showSuccess("All data for order updated", "Edit order");
                }
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            })
        );
    }
    
    addOrder(formData: Order): Observable<number> {
        const orderRequest: OrderRequest = new OrderRequest();
        orderRequest.id                  = formData.ID;
        orderRequest.uid                 = formData.uid;
        orderRequest.firstname           = formData.firstname;
        orderRequest.lastname            = formData.lastname;
        orderRequest.country             = formData.country;
        orderRequest.ort                 = formData.ort;
        orderRequest.zip                 = formData.zip;
        orderRequest.address             = formData.address;
        orderRequest.warehouse           = formData.warehouse;
        orderRequest.phone               = formData.phone;
        orderRequest.email               = formData.email;
        orderRequest.status              = formData.status;
        orderRequest.delivery            = formData.delivery;
        orderRequest.payment             = formData.payment;
        orderRequest.comment             = formData.comment;
        orderRequest.currency            = formData.currency;
        orderRequest.cart                = new Cart();
        orderRequest.cart.couponId       = formData.couponId;
        orderRequest.cart.delivery_cost  = formData.delivery_cost;
        orderRequest.cart.extra_pay      = formData.extra_pay;
        orderRequest.cart.quantity       = formData.quantity;
        orderRequest.cart.subtotal       = formData.subtotal;
        orderRequest.cart.total          = formData.total;
        orderRequest.cart.items          = formData.items;
        return this.ordersBackendClient.createOrder(orderRequest)
        .pipe(
            tap((id: number) => {
                this.notificationService.showSuccess("New Order was created", "Add order");
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            })
        );
    }
    
    
    deleteOrder(id: number): Observable<boolean> {
        return this.ordersBackendClient.removeOrder(id)
        .pipe(
            tap(() => {
                this.notificationService.showSuccess("Order deleted", "Delete Order");
            }),
            catchError(errorInfo => {
                this.exceptionService.handleRequestErrorInfo(errorInfo.error);
                return EMPTY;
            })
        );
    }
}
