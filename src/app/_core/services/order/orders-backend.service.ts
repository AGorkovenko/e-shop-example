import {Injectable} from "@angular/core";
import {EMPTY, Observable, of} from "rxjs";
import {HttpParams} from "@angular/common/http";
import {CONFIG} from "../../config";
import {LoggerService} from "../logger.service";
import {HttpCachingClient} from "../http-caching-client.service";
import {Order} from "../../models/dto/order/order.model";
import {OrderViewRequest} from "../../models/dto/order/order-view-request.model";
import {OrderRequest} from "../../models/dto/order/order-request.model";
import {Cart} from "../../models/dto/cart/cart.model";

@Injectable()
export class OrdersBackendClient {
    
    ORDERS_URL       = "/orders";
    ORDERS_COUNT_URL = "/orders/count";
    ORDER_URL        = "/orders/{id}";
    
    constructor(private logger: LoggerService,
                private httpClient: HttpCachingClient) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    loadOrders(viewRequest?: OrderViewRequest, lang?: string): Observable<Order[]> {
        const url              = CONFIG.apiUrl + this.ORDERS_URL;
        let params: HttpParams = new HttpParams();
        if (!viewRequest) {
            viewRequest = new OrderViewRequest();
        }
        if (lang) {
            params = params.set("lang", lang);
        }
        if (viewRequest.order != null) {
            params = params.set("order", viewRequest.order);
        }
        if (viewRequest.sort != null) {
            params = params.set("sort", viewRequest.sort);
        }
        if (viewRequest.id != null) {
            params = params.set("id", viewRequest.id.toString());
        }
        if (viewRequest.q != null) {
            params = params.set("uid", viewRequest.q);
        }
        const offset = (viewRequest.page - 1) * viewRequest.pageSize;
        params       = params.set("offset", offset.toString());
        params       = params.set("limit", viewRequest.pageSize.toString());
        
        return this.httpClient.get(url, params);
    }
    
    loadCount(viewRequest: OrderViewRequest): Observable<number> {
        const url              = CONFIG.apiUrl + this.ORDERS_COUNT_URL;
        let params: HttpParams = new HttpParams();
        if (viewRequest.id != null) {
            params = params.set("id", viewRequest.id.toString());
        }
        if (viewRequest.q != null) {
            params = params.set("uid", viewRequest.q);
        }
        return this.httpClient.get<number>(url);
    }
    
    loadOrder(id: number, lang?: string): Observable<Order> {
        const url              = CONFIG.apiUrl + this.ORDER_URL.replace("{id}", id.toString());
        let params: HttpParams = new HttpParams();
        params                 = params.set("lang", lang);
        return this.httpClient.get(url, params);
    }
    
    updateOrder(request: OrderRequest): Observable<Order> {
        const url  = CONFIG.apiUrl + this.ORDER_URL.replace("{id}", request.id.toString());
        const body = {
            uid: request.uid,
            firstname: request.firstname,
            lastname: request.lastname,
            country: request.country,
            ort: request.ort,
            zip: request.zip,
            address: request.address,
            warehouse: request.warehouse,
            phone: request.phone,
            email: request.email,
            status: request.status,
            delivery: request.delivery,
            payment: request.payment,
            comment: request.comment,
            currency: request.currency,
            cart: {
                couponId: request.cart.couponId,
                delivery_cost: request.cart.delivery_cost,
                extra_pay: request.cart.extra_pay,
                quantity: request.cart.quantity,
                subtotal: request.cart.subtotal,
                total: request.cart.total,
                items: request.cart.items
            }
        };
        return this.httpClient.post<Order>(url, body);
    }
    
    createOrder(request: OrderRequest): Observable<number> {
        const url  = CONFIG.apiUrl + this.ORDERS_URL;
        const body = {
            uid: request.uid,
            firstname: request.firstname,
            lastname: request.lastname,
            country: request.country,
            ort: request.ort,
            zip: request.zip,
            address: request.address,
            warehouse: request.warehouse,
            phone: request.phone,
            email: request.email,
            status: request.status,
            delivery: request.delivery,
            payment: request.payment,
            comment: request.comment,
            currency: request.currency,
            cart: {
                couponId: request.cart.couponId,
                delivery_cost: request.cart.delivery_cost,
                extra_pay: request.cart.extra_pay,
                quantity: request.cart.quantity,
                subtotal: request.cart.subtotal,
                total: request.cart.total,
                items: request.cart.items
            }
        };
        return this.httpClient.post<number>(url, body);
    }
    
    removeOrder(id: number): Observable<boolean> {
        const url = CONFIG.apiUrl + this.ORDER_URL.replace("{id}", id.toString());
        return this.httpClient.delete<boolean>(url);
    }
}
