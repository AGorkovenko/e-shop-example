import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot} from "@angular/router";
import {ProfileService} from "./services/profile/profile.service";
import {tap} from "rxjs/operators";
import {CONFIG} from "./config";
import {Observable, of} from "rxjs";

@Injectable({providedIn: "root"})
export class AuthGuard implements CanActivate, CanActivateChild {
	
	constructor(private router: Router,
	            private profileService: ProfileService) {
	}
	
	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
		const tokenJson = JSON.parse(localStorage.getItem(CONFIG.profileLocalStorage));
		if (tokenJson) {
			return this.profileService.checkUserToken()
				.pipe(
					tap((check: boolean) => {
						if (!check) {
							this.clearAndRedirect();
						}
					})
				);
		} else {
			this.clearAndRedirect();
			return of(false);
		}
	}
	
	canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
		const tokenJson = JSON.parse(localStorage.getItem(CONFIG.profileLocalStorage));
		if (tokenJson) {
			return this.profileService.checkUserToken()
				.pipe(
					tap((check: boolean) => {
						if (!check) {
							this.clearAndRedirect();
						}
					})
				);
		} else {
			this.clearAndRedirect();
			return of(false);
		}
	}
	
	clearAndRedirect() {
		this.profileService.clearProfile();
		this.router.navigate(["/"]);
	}
}
