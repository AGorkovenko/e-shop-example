export enum StatusEmail {
	PENDING   = "PENDING",
	CONFIRMED = "CONFIRMED",
	EXPIRED   = "EXPIRED"
}
