export enum DeliveryType {
    PICKUP       = "PICKUP",
    NOVA_POSHTA  = "NOVA_POSHTA",
    PONY_EXPRESS = "PONY_EXPRESS",
    EXPRESS_RU   = "EXPRESS_RU",
    // DPD           = "DPD",
    // POCZTA_POLSKA = "POCZTA_POLSKA",
    // ARROWS        = "ARROWS",
    // DHL           = "DHL",
    // HERMES        = "HERMES",
    // UPS           = "UPS"
}

export namespace DeliveryType {
    
    export function values(): DeliveryType[] {
        return deliveryTypeData.map(deliveryTypeDataItem => deliveryTypeDataItem.deliveryType);
    }
    
    export function getDisplayName(deliveryType): string | null {
        const deliveryTypeDataItem = deliveryTypeData.find(_deliveryTypeDataItem => _deliveryTypeDataItem.deliveryType === deliveryType);
        return deliveryTypeDataItem ? deliveryTypeDataItem.displayName : null;
    }
    
    export function getDescription(deliveryType): string | null {
        const deliveryTypeDataItem = deliveryTypeData.find(_deliveryTypeDataItem => _deliveryTypeDataItem.deliveryType === deliveryType);
        return deliveryTypeDataItem ? deliveryTypeDataItem.description : null;
    }
    
    export function getCalculateMethod(deliveryType): boolean | null {
        const deliveryTypeDataItem = deliveryTypeData.find(_deliveryTypeDataItem => _deliveryTypeDataItem.deliveryType === deliveryType);
        return deliveryTypeDataItem ? deliveryTypeDataItem.managerCalculate : null;
    }
    
    interface DeliveryTypeData {
        deliveryType: DeliveryType;
        displayName: string;
        description: string;
        managerCalculate: boolean;
    }
    
    const deliveryTypeData: DeliveryTypeData[] = [
        {deliveryType: DeliveryType.PICKUP, displayName: "Self Pickup", description: "Self pickup in the office", managerCalculate: false},
        {deliveryType: DeliveryType.NOVA_POSHTA, displayName: "Nova Poshta", description: "Нова Пошта", managerCalculate: false},
        {deliveryType: DeliveryType.EXPRESS_RU, displayName: "Express.Ru", description: "Экспресс Точка Ру", managerCalculate: true},
        {deliveryType: DeliveryType.PONY_EXPRESS, displayName: "Pony Express", description: "Пони Экспресс", managerCalculate: true}
        // {deliveryType: DeliveryType.POCZTA_POLSKA, displayName: "Poczta Polska", description: "National Post of Poland", managerCalculate: true},
        // {deliveryType: DeliveryType.DPD, displayName: "DPD", description: "DPD group", managerCalculate: true},
        // {deliveryType: DeliveryType.ARROWS, displayName: "Arrows Service", description: "Arrows Service logistics", managerCalculate: true},
        // {deliveryType: DeliveryType.DHL, displayName: "DHL express", description: "Deutsche Post DHL Group", managerCalculate: true},
        // {deliveryType: DeliveryType.HERMES, displayName: "Hermes", description: "Hermes Logistik Gruppe Deutschland GmbH", managerCalculate: true},
        // {deliveryType: DeliveryType.UPS, displayName: "UPS", description: "United Parcel Service", managerCalculate: true}
    ];
}
