export enum SignInUp {
    LOGIN        = "LOGIN",
    REGISTRATION = "REGISTRATION",
    RECOVERY     = "RECOVERY"
}
