export enum SortType {
	LOWEST_PRICE = "LOWEST_PRICE",
	HIGHEST_PRICE = "HIGHEST_PRICE",
	NEWEST = "NEWEST",
	OLDEST = "OLDEST",
	ASC = "ASC",
	DESC = "DESC"
}

export namespace SortType {
	
	export function values(): SortType[] {
		return sortTypeData.map(sortTypeDataItem => sortTypeDataItem.sortType);
	}
	
	export function getDisplayName(sortType): string | null {
		const sortTypeDataItem = sortTypeData.find(_sortTypeDataItem => _sortTypeDataItem.sortType === sortType);
		return sortTypeDataItem ? sortTypeDataItem.displayName : null;
	}
	
	interface SortTypeData {
		sortType: SortType;
		displayName: string;
	}
	
	const sortTypeData: SortTypeData[] = [
		{sortType: SortType.LOWEST_PRICE, displayName: "Lowest price"},
		{sortType: SortType.HIGHEST_PRICE, displayName: "Highest price"},
		{sortType: SortType.NEWEST, displayName: "Newest"},
		{sortType: SortType.OLDEST, displayName: "Oldest"},
		{sortType: SortType.ASC, displayName: "Ascending"},
		{sortType: SortType.DESC, displayName: "Descending"}
	];
}
