export enum ContentType {
	POST    = "POST",
	PAGE    = "PAGE",
	PRODUCT = "PRODUCT"
}

export namespace ContentType {
	
	export function values(): ContentType[] {
		return contentTypeData.map(contentTypeDataItem => contentTypeDataItem.value);
	}
	
	export function getDisplayName(contentType): string | null {
		const contentTypeDataItem = contentTypeData.find(_contentTypeDataItem => _contentTypeDataItem.value === contentType);
		return contentTypeDataItem ? contentTypeDataItem.displayName : null;
	}
	
	export function getDisplayNamePlural(contentType): string | null {
		const contentTypeDataItem = contentTypeData.find(_contentTypeDataItem => _contentTypeDataItem.value === contentType);
		return contentTypeDataItem ? contentTypeDataItem.displayNamePlural : null;
	}
	
	export function getSlug(contentType): string | null {
		return this.getDisplayName(contentType).toLocaleLowerCase();
	}
	
	interface ContentTypeDataData {
		value: ContentType;
		displayName: string;
		displayNamePlural: string;
	}
	
	const contentTypeData: ContentTypeDataData[] = [
		{value: ContentType.PAGE, displayName: "Page", displayNamePlural: "Pages"},
		{value: ContentType.POST, displayName: "Post", displayNamePlural: "Posts"},
		{value: ContentType.PRODUCT, displayName: "Product", displayNamePlural: "Products"}
	];
}
