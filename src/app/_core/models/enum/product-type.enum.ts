export enum ProductType {
  SIMPLE = "SIMPLE",
  VARIABLE = "VARIABLE",
  VARIATION = "VARIATION"
}
