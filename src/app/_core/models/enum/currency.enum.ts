export enum Currency {
  EUR = "EUR",
  USD = "USD",
  UAH = "UAH",
  RUB = "RUB"
}

export class CurrencyData {
  value: Currency;
  symbol: string;
  displayName: string;
  format: string;
}

export namespace Currency {
  
  export function values(): Currency[] {
    return currencyData.map(item => item.value);
  }
  
  export function getCurrencyData(value: Currency): CurrencyData {
    return currencyData.find(item => item.value === value);
  }
  
  export function getDisplayName(value: Currency): string {
    return currencyData.find(item => item.value === value).displayName;
  }
  
  export function getSymbol(value: Currency): string {
    return currencyData.find(item => item.value === value).symbol;
  }
  
  export function getFormat(value: Currency): string {
    return currencyData.find(item => item.value === value).format;
  }
  
  const currencyData: CurrencyData[] = [
    {value: Currency.EUR, symbol: "€", displayName: "Euro", format: "1.0-2"},
    {value: Currency.USD, symbol: "$", displayName: "United States Dollar", format: "1.0-2"},
    {value: Currency.UAH, symbol: "₴", displayName: "Ukrainian hryvnia", format: "1.0-2"},
    {value: Currency.RUB, symbol: "₽", displayName: "Russian ruble", format: "1.0-2"}
  ];
}
