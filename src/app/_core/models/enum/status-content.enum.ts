export enum StatusContent {
	PUBLISH   = "PUBLISH", // Viewable by everyone
	FUTURE    = "FUTURE", // Scheduled to be published in a future date
	DRAFT     = "DRAFT", // Incomplete post viewable by anyone with proper user role
	PENDING   = "PENDING", // Awaiting a user with the publish_posts capability (typically a user assigned the Editor role) to publish
	PRIVATE   = "PIVATE", // Viewable only to WordPress users at Administrator level
	TRASH     = "TRASH", // Posts in the Trash are assigned the trash status
	AUTODRAFT = "AUTODRAFT", // Revisions that WordPress saves automatically while you are editing
	INHERIT   = "INHERIT" // Used with a child post (such as Attachments and Revisions) to determine the actual status from the parent post
}

export namespace StatusContent {
	
	export function values(): StatusContent[] {
		return StatusContentData.map(item => item.value);
	}
	
	export function getDisplayName(value: StatusContent): string {
		return StatusContentData.find(item => item.value === value).displayName;
	}
	
	export function getCssClass(value: StatusContent): string {
		return StatusContentData.find(item => item.value === value).cssClass;
	}
	
	interface StatusContentData {
		value: StatusContent;
		displayName: string;
		cssClass: string;
	}
	
	const StatusContentData: StatusContentData[] = [
		{value: StatusContent.PUBLISH, displayName: "Publish", cssClass: "badge-success"},
		{value: StatusContent.FUTURE, displayName: "Future", cssClass: "badge-secondary"},
		{value: StatusContent.DRAFT, displayName: "Draft", cssClass: "badge-light"},
		{value: StatusContent.PENDING, displayName: "Pending", cssClass: "badge-primary"},
		{value: StatusContent.PRIVATE, displayName: "Private", cssClass: "badge-outline-danger"},
		{value: StatusContent.TRASH, displayName: "Trash", cssClass: "badge-danger"},
		{value: StatusContent.AUTODRAFT, displayName: "Autodraft", cssClass: "badge-outline-light"},
		{value: StatusContent.INHERIT, displayName: "Inherit", cssClass: "badge-outline-primary"}
	];
}
