export class ContentTranslation {
	ID: number;
	parent: string;
	lang: string;
	title: string;
	content: string;
	excerpt: string;
}
