import {StatusContent} from "../../enum/status-content.enum";
import {ContentType} from "./content-type.enum";

export class Content {
	ID: number;
	date_created: string;
	date_modified: string;
	title: string;
	content: string;
	excerpt: string;
	status: StatusContent;
	slug: string;
	type: ContentType;
	order: number;
	site_id: number;
	cover?: string;
}
