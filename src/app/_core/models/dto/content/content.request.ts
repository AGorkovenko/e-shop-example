import {SelectOption} from "../../../../_shared/components/select/select-option.enum";
import {ContentType} from "./content-type.enum";

export class ContentRequest {
	id?: number;
	title: string;
	content: string;
	excerpt: string;
	status: SelectOption;
	slug: string;
	type: ContentType;
	order: number;
	site: SelectOption;
	lang: string = null;
	cover?: string = null;
}
