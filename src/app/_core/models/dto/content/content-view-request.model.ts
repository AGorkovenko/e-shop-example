import {ContentType} from "./content-type.enum";
import {SortType} from "../../enum/sort-type.enum";

export class ContentViewRequest {
	type: ContentType;
	q: string;
	order: string;
	sort: SortType;
	page: number = 1;
	pageSize: number = 12;
}
