import {SelectOption} from "../../../../_shared/components/select/select-option.enum";

export class UserDelivery {
	ID: number;
	uid: number;
	firstname: string;
	lastname: string;
	country: SelectOption;
	ort: SelectOption;
	zip: string;
	address: string;
	phone: string;
	updated: string;
}
