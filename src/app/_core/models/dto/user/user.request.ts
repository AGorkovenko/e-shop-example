import {SelectOption} from "../../../../_shared/components/select/select-option.enum";

export class UserRequest {
    id: number;
    login: string;
    email: string;
    role: SelectOption;
    firstname: string;
    lastname: string;
    ava: string;
    flag: string;
}
