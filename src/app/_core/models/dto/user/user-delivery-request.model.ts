import {SelectOption} from "../../../../_shared/components/select/select-option.enum";

export class UserDeliveryRequest {
	id: number;
	uid: number;
	firstname: string;
	lastname: string;
	country: SelectOption;
	ort: SelectOption;
	zip: number;
	address: string;
	phone: string;
}
