export enum UserRole {
    ALL           = "ALL",
    VIEWER        = "VIEWER",
    USER          = "USER",
    CUSTOMER      = "CUSTOMER",
    EDITOR        = "EDITOR",
    ADMINISTRATOR = "ADMINISTRATOR",
    SUPERADMIN    = "SUPERADMIN"
}

export namespace UserRole {
    
    export function values(exclude: UserRole[] = [UserRole.VIEWER]): UserRole[] {
        return userRoleData
        .filter(item => exclude.indexOf(item.enum) === -1)
        .map(item => item.enum);
    }
    
    export function getDisplayName(role: UserRole): string {
        return userRoleData.find(item => item.enum === role).displayName;
    }
    
    export function getValueToEnum(value: number): string {
        return userRoleData.find(item => item.value === value).enum;
    }
    
    export function getValueToDisplayName(value: number): string {
        return userRoleData.find(item => item.value === value).displayName;
    }
    
    export function getValue(role: UserRole): number {
        return userRoleData.find(item => item.enum === role).value;
    }
    
    interface UserRoleData {
        enum: UserRole;
        displayName: string;
        value: number;
    }
    
    const userRoleData: UserRoleData[] = [
        {enum: UserRole.ALL, displayName: "All users", value: null},
        {enum: UserRole.VIEWER, displayName: "Viewer", value: 0},
        {enum: UserRole.USER, displayName: "User", value: 1},
        {enum: UserRole.CUSTOMER, displayName: "Customer", value: 2},
        {enum: UserRole.EDITOR, displayName: "Editor", value: 3},
        {enum: UserRole.ADMINISTRATOR, displayName: "Administrator", value: 4},
        {enum: UserRole.SUPERADMIN, displayName: "Superadmin", value: 5}
    ];
}
