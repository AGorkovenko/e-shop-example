import {SortType} from "../../enum/sort-type.enum";

export class UserViewRequest {
	q: string;
	role: number;
	order: string;
	sort: SortType;
	page: number = 1;
	pageSize: number = 12;
}
