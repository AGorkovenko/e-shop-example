import {SelectOption} from "../../../../_shared/components/select/select-option.enum";

export class ContentMetaRequest {
	id?: number;
	meta_key: string;
	meta_value: string;
	site: SelectOption;
}
