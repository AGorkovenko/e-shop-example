import {SortType} from "../../enum/sort-type.enum";

export class ContentMetaViewRequest {
    keys: string[];
    order: string;
    sort: SortType;
    page: number     = 1;
    pageSize: number = 12;
}
