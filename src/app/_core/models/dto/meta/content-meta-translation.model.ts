export class ContentMetaTranslation {
	ID: number;
	parent: string;
	meta_value: string;
	lang: number;
}
