import {Currency, CurrencyData} from "../../enum/currency.enum";

export class ContentMeta {
    ID: number;
    meta_key: string;
    meta_value: string;
    site_id: number;
}
