export class ProductCategory {
  ID: number;
  name: string;
  description: string;
  discount: number;
  slug: string;
  cover: string;
  parent: number;
  order: number;
  site_id: number;
  children: ProductCategory[];
}
