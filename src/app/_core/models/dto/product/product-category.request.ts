export class ProductCategoryRequest {
  id: number;
  name: string;
  slug: string;
  cover: string;
  description: string;
  discount: number;
  order: number;
  site_id: number;
  parent: number;
  lang?: string;
}
