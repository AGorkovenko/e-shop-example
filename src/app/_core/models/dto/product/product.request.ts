import {SelectOption} from "../../../../_shared/components/select/select-option.enum";

export class ProductRequest {
  id: number;
  title: string;
  sku: string;
  original_sku: string;
  slug: string;
  category: number[];
  price: number;
  old_price: number;
  dealer_price: number;
  distributor_price: number;
  stock: number;
  description: string;
  photo: string;
  gallery: string[];
  excerpt: string;
  upsell: number[];
  crosssell: number[];
  site: SelectOption;
}
