export class CategoryOrder {
	ID: number;
	order: number;
	parent: number;
	children?: CategoryOrder[];
}
