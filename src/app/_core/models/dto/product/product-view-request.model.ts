import {SortType} from "../../enum/sort-type.enum";

export class ProductViewRequest {
    q: string;
    order: string;
    sort: SortType;
    category: number;
    minPrice: number;
    maxPrice: number;
    page: number     = 1;
    pageSize: number = 12;
}
