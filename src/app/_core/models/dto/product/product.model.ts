import {ProductType} from "../../enum/product-type.enum";

export class Product {
  ID: number;
  title: string;
  sku: string;
  original_sku: string;
  slug: string;
  category: number[];
  price: number;
  old_price: number;
  dealer_price: number;
  distributor_price: number;
  stock: number;
  description: string;
  photo: string;
  gallery: string[];
  date_created: string;
  date_updated: string;
  excerpt: string;
  upsell: Product[];
  crosssell: Product[];
  site_id: number;
  lang: string;
  attributes: ProductAttributeItem[];
  // TODO: rating, reviews
  rating: number;
  reviews: number;
  
  type: ProductType;
  variations: Product[];
}

export class ProductAttributeItem {
  label: string;
  name: string;
  value: string;
}
