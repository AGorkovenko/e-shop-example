export class ProductTranslation {
	ID: number;
	parent: string;
	lang: string;
	title: string;
	description: string;
	excerpt: string;
}
