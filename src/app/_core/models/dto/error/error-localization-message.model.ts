export class ErrorLocalizationMessages {
	[errorCode: string]: string
}
