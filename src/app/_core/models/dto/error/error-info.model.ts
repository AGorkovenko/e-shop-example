export interface ErrorInfo {
	code: string;
	message: string;
	data: string;
}
