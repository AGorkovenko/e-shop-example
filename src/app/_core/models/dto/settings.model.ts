import {SelectOption} from "../../../_shared/components/select/select-option.enum";
import {Currency} from "../enum/currency.enum";

export class Settings {
    site: SelectOption;
    lang: string;
    currency: Currency;
}
