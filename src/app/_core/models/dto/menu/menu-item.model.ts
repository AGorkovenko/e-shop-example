export class MenuItem {
    ID: number;
    parent: number;
    title: string;
    order: number;
    link: string;
    lang: string;
    children?: MenuItem[];
}
