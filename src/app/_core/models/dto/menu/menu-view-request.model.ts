export class MenuViewRequest {
	page: number = 1;
	pageSize: number = 12;
}
