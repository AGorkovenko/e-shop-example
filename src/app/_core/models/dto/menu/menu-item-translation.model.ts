export interface MenuItemTranslation {
	ID: number;
	parent: number;
	title: string;
}
