export class MenuItemRequest {
	ID: number;
	parent: number;
	title: string;
	link: string;
	lang?: string;
	parent_item: number
}
