import {SelectOption} from "../../../../_shared/components/select/select-option.enum";

export class MenuRequest {
	type: string;
	title: string;
	site: SelectOption;
}
