import {OrderStatus} from "./order-status.enum";
import {SelectOption} from "../../../../_shared/components/select/select-option.enum";
import {CartItem} from "../cart/cart-item.model";
import {Currency} from "../../enum/currency.enum";

export class Order {
    ID: number; // ID order, unic
    
    uid: number; // user id which create this order
    
    // this data can be different with data "user ID"
    firstname: string;
    lastname: string;
    country: SelectOption;
    ort: SelectOption;
    zip: string;
    address: string;
    warehouse: string; // addition information about delivery
    phone: string;
    email: string;
    
    comment: string; // comment for order
    status: OrderStatus; // order status
    
    currency: Currency; // currency when order was add
    items: CartItem[]; // json data about all items
    quantity: number; // quantity of all items
    subtotal: number; // subtotal for all items without discount and other sale
    delivery_cost: number; // how meny cost delivery option
    extra_pay: number; // some extra pay for payment or other
    total: number; // total (end sum for pay)
    couponId: number; // coupon ID for order
    
    delivery: SelectOption; // data about delivery, json in db
    payment: SelectOption; // data about payment, json in db
    
    date: string; // date when order was created
    update: string; // data when update order data
}
