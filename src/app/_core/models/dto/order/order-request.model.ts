import {SelectOption} from "../../../../_shared/components/select/select-option.enum";
import {OrderStatus} from "./order-status.enum";
import {Cart} from "../cart/cart.model";

export class OrderRequest {
    id?: number;
    uid: number;
    firstname: string;
    lastname: string;
    country: SelectOption;
    ort: SelectOption;
    zip: string;
    address: string;
    warehouse: string;
    phone: string;
    email: string;
    status: OrderStatus;
    delivery: SelectOption;
    payment: SelectOption;
    comment: string;
    currency: string;
    cart: Cart;
}
