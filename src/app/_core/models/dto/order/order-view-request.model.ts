import {SortType} from "../../enum/sort-type.enum";

export class OrderViewRequest {
    id: number;
    q: string;
    order: string;
    sort: SortType;
    page: number     = 1;
    pageSize: number = 12;
}
