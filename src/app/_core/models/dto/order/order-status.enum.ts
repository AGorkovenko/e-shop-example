export enum OrderStatus {
  PENDING = "PENDING", // Order received, no payment initiated. Awaiting payment (unpaid).
  ON_HOLD = "ON_HOLD", // Awaiting payment – stock is reduced, but you need to confirm payment.
  FAILED = "FAILED", // Payment failed or was declined (unpaid). Note that this status may not show immediately and instead show as Pending until verified (e.g., PayPal).
  PROCESSING = "PROCESSING", // Payment received (paid) and stock has been reduced; order is awaiting fulfillment. All product orders require processing, except those that only contain products which are both Virtual and Downloadable.
  CANCELLED = "CANCELLED", // Cancelled by an admin or the customer – stock is increased, no further action required.
  DISPATCHED = "DISPATCHED", // Order received and paid, at the sending stage
  COMPLETED = "COMPLETED", // Order fulfilled and complete – requires no further action.
  REFUNDED = "REFUNDED", // Refunded by an admin – no further action required.
}

export namespace OrderStatus {
  
  export function values(): OrderStatus[] {
    return orderStatusData.map(item => item.value);
  }
  
  export function getDisplayName(value: OrderStatus): string {
    return orderStatusData.find(item => item.value === value).displayName;
  }
  
  export function getCssClass(value: OrderStatus): string {
    return orderStatusData.find(item => item.value === value).cssClass;
  }
  
  export function fixCurrency(value: OrderStatus): boolean {
    return orderStatusData.find(item => item.value === value).fixedCurrency;
  }
  
  interface OrderStatusData {
    value: OrderStatus;
    displayName: string;
    cssClass: string;
    fixedCurrency: boolean;
  }
  
  const orderStatusData: OrderStatusData[] = [
    {value: OrderStatus.PENDING, displayName: "Pending", cssClass: "text-dark", fixedCurrency: false},
    {value: OrderStatus.FAILED, displayName: "Failed", cssClass: "text-danger", fixedCurrency: false},
    {value: OrderStatus.CANCELLED, displayName: "Cancelled", cssClass: "text-danger", fixedCurrency: false},
    {value: OrderStatus.ON_HOLD, displayName: "On hold", cssClass: "text-warning", fixedCurrency: true},
    {value: OrderStatus.PROCESSING, displayName: "Processing", cssClass: "text-warning", fixedCurrency: true},
    {value: OrderStatus.DISPATCHED, displayName: "Dispatched", cssClass: "text-dark", fixedCurrency: true},
    {value: OrderStatus.COMPLETED, displayName: "Completed", cssClass: "text-success", fixedCurrency: true},
    {value: OrderStatus.REFUNDED, displayName: "Refunded", cssClass: "text-primary", fixedCurrency: true}
  ];
}
