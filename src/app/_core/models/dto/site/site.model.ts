export class Site {
    ID: number;
    name: string;
    url: string;
    lang: string;
}
