import {SelectOption} from "../../../../_shared/components/select/select-option.enum";

export class SiteRequest {
	id?: number;
	name: string;
	url: string;
	lang?: SelectOption = null;
}
