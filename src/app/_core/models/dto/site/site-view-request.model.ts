import {SortType} from "../../enum/sort-type.enum";

export class SiteViewRequest {
	order: string;
	sort: SortType;
	page: number = 1;
	pageSize: number = 12;
}
