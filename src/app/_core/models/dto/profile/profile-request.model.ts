export class ProfileRequest {
	login: string;
	firstname: string;
	lastname: string;
	email: string;
}
