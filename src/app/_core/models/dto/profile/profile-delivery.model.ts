import {SelectOption} from "../../../../_shared/components/select/select-option.enum";

export class ProfileDelivery {
    ID: number;
    uid: number;
    firstname: string;
    lastname: string;
    country: SelectOption;
    ort: string;
    zip: string;
    address: string;
    phone: string;
    updated: string;
}
