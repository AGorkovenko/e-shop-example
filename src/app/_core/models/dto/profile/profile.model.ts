export class Profile {
	ID: number;
	login: string;
	email: string;
	role: number;
	firstname: string;
	lastname: string;
	registered: string;
	updated: string;
	ava: string;
	flag: string;
}
