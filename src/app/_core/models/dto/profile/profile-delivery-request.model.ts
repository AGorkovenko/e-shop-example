import {SelectOption} from "../../../../_shared/components/select/select-option.enum";

export class ProfileDelivaryRequest {
	ID: number;
	firstname: string;
	lastname: string;
	country: SelectOption;
	ort: string;
	zip: number;
	address: string;
	phone: string;
}
