export class ItemOrder {
	ID: number;
	order: number;
	parent_item: number;
	children?: ItemOrder[];
}
