import {CartItem} from "./cart-item.model";

export class Cart {
  items: CartItem[];
  quantity: number; // = sum all item.quantity
  subtotal: number; // = sum all item.total
  delivery_cost: number; // = delivery option cost
  extra_pay: number; // = extra cost
  total: number; // = sum all item.total with couponId apply
  couponId: number; // some couponId
}
