export class CartProductRequest {
    uid: number;
    pid: number;
    pvid: number;
    quantity: number;
    comment: string;
}
