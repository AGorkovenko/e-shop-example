export class CartItem {
  ID: number; // ID in db
  
  pid: number; // product id
  pvid: number; // product variation id
  
  title: string; // title of product
  sku: string; // sku of product
  original_sku: string; // original sku of product
  slug: string; // slug of product
  photo: string; // photo of product
  price: number; // product price
  old_price: number; // product old price
  
  dealer_price: number;
  distributor_price: number;
  
  new_price: number;  // editable, price only for this item and this user = (price || dealer_price || distributor_price) + couponId apply
  quantity: number;  // editable, quantity of this items
  total: number;  // editable, = new_price * quantity
  
  comment: string; // editable, some comment
}
