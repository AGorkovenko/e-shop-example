import {SelectOption} from "../../../../_shared/components/select/select-option.enum";

export class ShopSettingRequest {
    id?: number;
    setting: string;
    value: string;
    site: SelectOption;
}
