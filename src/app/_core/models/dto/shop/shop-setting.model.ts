export class ShopSetting {
    ID: number;
    setting: string;
    value: string;
    site_id: number;
}
