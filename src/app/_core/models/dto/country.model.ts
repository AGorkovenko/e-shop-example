export class Country {
	code: string;
	numeric: string;
	name: string;
}
