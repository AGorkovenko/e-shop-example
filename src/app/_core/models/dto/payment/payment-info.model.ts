import {Payment} from "./payment.enum";
import {PaymentStatus} from "./payment-status.enum";
import {Currency} from "../../enum/currency.enum";

export class PaymentInfo {
  ID: number;
  orderId: number;
  uid: number;
  type: Payment;
  systemId: string;
  status: PaymentStatus;
  amount: number;
  currency: Currency;
  data: any;
  date: string;
}
