export enum Payment {
    INVOICE         = "INVOICE",
    CASH            = "CASH",
    VISA_MASTERCARD = "VISA_MASTERCARD",
    PRIVAT_BANK     = "PRIVAT_BANK",
    // PAYPAL          = "PAYPAL",
    // TWOCHECKOUT     = "TWOCHECKOUT"
}

export namespace Payment {
    
    export function values(): Payment[] {
        return PaymentData.map(_paymentDataItem => _paymentDataItem.payment);
    }
    
    export function getDisplayName(payment): string | null {
        const PaymentDataItem = PaymentData.find(_paymentDataItem => _paymentDataItem.payment === payment);
        return PaymentDataItem ? PaymentDataItem.displayName : null;
    }
    
    export function getDescription(payment): string | null {
        const PaymentDataItem = PaymentData.find(_paymentDataItem => _paymentDataItem.payment === payment);
        return PaymentDataItem ? PaymentDataItem.description : null;
    }
    
    interface PaymentData {
        payment: Payment;
        displayName: string;
        description: string;
    }
    
    const PaymentData: PaymentData[] = [
        {payment: Payment.INVOICE, displayName: "Invoice", description: "Payment by bank transfer"},
        {payment: Payment.CASH, displayName: "Cash payment", description: "Cash on delivery at the office"},
        {payment: Payment.VISA_MASTERCARD, displayName: "Visa/Mastercard", description: "Credit Cards"},
        {payment: Payment.PRIVAT_BANK, displayName: "Privat Bank", description: "Privat Bank Ukraine"},
        // {payment: Payment.PAYPAL, displayName: "PayPal", description: "PayPal system"},
        // {payment: Payment.TWOCHECKOUT, displayName: "2Checkout", description: "2Checkout system"}
    ];
}
