export enum PaymentStatus {
  WAITING = "WAITING", // Order received, no payment initiated. Awaiting payment (unpaid).
  CANCELLED = "CANCELLED", // Cancelled by an admin or the customer – stock is increased, no further action required.
  COMPLETED = "COMPLETED", // Order fulfilled and complete – requires no further action.
  REFUNDED = "REFUNDED", // Refunded by an admin – no further action required.
  FAILED = "FAILED", // Payment failed or was declined (unpaid). Note that this status may not show immediately and instead show as Pending until verified (e.g., PayPal).
}

export namespace PaymentStatus {
  
  export function values(): PaymentStatus[] {
    return paymentStatusData.map(item => item.value);
  }
  
  export function getDisplayName(value: PaymentStatus): string {
    return paymentStatusData.find(item => item.value === value).displayName;
  }
  
  export function getCssClass(value: PaymentStatus): string {
    return paymentStatusData.find(item => item.value === value).cssClass;
  }
  
  export function fixCurrency(value: PaymentStatus): boolean {
    return paymentStatusData.find(item => item.value === value).fixedCurrency;
  }
  
  interface PaymentStatusData {
    value: PaymentStatus;
    displayName: string;
    cssClass: string;
    fixedCurrency: boolean;
  }
  
  const paymentStatusData: PaymentStatusData[] = [
    {value: PaymentStatus.WAITING, displayName: "Pending", cssClass: "text-dark", fixedCurrency: false},
    {value: PaymentStatus.FAILED, displayName: "Failed", cssClass: "text-danger", fixedCurrency: false},
    {value: PaymentStatus.CANCELLED, displayName: "Cancelled", cssClass: "text-danger", fixedCurrency: false},
    {value: PaymentStatus.COMPLETED, displayName: "Completed", cssClass: "text-success", fixedCurrency: true},
    {value: PaymentStatus.REFUNDED, displayName: "Refunded", cssClass: "text-primary", fixedCurrency: true}
  ];
}
