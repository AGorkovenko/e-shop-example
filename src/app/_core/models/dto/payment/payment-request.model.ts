import {PaymentStatus} from "./payment-status.enum";
import {Payment} from "./payment.enum";
import {Currency} from "../../enum/currency.enum";

export class PaymentRequest {
  id?: number;
  orderId: number;
  uid: number;
  type: Payment;
  systemId: string;
  status: PaymentStatus;
  amount: number;
  currency: Currency;
  data: any;
}
