export class Language {
    ID: number;
    name: string;
    code: string;
}
