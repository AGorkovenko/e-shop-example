import {User} from "../user/user.model";

export class Review {
  ID: number;
  pid: number;
  user: User;
  rating: number;
  date: string;
  review: string;
}
