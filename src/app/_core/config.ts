import {Currency} from "./models/enum/currency.enum";

export let CONFIG = {
    apiUrl: "/api/wapi/v1",
    profileLocalStorage: "AuthShopProfile",
    settingsCookie: "ShopSettings",
    temporaryUserCoockie: "wapi.temp.user.id",
    defCurrency: Currency.EUR
};
