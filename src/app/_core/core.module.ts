import {HTTP_INTERCEPTORS, HttpClientJsonpModule, HttpClientModule} from "@angular/common/http";
import {NgModule} from "@angular/core";
import {CookieService} from "./services/cookie.service";
import {LoggerService} from "./services/logger.service";
import {HttpCachingClient} from "./services/http-caching-client.service";
import {ProfileService} from "./services/profile/profile.service";
import {AuthTokenInterceptors} from "./services/interceptors/auth-token.interceptors";
import {ErrorInterceptor} from "./services/interceptors/error.interceptor";
import {MenuService} from "./services/menu/menu.service";
import {MetaService} from "./services/meta/meta.service";
import {TimeService} from "./services/time.service";
import {HistoryNavigationService} from "./services/history-navigation.service";
import {ToastrModule} from "ngx-toastr";
import {NotificationService} from "./services/notification.service";
import {ExceptionService} from "./services/exception.service";
import {NovaPoshtaService} from "./services/nova-poshta.service";
import {NovaPoshtaInterceptors} from "./services/interceptors/nova-poshta.interceptors";
import {CartService} from "./services/cart.service";
import {CurrencyService} from "./services/currency.service";
import {OrdersService} from "./services/order/orders.service";
import {PageNavigationService} from "./services/page-navigation";
import {SiteNavigationService} from "./services/site-navigation";
import {MetaLinkService} from "./services/meta-link.service";
import {SiteMetaDataService} from "./services/site-meta-data";
import {ContentMetaBackendClient} from "./services/meta/meta-backend.service";
import {SiteService} from "./services/site/site.service";
import {SiteBackendClient} from "./services/site/site-backend.service";
import {LanguageService} from "./services/language.service";
import {TranslateService} from "./services/translate.service";
import {TranslateInterceptors} from "./services/interceptors/translate.interceptors";
import {ContentService} from "./services/content/content.service";
import {ContentBackendClient} from "./services/content/content-backend.service";
import {UploaderService} from "./services/uploader.service";
import {MenuBackendClient} from "./services/menu/menu-backend.service";
import {CountryService} from "./services/country.service";
import {ProductsService} from "./services/product/products.service";
import {ProductsBackendClient} from "./services/product/products-backend.service";
import {ProductsCategoriesService} from "./services/product/products-categories.service";
import {ProductsCategoriesBackendClient} from "./services/product/products-categories-backend.service";
import {OrdersBackendClient} from "./services/order/orders-backend.service";
import {ShopService} from "./services/shop/shop.service";
import {LayoutManipulationService} from "./services/layout-manipulation.service";
import {LoaderService} from "./services/loader.service";
import {UsersService} from "./services/users/users.service";
import {UsersBackendClient} from "./services/users/users-backend.service";
import {SiteAppService} from "./services/site-app.service";
import {SignInUpService} from "./services/sign-in-up.service";
import {ProfileUpdateBackendClient} from "./services/profile/profile-update-backend-client.service";
import {ProfileUpdateService} from "./services/profile/profile-update.service";
import {ShopBackendClient} from "./services/shop/shop-backend.service";
import {PaymentService} from "./services/payment.service";

@NgModule({
    imports: [
        HttpClientModule,
        HttpClientJsonpModule,
        ToastrModule.forRoot()
    ],
    providers: [
        LoggerService,
        HttpCachingClient,
        CookieService,
        ProfileService,
        ProfileUpdateService,
        ProfileUpdateBackendClient,
        MetaService,
        ContentMetaBackendClient,
        TimeService,
        HistoryNavigationService,
        NotificationService,
        ExceptionService,
        NovaPoshtaService,
        CartService,
        CurrencyService,
        PageNavigationService,
        SiteNavigationService,
        SiteMetaDataService,
        MetaLinkService,
        SiteService,
        SiteBackendClient,
        LanguageService,
        TranslateService,
        LayoutManipulationService,
        ContentService,
        ContentBackendClient,
        UploaderService,
        MenuService,
        MenuBackendClient,
        CountryService,
        UsersService,
        UsersBackendClient,
        ProductsService,
        ProductsBackendClient,
        ProductsCategoriesService,
        ProductsCategoriesBackendClient,
        OrdersService,
        OrdersBackendClient,
        ShopService,
        ShopBackendClient,
        LoaderService,
        SiteAppService,
        SignInUpService,
        PaymentService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthTokenInterceptors,
            multi: true
        },
        {
            provide: ErrorInterceptor,
            useClass: AuthTokenInterceptors,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: NovaPoshtaInterceptors,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TranslateInterceptors,
            multi: true
        }
    ]
})
export class CoreModule {
}
