import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot} from "@angular/router";
import {Profile} from "./models/dto/profile/profile.model";
import {CONFIG} from "./config";

@Injectable({providedIn: "root"})
export class NoAuthGuard implements CanActivate, CanActivateChild {
	
	profile: Profile;
	
	constructor(private router: Router) {
	}
	
	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		if (localStorage.getItem(CONFIG.profileLocalStorage)) {
			this.router.navigate(["/"]);
			return false;
		} else {
			return true;
		}
	}
	
	canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		if (localStorage.getItem(CONFIG.profileLocalStorage)) {
			this.router.navigate(["/"]);
			return false;
		} else {
			return true;
		}
	}
}
