import {NgModule} from "@angular/core";
import {MainRoutingModule} from "./main-routing.module";
import {SharedModule} from "../_shared/shared.module";
import {MainComponent} from "./main.component";
import {HeadComponent} from "./components/head/head.component";
import {FooterComponent} from "./components/footer/footer.component";
import {TopComponent} from "./components/top/top.component";
import {MenuComponent} from "./components/menu/menu.component";
import {PageNotFoundComponent} from "../page-not-found/page-not-found.component";
import {HomeComponent} from "./components/home/home.component";
import {PageComponent} from "./components/page/page.component";
import {PageResolver} from "./resolvers/page.resolver";
import {SignInUpComponent} from "./components/sing-in-up/sign-in-up.component";
import {RecoveryComponent} from "./components/recovery/recovery.component";
import {ThankYouComponent} from "./components/static-page/thank-you.component";
import {SliderTopComponent} from "./components/slider/slider.component";

@NgModule({
    imports: [
        SharedModule,
        MainRoutingModule
    ],
    declarations: [
        MainComponent,
        HeadComponent,
        FooterComponent,
        TopComponent,
        MenuComponent,
        HomeComponent,
        SliderTopComponent,
        PageComponent,
        PageNotFoundComponent,
        SignInUpComponent,
        RecoveryComponent,
        ThankYouComponent
    ],
    providers: [
        PageResolver
    ]
})
export class MainModule {
}
