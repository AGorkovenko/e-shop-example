import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {MainComponent} from "./main.component";
import {HomeComponent} from "./components/home/home.component";
import {PageNotFoundComponent} from "../page-not-found/page-not-found.component";
import {PageComponent} from "./components/page/page.component";
import {PageResolver} from "./resolvers/page.resolver";
import {RecoveryComponent} from "./components/recovery/recovery.component";
import {RecoveryResolver} from "./resolvers/recovery.resolver";
import {ThankYouComponent} from "./components/static-page/thank-you.component";
import {ThankYouPageResolver} from "./resolvers/thank-you-page.resolver";

const routes: Routes = [
  {
    path: "",
    component: MainComponent,
    children: [
      {
        path: "",
        component: HomeComponent,
        data: {
          title: "Home page"
        }
      },
      {
        path: "shop",
        loadChildren: "../shop/shop.module#ShopModule",
        data: {
          title: "Shop"
        }
      },
      {
        path: "checkout",
        loadChildren: "../checkout/checkout.module#CheckoutModule",
        data: {
          title: "Checkout"
        }
      },
      {
        path: "cart",
        loadChildren: "../cart/cart.module#CartModule",
        data: {
          title: "Cart"
        }
      },
      {
        path: "profile",
        loadChildren: "../profile/profile.module#ProfileModule",
        data: {
          title: "Profile"
        }
      },
      {
        path: "thank-you",
        component: ThankYouComponent,
        data: {
          title: "Thank you page"
        },
        resolve: {
          page: ThankYouPageResolver
        }
      },
      {
        path: "404",
        component: PageNotFoundComponent,
        data: {
          title: "Error 404"
        }
      },
      {
        path: "recovery",
        component: RecoveryComponent,
        data: {
          title: "Recovery password"
        },
        resolve: {
          uid: RecoveryResolver
        }
      },
      {
        path: ":slug",
        component: PageComponent,
        data: {
          title: "Page"
        },
        resolve: {
          page: PageResolver
        }
      },
      {
        path: "**",
        component: PageNotFoundComponent,
        data: {
          title: "Error 404"
        }
        // redirectTo: "404"
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    RecoveryResolver,
    PageResolver,
    ThankYouPageResolver
  ]
})
export class MainRoutingModule {
}
