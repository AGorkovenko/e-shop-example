import {Component, OnDestroy, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {Subject} from "rxjs";
import {takeUntil, tap} from "rxjs/operators";
import {Content} from "../../../_core/models/dto/content/content.model";
import {LoggerService} from "../../../_core/services/logger.service";
import {SiteMetaDataService} from "../../../_core/services/site-meta-data";
import {ContentService} from "../../../_core/services/content/content.service";
import {ContentType} from "../../../_core/models/dto/content/content-type.enum";

@Component({
  selector: "app-page",
  template: `
    <div class="container">
      <div class="box">
        <div class="page page--full">
          <div class="page__content">
            <h1 *ngIf="page.title">{{page.title}}</h1>
            <div [innerHTML]="page.content"></div>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ["./page.component.scss"]
})
export class PageComponent implements OnInit, OnDestroy {
  
  page: Content;
  contentType = ContentType;
  
  isDestroyed$: Subject<void> = new Subject();
  
  constructor(private logger: LoggerService,
              private route: ActivatedRoute,
              private siteMetaDataService: SiteMetaDataService,
              private contentService: ContentService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  ngOnInit() {
    this.route.data
      .pipe(
        tap((data: { page: Content }) => {
          this.page = data.page;
          this.siteMetaDataService.setCustomTitle(this.page.type + " field id: " + this.page.ID);
          this.siteMetaDataService.setDescription("Edit " + this.page.type + " with id: " + this.page.ID);
        }),
        takeUntil(this.isDestroyed$)
      )
      .subscribe();
  }
  
  ngOnDestroy(): void {
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }
}
