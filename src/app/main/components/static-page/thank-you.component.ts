import {Component, OnDestroy, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {Subject} from "rxjs";
import {map, switchMap, takeUntil, tap} from "rxjs/operators";
import {Content} from "../../../_core/models/dto/content/content.model";
import {LoggerService} from "../../../_core/services/logger.service";
import {SiteMetaDataService} from "../../../_core/services/site-meta-data";
import {ContentType} from "../../../_core/models/dto/content/content-type.enum";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: "app-thank-you",
  template: `
    <div class="container">
      <div class="box">
        <div class="page page--full">
          <div class="page__content">
            <h1 *ngIf="page.title" class="text-center">{{page.title}}</h1>
            <div [innerHTML]="sanitization.bypassSecurityTrustHtml(page.content)"></div>
          </div>
        </div>
      </div>
    </div>
  `
})
export class ThankYouComponent implements OnInit, OnDestroy {
  
  page: Content;
  contentType = ContentType;
  
  isDestroyed$: Subject<void> = new Subject();
  
  constructor(private logger: LoggerService,
              private route: ActivatedRoute,
              private router: Router,
              private siteMetaDataService: SiteMetaDataService,
              public sanitization: DomSanitizer) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  ngOnInit() {
    this.route.data
    .pipe(
      tap((data: { page: Content }) => {
        this.page = data.page;
        this.siteMetaDataService.setCustomTitle(this.page.type + " field id: " + this.page.ID);
        this.siteMetaDataService.setDescription("Edit " + this.page.type + " with id: " + this.page.ID);
      }),
      switchMap(() => this.route.paramMap),
      map(() => window.history.state),
      tap((state: { orderId: number, paymentId: number, ty: boolean }) => {
        if (!state.ty) {
          this.router.navigate(["/"]);
        } else {
          if (state.orderId) this.page.content = this.page.content.replace("{orderId}", state.orderId.toString());
          if (state.paymentId) this.page.content = this.page.content.replace("{paymentId}", state.paymentId.toString());
        }
      }),
      takeUntil(this.isDestroyed$)
    )
    .subscribe();
  }
  
  ngOnDestroy(): void {
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }
}
