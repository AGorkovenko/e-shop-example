import {Component, OnInit} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";
import {Subject} from "rxjs";

@Component({
  selector: "app-homepage",
  template: `
    <app-slider-top></app-slider-top>
    <div class="container">
  
      <div class="block">
        <div class="block__name"></div>
        <div class="block-row block-row--4">
          <div class="block-column">
            <div class="info-block">
              <div class="info-block__icon">
                <app-icon name="delivery"></app-icon>
              </div>
              <div class="info-block__name">
                <div class="info-block-name-first">Free delivery</div>
                <div class="info-block-name-second">Worldwide from €75</div>
              </div>
            </div>
          </div>
          <div class="block-column">
            <div class="info-block">
              <div class="info-block__icon">
                <app-icon name="returns"></app-icon>
              </div>
              <div class="info-block__name">
                <div class="info-block-name-first">Easy returns</div>
                <div class="info-block-name-second">365 days for free returns</div>
              </div>
            </div>
          </div>
          <div class="block-column">
            <div class="info-block">
              <div class="info-block__icon">
                <app-icon name="payments"></app-icon>
              </div>
              <div class="info-block__name">
                <div class="info-block-name-first">Comfort payments</div>
                <div class="info-block-name-second">Credit card available</div>
              </div>
            </div>
          </div>
          <div class="block-column">
            <div class="info-block">
              <div class="info-block__icon">
                <app-icon name="gifts"></app-icon>
              </div>
              <div class="info-block__name">
                <div class="info-block-name-first">Free gifts</div>
                <div class="info-block-name-second">Get gifts and discounts</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  
  isDestroyed$: Subject<void> = new Subject();
  
  constructor(private logger: LoggerService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  ngOnInit() {
  }
  
}
