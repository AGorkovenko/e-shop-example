import {Component, OnDestroy, OnInit} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";
import {CartService} from "../../../_core/services/cart.service";
import {Cart} from "../../../_core/models/dto/cart/cart.model";
import {Observable, Subject} from "rxjs";
import {CurrencyService} from "../../../_core/services/currency.service";
import {Currency} from "../../../_core/models/enum/currency.enum";
import {takeUntil, tap} from "rxjs/operators";

@Component({
  selector: "app-head",
  template: `
    <header>
      <div class="container">
        <div class="head">
          <div class="head-logo">
            <a class="logo" [routerLink]="['/']">
              <img src="./assets/images/logo-snegovodomoto.svg" alt="SnegoVodoMoto">
            </a>
          </div>
          <div class="head-search">
            <div class="search">
              <input class="search__field" type="text" i18n-placeholder="Placeholder for search field in head@@headerSearchPlaceholder" placeholder="Search by id, Name, brand, SKU, etc.">
              <button class="search__button" type="submit" i18n="Button for search field in head@@headerSearchButton">Search</button>
            </div>
          </div>
          
          <div class="head-cart" *ngIf="cart$ | async as cart">
            <div class="cart-mini" appDropDown openClass="cart-mini--open">
              <app-icon name="cart"></app-icon>
              <div class="cart-mini__count">{{cart.quantity}}</div>
              <div class="cart-mini__total" *ngIf="cart.subtotal">{{currencyService.currencyExchange(cart.subtotal) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}</div>
              <div class="cart-mini-content">
                <div class="cart-mini-content__head" i18n="Title for shoping cart in header@@headerCartMyShopingCart">My Shopping Cart</div>
                <div class="cart-mini-content__products" *ngIf="cart.items.length; else emptyCart">
                  <div class="cart-mini-product" *ngFor="let item of cart.items;">
                    <div class="cart-mini-product__thumb">
                      <div class="image  image--thumb" *ngIf="item.photo" [ngStyle]="{'background-image': 'url(' + item.photo +'/thumb)'}"></div>
                      <div class="image  image--thumb" *ngIf="!item.photo" [ngStyle]="{'background-image': 'url(assets/images/placeholder-piston.svg)'}"></div>
                    </div>
                    <div class="cart-mini-product__info cart-mini-product-info">
                      <a class="cart-mini-product-info__name" [routerLink]="['/shop/product/' + item.slug]">{{item.title}}</a>
                      <div class="cart-mini-product-info__qty">Qty: <span>{{item.quantity}}</span> x <span>{{currencyService.currencyExchange(item.new_price) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}</span></div>
                    </div>
                  </div>
                </div>
                <ng-template #emptyCart>
                  <div class="text-center pt pb" i18n="@@headerCartYourCartIsEmpty">Your cart is empty</div>
                </ng-template>
                <div class="cart-mini-content__total" *ngIf="cart.subtotal">
                  <div class="subtotal-name" i18n="@@headerCartSubtotal">Subtotal</div>
                  <div class="subtotal-price">{{currencyService.currencyExchange(cart.subtotal) | currency:currentCurrency:currency.getSymbol(currentCurrency):currency.getFormat(currentCurrency)}}</div>
                </div>
                <div class="cart-mini-content__buttons" *ngIf="cart.items.length">
                  <a class="button button--outline-danger button--inline" [routerLink]="['/cart']" i18n="@@headerCartViewDetails">View Details</a>
                  <a class="button button--outline-default button--inline" [routerLink]="['/checkout']" i18n="@@headerCartCheckout">Checkout</a>
                </div>
              </div>
            </div>
          </div>
        
        </div>
      </div>
    </header>
  `,
  styleUrls: ["./head.component.scss"]
})
export class HeadComponent implements OnInit, OnDestroy {
  
  cart$: Observable<Cart>;
  currentCurrency: Currency;
  currency = Currency;
  
  isDestroyed$: Subject<void> = new Subject();
  
  constructor(private logger: LoggerService,
              private cartService: CartService,
              public currencyService: CurrencyService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  ngOnInit() {
    this.cart$ = this.cartService.cart$;
    
    this.currencyService.currentCurrency$
    .pipe(
      tap((currency: Currency) => {
        this.currentCurrency = currency;
      }),
      takeUntil(this.isDestroyed$)
    )
    .subscribe();
  }
  
  ngOnDestroy(): void {
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }
}
