import {Component, OnInit} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";
import {LanguageService} from "../../../_core/services/language.service";
import {switchMap, take, tap} from "rxjs/operators";
import {Language} from "../../../_core/models/dto/lenguage.model";
import {CurrencyService} from "../../../_core/services/currency.service";
import {Currency} from "../../../_core/models/enum/currency.enum";
import {environment} from "../../../../environments/environment";
import {SiteAppData, SiteAppService} from "../../../_core/services/site-app.service";
import {ContentMeta} from "../../../_core/models/dto/meta/content-meta.model";
import {SignInUpService} from "../../../_core/services/sign-in-up.service";
import {ProfileService} from "../../../_core/services/profile/profile.service";
import {SignInUp} from "../../../_core/models/enum/sign-in-up.enum";
import {Profile} from "../../../_core/models/dto/profile/profile.model";
import {DomSanitizer} from "@angular/platform-browser";
import {SiteNavigationService} from "../../../_core/services/site-navigation";

@Component({
  selector: "app-top",
  template: `
    <div class="top">
      <div class="container">
        <div class="top-wrap">
          <div class="top-left">
            <div class="top-socials">
              <div class="socials">
                <a class="social-link social-link--facebook" [href]="siteAppService.getMetaValue('facebook')">
                  <app-icon [name]="'facebook'"></app-icon>
                </a>
                <a class="social-link social-link--massenger" [href]="siteAppService.getMetaValue('massenger')">
                  <app-icon [name]="'massenger'"></app-icon>
                </a>
                <a class="social-link social-link--youtube" [href]="siteAppService.getMetaValue('youtube')">
                  <app-icon [name]="'youtube'"></app-icon>
                </a>
                <a class="social-link social-link--instagram" [href]="siteAppService.getMetaValue('instagram')">
                  <app-icon [name]="'instagram'"></app-icon>
                </a>
              </div>
            </div>
            <div class="top-contact">
              <a class="top-contact__item top-contact__item--email" [href]="sanitization.bypassSecurityTrustUrl('mailto:' + siteAppService.getMetaValue('phone-in-top'))" *ngIf="siteAppService.getMetaValue('phone-in-top')">
                <app-icon name="letter"></app-icon>
                <span>{{siteAppService.getMetaValue('email-in-top')}}</span>
              </a>
              <a class="top-contact__item top-contact__item--phone" [href]="sanitization.bypassSecurityTrustUrl('tel:' + siteAppService.getMetaValue('phone-in-top'))" *ngIf="siteAppService.getMetaValue('phone-in-top')">
                <app-icon name="phone"></app-icon>
                <span>{{siteAppService.getMetaValue('phone-in-top')}}</span>
              </a>
            </div>
          </div>
          <div class="top-right">
            <div class="top-languages">
              <div class="top-dropdown top-dropdown--languages" appDropDown openClass="top-dropdown--open">
                <div class="top-dropdown-current">
                  <div class="top-dropdown-current__name" *ngIf="locale">{{locale.toUpperCase()}}</div>
                  <app-icon name="toggle" classes="top-dropdown-current__toggle"></app-icon>
                </div>
                <div class="top-dropdown-list" *ngIf="languages">
                  <a [href]="getLanguageUrl(language) + currentPath" class="top-dropdown-list__item" *ngFor="let language of languages" [ngClass]="{'top-dropdown-list__item--active': language.code === locale}">
                    {{language.name.toUpperCase()}}
                  </a>
                </div>
              </div>
            </div>
            <div class="top-currency">
              <div class="top-dropdown top-dropdown--currency" [ngClass]="{'top-dropdown--open': openCurrency}" appClickOutside (clickOutside)="closeListCurrency()">
                <div class="top-dropdown-current" (click)="openListCurrency()">
                  <div class="top-dropdown-current__name" *ngIf="currentCurrency">{{currentCurrency}}, {{currency.getSymbol(currentCurrency)}}</div>
                  <app-icon name="toggle" classes="top-dropdown-current__toggle"></app-icon>
                </div>
                <div class="top-dropdown-list" *ngIf="currencies">
                  <div class="top-dropdown-list__item" *ngFor="let currencyItem of currencies" [ngClass]="{'top-dropdown-list__item--active': currencyItem === currentCurrency}" (click)="setCurrency(currencyItem)">
                    {{currencyItem}}, {{currency.getSymbol(currencyItem)}}</div>
                </div>
              </div>
            </div>
            <div class="top-user" *ngIf="!profile">
              <div class="top-user__button top-user__button--signin">
                <button class="top-user-button" (click)="openSignInModal()">
                  <div class="top-user-button__icon">
                    <app-icon name="user"></app-icon>
                  </div>
                  <span i18n="Sign top button in top@@topSingIn">Sing in</span>
                </button>
              </div>
              <div class="top-user__button top-user__button--signup">
                <button class="top-user-button" (click)="openSignUpModal()">
                  <span i18n="Register button in top@@topRegister">Register</span>
                </button>
              </div>
            </div>
            <div class="top-user" *ngIf="profile">
              <div class="top-user__button top-user__button--signin">
                <a [routerLink]="['/profile']" class="top-user-button">
                  <div class="top-user-button__icon">
                    <app-icon name="user"></app-icon>
                  </div>
                  <span>{{profile.login}}</span>
                </a>
              </div>
              <div class="top-user__button top-user__button--signup">
                <a [routerLink]="['/logout']" class="top-user-button">
                  <span i18n="Logout link in top@@topLogout">Logout</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ["./top.component.scss"]
})
export class TopComponent implements OnInit {
  
  languages: Language[] = null;
  locale: string = null;
  openCurrency: boolean = false;
  
  currentCurrency: Currency;
  currency = Currency;
  currencies: Currency[];
  
  profile: Profile = null;
  
  currentPath: string = "";
  
  metas: ContentMeta[] = [];
  
  constructor(private logger: LoggerService,
              private languageService: LanguageService,
              private currencyService: CurrencyService,
              public siteAppService: SiteAppService,
              private signInUpService: SignInUpService,
              private profileService: ProfileService,
              public sanitization: DomSanitizer,
              public siteNavigationService: SiteNavigationService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  ngOnInit() {
    this.currencies = this.currency.values();
    
    this.languageService.languages$
      .pipe(
        tap(languages => {
          this.languages = languages;
        }),
        switchMap(() => this.languageService.currentLocale$),
        tap(locale => {
          if (locale) {
            this.locale = locale;
          } else {
            this.locale = this.languages[0].code;
          }
        })
      )
      .subscribe();
    
    this.currencyService.currentCurrency$
      .pipe(
        tap((currency: Currency) => {
          this.currentCurrency = currency;
        })
      )
      .subscribe();
    
    this.siteAppService.siteAppData$
      .pipe(
        tap((data: SiteAppData) => {
          if (data && data.metas) {
            this.metas = data.metas;
          }
        })
      )
      .subscribe();
    
    this.profileService.profile$
      .pipe(
        tap((profile: Profile) => {
          this.profile = profile;
        })
      )
      .subscribe();
    
    this.siteNavigationService.getCurrentPagePath()
      .pipe(
        tap((path: string) => {
          this.currentPath = path;
        })
      )
      .subscribe();
  }
  
  getLanguageUrl(language: Language): string {
    return environment.siteAddress + "/" + language.code;
  }
  
  openListCurrency() {
    this.openCurrency = true;
  }
  
  closeListCurrency() {
    this.openCurrency = false;
  }
  
  openSignInModal() {
    this.signInUpService.openModal(SignInUp.LOGIN);
  }
  
  openSignUpModal() {
    this.signInUpService.openModal(SignInUp.REGISTRATION);
  }
  
  setCurrency(currency: Currency) {
    this.currencyService.setCurrency(currency);
    this.closeListCurrency();
  }
  
}
