import {Component, OnDestroy, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {Subject} from "rxjs";
import {switchMap, takeUntil, tap} from "rxjs/operators";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {LoggerService} from "../../../_core/services/logger.service";
import {PasswordValidation} from "../../../_core/password.validatator";
import {ProfileUpdateService} from "../../../_core/services/profile/profile-update.service";
import {SignInUpService} from "../../../_core/services/sign-in-up.service";

@Component({
    selector: "app-recovery",
    template: `
      <div class="container pt">
        <div class="box box--half">
          <div class="page">
            <div class="page__content">
              <h1 i18n="Title recovery password page@@titleRecoverypassword">Recovery password</h1>
          
              <form class="form" [formGroup]="passwordForm" *ngIf="!isPasswordChanged">
                <div class="form-line">
                  <label i18n="Label password on recovery password page@@labelNewPassword">New password</label>
                  <input class="form-element" type="password" formControlName="password" [ngClass]="{'form-element--error': passwordForm.get('password').invalid && passwordForm.get('password').touched}">
                  <div class="from-help" i18n="Tip on recovery password page@@recoveryPasswordTip">(min 6 symbols)</div>
                </div>
                <div class="form-line">
                  <label i18n="Label confirm password on recovery password page@@labelConfirmPassword">Confirm new password</label>
                  <input class="form-element" type="password" formControlName="repassword" [ngClass]="{'form-element--error': passwordForm.get('repassword').invalid && passwordForm.get('repassword').touched}">
                </div>
                <div class="form-line form-line--last">
                  <button class="button button--inline button--danger" (click)="changePassword()" i18n="Button on recovery password page@@buttonChangePassword">Change</button>
                </div>
              </form>
          
              <h3 *ngIf="isPasswordChanged" i18n="Success changed password@@buttonSuccessfullyPassword" (click)="openLoginModal()">You have successfully reset your password. Use new password for
                <button type="button" class="button button--link">login</button>
                on site
              </h3>
            </div>
          </div>
        </div>
      </div>
    `
})
export class RecoveryComponent implements OnInit, OnDestroy {
    
    uid: number;
    code: string;
    passwordForm: FormGroup;
    isPasswordChanged: boolean = false;
    
    isDestroyed$: Subject<void> = new Subject();
    
    constructor(private logger: LoggerService,
                private route: ActivatedRoute,
                private router: Router,
                private fb: FormBuilder,
                private profileUpdateService: ProfileUpdateService,
                private signInUpService: SignInUpService) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    ngOnInit(): void {
        this.route.data
        .pipe(
            tap((data: { uid: number }) => {
                this.uid = data.uid;
            }),
            switchMap(() => this.route.queryParams),
            tap((params: { resetCode: string }) => {
                this.code = params.resetCode;
            }),
            takeUntil(this.isDestroyed$)
        )
        .subscribe();
        
        this.initForm();
    }
    
    initForm(login_email?: string) {
        this.passwordForm = this.fb.group({
            password: new FormControl("", [Validators.required, Validators.minLength(6)]),
            repassword: new FormControl("", [Validators.required, Validators.minLength(6)])
        }, {
            validator: PasswordValidation.MatchPassword
        });
    }
    
    changePassword() {
        if (this.passwordForm.valid) {
            const formData = this.passwordForm.getRawValue();
            this.profileUpdateService.passwordChange(formData.password, formData.repassword, this.code, this.uid)
            .pipe(
                tap(change => {
                    if (change) {
                        this.passwordForm.reset();
                        this.isPasswordChanged = change;
                    }
                }),
                takeUntil(this.isDestroyed$)
            )
            .subscribe();
        }
    }
    
    openLoginModal() {
        this.signInUpService.openModal();
    }
    
    ngOnDestroy(): void {
        this.isDestroyed$.next();
        this.isDestroyed$.complete();
    }
}
