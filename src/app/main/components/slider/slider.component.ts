import {Component, OnDestroy, OnInit} from "@angular/core";
import {Subject} from "rxjs";
import {LoggerService} from "../../../_core/services/logger.service";

@Component({
	selector: "app-slider-top",
	template: `
        <div class="slider" style="background-image: url(./assets/images/slider.jpg)">
            <div class="slider__wrap">
                <div class="container">
                    <div class="slider-title">SELECT YOUR VEHICLE</div>
                    <div class="slider-finder">
                        <div class="finder">
                            <div class="finder__column">
                                <!--.search--life-->
                                <!--.search--open-->
                                <div class="form-element select select--finder">
                                    <div class="select__value">
                                        <div class="select__value-first">Select option</div>
                                        <div class="select__value-second"></div>
                                    </div>
                                    <div class="select__caret">
                                        <app-icon name="toggle"></app-icon>
                                    </div>
                                    <div class="select__search">
                                        <input class="form-element" type="text" placeholder="Typing some symbols ...">
                                    </div>
                                    <div class="select__options">
                                        <div class="select__option">
                                            <div class="select__option-fisrt">Yamaha</div>
                                            <div class="select__option-second"></div>
                                        </div>
                                        <div class="select__option">
                                            <div class="select__option-fisrt">Suzuki</div>
                                            <div class="select__option-second"></div>
                                        </div>
                                        <div class="select__option">
                                            <div class="select__option-fisrt">Kawasaki</div>
                                            <div class="select__option-second"></div>
                                        </div>
                                        <div class="select__option">
                                            <div class="select__option-fisrt">Polaris</div>
                                            <div class="select__option-second"></div>
                                        </div>
                                        <div class="select__option">
                                            <div class="select__option-fisrt">Sea-Doo</div>
                                            <div class="select__option-second"></div>
                                        </div>
                                        <div class="select__default"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="finder__column">
                                <!--.search--life-->
                                <!--.search--open-->
                                <div class="form-element select select--finder">
                                    <div class="select__value">
                                        <div class="select__value-first">Select option</div>
                                        <div class="select__value-second"></div>
                                    </div>
                                    <div class="select__caret">
                                        <app-icon name="toggle"></app-icon>
                                    </div>
                                    <div class="select__search">
                                        <input class="form-element" type="text" placeholder="Typing some symbols ...">
                                    </div>
                                    <div class="select__options">
                                        <div class="select__option">
                                            <div class="select__option-fisrt">Yamaha</div>
                                            <div class="select__option-second"></div>
                                        </div>
                                        <div class="select__option">
                                            <div class="select__option-fisrt">Suzuki</div>
                                            <div class="select__option-second"></div>
                                        </div>
                                        <div class="select__option">
                                            <div class="select__option-fisrt">Kawasaki</div>
                                            <div class="select__option-second"></div>
                                        </div>
                                        <div class="select__option">
                                            <div class="select__option-fisrt">Polaris</div>
                                            <div class="select__option-second"></div>
                                        </div>
                                        <div class="select__option">
                                            <div class="select__option-fisrt">Sea-Doo</div>
                                            <div class="select__option-second"></div>
                                        </div>
                                        <div class="select__default"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="finder__column">
                                <!--.search--life-->
                                <!--.search--open-->
                                <div class="form-element select select--finder">
                                    <div class="select__value">
                                        <div class="select__value-first">Select option</div>
                                        <div class="select__value-second"></div>
                                    </div>
                                    <div class="select__caret">
                                        <app-icon name="toggle"></app-icon>
                                    </div>
                                    <div class="select__search">
                                        <input class="form-element" type="text" placeholder="Typing some symbols ...">
                                    </div>
                                    <div class="select__options">
                                        <div class="select__option">
                                            <div class="select__option-fisrt">Yamaha</div>
                                            <div class="select__option-second"></div>
                                        </div>
                                        <div class="select__option">
                                            <div class="select__option-fisrt">Suzuki</div>
                                            <div class="select__option-second"></div>
                                        </div>
                                        <div class="select__option">
                                            <div class="select__option-fisrt">Kawasaki</div>
                                            <div class="select__option-second"></div>
                                        </div>
                                        <div class="select__option">
                                            <div class="select__option-fisrt">Polaris</div>
                                            <div class="select__option-second"></div>
                                        </div>
                                        <div class="select__option">
                                            <div class="select__option-fisrt">Sea-Doo</div>
                                            <div class="select__option-second"></div>
                                        </div>
                                        <div class="select__default"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="finder__column finder__column--last">
                                <button class="button button--danger button--uppercase button--md" type="button">Search</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	`
})
export class SliderTopComponent implements OnInit, OnDestroy {
	
	isDestroyed$: Subject<void> = new Subject();
	
	constructor(private logger: LoggerService) {
		this.logger.debug(this.constructor.name + " is created.");
	}
	
	ngOnInit() {
	}
	
	ngOnDestroy(): void {
		this.isDestroyed$.next();
		this.isDestroyed$.complete();
	}
}
