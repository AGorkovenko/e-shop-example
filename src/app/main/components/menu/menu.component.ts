import {Component, OnDestroy, OnInit} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";
import {MenuService} from "../../../_core/services/menu/menu.service";
import {ProductsCategoriesService} from "../../../_core/services/product/products-categories.service";
import {switchMap, takeUntil, tap} from "rxjs/operators";
import {ProductCategory} from "../../../_core/models/dto/product/product-category.model";
import {Subject} from "rxjs";
import {MenuItem} from "../../../_core/models/dto/menu/menu-item.model";

@Component({
  selector: "app-menu",
  template: `
    <nav class="nav-menu">
      <div class="container">
        <div class="menu">
          
          <div class="menu-categories" appDropDown [openClass]="'menu-categories--open'">
            <app-icon name="humburger"></app-icon>
            <span i18n="Name item menu for categories list@@menuCategories">Categories</span>
            <app-icon name="toggle"></app-icon>
            <div class="menu-categories__dropdown">
              <div class="menu-categories-close" (click)="closeCategoriesMenu($event)">
                <app-icon name="delete"></app-icon>
              </div>
              <div class="categories-list">
                <div class="categories-list__item" *ngFor="let category of categories; let i = index" (click)="showSublist(i)" [ngClass]="{'categories-list__item--active': showedSublist === i}">
                  <span>{{category.name}}</span>
                  <app-icon name="current"></app-icon>
                </div>
              </div>
              <div class="categories-sublist categories-sublist--open">
                <div class="sublist" *ngFor="let subCategory of subCategories">
                  <a [routerLink]="'/shop/category/' + subCategory.slug" class="sublist__name">{{subCategory.name}}</a>
                  <a class="sublist__link" [routerLink]="['/shop/category/' + child.slug]" *ngFor="let child of subCategory.children">{{child.name}}</a>
                </div>
              </div>
            </div>
          </div>
          
          <div class="menu-main">
            <div class="menu-main__item" *ngFor="let item of menuItems;" routerLinkActive="menu-main__item--active" [routerLinkActiveOptions]="{exact: true}" [ngClass]="{'menu-main__item--parent': item.children.length > 0}">
              <a class="menu-link" [routerLink]="['/' + item.link]">
                <span>{{item.title}}</span>
                <app-icon name="toggle" *ngIf="item.children.length > 0"></app-icon>
              </a>
              <div class="submenu" *ngIf="item.children.length > 0">
                <div class="submenu__item" *ngFor="let child of item.children;" routerLinkActive="submenu__item--active" [routerLinkActiveOptions]="{exact: true}">
                  <a class="submenu-link" [routerLink]="['/' + child.link]">
                    <span>{{child.title}}</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
          
          <div class="menu-mobile">
            <div class="menu-mobile__item" appDropDown openClass="menu-mobile__item--open">
              <div class="menu-link menu-link--mobile" href="">
                <span i18n="Name buttom item for mobile menu@@menuMobileMenu">Menu</span>
              </div>
              <div class="submenu">
                <div class="submenu__item" *ngFor="let item of menuItems;" routerLinkActive="submenu__item--active" [routerLinkActiveOptions]="{exact: true}">
                  <a class="submenu-link" [routerLink]="['/' + item.link]">
                    <span>{{item.title}}</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav>
  `,
  styleUrls: ["./menu.component.scss"]
})
export class MenuComponent implements OnInit, OnDestroy {
  
  menuItems: MenuItem[] = [];
  categories: ProductCategory[] = [];
  subCategories: ProductCategory[] = [];
  showedSublist: number = 0;
  
  isDestroyed$: Subject<void> = new Subject();
  
  constructor(private logger: LoggerService,
              private menuService: MenuService,
              private productsCategoriesService: ProductsCategoriesService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  ngOnInit() {
    this.productsCategoriesService.getCategories()
      .pipe(
        tap((categories: ProductCategory[]) => {
          if (categories) {
            this.categories = categories;
            this.showSublist(this.showedSublist);
          }
        }),
        switchMap(() => this.menuService.getMenuItemsBySlug("main-menu")),
        tap((items: MenuItem[]) => {
          if (items) {
            this.menuItems = items;
          }
        }),
        takeUntil(this.isDestroyed$)
      )
      .subscribe();
  }
  
  closeCategoriesMenu(event: MouseEvent) {
    event.stopPropagation();
    const closeEvent = new CustomEvent("closeDropDown", {detail: true});
    document.dispatchEvent(closeEvent);
  }
  
  showSublist(index) {
    this.showedSublist = index;
    this.subCategories = this.categories[index].children;
  }
  
  ngOnDestroy(): void {
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }
  
}
