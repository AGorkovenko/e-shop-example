import {Component, OnDestroy, OnInit} from "@angular/core";
import {Subject} from "rxjs";
import {takeUntil, tap} from "rxjs/operators";
import {LoggerService} from "../../../_core/services/logger.service";
import {SignInUpService} from "../../../_core/services/sign-in-up.service";
import {SignInUp} from "../../../_core/models/enum/sign-in-up.enum";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ProfileService} from "../../../_core/services/profile/profile.service";
import {ProfileUpdateService} from "../../../_core/services/profile/profile-update.service";
import {PasswordValidation} from "../../../_core/password.validatator";

@Component({
  selector: "app-sign-in-up",
  template: `
    <app-modal [centered]="true" *ngIf="isModalOpened" (onBackgroundClick)="closeModal()" (onEscape)="closeModal()">
      <ng-container ngProjectAs="appModalHeader" #appModalHeader>
        <h2 *ngIf="modalStatus === signInUp.LOGIN">Login</h2>
        <h2 *ngIf="modalStatus === signInUp.REGISTRATION">Sing Up <span *ngIf="registrationForm.get('flag').value">as {{registrationForm.get('flag').value}}</span></h2>
        <h2 *ngIf="modalStatus === signInUp.RECOVERY">Pasword recovery</h2>
        <div class="window-close">
          <app-icon name="delete" (click)="closeModal()"></app-icon>
        </div>
      </ng-container>
      <ng-container ngProjectAs="appModalBody" #appModalBody>
        
        <form class="form" [formGroup]="loginForm" *ngIf="modalStatus === signInUp.LOGIN">
          <div class="form-line">
            <label i18n="Label in login modal@@modalEmailLoginLabel">Login (or E-mail)</label>
            <input class="form-element" type="type" formControlName="login_email" [ngClass]="{'form-element--error': loginForm.get('login_email').invalid && loginForm.get('login_email').touched}">
          </div>
          <div class="form-line">
            <label i18n="Label in login modal@@modalPasswordLabel">Password</label>
            <input class="form-element" type="password" formControlName="password" [ngClass]="{'form-element--error': loginForm.get('password').invalid && loginForm.get('password').touched}">
          </div>
          <div class="form-line">
            <button type="submit" class="button button--danger button--md button--uppercase" (click)="login()" i18n="Button in login modal@@modalLoginButton">Login</button>
          </div>
          <div class="form-columns text-center">
            <div class="form-line form-line--last">
              <a class="cursor-link" (click)="goToForm(signInUp.REGISTRATION)" i18n="Link to sign up modal from login modal@@modalRegistrationLink">Registration</a>
            </div>
            <div class="form-line form-line--last">
              <a class="cursor-link" (click)="goToForm(signInUp.RECOVERY)" i18n="Link to recovery modal from login modal@@modalRecoveryLink">Recovery password</a>
            </div>
          </div>
        </form>
        
        <form class="form" [formGroup]="registrationForm" *ngIf="modalStatus === signInUp.REGISTRATION">
          <div *ngIf="!registrationForm.get('flag').value">
            <label i18n="Label in sign up modal@@modalSelectRegisterLabel">Select by whom you want to register</label>
            <div class="form-columns">
              <div class="form-line">
                <button type="button" class="button button--danger button--md button--uppercase" i18n="Label in sign up modal@@modalDealerButton" (click)="selectFlag('dealer')">Dealer</button>
              </div>
              <div class="form-line">
                <button type="button" class="button button--danger button--md button--uppercase" i18n="Label in sign up modal@@modalDistributorButton" (click)="selectFlag('distributor')">Distributor</button>
              </div>
              <div class="form-line">
                <button type="button" class="button button--danger button--md button--uppercase" i18n="Label in sign up modal@@modalCustomerButton" (click)="selectFlag('customer')">Customer</button>
              </div>
            </div>
            <div class="form-line form-line--last" i18n="Button in sign up modal@@modalShortInformation">
              <a [routerLink]="['/what-different-between-dealer-and-customer']">Short information about different between Dealer, Distributor and Customer</a>
            </div>
          </div>
          
          <div *ngIf="registrationForm.get('flag').value">
            <div class="form-line">
              <label i18n="Label in sign up modal@@modalEmailLabel">E-mail</label>
              <input class="form-element" type="type" formControlName="email" [ngClass]="{'form-element--error': registrationForm.get('email').invalid && registrationForm.get('email').touched}">
            </div>
            <div class="form-line">
              <label i18n="Label in sign up modal@@modalLoginLabel">Login</label>
              <input class="form-element" type="type" formControlName="login" [ngClass]="{'form-element--error': registrationForm.get('login').invalid && registrationForm.get('login').touched}">
            </div>
            <div class="form-line">
              <label i18n="Label in sign up modal@@modalPasswordLabel">Password</label>
              <input class="form-element" type="password" formControlName="password" [ngClass]="{'form-element--error': registrationForm.get('password').invalid && registrationForm.get('password').touched}">
            </div>
            <div class="form-line">
              <label i18n="Label in sign up modal@@modalRepeatPasswordLabel">Repeat password</label>
              <input class="form-element" type="password" formControlName="repassword" [ngClass]="{'form-element--error': registrationForm.get('repassword').invalid && registrationForm.get('repassword').touched}">
            </div>
            <div class="form-line">
              <div class="checkbox" [ngClass]="{'checkbox--checked': registrationForm.get('agreement').value, 'checkbox--error': !registrationForm.get('agreement').value}">
                <label i18n="Label in sign up modal@@modalAcceptAgreementLabel">
                  <a [routerLink]="['/user-agreement']">I accept the user agreement</a>
                </label>
                <input type="checkbox" formControlName="agreement">
              </div>
            </div>
            <div class="form-columns">
              <div class="form-line form-line--last text-center">
                <a class="cursor-link" (click)="goToForm(signInUp.LOGIN)" i18n="Link to login modal from sign up modal@@modalLoginLink">Already have account</a>
              </div>
              <div class="form-line form-line--last">
                <button class="button button--danger button--md button--uppercase" i18n="Button in sign up modal@@modalSignUpButton" (click)="registration()">Sign Up</button>
              </div>
            </div>
          </div>
        </form>
        
        <form class="form" [formGroup]="recoveryForm" *ngIf="modalStatus === signInUp.RECOVERY">
          <div class="form-line">
            <label i18n="Label email for recovery modal@@modalRecoveryEmailLabel">Enter E-mail address, which you used during registration</label>
            <input class="form-element" type="type" formControlName="email">
          </div>
          <div class="form-line form-line--last">
            <button class="button button--danger button--md button--uppercase" i18n="Button in recovery modal@@modalRecoveryButton" (click)="recovery()">Recovery</button>
          </div>
        </form>
      
      </ng-container>
    </app-modal>
  `
})
export class SignInUpComponent implements OnInit, OnDestroy {
  
  signInUp = SignInUp;
  isModalOpened: boolean = false;
  modalStatus: SignInUp = null;
  
  loginForm: FormGroup;
  registrationForm: FormGroup;
  recoveryForm: FormGroup;
  
  isDestroyed$: Subject<void> = new Subject();
  
  constructor(private logger: LoggerService,
              private signInUpService: SignInUpService,
              private fb: FormBuilder,
              private profileService: ProfileService,
              private profileUpdateService: ProfileUpdateService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  ngOnInit() {
    this.signInUpService.openModalSource$
      .pipe(takeUntil(this.isDestroyed$))
      .subscribe((status: SignInUp) => {
        this.modalStatus = status;
        this.isModalOpened = !!status;
        this.initForm(status);
      });
  }
  
  initForm(status: SignInUp) {
    switch (status) {
      case SignInUp.LOGIN:
        this.initLoginForm();
        break;
      case SignInUp.REGISTRATION:
        this.initRegistrationForm();
        break;
      case SignInUp.RECOVERY:
        this.initRecoveryForm();
        break;
      default:
        this.initLoginForm();
    }
  }
  
  initLoginForm(login_email?: string) {
    this.loginForm = this.fb.group({
      login_email: new FormControl(login_email ? login_email : null, [Validators.required, Validators.minLength(2)]),
      password: new FormControl(null, [Validators.required, Validators.minLength(4)])
    });
  }
  
  initRegistrationForm() {
    this.registrationForm = this.fb.group({
      flag: new FormControl(null, [Validators.required]),
      login: new FormControl(null, [Validators.required, Validators.minLength(2)]),
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, Validators.minLength(4)]),
      repassword: new FormControl(null, [Validators.required]),
      agreement: new FormControl(true, [Validators.required])
    }, {
      validator: PasswordValidation.MatchPassword
    });
  }
  
  initRecoveryForm() {
    this.recoveryForm = this.fb.group({
      email: new FormControl(null, [Validators.required, Validators.email])
    });
  }
  
  closeModal() {
    this.signInUpService.closeModal();
  }
  
  goToForm(status: SignInUp) {
    this.modalStatus = status;
    this.initForm(status);
  }
  
  selectFlag(flag?: string) {
    if (flag) {
      this.registrationForm.patchValue({"flag": flag});
    }
  }
  
  login() {
    if (this.loginForm.valid) {
      const formData = this.loginForm.getRawValue();
      this.profileService.login(formData.login_email, formData.password)
        .pipe(
          tap(login => {
            if (login) {
              this.signInUpService.closeModal();
            } else {
              this.loginForm.get("password").reset();
            }
          }),
          takeUntil(this.isDestroyed$)
        )
        .subscribe();
    }
  }
  
  registration() {
    if (this.registrationForm.valid && this.registrationForm.get("agreement").value) {
      const formData = this.registrationForm.getRawValue();
      this.profileService.singup(formData)
        .pipe(
          tap(singup => {
            if (singup) {
              this.signInUpService.closeModal();
            } else {
              this.registrationForm.get("password").reset();
              this.registrationForm.get("repassword").reset();
            }
          }),
          takeUntil(this.isDestroyed$)
        )
        .subscribe();
    }
  }
  
  recovery() {
    if (this.recoveryForm.valid) {
      const formData = this.recoveryForm.getRawValue();
      this.profileUpdateService.passwordRecoveryRequest(formData.email)
      .pipe(
          tap(request => {
            if (request) this.signInUpService.closeModal();
          }),
          takeUntil(this.isDestroyed$)
      )
      .subscribe();
    }
  }
  
  ngOnDestroy(): void {
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }
}
