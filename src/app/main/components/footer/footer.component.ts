import {Component, OnInit} from "@angular/core";
import {LoggerService} from "../../../_core/services/logger.service";
import {TimeService} from "../../../_core/services/time.service";
import {ContentMeta} from "../../../_core/models/dto/meta/content-meta.model";
import {SiteAppData, SiteAppService} from "../../../_core/services/site-app.service";
import {switchMap, takeUntil, tap} from "rxjs/operators";
import {MenuItem} from "../../../_core/models/dto/menu/menu-item.model";
import {MenuService} from "../../../_core/services/menu/menu.service";
import {Subject} from "rxjs";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
    selector: "app-footer",
    template: `
        <footer>
            <div class="container">
                <div class="footer">
                    <div class="footer__column footer__column--first">
                        <h4 i18n="Title first column in footer@@footerColumnOne">Get in touch</h4>
                        <div class="contacts">
                            <div class="contacts__line" *ngIf="siteAppService.getMetaValue('address')">
                                <app-icon name="location"></app-icon>
                                <div>{{siteAppService.getMetaValue('address')}}</div>
                            </div>
                            <div class="contacts__line" *ngIf="siteAppService.getMetaValue('phone-in-footer')">
                                <app-icon name="phone"></app-icon>
                                <div>
                                    {{siteAppService.getMetaValue('phone-in-footer')}}
                                    <span>{{siteAppService.getMetaValue('open-time')}}</span>
                                </div>
                            </div>
                            <div class="contacts__line" *ngIf="siteAppService.getMetaValue('email-in-footer')">
                                <app-icon name="letter"></app-icon>
                                <div>
                                    <a [href]="sanitization.bypassSecurityTrustUrl('maito:' + siteAppService.getMetaValue('email-in-footer'))">{{siteAppService.getMetaValue('email-in-footer')}}</a>
                                </div>
                            </div>
                        </div>
                        <h4 i18n="Title second column in footer@@footerColumnSeconde">Join us</h4>
                        <div class="socials">
                            <a class="social-link social-link--facebook" [href]="siteAppService.getMetaValue('facebook')">
                                <app-icon [name]="'facebook'"></app-icon>
                            </a>
                            <a class="social-link social-link--massenger" [href]="siteAppService.getMetaValue('massenger')">
                                <app-icon [name]="'massenger'"></app-icon>
                            </a>
                            <a class="social-link social-link--youtube" [href]="siteAppService.getMetaValue('youtube')">
                                <app-icon [name]="'youtube'"></app-icon>
                            </a>
                            <a class="social-link social-link--instagram" [href]="siteAppService.getMetaValue('instagram')">
                                <app-icon [name]="'instagram'"></app-icon>
                            </a>
                        </div>
                    </div>
                    <div class="footer__column footer__column--second">
                        <div class="footer-menu">
                            <div class="footer-menu__links">
                                <h4 i18n="Title third column in footer@@footerInformationThird">Information</h4>
                                <ul class="menu-footer-links">
                                    <li class="menu-footer-links__item" *ngFor="let item of menuItemsOne;">
                                      <a class="menu-footer-link" [routerLink]="['/' + item.link]">{{item.title}}</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="footer-menu__links">
                                <h4 i18n="Title fourth column in footer@@footerColumnFourth">Why buy from us</h4>
                                <ul class="menu-footer-links">
                                  <li class="menu-footer-links__item" *ngFor="let item of menuItemsTwo;">
                                    <a class="menu-footer-link" [routerLink]="['/' + item.link]">{{item.title}}</a>
                                  </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="footer__column footer__column--third">
                        <h4 i18n="Title for contact form in footer@@footerContactUs">Contact us</h4>
                        <div class="footer-form">
                            <form>
                                <div class="form-line">
                                    <label i18n="Label for contact form in footer@@footerFormYourName">Your name</label>
                                    <input class="form-element" type="text">
                                </div>
                                <div class="form-line">
                                    <label i18n="Label for contact form in footer@@footerFormYourEmail">Your E-mail</label>
                                    <input class="form-element" type="text">
                                </div>
                                <div class="form-line">
                                    <label i18n="Label for contact form in footer@@footerFormYourMessage">Your Message</label>
                                    <textarea class="form-element form-element--textarea"></textarea>
                                </div>
                                <div class="form-line">
                                    <button class="button button--inline button--danger button--uppercase" i18n="Button for contact form in footer@@footerFormSend">Send</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="subfooter">
                    <div class="copyright" i18n="Copyright in footer@@footerCopyright">Copyright © {{year}} GTI powersport parts</div>
                    <div class="subfooter-icons">
                        <div class="subfooter-icon"><img alt="visa" src="assets/images/visa.svg"></div>
                        <div class="subfooter-icon"><img alt="mastercard" src="assets/images/mastercard.svg"></div>
                        <div class="subfooter-icon"><img alt="paypal" src="assets/images/PAYPAL.svg"></div>
                    </div>
                </div>
            </div>
        </footer>
    `,
    styleUrls: ["./footer.component.scss"]
})
export class FooterComponent implements OnInit {
    
    year: string;
    metas: ContentMeta[] = [];
    menuItemsOne: MenuItem[] = [];
    menuItemsTwo: MenuItem[] = [];
  
    isDestroyed$: Subject<void> = new Subject();
    
    constructor(private logger: LoggerService,
                private timeService: TimeService,
                private menuService: MenuService,
                public siteAppService: SiteAppService,
                public sanitization: DomSanitizer) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    ngOnInit() {
        this.year = this.timeService.getTimeStamp(new Date, "YYYY");
  
        this.menuService.getMenuItemsBySlug("information")
          .pipe(
            tap((items: MenuItem[]) => {
              if (items) {
                this.menuItemsOne = items;
              }
            }),
            switchMap(() => this.menuService.getMenuItemsBySlug("why-buy-from-us")),
            tap((items: MenuItem[]) => {
              if (items) {
                this.menuItemsTwo = items;
              }
            }),
            takeUntil(this.isDestroyed$)
          )
          .subscribe();
    
        this.siteAppService.siteAppData$
        .pipe(
            tap((data: SiteAppData) => {
                if (data && data.metas) {
                    this.metas = data.metas;
                }
            }),
            takeUntil(this.isDestroyed$)
        )
        .subscribe();
    }
    
}
