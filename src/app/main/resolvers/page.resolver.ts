import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, Router} from "@angular/router";
import {EMPTY, Observable} from "rxjs";
import {catchError, tap} from "rxjs/operators";
import {LoggerService} from "../../_core/services/logger.service";
import {ExceptionService} from "../../_core/services/exception.service";
import {ContentBackendClient} from "../../_core/services/content/content-backend.service";
import {ContentType} from "../../_core/models/dto/content/content-type.enum";
import {Content} from "../../_core/models/dto/content/content.model";

@Injectable()
export class PageResolver implements Resolve<Content> {
  
  constructor(private logger: LoggerService,
              private exceptionService: ExceptionService,
              private contentBackendClient: ContentBackendClient,
              private router: Router) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  resolve(route: ActivatedRouteSnapshot): Observable<Content> {
    const slug = route.params["slug"];
    return this.contentBackendClient.loadContentBySlug(slug, ContentType.PAGE)
      .pipe(
        catchError(errorInfo => {
          this.exceptionService.handleRequestErrorInfo(errorInfo.error);
          this.router.navigate(["/404"]);
          return EMPTY;
        }),
        tap((content: Content) => {
          this.logger.info("Loaded page", content);
        })
      );
  }
}
