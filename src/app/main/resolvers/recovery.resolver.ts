import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, Router} from "@angular/router";
import {Observable} from "rxjs";
import {LoggerService} from "../../_core/services/logger.service";
import {ProfileService} from "../../_core/services/profile/profile.service";
import {ExceptionService} from "../../_core/services/exception.service";
import {tap} from "rxjs/operators";

@Injectable()
export class RecoveryResolver implements Resolve<number> {
    
    constructor(private logger: LoggerService,
                private exceptionService: ExceptionService,
                private profileService: ProfileService,
                private router: Router) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
    resolve(route: ActivatedRouteSnapshot): Observable<number> {
        const code = route.queryParams["resetCode"] ? route.queryParams["resetCode"] : null;
        return this.profileService.checkActivationCode(code)
        .pipe(
            tap((uid: number) => {
                if (!uid) {
                    this.router.navigate(["/"]);
                }
            })
        );
    }
}
