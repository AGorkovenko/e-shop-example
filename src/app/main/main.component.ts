import {Component, OnDestroy, OnInit} from "@angular/core";
import {LoggerService} from "../_core/services/logger.service";
import {Subject, Subscription} from "rxjs";
import {SiteMetaDataService} from "../_core/services/site-meta-data";
import {SiteNavigationService} from "../_core/services/site-navigation";
import {HistoryNavigationService} from "../_core/services/history-navigation.service";
import {SiteAppService} from "../_core/services/site-app.service";
import {CartService} from "../_core/services/cart.service";

@Component({
  selector: "app-main",
  template: `
    <app-top></app-top>
    <app-head></app-head>
    <app-menu></app-menu>
    <app-breadcrumbs></app-breadcrumbs>
    <router-outlet></router-outlet>
    <app-footer></app-footer>
    <app-sign-in-up></app-sign-in-up>
  `
})
export class MainComponent implements OnInit, OnDestroy {
  
  siteTitleSubscribe: Subscription;
  cartSubscribtion: Subscription;
  isDestroyed$: Subject<void> = new Subject();
  
  constructor(private logger: LoggerService,
              private historyNavigationService: HistoryNavigationService,
              private siteNavigationService: SiteNavigationService,
              private siteMetaDataService: SiteMetaDataService,
              private siteAppService: SiteAppService,
              private cartService: CartService) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  ngOnInit() {
    this.historyNavigationService.loadRouting();
    this.siteNavigationService.startPageTitleGeneration();
    this.siteNavigationService.enableCloser();
    this.siteTitleSubscribe = this.siteMetaDataService.enableSiteTitleSetting();
    this.siteAppService.initData();
    this.cartSubscribtion = this.cartService.checkingCart();
    
  }
  
  ngOnDestroy(): void {
    this.siteTitleSubscribe.unsubscribe();
    this.cartSubscribtion.unsubscribe();
    this.isDestroyed$.next();
    this.isDestroyed$.complete();
  }
}
