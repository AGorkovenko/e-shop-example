import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, Router} from "@angular/router";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";
import {ProfileService} from "../_core/services/profile/profile.service";
import {LoggerService} from "../_core/services/logger.service";
import {NotificationService} from "../_core/services/notification.service";

@Injectable()
export class EmailResolver implements Resolve<boolean> {
  
  constructor(private logger: LoggerService,
              private notificationService: NotificationService,
              private profileService: ProfileService,
              private router: Router) {
    this.logger.debug(this.constructor.name + " is created.");
  }
  
  resolve(route: ActivatedRouteSnapshot): Observable<boolean> {
    const email = route.queryParams["e"] ? route.queryParams["e"] : null;
    const code = route.queryParams["confirmationCode"] ? route.queryParams["confirmationCode"] : null;
    return this.profileService.confirmationEmail(email, code)
      .pipe(
        tap((confir: boolean) => {
          if (confir) {
            this.notificationService.showSuccess("Your E-mail has been verified", "Check E-mail");
            this.profileService.loadProfileSource();
            this.router.navigate(["/"]);
          }
        })
      );
  }
}
