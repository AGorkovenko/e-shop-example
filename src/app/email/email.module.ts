import {NgModule} from "@angular/core";
import {SharedModule} from "../_shared/shared.module";
import {EmailRoutingModule} from "./email-routing.module";
import {EmailComponent} from "./email.component";

@NgModule({
    imports: [
        EmailRoutingModule,
        SharedModule
    ],
    declarations: [
        EmailComponent
    ]
})
export class EmailModule {
}
