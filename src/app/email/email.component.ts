import {Component} from "@angular/core";
import {LoggerService} from "../_core/services/logger.service";

@Component({
    selector: "app-email",
    template: `Confirmation email`
})
export class EmailComponent {
    constructor(private logger: LoggerService) {
        this.logger.debug(this.constructor.name + " is created.");
    }
    
}
