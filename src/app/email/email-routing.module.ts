import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {EmailComponent} from "./email.component";
import {EmailResolver} from "./email.resolver";

const routes: Routes = [
    {
        path: "",
        component: EmailComponent,
        resolve: {
            email: EmailResolver
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [EmailResolver]
})
export class EmailRoutingModule {
}
